------------------------- Production DB Server -------------------------
-- open terminal login as postgres in operation system
vagrant ssh postgres
sudo su
su - postgres

-- to create data directory for each tablespace
mkdir -p /var/lib/pgsql/10/grp/grptbs
mkdir -p /var/lib/pgsql/10/grp/logtbs_y2016m01

-- login database of postgres using postgres user
vagrant ssh postgres
sudo -u postgres psql -U postgres

-- Create User
create user grp password 'grp';
alter user grp with superuser;

-- Create tablespace from 00-init/

-- Create Database
create database grp WITH owner=grp encoding='UTF8';
create database grp WITH owner=grp encoding='UTF8' tablespace = grptbs;

-- Create Schema
create schema if not exists cmn authorization grp;
create schema if not exists sec authorization grp;
create schema if not exists ast authorization grp;
create schema if not exists prc authorization grp;
create schema if not exists hrm authorization grp;

-- Rename Schema
alter schema public rename to my_schema;
alter schema original_public rename to public;

create database testgrp WITH owner=grp encoding='UTF8';
sudo -u postgres PGPASSWORD="postgres" psql -U postgres -d testgrp -f /opt/backup/db_bak/grp/grp__2019-01-17__23.55.01.sql

sudo -u postgres psql -U grp -d grp
