
drop database if exists grp;
drop tablespace if exists grpspace;
SELECT * FROM pg_tablespace;

-- Extra work
create tablespace grpspace location '/var/lib/pgsql/10/grp';
create database grp WITH owner=grp encoding='UTF8' tablespace = grpspace;

-- useful DDL of partition
alter table AntarikaRequestLog detach partition AntarikaRequestLog_y2018m01;
drop table if exists AntarikaRequestLog_y2018m01;
