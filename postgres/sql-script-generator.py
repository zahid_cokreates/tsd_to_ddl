#!/usr/bin/env python3.6
  
import json
import os
import shutil
import sys
import time
import string
import gspread
from gspread import WorksheetNotFound
from oauth2client.service_account import ServiceAccountCredentials


class SqlScriptGenerator(object):

    _CREDENTIAL = '../config/credential.json'
    _GOOGLE_SHEET_NAME = 'GRP__Table-State-Data'
#    _SHEET_TAB_NAME = ['cmn', 'sec', 'ast', 'hrm', 'prc']
    _SHEET_TAB_NAME = sys.argv[2:]
    print(_SHEET_TAB_NAME)
    def __init__(self):
        self.start_time = int(round(time.time() * 1000))

    def load_script(self, json_file_name):

        modules = {}
        g_sheet = self.g.open(self._GOOGLE_SHEET_NAME)

        for num, tab in enumerate(self._SHEET_TAB_NAME, start=1):
            print("{}. {} reading...".format(num, tab))
            module_name = '{:02}'.format(num) + '-' + tab
            modules[module_name] = []
            range = g_sheet.worksheet('schema-' + tab).get_all_values()
            keys = [k.replace('.', '_') for k in range[2]]
            rows = [dict(zip(keys, row)) for row in range[3:]]
            rows.sort(key=lambda x: (int(x['component_no']), int(x['subcomponent_no']), int(x['table_no']), int(x['column_no'])), reverse=False)

            for row in rows:

                # Component
                components = modules[module_name]
                component_name = row['component_no'] + '-' + row['component_name']
                try:
                    component = next(d for d in components if d['component'] == component_name)
                except StopIteration as e:
                    component = { 'component' : component_name, 'scripts' : [] }
                    components.append(component)

                # Sub-Component/Script
                scripts = component['scripts']
                script_name = '{:02}'.format(num) + '_' + row['component_no'] + '_' + row['subcomponent_no'] + '__' + row['subcomponent_name'] + '.sql'
                try:
                    script = next(d for d in scripts if d['script'] == script_name)
                except StopIteration as e:
                    script = { 'script' : script_name, 'tables' : [] }
                    scripts.append(script)

                # Table
                tables = script['tables']
                try:
                    table = next(d for d in tables if d['tableName'] == row['table_name'])
                except StopIteration as e:
                    table = { 'schema' : tab, 'tableName' : row['table_name'], 'sortOrder' : int(row['table_no']), 'description' : row['description'], 'columns' : [], 'constraints' : [], 'data' : [] }
                    tables.append(table)

                # Column
                if row['column_no'] != '0' and row['column_name'] != '-':
                    columns = table['columns']
                    column_keys = ['precision', 'scale', 'required', 'default', 'index', 'composite', 'ordinal', 'partition', 'description']
                    column = {x:row[x] for x in column_keys}
                    column['columnName'] = row['column_name']
                    column['sortOrder'] = int(row['column_no'])
                    column['type'] = row['column_type']
                    columns.append(column)

                # Constraint
                if row['column_no'] != '-' and row['constraint_type'] != '-':
                    constraints = table['constraints']
                    constraint = { 'columnName' : row['column_name'], 'constraintType' : row['constraint_type'], 'check' : row['check_constraint'],
                        'reference' : row['reference'], 'sortOrder' : int(row['column_no']) }
                    constraints.append(constraint)

                modules[module_name] = components

        for module_name, components in modules.items():
            try:
                g_master_data_sheet = self.g.open('GRP__' + module_name.split("-")[1].strip() + '__master-data')
            except WorksheetNotFound as e:
                pass  # already exists
            for c, component in enumerate(components):
                for s, script in enumerate(component['scripts']):
                    tables = script['tables']
                    for t, table in enumerate(tables):
                        try:
                            table['data'] = g_master_data_sheet.worksheet(table['tableName']).get_all_values()
                        except WorksheetNotFound as e:
                            pass  # already exists

        with open(json_file_name, 'w') as jsonFile:
            print(json.dumps(modules, indent=4), file=jsonFile)

    def generate_index_data(self, table, range):
        self.sql = range
        self.sql.sort(key=lambda x: x['sortOrder'], reverse=False)

        self.alphabets_dict = {}
        self.unique_alphabets_dict = {}
        alphabets = string.ascii_uppercase
        for i in alphabets:
            self.alphabets_dict[i] = ''
            self.unique_alphabets_dict[i] = ''

        for c, constraint in enumerate(self.sql):
            if constraint['ordinal'] != '-':
                value = constraint['ordinal'].split('-')
                constraint['ordinal-order'] = int(value[1])
                constraint['ordinal-combo'] = value[0]
            else:
                constraint['ordinal-order'] = 0
                constraint['ordinal-combo'] = ''

        self.sql.sort(key=lambda x: (x['ordinal-combo'].lower(), x['ordinal-order']), reverse=False)

        self.unique = ''

        composite_array_full = []
        unique_composite_array_full = []

        for c, constraint in enumerate(self.sql):
            composite_array = {'ordinal-combo': '', 'columnName': ''}
            unique_composite_array = {'ordinal-combo': '', 'columnName': ''}

            if constraint['index'] != '-':

                if constraint['index'].lower() == 'unique':
                    unique_name = 'index_' + constraint['columnName']
                    if constraint['composite'].lower() == 'yes':
                        if constraint['ordinal']:
                            unique_composite_array['ordinal-combo'] = constraint['ordinal-combo']
                            unique_composite_array['columnName'] = constraint['columnName']
                            unique_composite_array_full.append(unique_composite_array)
                            uniqueTableName = table['tableName']
                    else:
                        self.index_script += 'create unique index ' + unique_name + '_' + table['tableName'] + ' on "' + table['schema'] + '".' + table['tableName'] + '(' + constraint['columnName'] + ');\n\n'
                else:
                    if constraint['composite'].lower() == 'yes':
                        if constraint['ordinal']:
                            composite_array['ordinal-combo'] = constraint['ordinal-combo']
                            composite_array['columnName'] = constraint['columnName']
                            composite_array_full.append(composite_array)
                            TableName = table['tableName']
                    else:
                        if constraint['ordinal'].lower() != '-':
                            unique_name = 'index_' + constraint['columnName']
                            self.index_script += 'create index ' + unique_name + '_' + table['tableName'] + ' on "' + table['schema'] + '".' + table['tableName'] + '(' + constraint['columnName'] + ');\n\n'

        for x in composite_array_full:
            key = x['ordinal-combo']
            value = x['columnName']

            self.alphabets_dict[key] = self.alphabets_dict[key] + ' ' + value

        for x in unique_composite_array_full:
            key = x['ordinal-combo']
            value = x['columnName']

            self.unique_alphabets_dict[key] = self.unique_alphabets_dict[key] + ' ' + value

        for x in self.unique_alphabets_dict:
            if(self.unique_alphabets_dict[x] != ""):
                unique_test0 = self.unique_alphabets_dict[x].lstrip()
                unique_test = unique_test0.split(' ')
                unique_name = 'index_' + '_'.join(unique_test)
                self.index_script += 'create unique index ' + unique_name + '_' + uniqueTableName + ' on ' + uniqueTableName + ' (' + ', '.join(unique_test) + ') ;\n\n'

        for x in self.alphabets_dict:
            if(self.alphabets_dict[x] != ""):
                test0 = self.alphabets_dict[x].lstrip()
                test = test0.split(' ')
                unique_name = 'index_' + '_'.join(test)
                self.index_script += 'create index ' + unique_name + '_' + uniqueTableName + ' on ' + TableName + ' (' + ', '.join(test) + ') ;\n\n'

    def generate_data(self, table, range):
        insert_sql = '-- ' + table['tableName'] + '\n'
        cols = range[0]
        for row in range[1:]:
            insert_sql += 'insert into "' + table['schema'] + '".' + table['tableName'] + ' (' + ', '.join(cols) + ') values ('
            insert_sql += self.generate_value(table, cols, row)
            insert_sql += ');\n'
        insert_sql += 'commit;\n\n'
        return insert_sql

    def generate_value(self, table, cols, row):
        values = ''
        for c, col in enumerate(cols):
            idx_column = self.find_index(table['columns'], 'columnName', col)
            if idx_column == -1:
                continue
            column = table['columns'][idx_column]
            if column['type'] is 'numeric':
                values += row[c] + ','
            else:
                if row[c]:
                    values += '\'' + row[c] + '\','
                else:
                    values += 'null,'
        return values[:-1]

    def generate_script(self, deployment_type, json_file_name):

        self.index_script = ''

        with open(json_file_name, 'r') as data_file:
            modules = json.loads(data_file.read())

        for module_name, components in modules.items():
            for c, component in enumerate(components):
                for s, script in enumerate(component['scripts']):
                    sql_table_script = ''
                    sql_insert_script = ''

                    script['tables'].sort(key=lambda x: x['sortOrder'], reverse=False)
                    tables = script['tables']

                    for t, table in enumerate(tables):
                        partition_column = None
                        table['columns'].sort(key=lambda x: x['sortOrder'], reverse=False)
                        columns = table['columns']
                        # Write Comment
                        sql_table_script += '/*\n' + table['description'] + '\n'
                        for c, column in enumerate(columns):
                            sql_table_script += "{:30} : {}\n".format(column['columnName'], column['description'])
                        sql_table_script += '*/\n'

                        # Write Table Script
                        sql_table_script += "{:30} {}\n".format('create table',  '"' + table['schema'] + '".' + table['tableName'])
                        sql_table_script += '(\n'
                        last_col_index = len(columns) - 1
                        for c, column in enumerate(columns):
                            if column['partition'].lower() == 'yes':
                                partition_column = column['columnName']

                            type = column['type']
                            if column['precision'] != '-':
                                if column['type'] == 'numeric':
                                    if column['scale'] != '-':
                                        type += '(' + column['precision'] + ',' + column['scale'] + ')'
                                    else:
                                        type += '(' + column['precision'] + ')'
                                elif column['type'] == 'varchar':
                                    type += '(' + column['precision'] + ')'
                                else:
                                    type += ''

                            required = ''
                            if column['required'] == 'Yes':
                                required = 'not null'

                            default = ''
                            if column['default'] != '-':
                                if column['default'] == 'current_user':
                                    default = 'default ' + column['default']
                                elif column['type'] == 'varchar' or column['type'] == 'text':
                                    default = 'default \'' + column['default'] + '\''
                                else:
                                    default = 'default ' + column['default']

                            if default == '':
                                if required == '':
                                    type_value = type
                                else:
                                    type_value = "{:60}".format(type) + required
                            else:
                                type_value = "{:60}".format(type) + "{:15}".format(required) + default

                            if c == last_col_index:
                                if len(table['constraints']) > 0:
                                    type_value += ','
                            else:
                                type_value += ','

                            sql_table_script += "{:30} {}\n".format(column['columnName'], type_value)

                        # Write Constraints
                        table['constraints'].sort(key=lambda x: x['sortOrder'], reverse=False)
                        constraints = table['constraints']
                        last_cons_index = len(constraints) - 1
                        for c, constraint in enumerate(constraints):
                            if constraint['constraintType'].lower() == 'Primary'.lower():
                                constraint_name = 'pk_' + table['tableName']
                                constraint_des = "{:14} {}".format('primary key', '(' + constraint['columnName'] + ')')
                            elif constraint['constraintType'].lower() == 'Foreign'.lower():
                                ref_values = constraint['reference'].split(".")
                                constraint_name = 'fk_' + constraint['columnName'] + '_' + table['tableName']
                                constraint_des = "{:14} {}".format('foreign key', '(' + constraint['columnName'] + ')\n')
                                constraint_des += "{:40} {:59} {:14} {}".format('', '', 'references', '"' + ref_values[0].strip() + '".' + ref_values[1].strip() + '(' + ref_values[2].strip() + ')')
                                if len(ref_values) == 4:
                                    constraint_des += "{:10} {}".format('', ref_values[2].strip())
                            elif constraint['constraintType'].lower() == 'Unique'.lower():
                                constraint_name = 'uk_' + constraint['columnName'] + '_' + table['tableName']
                                constraint_des = "{:14} {}".format('unique', '(' + constraint['columnName'] + ')')
                            elif constraint['constraintType'].lower() == 'Check'.lower():
                                constraint_name = 'ck_' + constraint['columnName'] + '_' + table['tableName']
                                constraint_des = "{:14} {}".format('check', '(')
                                check_values = constraint['check'].split(",")
                                last_check_value = len(check_values) - 1
                                for c_idx, cv in enumerate(check_values):
                                    constraint_des += constraint['columnName'] + ' = \'' + cv.strip() + '\''
                                    if c_idx != last_check_value:
                                        constraint_des += ' or '
                                constraint_des += ')'

                            const_col = "{:30} {:70}{}".format('constraint', constraint_name, constraint_des)

                            if c != last_cons_index:
                                const_col += ','

                            sql_table_script += const_col + '\n'

                        if partition_column and deployment_type == 'prod':
                            sql_table_script = sql_table_script + ') partition by range (' + partition_column + ');\n\n'
                        else:
                            sql_table_script += ');\n\n'

                        if table['data'] and len(table['data']) > 0:
                            sql_insert_script += self.generate_data(table, table['data'])

                        if table['columns']:
                            self.generate_index_data(table, table['columns'])

                    # Table Script
                    file_name = 'R__01_' + script['script']
                    script_dir = os.path.dirname(os.path.abspath(__file__))
                    dest_dir = os.path.join(script_dir, '01-table', module_name, component['component'])
                    try:
                        os.makedirs(dest_dir)
                    except OSError:
                        pass  # already exists
                    path = os.path.join(dest_dir, file_name)
                    with open(path, 'w') as sql_file:
                        print(sql_table_script, file=sql_file)

                    # Insert Script
                    if sql_insert_script:
                        file_name = 'R__07_' + script['script']
                        script_dir = os.path.dirname(os.path.abspath(__file__))
                        dest_dir = os.path.join(script_dir, '07-insert', module_name, component['component'])
                        try:
                            os.makedirs(dest_dir)
                        except OSError:
                            pass  # already exists
                        path = os.path.join(dest_dir, file_name)
                        with open(path, 'w') as sql_file:
                            print(sql_insert_script, file=sql_file)


        # Index Script
        if self.index_script:
            file_name = 'R__03_00_00_00__createIndex.sql'
            script_dir = os.path.dirname(os.path.abspath(__file__))
            dest_dir = os.path.join(script_dir, '03-index')
            try:
                os.makedirs(dest_dir)
            except OSError:
                pass  # already exists
            path = os.path.join(dest_dir, file_name)
            with open(path, 'w') as sql_file:
                print(self.index_script, file=sql_file)

    def find_index(self, list, property_name, property_value):
        for i, data in enumerate(list):
            if property_name in data and data[property_name] == property_value:
                return i
        return -1

    def validate_gsheet(self, rows):
        for row in rows:
            if not row['column'] or row['column'] is None or row['column'].strip() in ('', '-'):
                print('Column cannot contain empty', row['column'])
                return False
        return True

    def set_up(self):
        self.scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
        self.credentials = ServiceAccountCredentials.from_json_keyfile_name(self._CREDENTIAL, self.scope)
        self.g = gspread.authorize(self.credentials)

    def run(self, deployment_type=None, json_file_name=None):
        if json_file_name is None:
            self.set_up()
            json_file_name = 'postgres.json'
            self.load_script(json_file_name)
        self.generate_script(deployment_type, json_file_name)
        print('TSD gsheet reading complete')


if __name__ == '__main__':
    generator = SqlScriptGenerator()

    if os.path.exists("./01-table"):
        shutil.rmtree('./01-table', ignore_errors=False, onerror=None)
    if os.path.exists("./07-insert"):
        shutil.rmtree('./07-insert', ignore_errors=False, onerror=None)
    os.mkdir('./01-table')
    os.mkdir('./07-insert')
    
    generator.run(sys.argv[1])
