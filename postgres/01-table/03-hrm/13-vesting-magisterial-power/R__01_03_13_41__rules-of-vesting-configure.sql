/*
table for defining rules and regulations regarding vesting magisterial power
oid                            : surrogate primary key
rules_and_regulations          : rules and regulations in text
assigned_by                    : rules and regulations assigned by whom
assigned_on                    : the date of assigned on 
*/
create table                   "hrm".rules_of_vesting_magisterial_power
(
oid                            varchar(128),
rules_and_regulations          text,
assigned_by                    varchar(128),
assigned_on                    timestamp,
constraint                     pk_rules_of_vesting_magisterial_power                                 primary key    (oid),
constraint                     fk_assigned_by_rules_of_vesting_magisterial_power                     foreign key    (assigned_by)
                                                                                                     references     "hrm".employee_master_info(oid)
);


