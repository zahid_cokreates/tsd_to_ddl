/*
Vesting of magisterial power details
oid                            : Surrogate primary key
employee_oid                   : foreign key
area_of_work                   : area of work
work_description               : work description
reason_of_vesting              : reason of vesting
substitute_department          : substitute department
substitute_code                : substitute code
duration                       : duration
start_date                     : vesting start date 
end_date                       : vesting end date 
go_code                        : Govt. order code
government_order               : Govt. order 
notes                          : notes
*/
create table                   "hrm".vesting_of_magisterial_power
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
area_of_work                   varchar(256)                                                not null,
work_description               text                                                        not null,
reason_of_vesting              varchar(256)                                                not null,
substitute_department          varchar(256)                                                not null,
substitute_code                varchar(128)                                                not null,
duration                       numeric(20,0)                                               not null,
start_date                     timestamp                                                   not null,
end_date                       timestamp                                                   not null,
go_code                        varchar(128)                                                not null,
government_order               varchar(1024)                                               not null,
notes                          text,
constraint                     pk_vesting_of_magisterial_power                                       primary key    (oid),
constraint                     fk_employee_oid_vesting_of_magisterial_power                          foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);


