/*
table for defining training type and details
oid                            : oid
type_name                      : typeName
type_code                      : typeCode
type_description               : typeDescription
principal_department           : principalDepartment
*/
create table                   "hrm".training_type
(
oid                            varchar(128)                                                not null,
type_name                      varchar(256)                                                not null,
type_code                      varchar(256)                                                not null,
type_description               text                                                        not null,
principal_department           varchar(256)                                                not null,
constraint                     pk_training_type                                                      primary key    (oid)
);


