/*
table for recording trainer's details
oid                            : surrogate primary key
trainer_oid                    : trainer Name
training_history               : -
*/
create table                   "hrm".trainer_detail
(
oid                            varchar(128)                                                not null,
trainer_oid                    varchar(256)                                                not null,
training_history               varchar                                                     not null,
constraint                     pk_trainer_detail                                                     primary key    (oid),
constraint                     fk_trainer_oid_trainer_detail                                         foreign key    (trainer_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);

/*
result of trainee in the training program
oid                            : surrogate primary key
trainer                        : trainer name
trainee                        : trainee name
training                       : training id
result                         : result of the trainee
*/
create table                   "hrm".trainee_result
(
oid                            varchar(128)                                                not null,
trainer                        varchar(128)                                                not null,
trainee                        varchar(128)                                                not null,
training                       varchar(128)                                                not null,
result                         varchar(100)                                                not null,
constraint                     pk_trainee_result                                                     primary key    (oid),
constraint                     fk_trainer_trainee_result                                             foreign key    (trainer)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_trainee_trainee_result                                             foreign key    (trainee)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_training_trainee_result                                            foreign key    (training)
                                                                                                     references     "hrm".training_type(oid)
);

/*
table for participation confirmation of selected employees
oid                            : surrogate primary key
training_oid                   : training Id
trainee_oid                    : trainee Id
is_attendant                   : Is Attendant
reason                         : Reason of not confirming or confirming
*/
create table                   "hrm".pending_participation_confirmation
(
oid                            varchar(128)                                                not null,
training_oid                   varchar(128)                                                not null,
trainee_oid                    varchar(128)                                                not null,
is_attendant                   varchar(32)                                                 not null,
reason                         varchar(1024),
constraint                     pk_pending_participation_confirmation                                 primary key    (oid),
constraint                     fk_training_oid_pending_participation_confirmation                    foreign key    (training_oid)
                                                                                                     references     "hrm".training_type(oid),
constraint                     fk_trainee_oid_pending_participation_confirmation                     foreign key    (trainee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     ck_is_attendant_pending_participation_confirmation                    check          (is_attendant = 'Yes' or is_attendant = 'No')
);


