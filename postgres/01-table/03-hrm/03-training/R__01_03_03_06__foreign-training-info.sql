/*
If any employee accomplish any foreign training there must be a record in the Personal Data Sheet
oid                            : surrogate primary key
training_type_oid              : training Type code
training_title                 : training Title
institution                    : institution that is organising the training program
country                        : country
starting_from                  : training starting Form
end_to                         : training end To
sponsoring_agency              : name of the sponsoring Agency
*/
create table                   "hrm".foreign_training
(
oid                            varchar(128)                                                not null,
training_type_oid              varchar(128)                                                not null,
training_title                 varchar(256)                                                not null,
institution                    varchar(256)                                                not null,
country                        varchar(256)                                                not null,
starting_from                  timestamp                                                   not null,
end_to                         timestamp                                                   not null,
sponsoring_agency              varchar(256)                                                not null,
constraint                     pk_foreign_training                                                   primary key    (oid),
constraint                     fk_training_type_oid_foreign_training                                 foreign key    (training_type_oid)
                                                                                                     references     "hrm".training_type(oid)
);


