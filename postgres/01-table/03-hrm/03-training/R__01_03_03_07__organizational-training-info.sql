/*
table for setting up and creating new training program
oid                            : surrogate primary key
is_mandatory                   : if the training is Mandatory for selected employee
duration                       : duration of the training program
training_type_oid              : training Type
objective                      : objective of the training program
descriptions                   : descriptions of the training program
training_name                  : trainingName
start_date                     : start Date
end_date                       : end Date
start_time                     : start Time
end_time                       : end Time
organization                   : organizationname
designation_required           : designation Required
education_required             : education Required
*/
create table                   "hrm".organisational_training
(
oid                            varchar(128)                                                not null,
is_mandatory                   varchar(32)                                                 not null,
duration                       numeric(20,2)                                               not null,
training_type_oid              varchar(128)                                                not null,
objective                      text,
descriptions                   text                                                        not null,
training_name                  varchar(256)                                                not null,
start_date                     timestamp                                                   not null,
end_date                       timestamp                                                   not null,
start_time                     timestamp                                                   not null,
end_time                       timestamp                                                   not null,
organization                   varchar(256)                                                not null,
designation_required           text                                                        not null,
education_required             text                                                        not null,
constraint                     pk_organisational_training                                            primary key    (oid),
constraint                     ck_is_mandatory_organisational_training                               check          (is_mandatory = 'Yes' or is_mandatory = 'No'),
constraint                     fk_training_type_oid_organisational_training                          foreign key    (training_type_oid)
                                                                                                     references     "hrm".training_type(oid)
);


