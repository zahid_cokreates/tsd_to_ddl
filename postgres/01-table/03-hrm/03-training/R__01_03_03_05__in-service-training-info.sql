/*
An in-service training program is a professional training or staff development effort, where professionals are trained and discuss their work with others in their peer group.
oid                            : surrogate primary key
training_type_oid              : training Type code
training_title                 : trainingTitle
institution                    : institution that is organising the training program
location                       : location of the institution
starting_from                  : training starting Form
end_to                         : training end To
sponsoring_agency              : name of the sponsoring Agency
*/
create table                   "hrm".in_service_training
(
oid                            varchar(128)                                                not null,
training_type_oid              varchar(128)                                                not null,
training_title                 varchar(256)                                                not null,
institution                    varchar(256)                                                not null,
location                       varchar(256)                                                not null,
starting_from                  timestamp                                                   not null,
end_to                         timestamp                                                   not null,
sponsoring_agency              varchar(256),
constraint                     pk_in_service_training                                                primary key    (oid),
constraint                     fk_training_type_oid_in_service_training                              foreign key    (training_type_oid)
                                                                                                     references     "hrm".training_type(oid)
);


