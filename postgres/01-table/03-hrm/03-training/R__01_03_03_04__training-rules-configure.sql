/*
table for defining rules and regulations regarding training and development 
oid                            : surrogate primary key
training_type                  : training type
rules_and_regulations          : rules and regulations in text
assigned_by                    : rules and regulations assigned by whom
assigned_on                    : the date of assigned on 
*/
create table                   "hrm".rules_regarding_training
(
oid                            varchar(128),
training_type                  varchar(256),
rules_and_regulations          text,
assigned_by                    varchar(128),
assigned_on                    timestamp,
constraint                     pk_rules_regarding_training                                           primary key    (oid),
constraint                     fk_assigned_by_rules_regarding_training                               foreign key    (assigned_by)
                                                                                                     references     "hrm".employee_master_info(oid)
);


