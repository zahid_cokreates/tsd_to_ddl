/*
Table for recording survey questions for trainees about the outcome of training program
oid                            : surrogate primary key
training_oid                   : training Id
question                       : question
*/
create table                   "hrm".survey_questionnaire_for_trainee
(
oid                            varchar(128)                                                not null,
training_oid                   varchar(128)                                                not null,
question                       varchar(1000)                                               not null,
constraint                     pk_survey_questionnaire_for_trainee                                   primary key    (oid),
constraint                     fk_training_oid_survey_questionnaire_for_trainee                      foreign key    (training_oid)
                                                                                                     references     "hrm".training_type(oid)
);

/*
table for recording survey answers of the trainee about the training program
oid                            : surrogate primary key
training_type_oid              : training Id
trainer_detail_oid             : training Name
trainee_id                     : trainee Id
survey_question_code           : survey Question Code
answer                         : answer
*/
create table                   "hrm".survey_for_trainee
(
oid                            varchar(128)                                                not null,
training_type_oid              varchar(128)                                                not null,
trainer_detail_oid             varchar(128)                                                not null,
trainee_id                     varchar(128)                                                not null,
survey_question_code           varchar(128)                                                not null,
answer                         varchar(1000)                                               not null,
constraint                     pk_survey_for_trainee                                                 primary key    (oid),
constraint                     fk_training_type_oid_survey_for_trainee                               foreign key    (training_type_oid)
                                                                                                     references     "hrm".training_type(oid),
constraint                     fk_trainer_detail_oid_survey_for_trainee                              foreign key    (trainer_detail_oid)
                                                                                                     references     "hrm".trainer_detail(oid),
constraint                     fk_trainee_id_survey_for_trainee                                      foreign key    (trainee_id)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_survey_question_code_survey_for_trainee                            foreign key    (survey_question_code)
                                                                                                     references     "hrm".survey_questionnaire_for_trainee(oid)
);

/*
Table for recording survey questions for trainer about the outcome of training program
oid                            : surrogate primary key
training_type_oid              : training Id
trainer_detail_oid             : training Name
question                       : question
*/
create table                   "hrm".survey_questionnaire_for_trainer
(
oid                            varchar(128)                                                not null,
training_type_oid              varchar(128)                                                not null,
trainer_detail_oid             varchar(128)                                                not null,
question                       varchar(1024)                                               not null,
constraint                     pk_survey_questionnaire_for_trainer                                   primary key    (oid),
constraint                     fk_training_type_oid_survey_questionnaire_for_trainer                 foreign key    (training_type_oid)
                                                                                                     references     "hrm".training_type(oid),
constraint                     fk_trainer_detail_oid_survey_questionnaire_for_trainer                foreign key    (trainer_detail_oid)
                                                                                                     references     "hrm".trainer_detail(oid)
);

/*
table for recording survey answers of the trainee about the training program
oid                            : surrogate primary key
training_type_oid              : training Id
trainer_detail_oid             : training Name
trainer_code                   : trainee Id
survey_question_code           : survey Question Code
answer                         : answer
*/
create table                   "hrm".survey_for_trainer
(
oid                            varchar(128)                                                not null,
training_type_oid              varchar(128)                                                not null,
trainer_detail_oid             varchar(128)                                                not null,
trainer_code                   varchar(128)                                                not null,
survey_question_code           varchar(128)                                                not null,
answer                         varchar(1024)                                               not null,
constraint                     pk_survey_for_trainer                                                 primary key    (oid),
constraint                     fk_training_type_oid_survey_for_trainer                               foreign key    (training_type_oid)
                                                                                                     references     "hrm".training_type(oid),
constraint                     fk_trainer_detail_oid_survey_for_trainer                              foreign key    (trainer_detail_oid)
                                                                                                     references     "hrm".trainer_detail(oid),
constraint                     fk_trainer_code_survey_for_trainer                                    foreign key    (trainer_code)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_survey_question_code_survey_for_trainer                            foreign key    (survey_question_code)
                                                                                                     references     "hrm".survey_questionnaire_for_trainee(oid)
);


