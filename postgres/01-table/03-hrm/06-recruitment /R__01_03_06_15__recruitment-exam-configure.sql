/*
table for recording exam configuration
oid                            : surrogate primary key
exam_type                      : exam Type
total_marks                    : total Marks
pass_marks                     : pass Marks
assigned_by                    : exam configuration assigned by
*/
create table                   "hrm".exam_configuration
(
oid                            varchar(128)                                                not null,
exam_type                      varchar(256)                                                not null,
total_marks                    numeric(20,2)                                               not null,
pass_marks                     numeric(20,2)                                               not null,
assigned_by                    varchar(256)                                                not null,
constraint                     pk_exam_configuration                                                 primary key    (oid)
);


