/*
ACR medical report details
oid                            : Surrogate primary key
employee_oid                   : foreign key
height                         : height
weight                         : weight
eye_power                      : eye power
blood_group                    : blood group
blood_pressure                 : blood pressure
medical_class_division         : medical class division
attachment                     : attachment
date                           : timestamp
esign                          : esign
*/
create table                   "hrm".acr_medical_report
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(256)                                                not null,
height                         numeric(20,2)                                               not null,
weight                         numeric(20,2)                                               not null,
eye_power                      numeric(20,0)                                               not null,
blood_group                    varchar(256)                                                not null,
blood_pressure                 varchar(256)                                                not null,
medical_class_division         varchar(256)                                                not null,
attachment                     varchar(1024)                                               not null,
date                           timestamp                                                   not null,
esign                          varchar(256)                                                not null,
constraint                     pk_acr_medical_report                                                 primary key    (oid),
constraint                     fk_employee_oid_acr_medical_report                                    foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);

/*
ACR main report details
oid                            : Surrogate primary key
employee_oid                   : foreign key
moral                          : moral
intellectual                   : intellectual
materialistic                  : materialistic
recommend_for_training         : recommendation for training
qualification_for_promotion    : qualification for promotion
other_recommendation           : other recommendation
esign                          : esign
designation_en                 : designation
designation_bn                 : designation
*/
create table                   "hrm".acr_main_report
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
moral                          numeric(20,0)                                               not null,
intellectual                   numeric(20,0)                                               not null,
materialistic                  numeric(20,0)                                               not null,
recommend_for_training         numeric(20,0)                                               not null,
qualification_for_promotion    numeric(20,0)                                               not null,
other_recommendation           varchar(256)                                                not null,
esign                          varchar(256)                                                not null,
designation_en                 varchar(256)                                                not null,
designation_bn                 varchar(256)                                                not null,
constraint                     pk_acr_main_report                                                    primary key    (oid),
constraint                     fk_employee_oid_acr_main_report                                       foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);

/*
ACR summary details 
oid                            : Surrogate primary key
employee_oid                   : foreign key
date_of_submission             : date of submission
subject_of_evaluation          : subject of evaluation
obtained_marks                 : obtained marks
total_obtained_marks           : total obtained marks
esign                          : esign
*/
create table                   "hrm".acr_summary
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(256)                                                not null,
date_of_submission             timestamp                                                   not null,
subject_of_evaluation          varchar(256)                                                not null,
obtained_marks                 numeric(20,2)                                               not null,
total_obtained_marks           numeric(20,2)                                               not null,
esign                          varchar(256)                                                not null,
constraint                     pk_acr_summary                                                        primary key    (oid),
constraint                     fk_employee_oid_acr_summary                                           foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);

/*
Employee Performance Summary
oid                            : Surrogate primary key
unit_weight_of_perf_metrics    : unit weight of performance metrics
performance_metrics            : performance metrics
*/
create table                   "hrm".employee_perf_summary
(
oid                            varchar(128)                                                not null,
unit_weight_of_perf_metrics    numeric(20,0)                                               not null,
performance_metrics            varchar(256)                                                not null,
constraint                     pk_employee_perf_summary                                              primary key    (oid )
);


