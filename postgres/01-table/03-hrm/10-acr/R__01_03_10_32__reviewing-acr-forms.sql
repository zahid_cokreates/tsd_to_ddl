/*
ACR Form forwarding process to Controlling Authority 
oid                            : Surrogate primary key
employee_oid                   : foreign key
submission_date_ca             : date of submission by CA
subject_of_evaluation          : subject of evaluation
marks_from_ca                  : obtained marks from CA
total_marks_ca                 : total marks obtained from CA
acr_form_upload                : ACR form upload
e_sign_of_authority            : esign of authority 
*/
create table                   "hrm".acr_under_controlling_auth
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
submission_date_ca             timestamp                                                   not null,
subject_of_evaluation          varchar(256)                                                not null,
marks_from_ca                  numeric(20,2)                                               not null,
total_marks_ca                 numeric(20,2)                                               not null,
acr_form_upload                varchar(1024)                                               not null,
e_sign_of_authority            varchar(256)                                                not null,
constraint                     pk_acr_under_controlling_auth                                         primary key    (oid),
constraint                     fk_employee_oid_acr_under_controlling_auth                            foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);

/*
ACR Form forwarding process to Counter Signing Authority 
oid                            : Surrogate primary key
employee_oid                   : foreign key
submission_date_ca             : date of submission by CA
total_marks_ca                 : total marks obtained from CA
submission_date_csa            : date of submission by CSA
total_marks_csa                : total marks obtained from CSA
acr_form_upload                : ACR form upload
comments                       : comments
e_sign_of_auth                 : esign of authority 
*/
create table                   "hrm".acr_under_counter_signing_auth
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
submission_date_ca             timestamp                                                   not null,
total_marks_ca                 varchar(128)                                                not null,
submission_date_csa            numeric(20,2)                                               not null,
total_marks_csa                numeric(20,2)                                               not null,
acr_form_upload                varchar(1024)                                               not null,
comments                       text,
e_sign_of_auth                 varchar(256)                                                not null,
constraint                     pk_acr_under_counter_signing_auth                                     primary key    (oid),
constraint                     fk_employee_oid_acr_under_counter_signing_auth                        foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_total_marks_ca_acr_under_counter_signing_auth                      foreign key    (total_marks_ca)
                                                                                                     references     "hrm".acr_under_controlling_auth(oid)
);

/*
ACR list under controlling ministry
oid                            : Surrogate primary key
employee_oid                   : foreign key
submission_date                : date of submission
submission_date_ca             : date of submission by CA
total_marks_ca                 : total marks obtained from CA
submission_date_csa            : date of submission by CSA
total_marks_csa                : total marks obtained from CSA
date_of_filled_up_form         : date of filled up form
e_sign_of_auth                 : esign of authority
*/
create table                   "hrm".acr_under_controlling_ministry
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
submission_date                timestamp                                                   not null,
submission_date_ca             timestamp                                                   not null,
total_marks_ca                 numeric(20,2)                                               not null,
submission_date_csa            timestamp                                                   not null,
total_marks_csa                numeric(20,2)                                               not null,
date_of_filled_up_form         timestamp                                                   not null,
e_sign_of_auth                 varchar(256)                                                not null,
constraint                     pk_acr_under_controlling_ministry                                     primary key    (oid),
constraint                     fk_employee_oid_acr_under_controlling_ministry                        foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);


