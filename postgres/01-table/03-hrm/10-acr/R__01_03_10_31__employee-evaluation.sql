/*
Specify evaluation partition along with category , form part no. in ACR Form
oid                            : Surrogate primary key 
acr_form_part_number           : ACR form part number 
acr_form_part_title            : ACR form part title 
acr_form_evaluation_category   : ACR form parts' evaluation category
*/
create table                   "hrm".employee_evaluation_category_setup
(
oid                            varchar(128)                                                not null,
acr_form_part_number           numeric(20,2)                                               not null,
acr_form_part_title            varchar(256)                                                not null,
acr_form_evaluation_category   varchar(256)                                                not null,
constraint                     pk_employee_evaluation_category_setup                                 primary key    (oid )
);


