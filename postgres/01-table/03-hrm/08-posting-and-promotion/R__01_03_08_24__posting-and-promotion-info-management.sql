/*
table for recording employee's current posting
oid                            : surrogate primary key
employee_oid                   : employee Id
division                       : division
district                       : district
starting_from                  : current posting starting Form
end_to                         : current posting end To
last_posting                   : last Posting id
pay_scale                      : pay Scale
no_of_go                       : id of Government order
*/
create table                   "hrm".posting_info
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
division                       varchar(200)                                                not null,
district                       varchar(200)                                                not null,
starting_from                  timestamp                                                   not null,
end_to                         timestamp                                                   not null,
last_posting                   timestamp                                                   not null,
pay_scale                      numeric(20,2)                                               not null,
no_of_go                       varchar(128)                                                not null,
constraint                     pk_posting_info                                                       primary key    (oid),
constraint                     fk_employee_oid_posting_info                                          foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);

/*
table for the details of employee's promotion.
oid                            : surrogate primary key
employee_oid                   : employee Id
date_of_promotion              : date Of Promotion
rank_en                        : rank 
rank_bn                        : rank 
nature_of_promotion            : nature Of Promotion
pay_scale                      : pay Scale
no_of_go                       : id of Government order
*/
create table                   "hrm".promotion_detail
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
date_of_promotion              timestamp                                                   not null,
rank_en                        varchar(256)                                                not null,
rank_bn                        varchar(256)                                                not null,
nature_of_promotion            varchar(256)                                                not null,
pay_scale                      varchar(256)                                                not null,
no_of_go                       varchar(256)                                                not null,
constraint                     pk_promotion_detail                                                   primary key    (oid),
constraint                     fk_employee_oid_promotion_detail                                      foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);

/*
promotion/posting details of the employee who is in the queue for promotion/posting
oid                            : surrogate primary key
employee_oid                   : employee Id
no_of_go                       : id of Government order
*/
create table                   "hrm".employee_in_queue_for_promotion_or_posting
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
no_of_go                       varchar(256)                                                not null,
constraint                     pk_employee_in_queue_for_promotion_or_posting                         primary key    (oid),
constraint                     fk_employee_oid_employee_in_queue_for_promotion_or_posting            foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);

/*
Deputation is working in a different location for a period of time until assignment is over or the said period is over. To update deputation Info of employees, admin fills up deputation form according to Government Order.
oid                            : surrogate primary key
type_ingoing                   : In going type
type_outgoing                  : Out going type
employee_oid                   : employee Id
previous_ministry              : previous Ministry
deputed_ministry               : deputed Ministry
previous_organization          : previous Organization
deputed_organization           : deputed Organization
no_of_go                       : id of Government order
order_date                     : order Date
start_date                     : start Date
time_period                    : time Period
*/
create table                   "hrm".deputation_info
(
oid                            varchar(128)                                                not null,
type_ingoing                   varchar(256)                                                not null,
type_outgoing                  varchar(256)                                                not null,
employee_oid                   varchar(128)                                                not null,
previous_ministry              varchar(256)                                                not null,
deputed_ministry               varchar(256)                                                not null,
previous_organization          varchar(256)                                                not null,
deputed_organization           varchar(256)                                                not null,
no_of_go                       varchar(256)                                                not null,
order_date                     timestamp                                                   not null,
start_date                     timestamp                                                   not null,
time_period                    numeric(20,2)                                               not null,
constraint                     pk_deputation_info                                                    primary key    (oid),
constraint                     fk_employee_oid_deputation_info                                       foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);


