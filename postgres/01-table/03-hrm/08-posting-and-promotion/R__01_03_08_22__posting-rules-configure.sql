/*
table for defining rules and regulations regarding posting
oid                            : surrogate primary key
rules_and_regulations          : rules and regulations in text
assigned_by                    : rules and regulations assigned by whom
assigned_on                    : the date of assigned on 
*/
create table                   "hrm".rules_regarding_posting
(
oid                            varchar(128),
rules_and_regulations          text,
assigned_by                    varchar(128),
assigned_on                    timestamp,
constraint                     pk_rules_regarding_posting                                            primary key    (oid),
constraint                     fk_assigned_by_rules_regarding_posting                                foreign key    (assigned_by)
                                                                                                     references     "hrm".employee_master_info(oid)
);

/*
table for defining rules and regulations regarding promotion
oid                            : surrogate primary key
rules_and_regulations          : rules and regulations in text
assigned_by                    : rules and regulations assigned by whom
assigned_on                    : the date of assigned on 
*/
create table                   "hrm".rules_regarding_promotion
(
oid                            varchar(128),
rules_and_regulations          text,
assigned_by                    varchar(128),
assigned_on                    timestamp,
constraint                     pk_rules_regarding_promotion                                          primary key    (oid),
constraint                     fk_assigned_by_rules_regarding_promotion                              foreign key    (assigned_by)
                                                                                                     references     "hrm".employee_master_info(oid)
);


