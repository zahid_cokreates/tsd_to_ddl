/*
table for pormotion/posting committee details
oid                            : surrogate primary key
committee_name                 : committee Name
short_description              : short Description
member_oid                     : member Id
role                           : role
*/
create table                   "hrm".posting_and_promotion_committee_setup
(
oid                            varchar(128)                                                not null,
committee_name                 varchar(256)                                                not null,
short_description              text                                                        not null,
member_oid                     varchar(128)                                                not null,
role                           varchar(256)                                                not null,
constraint                     pk_posting_and_promotion_committee_setup                              primary key    (oid),
constraint                     fk_member_oid_posting_and_promotion_committee_setup                   foreign key    (member_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);


