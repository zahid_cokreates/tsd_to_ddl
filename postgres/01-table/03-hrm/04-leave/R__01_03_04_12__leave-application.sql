/*
table for recording applications for leave
oid                            : surrogate primary key
application_code               : application Id
leave_oid                      : leave Id
applicant_oid                  : applicant Id
total_no_of_days_leave         : total No Of Days Leave taken
total_no_of_using_leave        : total No Of Using Leave
leave_remaining                : leave Remaining
leave_from                     : leave taken from
leave_to                       : leave taken to
duration_of_leave              : duration of the leave 
attachment_url                 : attachment Url
substitute_oid                 : substitute Id
*/
create table                   "hrm".apply_for_leave
(
oid                            varchar(128)                                                not null,
application_code               varchar(128)                                                not null,
leave_oid                      varchar(128)                                                not null,
applicant_oid                  varchar(128)                                                not null,
total_no_of_days_leave         numeric(20,2)                                               not null,
total_no_of_using_leave        numeric(20,2)                                               not null,
leave_remaining                numeric(20,2)                                               not null,
leave_from                     timestamp                                                   not null,
leave_to                       timestamp                                                   not null,
duration_of_leave              numeric(20,2)                                               not null,
attachment_url                 varchar(1024),
substitute_oid                 varchar(128),
constraint                     pk_apply_for_leave                                                    primary key    (oid),
constraint                     fk_leave_oid_apply_for_leave                                          foreign key    (leave_oid)
                                                                                                     references     "hrm".leave_setup(oid),
constraint                     fk_applicant_oid_apply_for_leave                                      foreign key    (applicant_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_substitute_oid_apply_for_leave                                     foreign key    (substitute_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);


