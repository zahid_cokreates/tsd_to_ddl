/*
Table for recording leave type and setup the details requirement of that leave name type
oid                            : surrogate primary key
leave_name                     : Leave Name
gender                         : Gender
no_of_time                     : no Of Times leave can be taken
duration                       : duration of the leave can be taken
eligibility                    : eligibility of leave types
description                    : description of the leave type
*/
create table                   "hrm".leave_setup
(
oid                            varchar(128)                                                not null,
leave_name                     varchar(128)                                                not null,
gender                         varchar(32)                                                 not null,
no_of_time                     numeric(20,2)                                               not null,
duration                       numeric(20,2)                                               not null,
eligibility                    varchar(256)                                                not null,
description                    text,
constraint                     pk_leave_setup                                                        primary key    (oid),
constraint                     ck_gender_leave_setup                                                 check          (gender = 'Male' or gender = 'Female' or gender = 'Other'),
constraint                     ck_eligibility_leave_setup                                            check          (eligibility = 'Yes' or eligibility = 'No')
);


