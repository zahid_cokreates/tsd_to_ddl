/*
table for recording approval or disapproval of substitute
oid                            : surrogate primary key
leave_oid                      : leave Id
substitute_oid                 : substitute Id
approved_from                  : duty from 
approved_to                    : duty to
approval_status                : approval Status
reason                         : reason of disapprove
substitute_approval_code       : substitute Approval Id
*/
create table                   "hrm".leave_approval_by_substitute
(
oid                            varchar(128)                                                not null,
leave_oid                      varchar(128)                                                not null,
substitute_oid                 varchar(128)                                                not null,
approved_from                  timestamp                                                   not null,
approved_to                    timestamp                                                   not null,
approval_status                varchar(32)                                                 not null,
reason                         text,
substitute_approval_code       varchar(128)                                                not null,
constraint                     pk_leave_approval_by_substitute                                       primary key    (oid),
constraint                     fk_leave_oid_leave_approval_by_substitute                             foreign key    (leave_oid)
                                                                                                     references     "hrm".leave_setup(oid),
constraint                     fk_substitute_oid_leave_approval_by_substitute                        foreign key    (substitute_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     ck_approval_status_leave_approval_by_substitute                       check          (approval_status = 'Yes' or approval_status = 'No')
);

/*
Table for keeping the record of the leave approval
oid                            : surrogate primary key
leave_oid                      : leave name id
application_oid                : application Code
approval_status                : approval Status
approved_by_post               : post of the employee, approved By 
approved_by_oId                : Id of the employee, approved By 
approval_code                  : approval Id
comments                       : comments for approving or not approving
*/
create table                   "hrm".leave_approval_by_admin
(
oid                            varchar(128)                                                not null,
leave_oid                      varchar(128)                                                not null,
application_oid                varchar(128)                                                not null,
approval_status                varchar(32)                                                 not null,
approved_by_post               varchar(256)                                                not null,
approved_by_oId                varchar(128)                                                not null,
approval_code                  varchar(128)                                                not null,
comments                       text,
constraint                     pk_leave_approval_by_admin                                            primary key    (oid),
constraint                     fk_leave_oid_leave_approval_by_admin                                  foreign key    (leave_oid)
                                                                                                     references     "hrm".leave_setup(oid),
constraint                     fk_application_oid_leave_approval_by_admin                            foreign key    (application_oid)
                                                                                                     references     "hrm".apply_for_leave(oid),
constraint                     fk_approved_by_oId_leave_approval_by_admin                            foreign key    (approved_by_oId)
                                                                                                     references     "hrm".employee_master_info(oid)
);


