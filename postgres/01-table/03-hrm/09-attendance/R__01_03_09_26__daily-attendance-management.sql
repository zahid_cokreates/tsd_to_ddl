/*
record of daily attendance of the institution
oid                            : surrogate primary key
employee_oid                   : employee Id
date                           : timestamp
is_present                     : is Present
*/
create table                   "hrm".daily_attendance
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
date                           timestamp                                                   not null,
is_present                     varchar(256)                                                not null,
constraint                     pk_daily_attendance                                                   primary key    (oid),
constraint                     fk_employee_oid_daily_attendance                                      foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     ck_is_present_daily_attendance                                        check          (is_present = 'Yes' or is_present = 'No')
);


