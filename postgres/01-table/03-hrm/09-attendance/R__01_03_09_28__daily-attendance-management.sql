/*
record of employee's currently being in the meeting outside the institution 
oid                            : surrogate primary key
employee_oid                   : employee Id
meeting_or_event_venue         : meeting Or Event Venue
meeting_time                   : meeting Time
attendance_id                  : attendance Id 
*/
create table                   "hrm".at_meeting_or_events
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
meeting_or_event_venue         varchar(256)                                                not null,
meeting_time                   timestamp                                                   not null,
attendance_id                  varchar(256)                                                not null,
constraint                     pk_at_meeting_or_events                                               primary key    (oid),
constraint                     fk_employee_oid_at_meeting_or_events                                  foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);


