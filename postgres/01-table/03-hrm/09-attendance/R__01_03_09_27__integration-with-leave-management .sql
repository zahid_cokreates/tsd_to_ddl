/*
the list of employees, currently in leave
oid                            : surrogate primary key
employee_oid                   : employee Id
leave_status_oid               : leave Status
*/
create table                   "hrm".at_leave
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
leave_status_oid               varchar(128)                                                not null,
constraint                     pk_at_leave                                                           primary key    (oid),
constraint                     fk_employee_oid_at_leave                                              foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_leave_status_oid_at_leave                                          foreign key    (leave_status_oid)
                                                                                                     references     "hrm".daily_attendance(oid)
);


