/*
table for penalty against the absence of the employee
oid                            : surrogate primary key
date                           : timestamp
employee_oid                   : employee Id
cause                          : cause
action                         : action
remarks                        : remarks
penalty_code                   : penalty Id
*/
create table                   "hrm".penalty_for_attendance
(
oid                            varchar(128)                                                not null,
date                           timestamp                                                   not null,
employee_oid                   varchar(128)                                                not null,
cause                          text                                                        not null,
action                         text                                                        not null,
remarks                        text,
penalty_code                   varchar(128)                                                not null,
constraint                     pk_penalty_for_attendance                                             primary key    (oid),
constraint                     fk_employee_oid_penalty_for_attendance                                foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);

/*
table for justification of the employee who got penalty 
oid                            : surrogate primary key
penalty_oid                    : penalty Id
employee_oid                   : employee Id
justification                  : justification
attachment_url                 : attachment Url
*/
create table                   "hrm".justify_against_penalty
(
oid                            varchar(128)                                                not null,
penalty_oid                    varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
justification                  text                                                        not null,
attachment_url                 varchar(1024)                                               not null,
constraint                     pk_justify_against_penalty                                            primary key    (oid),
constraint                     fk_penalty_oid_justify_against_penalty                                foreign key    (penalty_oid)
                                                                                                     references     "hrm".penalty_for_attendance(oid),
constraint                     fk_employee_oid_justify_against_penalty                               foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);

/*
table for attendance penalty committee
oid                            : surrogate primary key
committee_oid                  : -
committee_name                 : committee Name
short_description              : short Description
member_oid                     : foreign key
member_name_en                 : member Name
member_name_bn                 : member Name
designation_en                 : designation
designation_bn                 : designation
role                           : role
*/
create table                   "hrm".attendance_penalty_committee
(
oid                            varchar(128)                                                not null,
committee_oid                  varchar(128)                                                not null,
committee_name                 varchar(256)                                                not null,
short_description              text                                                        not null,
member_oid                     varchar(128)                                                not null,
member_name_en                 varchar(256)                                                not null,
member_name_bn                 varchar(256)                                                not null,
designation_en                 varchar(256)                                                not null,
designation_bn                 varchar(256)                                                not null,
role                           varchar(256)                                                not null,
constraint                     pk_attendance_penalty_committee                                       primary key    (oid),
constraint                     fk_committee_oid_attendance_penalty_committee                         foreign key    (committee_oid)
                                                                                                     references     "cmn".committee_setup_line(oid),
constraint                     fk_member_oid_attendance_penalty_committee                            foreign key    (member_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);

/*
The list of penalty committee
oid                            : surrogate primary key
attendance_committee_oid       : committee Id
committee_name                 : committee Name
*/
create table                   "hrm".penalty_committee_list
(
oid                            varchar(128)                                                not null,
attendance_committee_oid       varchar(128)                                                not null,
committee_name                 varchar(256)                                                not null,
constraint                     pk_penalty_committee_list                                             primary key    (oid),
constraint                     fk_attendance_committee_oid_penalty_committee_list                    foreign key    (attendance_committee_oid)
                                                                                                     references     "hrm".attendance_penalty_committee(oid)
);


