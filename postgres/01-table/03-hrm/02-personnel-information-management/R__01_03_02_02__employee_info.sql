/*
-
oid                            : -
name_en                        : -
name_bn                        : -
grp_username                   : -
govt_id                        : -
enothi_id                      : -
join_date                      : -
batch_oid                      : -
cadre_oid                      : -
grade_oid                      : -
is_freedom_fighter             : -
is_descendant_of_freedom_fighter : -
status                         : -
gazetted_date                  : -
photo_url                      : -
signature_url                  : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".employee_master_info
(
oid                            varchar(128)                                                not null,
name_en                        varchar(256)                                                not null,
name_bn                        varchar(256)                                                not null,
grp_username                   varchar(128),
govt_id                        varchar(32),
enothi_id                      varchar(32),
join_date                      timestamp                                                   not null,
batch_oid                      varchar(128),
cadre_oid                      varchar(128),
grade_oid                      varchar(128),
is_freedom_fighter             varchar(32)                                                 not null,
is_descendant_of_freedom_fighter varchar(32)                                                 not null,
status                         varchar(32)                                                 not null,
gazetted_date                  timestamp,
photo_url                      varchar(512),
signature_url                  varchar(512),
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_employee_master_info                                               primary key    (oid),
constraint                     fk_grp_username_employee_master_info                                  foreign key    (grp_username)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_batch_oid_employee_master_info                                     foreign key    (batch_oid)
                                                                                                     references     "hrm".batch(oid),
constraint                     fk_cadre_oid_employee_master_info                                     foreign key    (cadre_oid)
                                                                                                     references     "hrm".cadre(oid),
constraint                     fk_grade_oid_employee_master_info                                     foreign key    (grade_oid)
                                                                                                     references     "hrm".grade(oid),
constraint                     ck_is_freedom_fighter_employee_master_info                            check          (is_freedom_fighter = 'Yes' or is_freedom_fighter = 'No'),
constraint                     ck_is_descendant_of_freedom_fighter_employee_master_info              check          (is_descendant_of_freedom_fighter = 'Yes' or is_descendant_of_freedom_fighter = 'No'),
constraint                     ck_status_employee_master_info                                        check          (status = 'Active' or status = 'Inactive'),
constraint                     fk_created_by_employee_master_info                                    foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_employee_master_info                                    foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
-
oid                            : -
employee_oid                   : -
employment_type_oid            : -
system_role_id                 : -
office_id                      : -
office_unit_id                 : -
office_unit_post_id            : -
incharge_label_en              : -
incharge_label_bn              : -
is_default_profile             : -
is_office_admin                : -
is_office_head                 : -
node_level                     : -
node_sequence                  : -
joining_date                   : -
last_office_date               : -
status_change_date             : -
status                         : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".employee_office
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
employment_type_oid            varchar(128)                                                not null,
system_role_id                 varchar(128),
office_id                      varchar(128)                                                not null,
office_unit_id                 varchar(128)                                                not null,
office_unit_post_id            varchar(128)                                                not null,
incharge_label_en              varchar(256),
incharge_label_bn              varchar(256),
is_default_profile             varchar(32),
is_office_admin                varchar(32),
is_office_head                 varchar(32),
node_level                     varchar(32),
node_sequence                  varchar(32),
joining_date                   timestamp,
last_office_date               timestamp,
status_change_date             timestamp,
status                         varchar(32),
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_employee_office                                                    primary key    (oid),
constraint                     fk_employee_oid_employee_office                                       foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_employment_type_oid_employee_office                                foreign key    (employment_type_oid)
                                                                                                     references     "hrm".employment_type(oid),
constraint                     fk_office_id_employee_office                                          foreign key    (office_id)
                                                                                                     references     "cmn".office(oid),
constraint                     fk_office_unit_id_employee_office                                     foreign key    (office_unit_id)
                                                                                                     references     "cmn".office_unit(oid),
constraint                     fk_office_unit_post_id_employee_office                                foreign key    (office_unit_post_id)
                                                                                                     references     "cmn".office_unit_post(oid),
constraint                     ck_is_default_profile_employee_office                                 check          (is_default_profile = 'Yes' or is_default_profile = 'No'),
constraint                     ck_is_office_admin_employee_office                                    check          (is_office_admin = 'Yes' or is_office_admin = 'No'),
constraint                     ck_is_office_head_employee_office                                     check          (is_office_head = 'Yes' or is_office_head = 'No'),
constraint                     ck_status_employee_office                                             check          (status = 'Active' or status = 'Inactive'),
constraint                     fk_created_by_employee_office                                         foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_employee_office                                         foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
-
oid                            : -
employee_oid                   : -
father_name_en                 : -
father_name_bn                 : -
mother_name_en                 : -
mother_name_bn                 : -
present_address_oid            : -
permanent_address_oid          : -
email_address                  : -
mobile_no                      : -
phone_no                       : -
blood_group                    : -
date_of_birth                  : -
birth_place                    : -
gender                         : -
marital_status                 : -
etin                           : -
nid                            : -
bank_account                   : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".employee_personal_info
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
father_name_en                 varchar(256)                                                not null,
father_name_bn                 varchar(256)                                                not null,
mother_name_en                 varchar(256)                                                not null,
mother_name_bn                 varchar(256)                                                not null,
present_address_oid            varchar(128),
permanent_address_oid          varchar(128),
email_address                  varchar(256)                                                not null,
mobile_no                      varchar(256)                                                not null,
phone_no                       varchar(256),
blood_group                    varchar(32),
date_of_birth                  timestamp                                                   not null,
birth_place                    varchar(512),
gender                         varchar(32),
marital_status                 varchar(32)                                                 not null,
etin                           varchar(32),
nid                            varchar(32),
bank_account                   varchar(32),
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_employee_personal_info                                             primary key    (oid),
constraint                     fk_employee_oid_employee_personal_info                                foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_present_address_oid_employee_personal_info                         foreign key    (present_address_oid)
                                                                                                     references     "hrm".address_info(oid),
constraint                     fk_permanent_address_oid_employee_personal_info                       foreign key    (permanent_address_oid)
                                                                                                     references     "hrm".address_info(oid),
constraint                     ck_gender_employee_personal_info                                      check          (gender = 'Male' or gender = 'Female' or gender = 'Other'),
constraint                     ck_marital_status_employee_personal_info                              check          (marital_status = 'Unmarried' or marital_status = 'Married' or marital_status = 'Divorced' or marital_status = 'Widow' or marital_status = 'Widower'),
constraint                     fk_created_by_employee_personal_info                                  foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_employee_personal_info                                  foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
-
oid                            : -
employee_oid                   : -
designation_en                 : -
name_en                        : -
name_bn                        : -
designation_bn                 : -
occupation_en                  : -
occupation_bn                  : -
nid                            : -
etin                           : -
is_govt_employee               : -
govt_id                        : -
mobile_no                      : -
phone_no                       : -
home_district                  : -
organization                   : -
organization_location          : -
permanent_address              : -
present_address                : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".employee_spouse_info
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
designation_en                 varchar(256),
name_en                        varchar(256)                                                not null,
name_bn                        varchar(256)                                                not null,
designation_bn                 varchar(256),
occupation_en                  varchar(256),
occupation_bn                  varchar(256),
nid                            varchar(32),
etin                           varchar(32),
is_govt_employee               varchar(32),
govt_id                        varchar(32),
mobile_no                      varchar(256),
phone_no                       varchar(256),
home_district                  varchar(256),
organization                   varchar(256),
organization_location          varchar(256),
permanent_address              varchar(512),
present_address                varchar(512),
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     fk_employee_oid_employee_spouse_info                                  foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     ck_is_govt_employee_employee_spouse_info                              check          (is_govt_employee = 'Yes' or is_govt_employee = 'No'),
constraint                     fk_created_by_employee_spouse_info                                    foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_employee_spouse_info                                    foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
-
oid                            : -
employee_oid                   : -
name_en                        : -
name_bn                        : -
birth_date                     : 
is_widow                       : -
has_disability                 : -
gender                         : -
relation                       : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".employee_children_info
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
name_en                        varchar(256)                                                not null,
name_bn                        varchar(256)                                                not null,
birth_date                     timestamp,
is_widow                       varchar(32),
has_disability                 varchar(32),
gender                         varchar(256),
relation                       varchar(256),
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_employee_children_info                                             primary key    (oid),
constraint                     fk_employee_oid_employee_children_info                                foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     ck_is_widow_employee_children_info                                    check          (is_widow = 'Yes' or is_widow = 'No'),
constraint                     ck_has_disability_employee_children_info                              check          (has_disability = 'Yes' or has_disability = 'No'),
constraint                     ck_gender_employee_children_info                                      check          (gender = 'Male' or gender = 'Female' or gender = 'Other'),
constraint                     fk_created_by_employee_children_info                                  foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_employee_children_info                                  foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
The tables for managing educational Info of all officers and authorities. 
oid                            : surrogate primary key
employee_oid                   : the oid of employee as foreign key
degree_title                   : title of certificate or degree
institution_name               : name of the institution from where the certificate was achieved 
principal_subject              : principal subject of education experience
gpa_or_cgpa                    : result or GPA of the employee
year_of_passing                : year of passing
distinction                    : if permission on yes from GO with date
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".employee_education_qualification
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
degree_title                   varchar(256)                                                not null,
institution_name               varchar(256)                                                not null,
principal_subject              varchar(256)                                                not null,
gpa_or_cgpa                    varchar(256)                                                not null,
year_of_passing                numeric(4,0)                                                not null,
distinction                    varchar(256)                                                not null,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_employee_education_qualification                                   primary key    (oid),
constraint                     fk_employee_oid_employee_education_qualification                      foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_created_by_employee_education_qualification                        foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_employee_education_qualification                        foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
service history of the employee as a governmental employee
oid                            : surrogate primary key
employee_oid                   : employee Code
illness                        : date of Entry in Government Service
from_date                      : date of receiving Gazette
till_date                      : dateo f Encadrement
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".employee_medical_info
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
illness                        varchar(256)                                                not null,
from_date                      timestamp,
till_date                      timestamp,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_employee_medical_info                                              primary key    (oid),
constraint                     fk_employee_oid_employee_medical_info                                 foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_created_by_employee_medical_info                                   foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_employee_medical_info                                   foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
-
oid                            : -
employee_oid                   : -
language_oid                   : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".employee_language_skill
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
language_oid                   varchar(128)                                                not null,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_employee_language_skill                                            primary key    (oid),
constraint                     fk_employee_oid_employee_language_skill                               foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_language_oid_employee_language_skill                               foreign key    (language_oid)
                                                                                                     references     "hrm".language(oid),
constraint                     fk_created_by_employee_language_skill                                 foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_employee_language_skill                                 foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
-
oid                            : -
employee_oid                   : -
qualification                  : -
sort_order                     : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".employee_additional_prof_qualification
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
qualification                  varchar(256)                                                not null,
sort_order                     numeric(5,0)                                                not null,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_employee_additional_prof_qualification                             primary key    (oid),
constraint                     fk_employee_oid_employee_additional_prof_qualification                foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_created_by_employee_additional_prof_qualification                  foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_employee_additional_prof_qualification                  foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
-
oid                            : -
employee_oid                   : -
employer_name_en               : -
employer_name_bn               : -
employer_address               : -
type_of_service                : -
position                       : -
service_from                   : -
service_till                   : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".employee_other_services
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
employer_name_en               varchar(256)                                                not null,
employer_name_bn               varchar(256)                                                not null,
employer_address               varchar(512),
type_of_service                varchar(32)                                                 not null,
position                       varchar(256),
service_from                   timestamp,
service_till                   timestamp,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     fk_created_by_employee_other_services                                 foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_employee_other_services                                 foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
type of Cadre
oid                            : -
employee_oid                   : -
nature_of_offence              : -
punishment                     : -
date                           : -
remarks                        : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".employee_disciplinary_action
(
oid                            varchar(128),
employee_oid                   varchar(128)                                                not null,
nature_of_offence              varchar(256)                                                not null,
punishment                     varchar(256),
date                           timestamp                                                   not null,
remarks                        text,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_employee_disciplinary_action                                       primary key    (oid),
constraint                     fk_employee_oid_employee_disciplinary_action                          foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_created_by_employee_disciplinary_action                            foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_employee_disciplinary_action                            foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
-
oid                            : -
employee_oid                   : -
award_title                    : -
ground                         : -
date_of_award                  : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".employee_award
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
award_title                    varchar(256)                                                not null,
ground                         text,
date_of_award                  timestamp,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_employee_award                                                     primary key    (oid),
constraint                     fk_employee_oid_employee_award                                        foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_created_by_employee_award                                          foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_employee_award                                          foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
-
oid                            : -
employee_oid                   : -
publication_title              : -
publication_type               : -
date_of_publication            : -
description                    : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".employee_publication
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
publication_title              varchar(256)                                                not null,
publication_type               varchar(256),
date_of_publication            timestamp,
description                    text                                                                       default '',
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_employee_publication                                               primary key    (oid),
constraint                     fk_employee_oid_employee_publication                                  foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_created_by_employee_publication                                    foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_employee_publication                                    foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
-
oid                            : -
employee_oid                   : -
geo_country_oid                : -
purpose                        : -
travel_from                    : -
travel_till                    : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".employee_foreign_travel
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
geo_country_oid                varchar(128)                                                not null,
purpose                        text,
travel_from                    timestamp,
travel_till                    timestamp,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_employee_foreign_travel                                            primary key    (oid),
constraint                     fk_employee_oid_employee_foreign_travel                               foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_geo_country_oid_employee_foreign_travel                            foreign key    (geo_country_oid)
                                                                                                     references     "cmn".geo_country(oid),
constraint                     fk_created_by_employee_foreign_travel                                 foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_employee_foreign_travel                                 foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
-
oid                            : -
employee_oid                   : -
probation_from                 : -
probation_till                 : -
extended_probation_from        : -
extended_probation_till        : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".employee_probation
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
probation_from                 timestamp                                                   not null,
probation_till                 timestamp,
extended_probation_from        timestamp,
extended_probation_till        timestamp,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_employee_probation                                                 primary key    (oid),
constraint                     fk_employee_oid_employee_probation                                    foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_created_by_employee_probation                                      foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_employee_probation                                      foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
-
oid                            : -
employee_oid                   : -
confirmation_date              : -
confirmation_by                : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".employee_confirmation
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
confirmation_date              timestamp                                                   not null,
confirmation_by                varchar(256),
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_employee_confirmation                                              primary key    (oid),
constraint                     fk_employee_oid_employee_confirmation                                 foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_created_by_employee_confirmation                                   foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_employee_confirmation                                   foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
-
oid                            : -
employee_oid                   : -
course_title                   : -
training_institution_name      : -
training_from                  : -
training_till                  : -
training_type                  : -
grade                          : -
geo_country_oid                : -
source_of_fund                 : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".employee_training_info
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(256)                                                not null,
course_title                   varchar(256)                                                not null,
training_institution_name      varchar(256),
training_from                  timestamp,
training_till                  timestamp,
training_type                  varchar(256),
grade                          varchar(32),
geo_country_oid                varchar(256),
source_of_fund                 varchar(256),
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_employee_training_info                                             primary key    (oid),
constraint                     fk_employee_oid_employee_training_info                                foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_geo_country_oid_employee_training_info                             foreign key    (geo_country_oid)
                                                                                                     references     "cmn".geo_country(oid),
constraint                     fk_created_by_employee_training_info                                  foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_employee_training_info                                  foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);


