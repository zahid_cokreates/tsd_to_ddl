/*
table for recording the call for exam for recruitment
oid                            : surrogate primary key
circular_oid                   : circular Id
exam_configuration_oid         : -
exam_code                      : exam Id
exam_date                      : exam Date
exam_type                      : exam Type
exam_start                     : exam Start
exam_end                       : exam End
*/
create table                   "hrm".call_for_exam
(
oid                            varchar(128)                                                not null,
circular_oid                   varchar(128)                                                not null,
exam_configuration_oid         varchar(128)                                                not null,
exam_code                      varchar(128)                                                not null,
exam_date                      timestamp                                                   not null,
exam_type                      varchar(256)                                                not null,
exam_start                     timestamp                                                   not null,
exam_end                       timestamp                                                   not null,
constraint                     pk_call_for_exam                                                      primary key    (oid),
constraint                     fk_circular_oid_call_for_exam                                         foreign key    (circular_oid)
                                                                                                     references     "hrm".job_circular(oid),
constraint                     fk_exam_configuration_oid_call_for_exam                               foreign key    (exam_configuration_oid)
                                                                                                     references     "hrm".exam_configuration(oid),
constraint                     ck_exam_type_call_for_exam                                            check          (exam_type = 'Viva' or exam_type = 'Written' or exam_type = 'Presentation' or exam_type = 'Interview')
);

/*
table for recording the exam venue of the applicants
oid                            : surrogate primary key
applicant_oid                  : applicant's Id
shortlist_oid                  : shortlist Id
district                       : district of the exam venue
venue                          : address of the venue
exam_oid                       : exam Id
*/
create table                   "hrm".exam_venue
(
oid                            varchar(128)                                                not null,
applicant_oid                  varchar(128)                                                not null,
shortlist_oid                  varchar(128)                                                not null,
district                       varchar(256)                                                not null,
venue                          varchar(512)                                                not null,
exam_oid                       varchar(128)                                                not null,
constraint                     pk_exam_venue                                                         primary key    (oid),
constraint                     fk_applicant_oid_exam_venue                                           foreign key    (applicant_oid)
                                                                                                     references     "hrm".application_form_for_recruitment(oid),
constraint                     fk_shortlist_oid_exam_venue                                           foreign key    (shortlist_oid)
                                                                                                     references     "hrm".create_short_list(oid),
constraint                     fk_exam_oid_exam_venue                                                foreign key    (exam_oid)
                                                                                                     references     "hrm".call_for_exam(oid)
);

/*
table for recording the admit card Info of examinees
oid                            : surrogate primary key
exam_oid                       : exam Id
applicant_oid                  : applicant Id
roll_no                        : roll No
exam_date                      : exam Date
exam_venue                     : exam Venue
exam_type                      : exam Type
*/
create table                   "hrm".admit_card
(
oid                            varchar(128)                                                not null,
exam_oid                       varchar(128)                                                not null,
applicant_oid                  varchar(128)                                                not null,
roll_no                        numeric(20,2)                                               not null,
exam_date                      timestamp                                                   not null,
exam_venue                     varchar(256)                                                not null,
exam_type                      varchar(256)                                                not null,
constraint                     pk_admit_card                                                         primary key    (oid),
constraint                     fk_exam_oid_admit_card                                                foreign key    (exam_oid)
                                                                                                     references     "hrm".call_for_exam(oid),
constraint                     fk_applicant_oid_admit_card                                           foreign key    (applicant_oid)
                                                                                                     references     "hrm".application_form_for_recruitment(oid)
);

/*
table of the result of the examinees
oid                            : surrogate primary key
attachment_url                 : attachment Url
circular_oid                   : circular code
written_marks                  : written Marks
viva_marks                     : viva Marks
applicant_oid                  : applicant Code
*/
create table                   "hrm".recruitment_result
(
oid                            varchar(128)                                                not null,
attachment_url                 varchar(1024)                                               not null,
circular_oid                   varchar(128)                                                not null,
written_marks                  numeric(20,2)                                               not null,
viva_marks                     numeric(20,2)                                               not null,
applicant_oid                  varchar(128)                                                not null,
constraint                     pk_recruitment_result                                                 primary key    (oid),
constraint                     fk_circular_oid_recruitment_result                                    foreign key    (circular_oid)
                                                                                                     references     "hrm".job_circular(oid),
constraint                     fk_applicant_oid_recruitment_result                                   foreign key    (applicant_oid)
                                                                                                     references     "hrm".application_form_for_recruitment(oid)
);


