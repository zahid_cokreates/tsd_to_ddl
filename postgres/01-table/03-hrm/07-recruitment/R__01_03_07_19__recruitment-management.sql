/*
table for listing a short-list from the application for recruitment 
oid                            : surrogate primary key
short_list_code                : short List Id
application_code               : application Id
application_name               : application Name
educational_qualification      : educational Qualification
experience                     : experience
short_list_created_by_oid      : shortList Created By Id
district                       : district
*/
create table                   "hrm".create_short_list
(
oid                            varchar(128)                                                not null,
short_list_code                varchar(128)                                                not null,
application_code               varchar(128)                                                not null,
application_name               varchar(256)                                                not null,
educational_qualification      text                                                        not null,
experience                     text                                                        not null,
short_list_created_by_oid      varchar(128)                                                not null,
district                       varchar(256)                                                not null,
constraint                     pk_create_short_list                                                  primary key    (oid),
constraint                     fk_application_code_create_short_list                                 foreign key    (application_code)
                                                                                                     references     "hrm".application_form_for_recruitment(oid),
constraint                     fk_short_list_created_by_oid_create_short_list                        foreign key    (short_list_created_by_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);


