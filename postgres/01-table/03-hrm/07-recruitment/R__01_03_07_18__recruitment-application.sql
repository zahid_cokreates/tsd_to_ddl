/*
table for keeping record of application for recruitment or joining the post
oid                            : surrogate primary key
circular_oid                   : circular Id
applicant_code                 : applicant Id
application_code               : application Id
application_date               : date 
name                           : name
nid_no                         : nidNo
date_of_birth                  : date Of Birth
age                            : age
father_name_en                 : father Name
father_name_bn                 : father Name
mother_name_en                 : mother Name
mother_name_bn                 : mother Name
address                        : address
contact_number                 : contact Number
gender                         : gender
occupation                     : occupation
religion                       : religion
nationality                    : nationality
board                          : board
educational_qualification      : educational Qualification
experience                     : experience in government service 
quota                          : quota
*/
create table                   "hrm".application_form_for_recruitment
(
oid                            varchar(128)                                                not null,
circular_oid                   varchar(128)                                                not null,
applicant_code                 varchar(128)                                                not null,
application_code               varchar(128)                                                not null,
application_date               timestamp                                                   not null,
name                           varchar(256)                                                not null,
nid_no                         varchar(256)                                                not null,
date_of_birth                  timestamp                                                   not null,
age                            numeric(20,2)                                               not null,
father_name_en                 varchar(256)                                                not null,
father_name_bn                 varchar(256)                                                not null,
mother_name_en                 varchar(256)                                                not null,
mother_name_bn                 varchar(256)                                                not null,
address                        varchar(512)                                                not null,
contact_number                 varchar(256)                                                not null,
gender                         varchar(32)                                                 not null,
occupation                     varchar(256)                                                not null,
religion                       varchar(32)                                                 not null,
nationality                    varchar(256)                                                not null,
board                          varchar(256)                                                not null,
educational_qualification      text                                                        not null,
experience                     text                                                        not null,
quota                          varchar(256)                                                not null,
constraint                     pk_application_form_for_recruitment                                   primary key    (oid),
constraint                     fk_circular_oid_application_form_for_recruitment                      foreign key    (circular_oid)
                                                                                                     references     "hrm".job_circular(oid),
constraint                     ck_gender_application_form_for_recruitment                            check          (gender = 'Male' or gender = 'Female' or gender = 'Other'),
constraint                     ck_religion_application_form_for_recruitment                          check          (religion = 'Islam' or religion = 'Hinduism' or religion = 'Buddhism' or religion = 'Christianity' or religion = 'Other'),
constraint                     ck_quota_application_form_for_recruitment                             check          (quota = 'Physically challenges' or quota = 'Woman' or quota = 'Family of freedom fighter' or quota = 'Ethnic minorities' or quota = 'Based on merit' or quota = 'others')
);


