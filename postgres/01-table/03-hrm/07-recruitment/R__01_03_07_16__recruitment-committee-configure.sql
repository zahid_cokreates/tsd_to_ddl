/*
table for keeping the record of the committee for recruitment 
oid                            : surrogate primary key
committee_oid                  : committee Id
committee_name                 : committee Name
short_description              : short Description on the duty of the committee
member_oid                     :  id of membders
member_name_en                 : members' Name
member_name_bn                 : members' Name
designation_en                 : designation of the members
designation_bn                 : designation of the members
role                           : role of the members
*/
create table                   "hrm".recruitment_committee
(
oid                            varchar(128)                                                not null,
committee_oid                  varchar(128)                                                not null,
committee_name                 varchar(256)                                                not null,
short_description              text                                                        not null,
member_oid                     varchar(128)                                                not null,
member_name_en                 varchar(256)                                                not null,
member_name_bn                 varchar(256)                                                not null,
designation_en                 varchar(256)                                                not null,
designation_bn                 varchar(256)                                                not null,
role                           varchar(256)                                                not null,
constraint                     pk_recruitment_committee                                              primary key    (oid),
constraint                     fk_committee_oid_recruitment_committee                                foreign key    (committee_oid)
                                                                                                     references     "cmn".committee_setup_line(oid),
constraint                     fk_member_oid_recruitment_committee                                   foreign key    (member_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);


