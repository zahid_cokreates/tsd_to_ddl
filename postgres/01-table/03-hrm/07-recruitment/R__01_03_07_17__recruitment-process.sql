/*
table for recording the details of job circular
oid                            : surrogate primary key
post_oid                       : postId
position_name                  : positionName
project                        : project
number_of_position             : numberOfPosition
job_type                       : jobType
educational_qualification      : educationalQualification
age_duration                   : ageDuration
application_fee                : applicationFee
other_qualification            : otherQualification
least_experience               : leastExperience
preferable_district_to_attend_exam : preferableDistrictToAttendExam
date_of_exam                   : dateOfExam
*/
create table                   "hrm".job_circular
(
oid                            varchar(128)                                                not null,
post_oid                       varchar(128)                                                not null,
position_name                  varchar(256)                                                not null,
project                        varchar(256),
number_of_position             varchar(32)                                                 not null,
job_type                       varchar(256)                                                not null,
educational_qualification      varchar(256)                                                not null,
age_duration                   varchar(256)                                                not null,
application_fee                varchar(256)                                                not null,
other_qualification            varchar(256)                                                not null,
least_experience               varchar(256)                                                not null,
preferable_district_to_attend_exam varchar(256)                                                not null,
date_of_exam                   timestamp                                                   not null,
constraint                     pk_job_circular                                                       primary key    (oid)
);

/*
table for recording the approval of job circular
oid                            : surrogate primary key
circular_oid                   : circular code
approved_by_oid                : approved By Id
approved_by_post_oid           : approved By Post
approval_code                  : approval Id
approval_status                : approval Status
comments                       : comments
*/
create table                   "hrm".job_circular_approval
(
oid                            varchar(128)                                                not null,
circular_oid                   varchar(128)                                                not null,
approved_by_oid                varchar(128)                                                not null,
approved_by_post_oid           varchar(256)                                                not null,
approval_code                  varchar(128)                                                not null,
approval_status                varchar(32)                                                 not null,
comments                       text,
constraint                     pk_job_circular_approval                                              primary key    (oid),
constraint                     fk_circular_oid_job_circular_approval                                 foreign key    (circular_oid)
                                                                                                     references     "hrm".job_circular(oid),
constraint                     fk_approved_by_oid_job_circular_approval                              foreign key    (approved_by_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     ck_approval_status_job_circular_approval                              check          (approval_status = 'Yes' or approval_status = 'No')
);


