/*
List of selected candidates of examinee list
oid                            : surrogate primary key
applicant_oid                  : applicant Id
circular_oid                   : circular Id
is_selected                    : is Selected
*/
create table                   "hrm".select_candidate
(
oid                            varchar(128)                                                not null,
applicant_oid                  varchar(128)                                                not null,
circular_oid                   varchar(128)                                                not null,
is_selected                    varchar(200)                                                not null,
constraint                     pk_select_candidate                                                   primary key    (oid),
constraint                     fk_applicant_oid_select_candidate                                     foreign key    (applicant_oid)
                                                                                                     references     "hrm".application_form_for_recruitment(oid),
constraint                     fk_circular_oid_select_candidate                                      foreign key    (circular_oid)
                                                                                                     references     "hrm".job_circular(oid),
constraint                     ck_is_selected_select_candidate                                       check          (is_selected = 'Yes' or is_selected = 'No')
);

/*
table of the list of final selected candidates after police verification and medical test
oid                            : surrogate primary key
candidate_oid                  : candidate Id
police_verification_attachment_url : police Verification Attachment Url
medical_verification_url       : Medical Verification Url
remarks                        : remarks
*/
create table                   "hrm".final_selection
(
oid                            varchar(128)                                                not null,
candidate_oid                  varchar(128)                                                not null,
police_verification_attachment_url varchar(1024)                                               not null,
medical_verification_url       varchar(1024)                                               not null,
remarks                        varchar(1024)                                               not null,
constraint                     pk_final_selection                                                    primary key    (oid),
constraint                     fk_candidate_oid_final_selection                                      foreign key    (candidate_oid)
                                                                                                     references     "hrm".select_candidate(oid)
);


