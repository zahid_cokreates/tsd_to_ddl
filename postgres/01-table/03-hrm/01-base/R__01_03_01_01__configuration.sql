/*
service history of the employee as a governmental employee
oid                            : surrogate primary key
name_en                        : -
name_bn                        : -
batch_year_en                  : -
batch_year_bn                  : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".batch
(
oid                            varchar(128)                                                not null,
name_en                        varchar(256)                                                not null,
name_bn                        varchar(256)                                                not null,
batch_year_en                  varchar(256)                                                not null,
batch_year_bn                  varchar(256)                                                not null,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_batch                                                              primary key    (oid),
constraint                     fk_created_by_batch                                                   foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_batch                                                   foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
service history of the employee as a governmental employee
oid                            : surrogate primary key
name_en                        : -
name_bn                        : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".cadre
(
oid                            varchar(128)                                                not null,
name_en                        varchar(256)                                                not null,
name_bn                        varchar(256)                                                not null,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_cadre                                                              primary key    (oid),
constraint                     fk_created_by_cadre                                                   foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_cadre                                                   foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
service history of the employee as a governmental employee
oid                            : surrogate primary key
name_en                        : -
name_bn                        : -
sort_order                     : -
cadre_oid                      : Reference to primary key
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".grade
(
oid                            varchar(128)                                                not null,
name_en                        varchar(256)                                                not null,
name_bn                        varchar(256)                                                not null,
sort_order                     numeric(5,0),
cadre_oid                      varchar(128),
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_grade                                                              primary key    (oid),
constraint                     fk_cadre_oid_grade                                                    foreign key    (cadre_oid)
                                                                                                     references     "hrm".cadre(oid),
constraint                     fk_created_by_grade                                                   foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_grade                                                   foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
-
oid                            : -
village_house_no_road_no       : -
police_station                 : -
post_office                    : -
postal_code                    : -
district_oid                   : -
division_oid                   : -
country_oid                    : -
phone_no                       : -
is_deleted                     : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
*/
create table                   "hrm".address_info
(
oid                            varchar(128)                                                not null,
village_house_no_road_no       varchar(256)                                                not null,
police_station                 varchar(256),
post_office                    varchar(256),
postal_code                    varchar(256),
district_oid                   varchar(128),
division_oid                   varchar(128),
country_oid                    varchar(128),
phone_no                       varchar(256),
is_deleted                     varchar(32)                                                 not null,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
constraint                     pk_address_info                                                       primary key    (oid),
constraint                     fk_district_oid_address_info                                          foreign key    (district_oid)
                                                                                                     references     "cmn".geo_district(oid),
constraint                     fk_division_oid_address_info                                          foreign key    (division_oid)
                                                                                                     references     "cmn".geo_division(oid),
constraint                     fk_country_oid_address_info                                           foreign key    (country_oid)
                                                                                                     references     "cmn".geo_country(oid),
constraint                     fk_created_by_address_info                                            foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_address_info                                            foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
-
oid                            : -
name_en                        : -
name_bn                        : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".language
(
oid                            varchar(128)                                                not null,
name_en                        varchar(256)                                                not null,
name_bn                        varchar(256)                                                not null,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_language                                                           primary key    (oid),
constraint                     fk_created_by_language                                                foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_language                                                foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
-
oid                            : -
name_en                        : -
name_bn                        : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".employment_type
(
oid                            varchar(128)                                                not null,
name_en                        varchar(256)                                                not null,
name_bn                        varchar(256)                                                not null,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_employment_type                                                    primary key    (oid),
constraint                     fk_created_by_employment_type                                         foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_employment_type                                         foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
-
oid                            : -
announce_date                  : -
week_day                       : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".weekend
(
oid                            varchar(128)                                                not null,
announce_date                  timestamp                                                   not null,
week_day                       varchar(256)                                                not null,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_weekend                                                            primary key    (oid),
constraint                     fk_created_by_weekend                                                 foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_weekend                                                 foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);

/*
-
oid                            : -
name_en                        : -
name_bn                        : -
holiday_date                   : -
depends_on_moon                : -
is_multi_dated                 : -
from_date                      : -
to_date                        : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
is_deleted                     : -
*/
create table                   "hrm".holiday
(
oid                            varchar(128)                                                not null,
name_en                        varchar(256)                                                not null,
name_bn                        varchar(256)                                                not null,
holiday_date                   timestamp                                                   not null,
depends_on_moon                varchar(32)                                                 not null,
is_multi_dated                 varchar(32)                                                 not null,
from_date                      timestamp,
to_date                        timestamp,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
is_deleted                     varchar(32)                                                 not null,
constraint                     pk_holiday                                                            primary key    (oid),
constraint                     fk_created_by_holiday                                                 foreign key    (created_by)
                                                                                                     references     "sec".grp_user(oid),
constraint                     fk_updated_by_holiday                                                 foreign key    (updated_by)
                                                                                                     references     "sec".grp_user(oid)
);


