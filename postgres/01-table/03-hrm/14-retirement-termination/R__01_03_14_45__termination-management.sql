/*
Employee Termination Details
oid                            : Surrogate primary key
employee_oid                   : foreign key
employee_post_oid              : foreign key
date_of_termination            : date of termination
reason                         : reason
remarks                        : remarks 
go_code                        : Govt. order code
goverment_order                : goverment Order 
decision_status                : decision status 
application_code               : application code 
decision_by                    : foreign key
*/
create table                   "hrm".termination
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
employee_post_oid              varchar(256)                                                not null,
date_of_termination            timestamp                                                   not null,
reason                         varchar(256)                                                not null,
remarks                        text,
go_code                        varchar(128)                                                not null,
goverment_order                varchar(1024)                                               not null,
decision_status                varchar(256)                                                not null,
application_code               varchar(128)                                                not null,
decision_by                    varchar(256)                                                not null,
constraint                     pk_termination                                                        primary key    (oid),
constraint                     fk_employee_oid_termination                                           foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_decision_by_termination                                            foreign key    (decision_by)
                                                                                                     references     "hrm".employee_master_info(oid)
);


