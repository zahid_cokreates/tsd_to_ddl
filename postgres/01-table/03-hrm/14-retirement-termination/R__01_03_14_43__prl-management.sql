/*
PRL upon retirement details
oid                            : Surrogate primary key
employee_oid                   : foreign key
duration_from                  : working date (from)
duration_to                    : working date (to)
prl_duration                   : PRL duration
file_title                     : file title 
file_code                      : file code
file_type                      : file type
attachment                     : attachment
decision_status                : decision status 
application_code               : application code 
decision_by                    : foreign key
*/
create table                   "hrm".apply_for_prl
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
duration_from                  timestamp                                                   not null,
duration_to                    timestamp                                                   not null,
prl_duration                   numeric(20,0)                                               not null,
file_title                     varchar(256)                                                not null,
file_code                      varchar(128)                                                not null,
file_type                      varchar(256)                                                not null,
attachment                     varchar(1024)                                               not null,
decision_status                varchar(256)                                                not null,
application_code               varchar(128)                                                not null,
decision_by                    varchar(256)                                                not null,
constraint                     pk_apply_for_prl                                                      primary key    (oid),
constraint                     fk_employee_oid_apply_for_prl                                         foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_decision_by_apply_for_prl                                          foreign key    (decision_by)
                                                                                                     references     "hrm".employee_master_info(oid)
);


