/*
Retirement & termination details
oid                            : Surrogate primary key
employee_oid                   : foreign key
employee_post_oid              : foreign key
noc_upload                     : NOC upload
ipc_upload                     : LPC upload
date_of_retirement             : date of retirement 
employed_from                  : Work period start day
employed_upto                  : Work period end day
bank_account_no                : bank account no.
bank_name                      : bank name
bank_branch                    : bank branch
application_code               : application code 
decision_by                    : foreign key
current_post_joining_date      : current post joining date
current_post_retirement_date   : current post retirement date
prl_status                     : PRL status
rate_of_retirement_allowance   : rate of retirement allowance 
total_amount_of_retirement_allowance : total amount of retirement allowance
monthly_amount_of_retirement_allowance : monthlt amount of allowance
total_gratuity                 : total gratuity 
loan_history                   : loan history
notes                          : notes
pension_code                   : pension code
decision_status                : decision status
*/
create table                   "hrm".pension_procedure_for_employee
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
employee_post_oid              varchar(256)                                                not null,
noc_upload                     varchar(1024)                                               not null,
ipc_upload                     varchar(1024)                                               not null,
date_of_retirement             timestamp                                                   not null,
employed_from                  timestamp                                                   not null,
employed_upto                  timestamp                                                   not null,
bank_account_no                numeric(20,0)                                               not null,
bank_name                      varchar(256)                                                not null,
bank_branch                    varchar(256)                                                not null,
application_code               varchar(128)                                                not null,
decision_by                    varchar(256)                                                not null,
current_post_joining_date      timestamp                                                   not null,
current_post_retirement_date   timestamp                                                   not null,
prl_status                     varchar(256)                                                not null,
rate_of_retirement_allowance   numeric(20,2)                                               not null,
total_amount_of_retirement_allowance numeric(20,2)                                               not null,
monthly_amount_of_retirement_allowance numeric(20,2)                                               not null,
total_gratuity                 numeric(20,2)                                               not null,
loan_history                   varchar(256)                                                not null,
notes                          varchar(256),
pension_code                   varchar(128)                                                not null,
decision_status                varchar(256)                                                not null,
constraint                     pk_pension_procedure_for_employee                                     primary key    (oid),
constraint                     fk_employee_oid_pension_procedure_for_employee                        foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_decision_by_pension_procedure_for_employee                         foreign key    (decision_by)
                                                                                                     references     "hrm".employee_master_info(oid)
);


