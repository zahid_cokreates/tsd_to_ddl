/*
Award setup details
oid                            : Surrogate primary key
award_title                    : award title
award_code                     : award code
description                    : description
department                     : department
notes                          : notes
*/
create table                   "hrm".award_setup
(
oid                            varchar(128)                                                not null,
award_title                    varchar(256)                                                not null,
award_code                     varchar(128)                                                not null,
description                    text                                                        not null,
department                     varchar(256)                                                not null,
notes                          text,
constraint                     pk_award_setup                                                        primary key    (oid)
);

/*
Publication setup details
oid                            : Surrogate primary key
Publication_title              : Publication title
publication_code               : publication code
description                    : description
department                     : department
notes                          : notes
*/
create table                   "hrm".publication_setup
(
oid                            varchar(128)                                                not null,
Publication_title              varchar(256)                                                not null,
publication_code               text                                                        not null,
description                    varchar(256)                                                not null,
department                     varchar(256)                                                not null,
notes                          text,
constraint                     pk_publication_setup                                                  primary key    (oid)
);


