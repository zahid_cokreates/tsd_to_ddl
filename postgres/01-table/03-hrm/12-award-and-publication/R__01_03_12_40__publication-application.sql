/*
Publication application details
oid                            : Surrogate primary key
employee_oid                   : foreign key
publication_title              : publication title
attachments                    : attachments
date_of_publication            : date of publication
notes                          : notes
decision_status                : decision status 
application_code               : application code 
decision_by                    : foreign key
*/
create table                   "hrm".publication_application
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
publication_title              varchar(256)                                                not null,
attachments                    varchar(1024)                                               not null,
date_of_publication            timestamp                                                   not null,
notes                          text,
decision_status                varchar(256)                                                not null,
application_code               varchar(128)                                                not null,
decision_by                    varchar(256)                                                not null,
constraint                     pk_publication_application                                            primary key    (oid),
constraint                     fk_employee_oid_publication_application                               foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid),
constraint                     fk_decision_by_publication_application                                foreign key    (decision_by)
                                                                                                     references     "hrm".employee_master_info(oid)
);


