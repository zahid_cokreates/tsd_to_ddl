/*
Investigation under appointing authority 
oid                            : Surrogate primary key
employee_oid                   : foreign key
date_of_submission             : date of submission
evaluation_by_aa               : evaluation by AA
decision_status                : decision status 
application_code               : application  code 
decision_by                    : decision taker
notes                          : notes
esign                          : esign 
*/
create table                   "hrm".investigation_under_appointing_auth
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(256)                                                not null,
date_of_submission             timestamp                                                   not null,
evaluation_by_aa               numeric(20,0)                                               not null,
decision_status                varchar(256)                                                not null,
application_code               varchar(128)                                                not null,
decision_by                    varchar(256)                                                not null,
notes                          text,
esign                          varchar(256)                                                not null,
constraint                     pk_investigation_under_appointing_auth                                primary key    (oid),
constraint                     fk_employee_oid_investigation_under_appointing_auth                   foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);

/*
Investigation under controlling authority 
oid                            : Surrogate primary key
employee_oid                   : foreign key
date_of_submission             : date of submission
evaluation_by_ca               : evaluation by CA
decision_status                : decision status 
application_code               : application  code 
decision_by                    : decision taker
notes                          : notes
esign                          : esign 
*/
create table                   "hrm".investigation_under_controlling_auth
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
date_of_submission             timestamp                                                   not null,
evaluation_by_ca               numeric(20,0)                                               not null,
decision_status                varchar(256)                                                not null,
application_code               varchar(128)                                                not null,
decision_by                    varchar(256)                                                not null,
notes                          text,
esign                          varchar(256)                                                not null,
constraint                     pk_investigation_under_controlling_auth                               primary key    (oid),
constraint                     fk_employee_oid_investigation_under_controlling_auth                  foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);

/*
Investigation committee feedback details
oid                            : Surrogate primary key
employee_oid                   : foreign key
date_of_submission             : date of submission
evaluation_by_investigation_team : evaluation by investigation team
notes                          : notes
esign                          : esign 
*/
create table                   "hrm".investigation_team_feedback
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(256)                                                not null,
date_of_submission             timestamp                                                   not null,
evaluation_by_investigation_team numeric(20,0)                                               not null,
notes                          text,
esign                          varchar(256)                                                not null,
constraint                     pk_investigation_team_feedback                                        primary key    (oid),
constraint                     fk_employee_oid_investigation_team_feedback                           foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);


