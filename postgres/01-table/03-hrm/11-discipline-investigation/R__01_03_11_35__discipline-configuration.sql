/*
Employee offense setup details 
oid                            : Surrogate primary key
offense_title                  : offense title 
offense_code                   : offense  code
description                    : description
start_date_of_offense          : start date of offense
end_date_of_offense            : end date of offense
go_number                      : govt. order no.
esign                          : esign
*/
create table                   "hrm".offense_setup
(
oid                            varchar(128)                                                not null,
offense_title                  varchar(256)                                                not null,
offense_code                   numeric(20,0)                                               not null,
description                    text                                                        not null,
start_date_of_offense          timestamp                                                   not null,
end_date_of_offense            timestamp                                                   not null,
go_number                      numeric(20,0)                                               not null,
esign                          varchar(256)                                                not null,
constraint                     pk_offense_setup                                                      primary key    (oid)
);

/*
Investigation team setup details 
oid                            : Surrogate primary key
committee_oid                  : -
employee_oid                   : foreign key
controlling_authority          : controlling authority
appointing_authority           : appointing authority
assign_investigator            : assign investigator
esign                          : esign
*/
create table                   "hrm".investigation_team_setup
(
oid                            varchar(128)                                                not null,
committee_oid                  varchar(128)                                                not null,
employee_oid                   varchar(256)                                                not null,
controlling_authority          varchar(256)                                                not null,
appointing_authority           varchar(256)                                                not null,
assign_investigator            varchar(256)                                                not null,
esign                          varchar(256)                                                not null,
constraint                     pk_investigation_team_setup                                           primary key    (oid),
constraint                     fk_committee_oid_investigation_team_setup                             foreign key    (committee_oid)
                                                                                                     references     "cmn".committee_setup(oid),
constraint                     fk_employee_oid_investigation_team_setup                              foreign key    (employee_oid)
                                                                                                     references     "hrm".employee_master_info(oid)
);


