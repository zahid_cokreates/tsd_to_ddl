/*
Assignment of role
oid                            : Surrogate primary key
menu_json                      : 
default_status                 : 
grp_role_oid                   : Reference to primary key
grp_user_oid                   : Reference to primary key
*/
create table                   "sec".grp_user_role
(
oid                            varchar(128)                                                not null,
menu_json                      text                                                        not null,
default_status                 varchar(128)                                                not null,
grp_role_oid                   varchar(128)                                                not null,
grp_user_oid                   varchar(128)                                                not null,
constraint                     pk_grp_user_role                                                      primary key    (oid),
constraint                     fk_grp_role_oid_grp_user_role                                         foreign key    (grp_role_oid)
                                                                                                     references     "sec".grp_role(oid),
constraint                     fk_grp_user_oid_grp_user_role                                         foreign key    (grp_user_oid)
                                                                                                     references     "sec".grp_user(oid)
);

/*
Assignment of role of one or more user
oid                            : Surrogate primary key
component_oid                  : Reference to primary key
grp_role_oid                   : Reference to primary key
*/
create table                   "sec".grp_role_permission
(
oid                            varchar(128)                                                not null,
component_oid                  varchar(128)                                                not null,
grp_role_oid                   varchar(128)                                                not null,
constraint                     pk_grp_role_permission                                                primary key    (oid),
constraint                     fk_component_oid_grp_role_permission                                  foreign key    (component_oid)
                                                                                                     references     "sec".component(oid),
constraint                     fk_grp_role_oid_grp_role_permission                                   foreign key    (grp_role_oid)
                                                                                                     references     "sec".grp_role(oid)
);


