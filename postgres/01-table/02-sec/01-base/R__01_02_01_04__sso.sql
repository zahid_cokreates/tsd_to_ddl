/*
-
token                          : -
user_id                        : -
client_id                      : -
expiry_date                    : -
*/
create table                   "sec".token
(
token                          text                                                        not null,
user_id                        varchar(256)                                                not null,
client_id                      varchar(256)                                                not null,
expiry_date                    date                                                        not null
);

/*
-
ac_id                          : -
token_id                       : -
token                          : -
authentication_id              : -
username                       : -
client_id                      : -
authentication                 : -
refresh_token                  : -
*/
create table                   "sec".access_token
(
ac_id                          text                                                        not null,
token_id                       text                                                        not null,
token                          text                                                        not null,
authentication_id              text                                                        not null,
username                       varchar(256)                                                not null,
client_id                      varchar(256)                                                not null,
authentication                 text                                                        not null,
refresh_token                  text                                                        not null,
constraint                     pk_access_token                                                       primary key    (ac_id)
);

/*
-
ref_id                         : -
token_id                       : -
token                          : -
username                       : -
authentication                 : -
*/
create table                   "sec".refresh_token
(
ref_id                         text                                                        not null,
token_id                       text                                                        not null,
token                          text                                                        not null,
username                       varchar(256)                                                not null,
authentication                 text                                                        not null,
constraint                     pk_refresh_token                                                      primary key    (ref_id)
);

/*
-
client_id                      : -
resource_ids                   : -
client_secret                  : -
scope                          : -
authorized_grant_types         : -
web_server_redirect_uri        : -
authorities                    : -
access_token_validity          : -
refresh_token_validity         : -
additional_information         : -
autoapprove                    : -
*/
create table                   "sec".oauth_client_details
(
client_id                      varchar(20)                                                 not null,
resource_ids                   varchar(256),
client_secret                  varchar(256)                                                not null,
scope                          varchar(256)                                                not null,
authorized_grant_types         varchar(256)                                                not null,
web_server_redirect_uri        varchar(256),
authorities                    varchar(256),
access_token_validity          numeric                                                     not null,
refresh_token_validity         numeric                                                     not null,
additional_information         varchar(4096),
autoapprove                    varchar(256)                                                not null,
constraint                     pk_oauth_client_details                                               primary key    (client_id)
);


