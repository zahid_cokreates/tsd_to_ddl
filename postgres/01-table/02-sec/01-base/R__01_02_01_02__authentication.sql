/*
GRP Role Info
oid                            : Surrogate primary key
name_en                        : System generated unique code
name_bn                        : Name of role
role_type                      : System generated unique code
description_en                 : Sort description about role
description_bn                 : Sort description about role
menu_json                      : 
*/
create table                   "sec".grp_role
(
oid                            varchar(128)                                                not null,
name_en                        varchar(256)                                                not null,
name_bn                        varchar(128)                                                not null,
role_type                      varchar(64)                                                                default 'Business',
description_en                 text,
description_bn                 text,
menu_json                      text                                                        not null       default '{}',
constraint                     pk_grp_role                                                           primary key    (oid),
constraint                     ck_role_type_grp_role                                                 check          (role_type = 'System' or role_type = 'Service' or role_type = 'Business')
);

/*
GRP all Users
oid                            : Surrogate primary key
username                       : 
password                       : 
status                         : Represent status of user's role
account_expired                : 
credentials_expired            : 
account_locked                 : 
*/
create table                   "sec".grp_user
(
oid                            varchar(128)                                                not null,
username                       varchar(256)                                                not null,
password                       varchar(128)                                                not null,
status                         varchar(32)                                                 not null,
account_expired                varchar(32)                                                 not null       default 'No',
credentials_expired            varchar(32)                                                 not null       default 'No',
account_locked                 varchar(32)                                                 not null       default 'No',
constraint                     pk_grp_user                                                           primary key    (oid),
constraint                     ck_status_grp_user                                                    check          (status = 'Active' or status = 'Inactive'),
constraint                     ck_account_expired_grp_user                                           check          (account_expired = 'Yes' or account_expired = 'No'),
constraint                     ck_credentials_expired_grp_user                                       check          (credentials_expired = 'Yes' or credentials_expired = 'No'),
constraint                     ck_account_locked_grp_user                                            check          (account_locked = 'Yes' or account_locked = 'No')
);


