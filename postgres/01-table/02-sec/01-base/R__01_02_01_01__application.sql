/*
Module Info
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
status                         : 
module_type                    : 
sort_order                     : 
*/
create table                   "sec".grp_module
(
oid                            varchar(128)                                                not null,
name_en                        varchar(128)                                                not null,
name_bn                        varchar(256)                                                not null,
status                         varchar(32)                                                 not null       default 'Active',
module_type                    varchar(32)                                                 not null,
sort_order                     numeric(5,0)                                                not null,
constraint                     pk_grp_module                                                         primary key    (oid),
constraint                     ck_status_grp_module                                                  check          (status = 'Active' or status = 'Inactive'),
constraint                     ck_module_type_grp_module                                             check          (module_type = 'General' or module_type = 'Business')
);

/*
SubModule Info
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
status                         : 
sort_order                     : 
module_oid                     : Reference to primary  key
*/
create table                   "sec".grp_submodule
(
oid                            varchar(128)                                                not null,
name_en                        varchar(128)                                                not null,
name_bn                        varchar(256)                                                not null,
status                         varchar(32)                                                 not null       default 'Active',
sort_order                     numeric(5,0)                                                not null,
module_oid                     varchar(128)                                                not null,
constraint                     pk_grp_submodule                                                      primary key    (oid),
constraint                     ck_status_grp_submodule                                               check          (status = 'Active' or status = 'Inactive'),
constraint                     fk_module_oid_grp_submodule                                           foreign key    (module_oid)
                                                                                                     references     "sec".grp_module(oid)
);

/*
Feature Info
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
status                         : 
sort_order                     : 
sub_module_oid                 : Reference to primary  key
*/
create table                   "sec".grp_feature
(
oid                            varchar(128)                                                not null,
name_en                        varchar(128)                                                not null,
name_bn                        varchar(256)                                                not null,
status                         varchar(32)                                                 not null       default 'Active',
sort_order                     numeric(5,0)                                                not null,
sub_module_oid                 varchar(128)                                                not null,
constraint                     pk_grp_feature                                                        primary key    (oid),
constraint                     ck_status_grp_feature                                                 check          (status = 'Active' or status = 'Inactive'),
constraint                     fk_sub_module_oid_grp_feature                                         foreign key    (sub_module_oid)
                                                                                                     references     "sec".grp_submodule(oid)
);

/*
Process Info
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
status                         : 
sort_order                     : 
feature_oid                    : Reference to primary  key
*/
create table                   "sec".grp_subfeature
(
oid                            varchar(128)                                                not null,
name_en                        varchar(128)                                                not null,
name_bn                        varchar(256)                                                not null,
status                         varchar(32)                                                 not null       default 'Active',
sort_order                     numeric(5,0),
feature_oid                    varchar(128),
constraint                     pk_grp_subfeature                                                     primary key    (oid),
constraint                     ck_status_grp_subfeature                                              check          (status = 'Active' or status = 'Inactive'),
constraint                     fk_feature_oid_grp_subfeature                                         foreign key    (feature_oid)
                                                                                                     references     "sec".grp_feature(oid)
);

/*
Office Module Info
oid                            : Surrogate primary key
status                         : 
module_oid                     : Reference to primary  key
office_oid                     : Reference to primary  key
dashboard_url                  : 
*/
create table                   "sec".office_module
(
oid                            varchar(128)                                                not null,
status                         varchar(32)                                                 not null       default 'Active',
module_oid                     varchar(128)                                                not null,
office_oid                     varchar(128)                                                not null,
dashboard_url                  varchar(256),
constraint                     pk_office_module                                                      primary key    (oid),
constraint                     ck_status_office_module                                               check          (status = 'Active' or status = 'Inactive'),
constraint                     fk_module_oid_office_module                                           foreign key    (module_oid)
                                                                                                     references     "sec".grp_module(oid),
constraint                     fk_office_oid_office_module                                           foreign key    (office_oid)
                                                                                                     references     "cmn".office(oid)
);

/*
Micro Service Info
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
status                         : 
*/
create table                   "sec".service
(
oid                            varchar(128)                                                not null,
name_en                        varchar(128)                                                not null,
name_bn                        varchar(256),
status                         varchar(32)                                                 not null       default 'Active',
constraint                     pk_service                                                            primary key    (oid),
constraint                     ck_status_service                                                     check          (status = 'Active' or status = 'Inactive')
);

/*
All Endpoint Informaiton to get data
oid                            : Surrogate primary key
end_point                      : 
status                         : 
service_oid                    : 
*/
create table                   "sec".resource
(
oid                            varchar(128)                                                not null,
end_point                      varchar(128)                                                not null,
status                         varchar(32)                                                 not null       default 'Active',
service_oid                    varchar(128)                                                not null,
constraint                     pk_resource                                                           primary key    (oid),
constraint                     ck_status_resource                                                    check          (status = 'Active' or status = 'Inactive'),
constraint                     fk_service_oid_resource                                               foreign key    (service_oid)
                                                                                                     references     "sec".service(oid)
);

/*
UI Component Info
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
status                         : 
url                            : url of the component
subfeature_oid                 : 
*/
create table                   "sec".component
(
oid                            varchar(128)                                                not null,
name_en                        varchar(128)                                                not null,
name_bn                        varchar(256)                                                not null,
status                         varchar(32)                                                 not null       default 'Active',
url                            varchar(256),
subfeature_oid                 varchar(128),
constraint                     pk_component                                                          primary key    (oid),
constraint                     ck_status_component                                                   check          (status = 'Active' or status = 'Inactive'),
constraint                     fk_subfeature_oid_component                                           foreign key    (subfeature_oid)
                                                                                                     references     "sec".grp_subfeature(oid)
);

/*
Component Resource Info
oid                            : Surrogate primary key
status                         : 
resource_oid                   : 
component_oid                  : 
*/
create table                   "sec".component_resource
(
oid                            varchar(128)                                                not null,
status                         varchar(32)                                                 not null       default 'Active',
resource_oid                   varchar(128)                                                not null,
component_oid                  varchar(128)                                                not null,
constraint                     pk_component_resource                                                 primary key    (oid),
constraint                     ck_status_component_resource                                          check          (status = 'Active' or status = 'Inactive'),
constraint                     fk_resource_oid_component_resource                                    foreign key    (resource_oid)
                                                                                                     references     "sec".resource(oid),
constraint                     fk_component_oid_component_resource                                   foreign key    (component_oid)
                                                                                                     references     "sec".component(oid)
);


