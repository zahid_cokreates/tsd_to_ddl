/*

oid                            : 
created_by                     : 
created_on                     : 
updated_by                     : 
updated_on                     : 
row_status                     : 
location                       : 
permission_type                : 
allocated_size                 : 
used_space                     : 
*/
create table                   "cmn".directory
(
oid                            varchar(128)                                                not null,
created_by                     varchar(128),
created_on                     timestamp                                                   not null,
updated_by                     varchar(128),
updated_on                     timestamp                                                   not null,
row_status                     varchar(32)                                                 not null,
location                       varchar(255)                                                not null,
permission_type                varchar(3),
allocated_size                 numeric(15)                                                 not null,
used_space                     numeric(15)                                                 not null,
constraint                     pk_directory                                                          primary key    (oid)
);

/*

oid                            : 
created_by                     : 
created_on                     : 
updated_by                     : 
updated_on                     : 
row_status                     : 
title                          : 
stored_name                    : 
file_size                      : 
description                    : 
dir_id                         : 
org_name                       : 
*/
create table                   "cmn".file
(
oid                            varchar(128)                                                not null,
created_by                     varchar(128),
created_on                     timestamp                                                   not null,
updated_by                     varchar(128),
updated_on                     timestamp                                                   not null,
row_status                     varchar(32)                                                 not null,
title                          varchar(128)                                                not null,
stored_name                    varchar(128)                                                not null,
file_size                      numeric(10,0)                                               not null,
description                    text,
dir_id                         varchar(128)                                                not null,
org_name                       varchar(255)                                                not null,
constraint                     pk_file                                                               primary key    (oid),
constraint                     fk_dir_id_file                                                        foreign key    (dir_id)
                                                                                                     references     "cmn".directory(oid)
);

/*

oid                            : 
created_by                     : 
created_on                     : 
updated_by                     : 
updated_on                     : 
row_status                     : 
request_oid                    : 
requested_by                   : 
*/
create table                   "cmn".request
(
oid                            varchar(128)                                                not null,
created_by                     varchar(128),
created_on                     timestamp                                                   not null,
updated_by                     varchar(128),
updated_on                     timestamp                                                   not null,
row_status                     varchar(32)                                                 not null,
request_oid                    varchar(128)                                                not null,
requested_by                   varchar(128),
constraint                     pk_request                                                            primary key    (oid)
);

/*

oid                            : 
created_by                     : 
created_on                     : 
updated_by                     : 
updated_on                     : 
row_status                     : 
file_oid                       : 
request_oid                    : 
*/
create table                   "cmn".file_request
(
oid                            varchar(128),
created_by                     varchar(128),
created_on                     timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
row_status                     varchar(32),
file_oid                       varchar(128)                                                not null,
request_oid                    varchar(128)                                                not null,
constraint                     fk_file_oid_file_request                                              foreign key    (file_oid)
                                                                                                     references     "cmn".file(oid),
constraint                     fk_request_oid_file_request                                           foreign key    (request_oid)
                                                                                                     references     "cmn".request(oid)
);


