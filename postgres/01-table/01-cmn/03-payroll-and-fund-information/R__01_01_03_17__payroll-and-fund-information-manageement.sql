/*
information of allowance set up
oid                            : Surrogate primary key
grade                          : employee grade
house_rent_allowance           : house rent allowance
medical_allowance              : medical allowance
mobile_allowance               : mobile allowance
other_allowance                : other allowance
percentage                     : percentage
start_date                     : employment start date
end_date                       : employment end date
*/
create table                   "cmn".allowance_setup
(
oid                            varchar(128)                                                not null,
grade                          varchar(256)                                                not null,
house_rent_allowance           numeric(20,2)                                               not null,
medical_allowance              numeric(20,2)                                               not null,
mobile_allowance               numeric(20,2)                                               not null,
other_allowance                numeric(20,2)                                               not null,
percentage                     numeric(20,2)                                               not null,
start_date                     date                                                        not null,
end_date                       date                                                        not null,
constraint                     pk_allowance_setup                                                    primary key    (oid)
);

/*
Payroll deduction setup details
oid                            : Surrogate primary key
grade                          : employee grade
house_rent_deduction           : house rent deduction
tax_deduction                  : tax deduction
gpf_deduction                  : gpf deduction
other_deduction                : other deduction
percentage                     : percentage
start_date                     : employment start date
end_date                       : employment end date
*/
create table                   "cmn".deduction_setup
(
oid                            varchar(128)                                                not null,
grade                          varchar(256)                                                not null,
house_rent_deduction           numeric(20,2)                                               not null,
tax_deduction                  numeric(20,2)                                               not null,
gpf_deduction                  numeric(20,2)                                               not null,
other_deduction                numeric(20,2)                                               not null,
percentage                     numeric(20,2)                                               not null,
start_date                     date                                                        not null,
end_date                       date                                                        not null,
constraint                     pk_deduction_setup                                                    primary key    (oid)
);

/*
Payroll process details
oid                            : Surrogate primary key
employee_oid                   : foreign key
house_rent                     : house rent
total_allowance                : total allowance
total_deduction                : total deduction
final_amount                   : final amount
*/
create table                   "cmn".payroll_process
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
house_rent                     numeric(20,2)                                               not null,
total_allowance                numeric(20,2)                                               not null,
total_deduction                numeric(20,2)                                               not null,
final_amount                   numeric(20,2)                                               not null,
constraint                     pk_payroll_process                                                    primary key    (oid)
);

/*
Salary bill preparation details
oid                            : Surrogate primary key
grade                          : employee grade
employee_type                  : employee type
employee_oid                   : foreign key
attachment                     : attachment
*/
create table                   "cmn".prepare_salary_bill
(
oid                            varchar(128)                                                not null,
grade                          varchar(256)                                                not null,
employee_type                  varchar(256)                                                not null,
employee_oid                   varchar(128)                                                not null,
attachment                     varchar(1024)                                               not null,
constraint                     pk_prepare_salary_bill                                                primary key    (oid)
);


