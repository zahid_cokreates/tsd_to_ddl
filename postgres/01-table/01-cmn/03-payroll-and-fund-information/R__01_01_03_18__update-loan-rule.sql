/*
Employee Loan Sanction details
oid                            : Surrogate primary key
employee_oid                   : foreign key
last_date_of_loan              : last date of loan
date_of_retirement             : date of retirement 
loan_type                      : loan type
loan_amount                    : loan amount
no_of_installment              : no of Installment 
loan_letter_upload             : upload loan letter
notes                          : notes
*/
create table                   "cmn".loan_sanction 
(
oid                            varchar(128)                                                not null,
employee_oid                   varchar(128)                                                not null,
last_date_of_loan              date                                                        not null,
date_of_retirement             date                                                        not null,
loan_type                      varchar(256)                                                not null,
loan_amount                    numeric(20,2)                                               not null,
no_of_installment              numeric(20,2)                                               not null,
loan_letter_upload             varchar(1024)                                               not null,
notes                          text,
constraint                     pk_loan_sanction                                                      primary key    (oid)
);


