/*

oid                            : notifcation primary key
created_by                     : Created by
created_on                     : Time of creation
updated_by                     : Updated by
updated_on                     : Time of Update
row_status                     : Status of row
tag                            : 
token                          : 
message_content                : 
url                            : 
request_oid                    : 
callbackapi                    : 
*/
create table                   "cmn".notification
(
oid                            varchar(128)                                                not null,
created_by                     varchar(128),
created_on                     timestamp                                                   not null,
updated_by                     varchar(128),
updated_on                     timestamp                                                   not null,
row_status                     varchar(32)                                                 not null,
tag                            varchar(3)                                                  not null,
token                          varchar(5)                                                  not null,
message_content                text                                                        not null,
url                            varchar(255),
request_oid                    varchar(128)                                                not null,
callbackapi                    varchar(255),
constraint                     pk_notification                                                       primary key    (oid)
);

/*

oid                            : notification recipient primary key
created_by                     : Created by
created_on                     : Time of creation
updated_by                     : Updated by
updated_on                     : Time of Update
row_status                     : Status of row
emp_id                         : 
org_id                         : 
u_id                           : 
status                         : 
n_id                           : 
*/
create table                   "cmn".notification_recipient
(
oid                            varchar(128)                                                not null,
created_by                     varchar(128),
created_on                     timestamp                                                   not null,
updated_by                     varchar(128),
updated_on                     timestamp                                                   not null,
row_status                     varchar(32)                                                 not null,
emp_id                         varchar(128)                                                not null,
org_id                         varchar(128)                                                not null,
u_id                           varchar(128)                                                not null,
status                         varchar(15)                                                 not null,
n_id                           varchar(128)                                                not null,
constraint                     pk_notification_recipient                                             primary key    (oid),
constraint                     fk_n_id_notification_recipient                                        foreign key    (n_id)
                                                                                                     references     "cmn".notification(oid)
);


