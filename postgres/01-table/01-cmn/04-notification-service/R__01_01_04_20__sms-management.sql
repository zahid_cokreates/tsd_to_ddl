/*

oid                            : message primary key
created_by                     : Created by
created_on                     : Time of creation
updated_by                     : Updated by
updated_on                     : Time of Update
row_status                     : Status of row
tag                            : 
token                          : 
request_oid                    : 
message_content                : 
*/
create table                   "cmn".message
(
oid                            varchar(128)                                                not null,
created_by                     varchar(128),
created_on                     timestamp                                                   not null,
updated_by                     varchar(128),
updated_on                     timestamp                                                   not null,
row_status                     varchar(32)                                                 not null,
tag                            varchar(3)                                                  not null,
token                          varchar(5)                                                  not null,
request_oid                    varchar(128)                                                not null,
message_content                text                                                        not null,
constraint                     pk_message                                                            primary key    (oid)
);

/*

oid                            :  primary key
created_by                     : Created by
created_on                     : Time of creation
updated_by                     : Updated by
updated_on                     : Time of Update
row_status                     : Status of row
emp_id                         : 
org_id                         : 
field_type                     : 
m_id                           : 
trial_count                    : 
status                         : 
status_code                    : 
error_code                     : 
contact_no                     : 
message_id                     : 
scheduled_time                 : 
time                           : 
*/
create table                   "cmn".smstracker
(
oid                            varchar(128)                                                not null,
created_by                     varchar(128),
created_on                     timestamp                                                   not null,
updated_by                     varchar(128),
updated_on                     timestamp                                                   not null,
row_status                     varchar(32)                                                 not null,
emp_id                         varchar(128),
org_id                         varchar(128),
field_type                     varchar(10)                                                 not null,
m_id                           varchar(128)                                                not null,
trial_count                    numeric(1,0)                                                not null,
status                         varchar(5)                                                  not null,
status_code                    varchar(5),
error_code                     varchar(5)                                                  not null,
contact_no                     varchar(13)                                                 not null,
message_id                     varchar(20),
scheduled_time                 timestamp                                                   not null,
time                           timestamp,
constraint                     pk_smstracker                                                         primary key    (oid),
constraint                     fk_m_id_smstracker                                                    foreign key    (m_id)
                                                                                                     references     "cmn".message(oid)
);


