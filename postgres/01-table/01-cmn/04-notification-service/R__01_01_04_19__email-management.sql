/*

oid                            : email primary key
created_by                     : Created by
created_on                     : Time of creation
updated_by                     : Updated by
updated_on                     : Time of Update
row_status                     : Status of row
subject                        : subject of email
message_content                : content of email
tag                            : service from which it has been ordered
token                          : 
status_txt                     : 
request_oid                    : 
hasattachment                  : 
scheduled_time                 : 
departure_time                 : 
*/
create table                   "cmn".email
(
oid                            varchar(128)                                                not null,
created_by                     varchar(128),
created_on                     timestamp                                                   not null,
updated_by                     varchar(128),
updated_on                     timestamp,
row_status                     varchar(32)                                                 not null,
subject                        varchar(256)                                                not null,
message_content                text                                                        not null,
tag                            varchar(3)                                                  not null,
token                          varchar(5)                                                  not null,
status_txt                     varchar(10)                                                 not null,
request_oid                    varchar(128)                                                not null,
hasattachment                  varchar(5),
scheduled_time                 timestamp                                                   not null,
departure_time                 timestamp,
constraint                     pk_email                                                              primary key    (oid)
);

/*

oid                            : email recipient primary key
created_by                     : Created by
created_on                     : Time of creation
updated_by                     : Updated by
updated_on                     : Time of Update
row_status                     : Status of row
emp_id                         : 
org_id                         : 
email_address                  : 
recipient_type                 : 
email_id                       : 
field_type                     : 
*/
create table                   "cmn".email_recipient
(
oid                            varchar(128)                                                not null,
created_by                     varchar(128),
created_on                     timestamp                                                   not null,
updated_by                     varchar(128),
updated_on                     timestamp,
row_status                     varchar(32)                                                 not null,
emp_id                         varchar(128)                                                not null,
org_id                         varchar(128)                                                not null,
email_address                  varchar(128)                                                not null,
recipient_type                 varchar(3)                                                  not null,
email_id                       varchar(128)                                                not null,
field_type                     varchar(10)                                                 not null,
constraint                     pk_email_recipient                                                    primary key    (oid),
constraint                     fk_email_id_email_recipient                                           foreign key    (email_id)
                                                                                                     references     "cmn".email(oid)
);

/*

oid                            : email attachment primary key
created_by                     : Created by
created_on                     : Time of creation
updated_by                     : Updated by
updated_on                     : Time of Update
row_status                     : Status of row
file_name                      : 
location                       : 
file_unique_name               : 
email_id                       : 
*/
create table                   "cmn".email_attachment
(
oid                            varchar(128)                                                not null,
created_by                     varchar(128),
created_on                     timestamp                                                   not null,
updated_by                     varchar(128),
updated_on                     timestamp,
row_status                     varchar(32)                                                 not null,
file_name                      varchar(128)                                                not null,
location                       varchar(255)                                                not null,
file_unique_name               varchar(275)                                                not null,
email_id                       varchar(128)                                                not null,
constraint                     pk_email_attachment                                                   primary key    (oid),
constraint                     fk_email_id_email_attachment                                          foreign key    (email_id)
                                                                                                     references     "cmn".email(oid)
);


