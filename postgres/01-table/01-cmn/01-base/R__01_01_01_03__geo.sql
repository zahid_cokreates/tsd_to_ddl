/*
Category of oranization such as Government,Autonomous,private etc
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
*/
create table                   "cmn".geo_country
(
oid                            varchar(128)                                                not null,
name_en                        varchar(256)                                                not null,
name_bn                        varchar(256)                                                not null,
constraint                     pk_geo_country                                                        primary key    (oid)
);

/*
Category of oranization such as Government,Autonomous,private etc
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
bbs_code                       : 
country_oid                    : 
*/
create table                   "cmn".geo_division
(
oid                            varchar(128)                                                not null,
name_en                        varchar(256)                                                not null,
name_bn                        varchar(256)                                                not null,
bbs_code                       varchar(32),
country_oid                    varchar(128)                                                not null,
constraint                     pk_geo_division                                                       primary key    (oid),
constraint                     fk_country_oid_geo_division                                           foreign key    (country_oid)
                                                                                                     references     "cmn".geo_country(oid)
);

/*
Category of oranization such as Government,Autonomous,private etc
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
bbs_code                       : 
division_oid                   : 
*/
create table                   "cmn".geo_district
(
oid                            varchar(128)                                                not null,
name_en                        varchar(256)                                                not null,
name_bn                        varchar(256)                                                not null,
bbs_code                       varchar(32),
division_oid                   varchar(128)                                                not null,
constraint                     pk_geo_district                                                       primary key    (oid),
constraint                     fk_division_oid_geo_district                                          foreign key    (division_oid)
                                                                                                     references     "cmn".geo_division(oid)
);

/*
Category of oranization such as Government,Autonomous,private etc
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
bbs_code                       : 
district_oid                   : 
*/
create table                   "cmn".geo_thana
(
oid                            varchar(128)                                                not null,
name_en                        varchar(64)                                                 not null,
name_bn                        varchar(128)                                                not null,
bbs_code                       varchar(32),
district_oid                   varchar(128)                                                not null,
constraint                     pk_geo_thana                                                          primary key    (oid),
constraint                     fk_district_oid_geo_thana                                             foreign key    (district_oid)
                                                                                                     references     "cmn".geo_district(oid)
);

/*
Category of oranization such as Government,Autonomous,private etc
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
bbs_code                       : 
district_oid                   : 
*/
create table                   "cmn".geo_upazila
(
oid                            varchar(128)                                                not null,
name_en                        varchar(64)                                                 not null,
name_bn                        varchar(128)                                                not null,
bbs_code                       varchar(32),
district_oid                   varchar(128)                                                not null,
constraint                     pk_geo_upazila                                                        primary key    (oid),
constraint                     fk_district_oid_geo_upazila                                           foreign key    (district_oid)
                                                                                                     references     "cmn".geo_district(oid)
);

/*
Category of oranization such as Government,Autonomous,private etc
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
bbs_code                       : 
upazila_oid                    : 
*/
create table                   "cmn".geo_union
(
oid                            varchar(128)                                                not null,
name_en                        varchar(64)                                                 not null,
name_bn                        varchar(128)                                                not null,
bbs_code                       varchar(32),
upazila_oid                    varchar(128)                                                not null,
constraint                     pk_geo_union                                                          primary key    (oid),
constraint                     fk_upazila_oid_geo_union                                              foreign key    (upazila_oid)
                                                                                                     references     "cmn".geo_upazila(oid)
);

/*
Category of oranization such as Government,Autonomous,private etc
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
bbs_code                       : 
union_oid                      : 
*/
create table                   "cmn".geo_union_ward
(
oid                            varchar(128)                                                not null,
name_en                        varchar(64)                                                 not null,
name_bn                        varchar(128)                                                not null,
bbs_code                       varchar(32),
union_oid                      varchar(128)                                                not null,
constraint                     pk_geo_union_ward                                                     primary key    (oid),
constraint                     fk_union_oid_geo_union_ward                                           foreign key    (union_oid)
                                                                                                     references     "cmn".geo_union(oid)
);

/*
Category of oranization such as Government,Autonomous,private etc
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
bbs_code                       : 
union_oid                      : 
*/
create table                   "cmn".geo_post_office
(
oid                            varchar(128)                                                not null,
name_en                        varchar(64)                                                 not null,
name_bn                        varchar(128)                                                not null,
bbs_code                       varchar(32),
union_oid                      varchar(128)                                                not null,
constraint                     pk_geo_post_office                                                    primary key    (oid),
constraint                     fk_union_oid_geo_post_office                                          foreign key    (union_oid)
                                                                                                     references     "cmn".geo_union(oid)
);

/*
Category of oranization such as Government,Autonomous,private etc
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
bbs_code                       : 
district_oid                   : 
*/
create table                   "cmn".city_corporation
(
oid                            varchar(128)                                                not null,
name_en                        varchar(64)                                                 not null,
name_bn                        varchar(128)                                                not null,
bbs_code                       varchar(32),
district_oid                   varchar(128)                                                not null,
constraint                     pk_city_corporation                                                   primary key    (oid),
constraint                     fk_district_oid_city_corporation                                      foreign key    (district_oid)
                                                                                                     references     "cmn".geo_district(oid)
);

/*
Category of oranization such as Government,Autonomous,private etc
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
bbs_code                       : 
district_oid                   : 
city_corporation_oid           : 
*/
create table                   "cmn".city_corporation_ward
(
oid                            varchar(128)                                                not null,
name_en                        varchar(64)                                                 not null,
name_bn                        varchar(128)                                                not null,
bbs_code                       varchar(32),
district_oid                   varchar(128)                                                not null,
city_corporation_oid           varchar(128)                                                not null,
constraint                     pk_city_corporation_ward                                              primary key    (oid),
constraint                     fk_district_oid_city_corporation_ward                                 foreign key    (district_oid)
                                                                                                     references     "cmn".geo_district(oid),
constraint                     fk_city_corporation_oid_city_corporation_ward                         foreign key    (city_corporation_oid)
                                                                                                     references     "cmn".city_corporation(oid)
);

/*
Category of oranization such as Government,Autonomous,private etc
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
bbs_code                       : 
upazila_oid                    : 
*/
create table                   "cmn".municipality
(
oid                            varchar(128)                                                not null,
name_en                        varchar(64)                                                 not null,
name_bn                        varchar(128)                                                not null,
bbs_code                       varchar(32),
upazila_oid                    varchar(128)                                                not null,
constraint                     pk_municipality                                                       primary key    (oid),
constraint                     fk_upazila_oid_municipality                                           foreign key    (upazila_oid)
                                                                                                     references     "cmn".geo_upazila(oid)
);

/*
Category of oranization such as Government,Autonomous,private etc
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
bbs_code                       : 
upazila_oid                    : 
municipality_oid               : 
*/
create table                   "cmn".municipality_ward
(
oid                            varchar(128)                                                not null,
name_en                        varchar(64)                                                 not null,
name_bn                        varchar(128)                                                not null,
bbs_code                       varchar(32),
upazila_oid                    varchar(128)                                                not null,
municipality_oid               varchar(128)                                                not null,
constraint                     pk_municipality_ward                                                  primary key    (oid),
constraint                     fk_upazila_oid_municipality_ward                                      foreign key    (upazila_oid)
                                                                                                     references     "cmn".geo_upazila(oid),
constraint                     fk_municipality_oid_municipality_ward                                 foreign key    (municipality_oid)
                                                                                                     references     "cmn".municipality(oid)
);


