/*
Category of oranization such as Government,Autonomous,private etc
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
sort_order                     : 
is_deleted                     : 
created_by                     : 
created_on                     : 
updated_by                     : 
updated_on                     : 
*/
create table                   "cmn".office_layer
(
oid                            varchar(128)                                                not null,
name_en                        varchar(128)                                                not null,
name_bn                        varchar(256)                                                not null,
sort_order                     numeric(5,0)                                                not null,
is_deleted                     varchar(32)                                                 not null       default 'No',
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
constraint                     pk_office_layer                                                       primary key    (oid)
);

/*
Category of oranization such as Government,Autonomous,private etc
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
sort_order                     : 
is_deleted                     : 
created_by                     : 
created_on                     : 
updated_by                     : 
updated_on                     : 
*/
create table                   "cmn".office_unit_category
(
oid                            varchar(128)                                                not null,
name_en                        varchar(128)                                                not null,
name_bn                        varchar(256)                                                not null,
sort_order                     numeric(5,0)                                                not null,
is_deleted                     varchar(32)                                                 not null       default 'No',
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
constraint                     pk_office_unit_category                                               primary key    (oid)
);

/*
Category of oranization such as Government,Autonomous,private etc
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
code_en                        : 
code_bn                        : 
address_en                     : 
address_bn                     : 
email                          : 
web                            : 
logo_url                       : 
status                         : 
sort_order                     : 
parent_oid                     : 
office_layer_oid               : 
ministry_oid                   : 
root_office_oid                : 
is_root_office                 : 
is_deleted                     : 
created_by                     : 
created_on                     : 
updated_by                     : 
updated_on                     : 
*/
create table                   "cmn".office
(
oid                            varchar(128)                                                not null,
name_en                        varchar(128)                                                not null,
name_bn                        varchar(256)                                                not null,
code_en                        varchar(128),
code_bn                        varchar(128),
address_en                     text,
address_bn                     text,
email                          varchar(128),
web                            varchar(128),
logo_url                       varchar(512),
status                         varchar(32)                                                 not null,
sort_order                     numeric(5,0)                                                not null,
parent_oid                     varchar(128),
office_layer_oid               varchar(128)                                                not null,
ministry_oid                   varchar(128),
root_office_oid                varchar(128),
is_root_office                 varchar(32)                                                 not null       default 'No',
is_deleted                     varchar(32)                                                 not null       default 'No',
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
constraint                     pk_office                                                             primary key    (oid),
constraint                     ck_status_office                                                      check          (status = 'Active' or status = 'Inactive'),
constraint                     fk_parent_oid_office                                                  foreign key    (parent_oid)
                                                                                                     references     "cmn".office(oid),
constraint                     fk_office_layer_oid_office                                            foreign key    (office_layer_oid)
                                                                                                     references     "cmn".office_layer(oid),
constraint                     fk_ministry_oid_office                                                foreign key    (ministry_oid)
                                                                                                     references     "cmn".office(oid),
constraint                     fk_root_office_oid_office                                             foreign key    (root_office_oid)
                                                                                                     references     "cmn".office(oid)
);

/*
Category of oranization such as Government,Autonomous,private etc
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
display_order                  : 
business_order                 : 
parent_oid                     : 
office_unit_category_oid       : 
office_oid                     : 
ministry_oid                   : 
is_deleted                     : 
created_by                     : 
created_on                     : 
updated_by                     : 
updated_on                     : 
*/
create table                   "cmn".office_unit
(
oid                            varchar(128)                                                not null,
name_en                        varchar(128)                                                not null,
name_bn                        varchar(256)                                                not null,
display_order                  numeric(5,0)                                                not null,
business_order                 numeric(5,0)                                                not null,
parent_oid                     varchar(128),
office_unit_category_oid       varchar(128)                                                not null,
office_oid                     varchar(128)                                                not null,
ministry_oid                   varchar(128)                                                not null,
is_deleted                     varchar(32)                                                 not null       default 'No',
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
constraint                     pk_office_unit                                                        primary key    (oid),
constraint                     fk_parent_oid_office_unit                                             foreign key    (parent_oid)
                                                                                                     references     "cmn".office_unit(oid),
constraint                     fk_office_unit_category_oid_office_unit                               foreign key    (office_unit_category_oid)
                                                                                                     references     "cmn".office_unit_category(oid),
constraint                     fk_office_oid_office_unit                                             foreign key    (office_oid)
                                                                                                     references     "cmn".office(oid),
constraint                     fk_ministry_oid_office_unit                                           foreign key    (ministry_oid)
                                                                                                     references     "cmn".office(oid)
);

/*
Category of oranization such as Government,Autonomous,private etc
oid                            : Surrogate primary key
post_oid                       : 
display_order                  : 
business_order                 : 
parent_oid                     : 
office_unit_oid                : 
phone_no                       : 
is_deleted                     : 
created_by                     : 
created_on                     : 
updated_by                     : 
updated_on                     : 
*/
create table                   "cmn".office_unit_post
(
oid                            varchar(128)                                                not null,
post_oid                       varchar(128)                                                not null,
display_order                  numeric(5,0)                                                not null,
business_order                 numeric(5,0)                                                not null,
parent_oid                     varchar(128),
office_unit_oid                varchar(128)                                                not null,
phone_no                       varchar(256),
is_deleted                     varchar(32)                                                 not null       default 'No',
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
constraint                     pk_office_unit_post                                                   primary key    (oid),
constraint                     fk_post_oid_office_unit_post                                          foreign key    (post_oid)
                                                                                                     references     "cmn".post(oid),
constraint                     fk_parent_oid_office_unit_post                                        foreign key    (parent_oid)
                                                                                                     references     "cmn".office_unit_post(oid),
constraint                     fk_office_unit_oid_office_unit_post                                   foreign key    (office_unit_oid)
                                                                                                     references     "cmn".office_unit(oid)
);

/*
Category of oranization such as Government,Autonomous,private etc
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
status                         : 
level_order                    : 
sort_order                     : 
parent_oid                     : 
office_layer_oid               : 
ministry_oid                   : 
*/
create table                   "cmn".office_origin
(
oid                            varchar(128)                                                not null,
name_en                        varchar(128)                                                not null,
name_bn                        varchar(256)                                                not null,
status                         varchar(32)                                                 not null,
level_order                    numeric(5,0)                                                not null,
sort_order                     numeric(5,0)                                                not null,
parent_oid                     varchar(128),
office_layer_oid               varchar(128)                                                not null,
ministry_oid                   varchar(128)                                                not null,
constraint                     pk_office_origin                                                      primary key    (oid),
constraint                     ck_status_office_origin                                               check          (status = 'Active' or status = 'Inactive'),
constraint                     fk_office_layer_oid_office_origin                                     foreign key    (office_layer_oid)
                                                                                                     references     "cmn".office_layer(oid),
constraint                     fk_ministry_oid_office_origin                                         foreign key    (ministry_oid)
                                                                                                     references     "cmn".office(oid)
);

/*
Category of oranization such as Government,Autonomous,private etc
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
status                         : 
level_order                    : 
sort_order                     : 
parent_oid                     : 
office_unit_category_oid       : 
office_origin_oid              : 
office_layer_oid               : 
ministry_oid                   : 
*/
create table                   "cmn".office_origin_unit
(
oid                            varchar(128)                                                not null,
name_en                        varchar(128),
name_bn                        varchar(256)                                                not null,
status                         varchar(32)                                                 not null,
level_order                    numeric(5,0),
sort_order                     numeric(5,0)                                                not null,
parent_oid                     varchar(128),
office_unit_category_oid       varchar(128),
office_origin_oid              varchar(128)                                                not null,
office_layer_oid               varchar(128)                                                not null,
ministry_oid                   varchar(128)                                                not null,
constraint                     pk_office_origin_unit                                                 primary key    (oid),
constraint                     ck_status_office_origin_unit                                          check          (status = 'Active' or status = 'Inactive'),
constraint                     fk_office_unit_category_oid_office_origin_unit                        foreign key    (office_unit_category_oid)
                                                                                                     references     "cmn".office_unit_category(oid),
constraint                     fk_office_origin_oid_office_origin_unit                               foreign key    (office_origin_oid)
                                                                                                     references     "cmn".office_origin(oid),
constraint                     fk_office_layer_oid_office_origin_unit                                foreign key    (office_layer_oid)
                                                                                                     references     "cmn".office_layer(oid),
constraint                     fk_ministry_oid_office_origin_unit                                    foreign key    (ministry_oid)
                                                                                                     references     "cmn".office(oid)
);

/*
Category of oranization such as Government,Autonomous,private etc
oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
status                         : 
level_order                    : 
sort_order                     : 
parent_oid                     : 
office_origin_unit_oid         : 
*/
create table                   "cmn".office_origin_unit_post
(
oid                            varchar(128)                                                not null,
name_en                        varchar(128)                                                not null,
name_bn                        varchar(256)                                                not null,
status                         varchar(32)                                                 not null,
level_order                    numeric(5,0)                                                not null,
sort_order                     numeric(5,0)                                                not null,
parent_oid                     varchar(128),
office_origin_unit_oid         varchar(128),
constraint                     pk_office_origin_unit_post                                            primary key    (oid),
constraint                     ck_status_office_origin_unit_post                                     check          (status = 'Active' or status = 'Inactive'),
constraint                     fk_office_origin_unit_oid_office_origin_unit_post                     foreign key    (office_origin_unit_oid)
                                                                                                     references     "cmn".office_origin_unit(oid)
);


