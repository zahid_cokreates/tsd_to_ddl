/*

oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
sort_order                     : 
is_deleted                     : 
created_by                     : 
created_on                     : 
updated_by                     : 
updated_on                     : 
*/
create table                   "cmn".post
(
oid                            varchar(128)                                                not null,
name_en                        varchar(128)                                                not null,
name_bn                        varchar(256)                                                not null,
sort_order                     numeric(5,0)                                                not null,
is_deleted                     varchar(32)                                                 not null       default 'No',
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
constraint                     pk_post                                                               primary key    (oid)
);


