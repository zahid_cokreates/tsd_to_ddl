/*
Item entitlement against designation  in fiscal year
oid                            : Surrogate primary key
name_en                        : -
name_bn                        : -
description                    : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
*/
create table                   "cmn".entitlement_type
(
oid                            varchar(128)                                                not null,
name_en                        varchar(256),
name_bn                        varchar(256),
description                    text,
created_by                     varchar(128),
created_on                     timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
constraint                     pk_entitlement_type                                                   primary key    (oid)
);

/*
-
oid                            : -
quantity                       : -
started_from                   : -
is_deleted                     : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
entitlement_type_oid           : -
office_oid                     : -
post_oid                       : -
item_group_oid                 : -
uom_oid                        : -
*/
create table                   "cmn".entitlement
(
oid                            varchar(128)                                                not null,
quantity                       numeric(20,2),
started_from                   varchar(128),
is_deleted                     varchar(32),
created_by                     varchar(128)                                                               default 'System',
created_on                     timestamp                                                                  default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
entitlement_type_oid           varchar(128),
office_oid                     varchar(128),
post_oid                       varchar(128),
item_group_oid                 varchar(128),
uom_oid                        varchar(128),
constraint                     pk_entitlement                                                        primary key    (oid),
constraint                     ck_is_deleted_entitlement                                             check          (is_deleted = 'Yes' or is_deleted = 'No'),
constraint                     fk_entitlement_type_oid_entitlement                                   foreign key    (entitlement_type_oid)
                                                                                                     references     "cmn".entitlement_type(oid),
constraint                     fk_office_oid_entitlement                                             foreign key    (office_oid)
                                                                                                     references     "cmn".office(oid),
constraint                     fk_post_oid_entitlement                                               foreign key    (post_oid)
                                                                                                     references     "cmn".post(oid),
constraint                     fk_item_group_oid_entitlement                                         foreign key    (item_group_oid)
                                                                                                     references     "cmn".item_group(oid),
constraint                     fk_uom_oid_entitlement                                                foreign key    (uom_oid)
                                                                                                     references     "cmn".uom(oid)
);


