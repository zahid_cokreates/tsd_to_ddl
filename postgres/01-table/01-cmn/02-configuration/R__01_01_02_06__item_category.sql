/*
Category of item group
oid                            : Surrogate primary key
name_en                        : Name of the category
name_bn                        : Bangla name of the category
status                         : Status of the category
description                    : Description of the category
created_by                     : System will generate created person ID
created_on                     : System will generate created time
updated_by                     : System will generate updated person ID (it's not mandatory)
updated_on                     : System will generate updated time
office_oid                     : System will generate office Id
is_deleted                     : Flag to perform soft delete
parent_category_oid            : Foreign Key
*/
create table                   "cmn".item_category
(
oid                            varchar(128)                                                not null,
name_en                        varchar(256)                                                not null,
name_bn                        varchar(256)                                                not null,
status                         varchar(32)                                                 not null       default 'Active',
description                    varchar(256),
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
office_oid                     varchar(128)                                                not null,
is_deleted                     varchar(32)                                                 not null       default 'No',
parent_category_oid            varchar(128),
constraint                     pk_item_category                                                      primary key    (oid),
constraint                     fk_parent_category_oid_item_category                                  foreign key    (parent_category_oid)
                                                                                                     references     "cmn".item_category(oid)
);


