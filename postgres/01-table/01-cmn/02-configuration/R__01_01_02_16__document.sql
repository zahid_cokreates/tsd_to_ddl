/*
Type of document
oid                            : Surrogate primary key
document_type_code             : System generated unique code
document_type_name             : Represent the name of document type
*/
create table                   "cmn".document_type
(
oid                            varchar(128)                                                not null,
document_type_code             varchar(256)                                                not null,
document_type_name             varchar(256)                                                not null,
constraint                     pk_document_type                                                      primary key    (oid)
);

/*
document details
oid                            : Surrogate primary key
document_code                  : System generated unique code
document_name                  : Represent name of document
document_type_oid              : Represent type of document
category                       : Category of document
path                           : Represent document path
description                    : Short description about document
*/
create table                   "cmn".document
(
oid                            varchar(128)                                                not null,
document_code                  varchar(256)                                                not null,
document_name                  varchar(256)                                                not null,
document_type_oid              varchar(128)                                                not null,
category                       varchar(256)                                                not null,
path                           varchar(1028)                                               not null,
description                    text,
constraint                     pk_document                                                           primary key    (oid),
constraint                     fk_document_type_oid_document                                         foreign key    (document_type_oid)
                                                                                                     references     "cmn".document_type(oid)
);


