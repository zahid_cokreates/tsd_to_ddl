/*
store master register
oid                            : Surrogate primary key 
store_code                     : System generated unique code
name_en                        : store name
name_bn                        : store name
location                       : Location of the store
contact_number                 : Contact number of the store
description                    : Other infotrmation added here
status                         : Check availivility 
opening_date                   : Starting date of store
is_deleted                     : Reference to primary key from employee_info table
created_by                     : Contact number of the store
created_on                     : Other infotrmation added here
updated_by                     : Check availivility 
updated_on                     : Starting date of store
office_oid                     : Foreign  key
store_in_charge_oid            : Foreign  key
store_keeper_oid               : Foreign  key
owned_by_oid                   : Reference to primary key from employee_info table
*/
create table                   "cmn".store
(
oid                            varchar(128)                                                not null,
store_code                     varchar(128),
name_en                        varchar(256),
name_bn                        varchar(256),
location                       text,
contact_number                 varchar(256),
description                    text,
status                         varchar(32),
opening_date                   timestamp,
is_deleted                     varchar(32),
created_by                     varchar(128)                                                               default 'System',
created_on                     timestamp                                                                  default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
office_oid                     varchar(128),
store_in_charge_oid            varchar(128),
store_keeper_oid               varchar(128),
owned_by_oid                   varchar(128),
constraint                     pk_store                                                              primary key    (oid),
constraint                     ck_status_store                                                       check          (status = 'Yes' or status = 'No'),
constraint                     ck_is_deleted_store                                                   check          (is_deleted = 'Yes' or is_deleted = 'No'),
constraint                     fk_office_oid_store                                                   foreign key    (office_oid)
                                                                                                     references     "cmn".office(oid)
);

/*
-
oid                            : Surrogate primary key 
name_en                        : Check availivility 
name_bn                        : Starting date of store
is_deleted                     : Reference to primary key from employee_info table
created_by                     : Contact number of the store
created_on                     : Other infotrmation added here
updated_by                     : Check availivility 
updated_on                     : Starting date of store
*/
create table                   "cmn".store_designation
(
oid                            varchar(128)                                                not null,
name_en                        varchar(256),
name_bn                        varchar(256),
is_deleted                     varchar(32),
created_by                     varchar(128)                                                               default 'System',
created_on                     timestamp                                                                  default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
constraint                     pk_store_designation                                                  primary key    (oid),
constraint                     ck_is_deleted_store_designation                                       check          (is_deleted = 'Yes' or is_deleted = 'No')
);

/*
-
oid                            : Surrogate primary key 
is_deleted                     : Check availivility 
created_by                     : Starting date of store
created_on                     : Reference to primary key from employee_info table
updated_by                     : Contact number of the store
updated_on                     : Other infotrmation added here
store_oid                      : Foreign key
item_group_oid                 : Foreign key
*/
create table                   "cmn".store_item_setup_mapping
(
oid                            varchar(128)                                                not null,
is_deleted                     varchar(32),
created_by                     varchar(128)                                                               default 'System',
created_on                     timestamp                                                                  default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
store_oid                      varchar(128),
item_group_oid                 varchar(128),
constraint                     pk_store_item_setup_mapping                                           primary key    (oid),
constraint                     ck_is_deleted_store_item_setup_mapping                                check          (is_deleted = 'Yes' or is_deleted = 'No'),
constraint                     fk_store_oid_store_item_setup_mapping                                 foreign key    (store_oid)
                                                                                                     references     "cmn".store(oid),
constraint                     fk_item_group_oid_store_item_setup_mapping                            foreign key    (item_group_oid)
                                                                                                     references     "cmn".item_group(oid)
);

/*
-
oid                            : Surrogate primary key 
active_date                    : Contact number of the store
is_deleted                     : Other infotrmation added here
created_by                     : Check availivility 
created_on                     : Starting date of store
updated_by                     : Reference to primary key from employee_info table
updated_on                     : Contact number of the store
store_oid                      : Other infotrmation added here
store_designation_oid          : Check availivility 
employee_oid                   : Starting date of store
*/
create table                   "cmn".store_employee
(
oid                            varchar(128)                                                not null,
active_date                    timestamp,
is_deleted                     varchar(32),
created_by                     varchar(128)                                                               default 'System',
created_on                     timestamp                                                                  default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
store_oid                      varchar(128),
store_designation_oid          varchar(128),
employee_oid                   varchar(128),
constraint                     pk_store_employee                                                     primary key    (oid),
constraint                     ck_is_deleted_store_employee                                          check          (is_deleted = 'Yes' or is_deleted = 'No'),
constraint                     fk_store_oid_store_employee                                           foreign key    (store_oid)
                                                                                                     references     "cmn".store(oid),
constraint                     fk_store_designation_oid_store_employee                               foreign key    (store_designation_oid)
                                                                                                     references     "cmn".store_designation(oid)
);


