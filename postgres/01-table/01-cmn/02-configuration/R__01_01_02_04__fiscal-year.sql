/*
Used for  accounting and budget purposes.
oid                            : Surrogate primary key
name_en                        : English name of every fiscal year
name_bn                        : Bangla name of every fiscal year
status                         : Status for every fiscal year
start_date                     : Year will start from
end_date                       : Year will end at
*/
create table                   "cmn".fiscal_year
(
oid                            varchar(128)                                                not null,
name_en                        varchar(128)                                                not null,
name_bn                        varchar(128)                                                not null,
status                         varchar(128)                                                not null,
start_date                     date                                                        not null,
end_date                       date                                                        not null,
constraint                     pk_fiscal_year                                                        primary key    (oid),
constraint                     ck_status_fiscal_year                                                 check          (status = 'Previous' or status = 'Current' or status = 'Next' or status = 'Upcoming')
);


