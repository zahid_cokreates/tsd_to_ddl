/*

oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
item_code                      : 
item_status                    : 
item_group_oid                 : 
created_by                     : 
created_on                     : 
updated_by                     : 
updated_on                     : 
office_oid                     : 
is_deleted                     : 
*/
create table                   "cmn".item
(
oid                            varchar(128)                                                not null,
name_en                        varchar(128)                                                not null,
name_bn                        varchar(128)                                                not null,
item_code                      varchar(32)                                                 not null,
item_status                    varchar(32)                                                 not null       default 'Active',
item_group_oid                 varchar(128)                                                not null,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
office_oid                     varchar(128)                                                not null,
is_deleted                     varchar(32)                                                 not null       default 'No',
constraint                     pk_item                                                               primary key    (oid)
);

/*

oid                            : Surrogate primary key
value_en                       : 
value_bn                       : 
item_oid                       : 
item_group_feature_mapping_oid : 
created_by                     : 
created_on                     : 
updated_by                     : 
updated_on                     : 
office_oid                     : 
is_deleted                     : 
*/
create table                   "cmn".item_feature
(
oid                            varchar(128)                                                not null,
value_en                       varchar(256),
value_bn                       varchar(256),
item_oid                       varchar(128),
item_group_feature_mapping_oid varchar(128),
created_by                     varchar(128)                                                               default 'System',
created_on                     timestamp                                                                  default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
office_oid                     varchar(128),
is_deleted                     varchar(32)                                                                default 'No',
constraint                     pk_item_feature                                                       primary key    (oid)
);


