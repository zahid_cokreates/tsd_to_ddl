/*
vendor master register
oid                            : Surrogate primary key
code                           : System generated unique code
name_en                        : vendor name
name_bn                        : Reference to primary key from ExternalpartyType table
contact_address                : Address of vendor
contact_no                     : vendor contact person number
email                          : Email of the vendor
billing_address                : Billing address of vendor
description                    : vendor info
status                         : vendor Status
created_by                     : Time of creation
created_on                     : Created by
updated_by                     : Time of updation
updated_on                     : Updated by
*/
create table                   "cmn".vendor
(
oid                            varchar(128)                                                not null,
code                           varchar(128)                                                not null,
name_en                        varchar(256)                                                not null,
name_bn                        varchar(256)                                                not null,
contact_address                varchar(512)                                                not null,
contact_no                     varchar(256)                                                not null,
email                          varchar(256)                                                not null,
billing_address                varchar(256)                                                not null,
description                    text,
status                         varchar(32)                                                 not null,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
constraint                     pk_vendor                                                             primary key    (oid),
constraint                     ck_status_vendor                                                      check          (status = 'Available' or status = 'Not available')
);


