/*
Group of item
oid                            : Surrogate primary key
item_group_code                : Code of the item group
name_en                        : Name of the category
name_bn                        : Bangla name of the category
uom_oid                        : Foreign Key
traditional                    : 
maintained_as                  : 
status                         : 
hs_code                        : 
item_category_oid              : 
is_consumable                  : 
created_by                     : 
created_on                     : 
updated_by                     : 
updated_on                     : 
office_oid                     : 
is_deleted                     : 
*/
create table                   "cmn".item_group
(
oid                            varchar(128)                                                not null,
item_group_code                varchar(32)                                                 not null,
name_en                        varchar(256)                                                not null,
name_bn                        varchar(256)                                                not null,
uom_oid                        varchar(128)                                                not null,
traditional                    varchar(32)                                                 not null       default 'No',
maintained_as                  varchar(32)                                                 not null       default 'FIFO',
status                         varchar(32)                                                 not null       default 'Active',
hs_code                        varchar(256)                                                               default '',
item_category_oid              varchar(128)                                                not null,
is_consumable                  varchar(128),
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
office_oid                     varchar(128)                                                not null,
is_deleted                     varchar(32)                                                 not null       default 'No',
constraint                     pk_item_group                                                         primary key    (oid),
constraint                     fk_uom_oid_item_group                                                 foreign key    (uom_oid)
                                                                                                     references     "cmn".uom(oid),
constraint                     fk_item_category_oid_item_group                                       foreign key    (item_category_oid)
                                                                                                     references     "cmn".item_category(oid),
constraint                     ck_is_consumable_item_group                                           check          (is_consumable = 'yes' or is_consumable = 'no')
);

/*

oid                            : Surrogate primary key
name_en                        : 
name_bn                        : 
created_by                     : 
created_on                     : 
updated_by                     : 
updated_on                     : 
office_oid                     : 
is_deleted                     : 
*/
create table                   "cmn".item_group_feature
(
oid                            varchar(128)                                                not null,
name_en                        varchar(256)                                                not null,
name_bn                        varchar(256)                                                not null,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
office_oid                     varchar(128)                                                not null,
is_deleted                     varchar(32)                                                 not null       default 'No',
constraint                     pk_item_group_feature                                                 primary key    (oid)
);

/*

oid                            : Surrogate primary key
is_mandatory                   : 
item_group_oid                 : 
item_group_feature_oid         : 
created_by                     : 
created_on                     : 
updated_by                     : 
updated_on                     : 
office_oid                     : 
is_deleted                     : 
*/
create table                   "cmn".item_group_feature_mapping
(
oid                            varchar(128)                                                not null,
is_mandatory                   varchar(32)                                                 not null       default 'No',
item_group_oid                 varchar(128)                                                not null,
item_group_feature_oid         varchar(128)                                                not null,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
office_oid                     varchar(128)                                                not null,
is_deleted                     varchar(32)                                                 not null       default 'No',
constraint                     pk_item_group_feature_mapping                                         primary key    (oid)
);


