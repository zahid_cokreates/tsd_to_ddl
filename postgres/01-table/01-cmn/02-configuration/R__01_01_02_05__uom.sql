/*
Unit of measurement category
oid                            : Surrogate primary key
name_en                        : Name of the uom category
name_bn                        : Bangla name of the uom category
created_by                     : System will generate created person ID
created_on                     : System will generate created time
updated_by                     : System will generate updated person ID (it's not mandatory)
updated_on                     : System will generate updated time
status                         : Status of uom category
*/
create table                   "cmn".uom_category
(
oid                            varchar(128)                                                not null,
name_en                        varchar(256)                                                not null,
name_bn                        varchar(256)                                                not null,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
status                         varchar(32)                                                 not null,
constraint                     pk_uom_category                                                       primary key    (oid)
);

/*
Unit of measurement
oid                            : Surrogate primary key
name_en                        : Name of the uom
name_bn                        : Bangla name of the uom
category_oid                   : Foreign Key
created_by                     : System will generate created person ID
created_on                     : System will generate created time
updated_by                     : System will generate updated person ID (it's not mandatory)
updated_on                     : System will generate updated time
status                         : Status of uom
*/
create table                   "cmn".uom
(
oid                            varchar(128)                                                not null,
name_en                        varchar(256)                                                not null,
name_bn                        varchar(256)                                                not null,
category_oid                   varchar(128)                                                not null,
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
status                         varchar(32)                                                 not null,
constraint                     pk_uom                                                                primary key    (oid),
constraint                     fk_category_oid_uom                                                   foreign key    (category_oid)
                                                                                                     references     "cmn".uom_category(oid)
);

/*
Conversion factor on unit
oid                            : Surrogate primary key
uom_oid_from                   : Foreign Key
uom_oid_to                     : Foreign Key
conversion_factor              : Conversion factor required for conversion
decimal_scale                  : No fo decimal presicion to be used
rounding_mode                  : Rounding policy for the conversion
created_by                     : System will generate created person ID
created_on                     : System will generate created time
updated_by                     : System will generate updated person ID (it's not mandatory)
updated_on                     : System will generate updated time
status                         : Status of uom conversion
*/
create table                   "cmn".uom_conversion
(
oid                            varchar(128)                                                not null,
uom_oid_from                   varchar(256)                                                not null,
uom_oid_to                     varchar(256)                                                not null,
conversion_factor              numeric(20,6)                                               not null,
decimal_scale                  numeric(20,6),
rounding_mode                  varchar(32),
created_by                     varchar(128)                                                not null       default 'System',
created_on                     timestamp                                                   not null       default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
status                         varchar(32)                                                 not null,
constraint                     pk_uom_conversion                                                     primary key    (oid),
constraint                     fk_uom_oid_from_uom_conversion                                        foreign key    (uom_oid_from)
                                                                                                     references     "cmn".uom(oid),
constraint                     fk_uom_oid_to_uom_conversion                                          foreign key    (uom_oid_to)
                                                                                                     references     "cmn".uom(oid)
);


