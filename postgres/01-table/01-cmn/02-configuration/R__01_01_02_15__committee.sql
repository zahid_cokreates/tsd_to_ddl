/*
committee type info
oid                            : Surrogate primary key
committee_type_code            : System generated unique code
committee_type                 : Name of committee type
approved_by                    : Reference to primary key from employee_info table
*/
create table                   "cmn".committee_type_setup
(
oid                            varchar(128)                                                not null,
committee_type_code            varchar(128)                                                not null,
committee_type                 varchar(256)                                                not null,
approved_by                    varchar(128)                                                not null,
constraint                     pk_committee_type_setup                                               primary key    (oid)
);

/*
committee info
oid                            : Surrogate primary key
committee_code                 : System generated unique code
committee_type_oid             : Reference to primary key from committeeType  table
committee_name                 : committee name
max_member                     : Maximum member limit
min_member                     : Maximum member limit
forming_date                   : Establish date of committee
purpose                        : Purpose behind committee establishment
comments                       : Any comments if required
status                         : Status of committee
organization_info_oid          : Reference to primary key from organization table
*/
create table                   "cmn".committee_setup
(
oid                            varchar(128)                                                not null,
committee_code                 varchar(128)                                                not null,
committee_type_oid             varchar(128)                                                not null,
committee_name                 varchar(256)                                                not null,
max_member                     varchar(3)                                                  not null,
min_member                     varchar(3)                                                  not null,
forming_date                   date                                                        not null,
purpose                        varchar(256)                                                not null,
comments                       text                                                        not null,
status                         varchar(32)                                                 not null,
organization_info_oid          varchar(128)                                                not null,
constraint                     pk_committee_setup                                                    primary key    (oid),
constraint                     fk_committee_type_oid_committee_setup                                 foreign key    (committee_type_oid)
                                                                                                     references     "cmn".committee_type_setup(oid),
constraint                     ck_status_committee_setup                                             check          (status = 'Active' or status = 'In-Active')
);

/*
Committe detail
oid                            : Surrogate primary key
committee_setup_line_code      : System generated unique code
committee_setup_oid            : Reference to primary key from committee table
employee_info_oid              : Reference to primary key from employee_info table
member_role                    : employee_info role name
orgaization_oid                : Reference to primary key from organization table
*/
create table                   "cmn".committee_setup_line
(
oid                            varchar(128)                                                not null,
committee_setup_line_code      varchar(128)                                                not null,
committee_setup_oid            varchar(128)                                                not null,
employee_info_oid              varchar(128)                                                not null,
member_role                    varchar(256)                                                not null,
orgaization_oid                varchar(128)                                                not null,
constraint                     pk_committee_setup_line                                               primary key    (oid),
constraint                     fk_committee_setup_oid_committee_setup_line                           foreign key    (committee_setup_oid)
                                                                                                     references     "cmn".committee_setup(oid)
);


