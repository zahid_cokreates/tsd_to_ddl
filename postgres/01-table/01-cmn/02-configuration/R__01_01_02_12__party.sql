/*
Third party master register
oid                            : Surrogate primary key 
party_type_code                : System generated unique code
party_type_name                : Party name
*/
create table                   "cmn".party_type
(
oid                            varchar(128)                                                not null,
party_type_code                varchar(128)                                                not null,
party_type_name                varchar(256)                                                not null,
constraint                     pk_party_type                                                         primary key    (oid)
);


