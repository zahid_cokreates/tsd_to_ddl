/*
-
oid                            : -
job_name                       : -
table_name                     : -
is_deleted                     : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
*/
create table                   "cmn".review_job_type
(
oid                            varchar(128)                                                not null,
job_name                       varchar(256),
table_name                     varchar(256),
is_deleted                     varchar(32),
created_by                     varchar(128)                                                               default 'System',
created_on                     timestamp                                                                  default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
constraint                     pk_review_job_type                                                    primary key    (oid),
constraint                     ck_is_deleted_review_job_type                                         check          (is_deleted = 'Yes' or is_deleted = 'No')
);

/*
-
oid                            : -
name_en                        : -
name_bn                        : -
priority                       : -
is_deleted                     : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
*/
create table                   "cmn".review_action_type
(
oid                            varchar(128)                                                not null,
name_en                        varchar(256),
name_bn                        varchar(256),
priority                       numeric(6),
is_deleted                     varchar(32),
created_by                     varchar(128)                                                               default 'System',
created_on                     timestamp                                                                  default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
constraint                     pk_review_action_type                                                 primary key    (oid),
constraint                     ck_is_deleted_review_action_type                                      check          (is_deleted = 'Yes' or is_deleted = 'No')
);

/*
-
oid                            : -
request_time                   : -
review_time                    : -
return_comment                 : -
is_complete                    : -
is_rejected                    : -
is_disabled                    : -
is_deleted                     : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
source_tbl                     : -
source_row_oid                 : -
requested_by_oid               : -
assigned_to_oid                : -
review_action_type_oid         : -
forwarded_oid                  : -
*/
create table                   "cmn".review_action
(
oid                            varchar(128)                                                not null,
request_time                   timestamp,
review_time                    timestamp,
return_comment                 text,
is_complete                    varchar(32),
is_rejected                    varchar(32),
is_disabled                    varchar(32),
is_deleted                     varchar(32),
created_by                     varchar(128)                                                               default 'System',
created_on                     timestamp                                                                  default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
source_tbl                     varchar(128),
source_row_oid                 varchar(128),
requested_by_oid               varchar(128),
assigned_to_oid                varchar(128),
review_action_type_oid         varchar(128),
forwarded_oid                  varchar(128),
constraint                     pk_review_action                                                      primary key    (oid),
constraint                     ck_is_complete_review_action                                          check          (is_complete = 'Yes' or is_complete = 'No'),
constraint                     ck_is_rejected_review_action                                          check          (is_rejected = 'Yes' or is_rejected = 'No'),
constraint                     ck_is_disabled_review_action                                          check          (is_disabled = 'Yes' or is_disabled = 'No'),
constraint                     ck_is_deleted_review_action                                           check          (is_deleted = 'Yes' or is_deleted = 'No'),
constraint                     fk_review_action_type_oid_review_action                               foreign key    (review_action_type_oid)
                                                                                                     references     "cmn".review_action_type(oid)
);

/*
-
oid                            : -
audited_qty                    : -
status                         : -
remarks                        : -
is_deleted                     : -
created_by                     : -
created_on                     : -
updated_by                     : -
updated_on                     : -
*/
create table                   "cmn".review_action_detail
(
oid                            varchar(128)                                                not null,
audited_qty                    numeric(20),
status                         varchar(256),
remarks                        text,
is_deleted                     varchar(128),
created_by                     varchar(128)                                                               default 'System',
created_on                     timestamp                                                                  default current_timestamp,
updated_by                     varchar(128),
updated_on                     timestamp,
constraint                     pk_review_action_detail                                               primary key    (oid)
);


