/*
Donor master register
oid                            : Surrogate primary key
donor_code                     : System generated unique code
donor_name                     : Donor name
party_type_oid                 : Reference to primary key from ExternalpartyType table
address                        : Address of donor
contact_no                     : Donor contact person number
organization                   : Donor organization name
description                    : Donor info
status                         : Donor Status
*/
create table                   "cmn".donor_info
(
oid                            varchar(128)                                                not null,
donor_code                     varchar(128)                                                not null,
donor_name                     varchar(256)                                                not null,
party_type_oid                 varchar(128)                                                not null,
address                        varchar(512)                                                not null,
contact_no                     varchar(256)                                                not null,
organization                   varchar(256)                                                not null,
description                    text,
status                         varchar(32)                                                 not null,
constraint                     pk_donor_info                                                         primary key    (oid),
constraint                     fk_party_type_oid_donor_info                                          foreign key    (party_type_oid)
                                                                                                     references     "cmn".party_type(oid),
constraint                     ck_status_donor_info                                                  check          (status = 'Available' or status = 'Not available')
);

/*
Donee master register
oid                            : Surrogate primary key
donee_code                     : System generated unique code
donee_name                     : Donee name
party_type_oid                 : Reference to primary key from ExternalpartyType table
address                        : Address of donee
contact_no                     : Donee contact person number
organization                   : Donee organization name
description                    : Donee info
status                         : Availability of donee
*/
create table                   "cmn".donee_info
(
oid                            varchar(128)                                                not null,
donee_code                     varchar(128)                                                not null,
donee_name                     varchar(256)                                                not null,
party_type_oid                 varchar(128)                                                not null,
address                        varchar(512),
contact_no                     varchar(256)                                                not null,
organization                   varchar(256)                                                not null,
description                    text,
status                         varchar(32)                                                 not null,
constraint                     pk_donee_info                                                         primary key    (oid),
constraint                     fk_party_type_oid_donee_info                                          foreign key    (party_type_oid)
                                                                                                     references     "cmn".party_type(oid),
constraint                     ck_status_donee_info                                                  check          (status = 'Available' or status = 'Not available')
);


