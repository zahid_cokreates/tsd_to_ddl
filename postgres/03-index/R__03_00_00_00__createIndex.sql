create unique index index_name_en_office_unit_category on "cmn".office_unit_category(name_en);

create unique index index_name_bn_office_unit_category on "cmn".office_unit_category(name_bn);

create unique index index_oid_uom_category on "cmn".uom_category(oid);

create unique index index_oid_uom on "cmn".uom(oid);

create unique index index_oid_uom_conversion on "cmn".uom_conversion(oid);

create unique index index_oid_item_category on "cmn".item_category(oid);

create unique index index_item_group_code_item_group on "cmn".item_group(item_group_code);

create unique index index_oid_item_group_feature on "cmn".item_group_feature(oid);

create unique index index_name_en_item_group_feature on "cmn".item_group_feature(name_en);

create unique index index_name_bn_item_group_feature on "cmn".item_group_feature(name_bn);

create unique index index_oid_item_group_feature_mapping on "cmn".item_group_feature_mapping(oid);

create unique index index_oid_item on "cmn".item(oid);

create unique index index_item_code_item on "cmn".item(item_code);

create unique index index_oid_item_feature on "cmn".item_feature(oid);

create unique index index_party_type_code_party_type on "cmn".party_type(party_type_code);

create unique index index_code_vendor on "cmn".vendor(code);

create unique index index_name_en_vendor on "cmn".vendor(name_en);

create unique index index_name_bn_vendor on "cmn".vendor(name_bn);

create unique index index_donor_code_donor_info on "cmn".donor_info(donor_code);

create unique index index_donee_code_donee_info on "cmn".donee_info(donee_code);

create unique index index_committee_type_code_committee_type_setup on "cmn".committee_type_setup(committee_type_code);

create unique index index_committee_code_committee_setup on "cmn".committee_setup(committee_code);

create unique index index_committee_setup_line_code_committee_setup_line on "cmn".committee_setup_line(committee_setup_line_code);

create unique index index_oid_document_type on "cmn".document_type(oid);

create unique index index_document_type_code_document_type on "cmn".document_type(document_type_code);

create unique index index_document_type_name_document_type on "cmn".document_type(document_type_name);

create unique index index_oid_document on "cmn".document(oid);

create unique index index_document_code_document on "cmn".document(document_code);


