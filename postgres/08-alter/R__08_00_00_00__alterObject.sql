UPDATE sec.grp_role
SET menu_json = '[{"name":"Dashboard","url":"dashboard","icon":"fa fa-building","children":[]},{"name":"Asset","url":"asset","icon":"fa fa-building","children":[{"name":"commissioning-acquisition","url":"commissioning-acquisition","icon":"fa fa-angle-down","children":[{"name":"temp-item","url":"temp-item","icon":"fa fa-angle-down","children":[{"name":"temp-item-add","url":"temp-item-add","icon":"fa fa-plus","children":[{"name":"temp-item-data-entry-operator","url":"temp-item-data-entry-operator","icon":"fa fa-list-ul"}]}]}]}]}]'
WHERE oid = 'AST.DATA-ENTRY-OPERATOR';


UPDATE sec.grp_role
SET menu_json = '[{"name":"Dashboard","url":"dashboard","icon":"fa fa-building","children":[]},{"name":"Asset","url":"asset","icon":"fa fa-building","children":[{"name":"commissioning-acquisition","url":"commissioning-acquisition","icon":"fa fa-angle-down","children":[{"name":"temp-item","url":"temp-item","icon":"fa fa-angle-down","children":[{"name":"qc","url":"qc","icon":"fa fa-plus","children":[{"name":"temp-item-committee","url":"temp-item-committee","icon":"fa fa-list-ul"}]}]}]}]}]'
WHERE oid = 'AST.QC-COMMITTEE';



