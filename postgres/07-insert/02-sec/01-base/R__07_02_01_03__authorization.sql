-- grp_user_role
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('tanim_pe','{}','No','PRC.PE','tanim');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('mashud_pe','{}','Yes','PRC.PE','mashud');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('mashud_office','{}','Yes','PRC.Officer','mashud');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('jakir_office','{}','Yes','PRC.Officer','jakir');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('jakir_pe','{}','Yes','PRC.PE','jakir');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('tasdiq_pe','{}','Yes','PRC.PE','tasdiq');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('kamal_office','{}','Yes','PRC.Officer','kamal');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('kamal_pe','{}','Yes','PRC.PE','kamal');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('tasdiq_office','{}','Yes','PRC.Officer','tasdiq');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('tasdiq_hope','[]','Yes','PRC.HOPE','tasdiq');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('tanim_ast_am','{}','Yes','AST.ASSET-MANAGER','tanim');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('tanim_ast_qc','{}','No','AST.QC-COMMITTEE','tanim');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('tanim_ast_aic','{}','No','AST.ASSET-IN-CHARGE','tanim');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('mashud_ast_aic','{}','Yes','AST.ASSET-IN-CHARGE','mashud');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('mashud_ast_te','{}','No','AST.TAGGING-ENTITY','mashud');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('shaon_ast_deo','{}','Yes','AST.DATA-ENTRY-OPERATOR','shaon');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('shaon_ast_qc','{}','No','AST.QC-COMMITTEE','shaon');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('shiropa_ast_aic','{}','Yes','AST.ASSET-IN-CHARGE','shiropa');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('shiropa_ast_te','{}','No','AST.TAGGING-ENTITY','shiropa');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('kabir_ast_te','{}','Yes','AST.TAGGING-ENTITY','kabir');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('kabir_ast_deo','{}','No','AST.DATA-ENTRY-OPERATOR','kabir');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('admin_office','{}','Yes','PRC.Officer','admin');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('admin_pe','{}','Yes','PRC.PE','admin');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('shaon_ast_em','{}','No','AST.EMPLOYEE','shaon');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('alim_ast_sm','{}','Yes','AST.STORE-MANAGER','alim');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('taief_ast_sm','{}','No','AST.STORE-MANAGER','taief');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('taief_ast_deo','{}','Yes','AST.DATA-ENTRY-OPERATOR','taief');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('taief_ast_qc','{}','No','AST.QC-COMMITTEE','taief');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('taief_ast_aic','{}','No','AST.ASSET-IN-CHARGE','taief');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('taief_ast_am','{}','No','AST.ASSET-MANAGER','taief');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('shiropa_ast_sm','{}','No','AST.STORE-MANAGER','shiropa');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('shaon_ast_et','{}','No','AST.EXPERT-TEAM','shaon');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('sujit_pe','{}','Yes','PRC.PE','sujit');
insert into "sec".grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) values ('nusrat_office','{}','Yes','PRC.Officer','nusrat');
commit;

-- grp_role_permission
insert into "sec".grp_role_permission (oid, component_oid, grp_role_oid) values ('1','8','AST.DATA-ENTRY-OPERATOR');
insert into "sec".grp_role_permission (oid, component_oid, grp_role_oid) values ('2','9','AST.DATA-ENTRY-OPERATOR');
commit;


