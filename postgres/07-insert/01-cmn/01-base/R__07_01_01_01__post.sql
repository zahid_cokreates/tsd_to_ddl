-- post
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('1','Secretary','সচিব','0');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('2','PS to Secretary','সচিবের ব্যাক্তিগত সহকারি','1');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('3','Personal Officer (P.O)','ব্যক্তিগত কর্মকর্তা','2');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('4','Additional Secretary','অতিরিক্ত সচিব','3');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('5','Office Support Staff','অফিস সহকারী স্টাফ','4');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('6','Joint Secretary','যুুগ্ম সচিব','5');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('7','Computer Operator','কর্মকর্তা অপারেটর','6');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('8','Deputy Secretary','ভারপ্রাপ্ত সচিব','7');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('9','Deputy Chief','ভারপ্রাপ্ত প্রধান','8');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('10','Senior Assistant Secretary /Assistant Secretary','সিনিয়র সহকারি সচিব / সহকারি সচিব','9');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('11','Administrative Officer (A.O)','প্রশাসনিক কর্মকর্তা','10');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('12','Office Asst-cum-Computer Operator','অফিস-সহকারী-কাম-কম্পিউটার অপারেটর','11');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('13','Senior Assistant Chief','সিনিয়র সহকারি প্রধান / সহকারি প্রধান','12');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('14','Accounts Officer','হিসাবরক্ষণ কর্মকর্তা','13');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('15','Assistant Accounts Officer (Former Accountant)','সহকারী হিসাবরক্ষণ কর্মকর্তা','14');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('16','Executive Director','নির্বাহী পরিচালক','15');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('17','Member','সদস্য','16');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('18','Director','পরিচালক','17');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('19','Deputy Director','উপ পরিচালক','18');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('20','Senior System Analyst','সিনিয়র সিস্টেমস এনালিষ্ট','19');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('21','Senior Programmer','সিনিয়র প্রোগ্রামার','20');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('22','Specialist','বিশেষজ্ঞ','21');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('23','Manager','ম্যানেজার','22');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('24','Assistant Director','সহকারী পরিচালক','23');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('25','Analyst','এনালিষ্ট','24');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('26','Architect','আর্কিটেক্ট','25');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('27','Administrator','এডমিনিস্ট্রেটর','26');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('28','Publication Officer','লাইব্রেরিয়ান কাম পাবলিকেশনস অফিসার','27');
insert into "cmn".post (oid, name_en, name_bn, sort_order) values ('29','Hardware Engineer','হার্ডওয়্যার ইঞ্জিনিয়ার','28');
commit;


