-- office_layer
insert into "cmn".office_layer (oid, name_en, name_bn, sort_order) values ('1','Ministry','মন্ত্রনালয়ের কার্যালয়','0');
insert into "cmn".office_layer (oid, name_en, name_bn, sort_order) values ('2','Division','বিভাগ','1');
insert into "cmn".office_layer (oid, name_en, name_bn, sort_order) values ('3','Directorate','অধিদপ্তর/পরিদপ্তর','2');
insert into "cmn".office_layer (oid, name_en, name_bn, sort_order) values ('4','Divisional Office','বিভাগীয় কার্যালয়','3');
insert into "cmn".office_layer (oid, name_en, name_bn, sort_order) values ('5','Regional Office','আঞ্চলিক কার্যালয়','4');
insert into "cmn".office_layer (oid, name_en, name_bn, sort_order) values ('6','District Office','জেলা পর্যায়ের কার্যালয়','5');
insert into "cmn".office_layer (oid, name_en, name_bn, sort_order) values ('7','Upazilla Office','উপজেলা পর্যায়ের কার্যালয়','6');
insert into "cmn".office_layer (oid, name_en, name_bn, sort_order) values ('8','Cell','সেল','7');
insert into "cmn".office_layer (oid, name_en, name_bn, sort_order) values ('9','Unit','ইউনিট','8');
insert into "cmn".office_layer (oid, name_en, name_bn, sort_order) values ('10','Institute','ইনস্টিটিউট','9');
insert into "cmn".office_layer (oid, name_en, name_bn, sort_order) values ('11','Academy/Training Institute','একাডেমি/ প্রশিক্ষণ কেন্দ্র','10');
insert into "cmn".office_layer (oid, name_en, name_bn, sort_order) values ('12','Commission','কমিশন','11');
insert into "cmn".office_layer (oid, name_en, name_bn, sort_order) values ('13','Authority','কতৃপক্ষ/অথোরিটি','12');
insert into "cmn".office_layer (oid, name_en, name_bn, sort_order) values ('14','Corporation','কর্পোরেশন','13');
insert into "cmn".office_layer (oid, name_en, name_bn, sort_order) values ('15','Council','কাউন্সিল','14');
insert into "cmn".office_layer (oid, name_en, name_bn, sort_order) values ('16','Company','কোম্পানি','15');
insert into "cmn".office_layer (oid, name_en, name_bn, sort_order) values ('17','Program','প্রকল্প/প্রোগ্রাম','16');
insert into "cmn".office_layer (oid, name_en, name_bn, sort_order) values ('18','Foundation','ফাউন্ডেশন','17');
insert into "cmn".office_layer (oid, name_en, name_bn, sort_order) values ('19','Board','বোর্ড','18');
insert into "cmn".office_layer (oid, name_en, name_bn, sort_order) values ('20','Beuro','ব্যুরো','19');
insert into "cmn".office_layer (oid, name_en, name_bn, sort_order) values ('21','Organization','সংস্থা','20');
commit;

-- office_unit_category
insert into "cmn".office_unit_category (oid, name_en, name_bn, sort_order) values ('1','Office','দপ্তর','1');
insert into "cmn".office_unit_category (oid, name_en, name_bn, sort_order) values ('2','Section','শাখা','2');
insert into "cmn".office_unit_category (oid, name_en, name_bn, sort_order) values ('3','Wing','অনুবিভাগ','3');
insert into "cmn".office_unit_category (oid, name_en, name_bn, sort_order) values ('4','Branch','অধিশাখা','4');
commit;

-- office
insert into "cmn".office (oid, name_en, name_bn, code_en, code_bn, address_en, address_bn, email, web, status, sort_order, parent_oid, office_layer_oid, ministry_oid, root_office_oid, is_root_office) values ('1','Posts and Telecommunications & ICT Division','ডাক, টেলিযোগাযোগ ও তথ্যপ্রযুক্তি মন্ত্রণালয়',null,null,null,'আইসিটি টাওয়ার, আগারগাঁও, শেরেবাংলা নগর, ঢাকা-১২০৭',null,null,'Active','0',null,'1',null,null,'No');
insert into "cmn".office (oid, name_en, name_bn, code_en, code_bn, address_en, address_bn, email, web, status, sort_order, parent_oid, office_layer_oid, ministry_oid, root_office_oid, is_root_office) values ('2','Posts and Telecommunications Division','ডাক ও টেলিযোগাযোগ বিভাগ',null,null,null,'আইসিটি টাওয়ার, আগারগাঁও, শেরেবাংলা নগর, ঢাকা-১২০৭',null,null,'Active','1','1','2','1',null,'Yes');
insert into "cmn".office (oid, name_en, name_bn, code_en, code_bn, address_en, address_bn, email, web, status, sort_order, parent_oid, office_layer_oid, ministry_oid, root_office_oid, is_root_office) values ('3','ICT Division','তথ্য ও যোগাযোগ প্রযুক্তি বিভাগ',null,null,null,'আইসিটি টাওয়ার [৫ম তলা] আগারগাঁও, শের-ই-বাংলা নগর, ঢাকা-১২০৭',null,null,'Active','2','1','2','1',null,'Yes');
insert into "cmn".office (oid, name_en, name_bn, code_en, code_bn, address_en, address_bn, email, web, status, sort_order, parent_oid, office_layer_oid, ministry_oid, root_office_oid, is_root_office) values ('4','Bangladesh Computer Council (BCC)','বাংলাদেশ কম্পিউটার কাউন্সিল (বিসিসি)',null,null,null,'আইসিটি টাওয়ার, আগারগাঁও, শেরেবাংলা নগর, ঢাকা-১২০৭',null,null,'Active','3','3','15','1','3','No');
insert into "cmn".office (oid, name_en, name_bn, code_en, code_bn, address_en, address_bn, email, web, status, sort_order, parent_oid, office_layer_oid, ministry_oid, root_office_oid, is_root_office) values ('5','Bangladesh e-Government ERP','বাংলাদেশ ই-গভর্নমেন্ট ইআরপি',null,null,null,'আইসিটি টাওয়ার, আগারগাঁও, শেরেবাংলা নগর, ঢাকা-১২০৭',null,null,'Active','4','4','17','1','3','No');
insert into "cmn".office (oid, name_en, name_bn, code_en, code_bn, address_en, address_bn, email, web, status, sort_order, parent_oid, office_layer_oid, ministry_oid, root_office_oid, is_root_office) values ('6','Ministry of Planning','পরিকল্পনা মন্ত্রণালয়',null,null,null,'শের-ই-বাংলা নগর, ঢাকা-১২০৭',null,null,'Active','0',null,'1',null,null,'No');
insert into "cmn".office (oid, name_en, name_bn, code_en, code_bn, address_en, address_bn, email, web, status, sort_order, parent_oid, office_layer_oid, ministry_oid, root_office_oid, is_root_office) values ('7','Planning Division','পরিকল্পনা বিভাগ',null,null,null,'বেগম রোকেয়া সরণী, ঢাকা',null,null,'Active','1','6','2','6',null,'Yes');
insert into "cmn".office (oid, name_en, name_bn, code_en, code_bn, address_en, address_bn, email, web, status, sort_order, parent_oid, office_layer_oid, ministry_oid, root_office_oid, is_root_office) values ('8','Statistics and Informatics Division','পরিসংখ্যান ও তথ্য ব্যবস্থাপনা বিভাগ',null,null,null,'ই২৭/এ, আগারগাঁও, ঢাকা-১২০৭',null,null,'Active','1','6','2','6',null,'Yes');
insert into "cmn".office (oid, name_en, name_bn, code_en, code_bn, address_en, address_bn, email, web, status, sort_order, parent_oid, office_layer_oid, ministry_oid, root_office_oid, is_root_office) values ('9','Implementation Monitoring & Evaluation Division','বাস্তবায়ন পরিবীক্ষণ ও মূল্যায়ন বিভাগ',null,null,null,'আইসিটি টাওয়ার, আগারগাঁও, শেরেবাংলা নগর, ঢাকা-১২০৭',null,null,'Active','1','6','2','6',null,'Yes');
insert into "cmn".office (oid, name_en, name_bn, code_en, code_bn, address_en, address_bn, email, web, status, sort_order, parent_oid, office_layer_oid, ministry_oid, root_office_oid, is_root_office) values ('10','Bangladesh Hi-Tech Park Authority','বাংলাদেশ হাই-টেক পার্ক কর্তৃপক্ষ',null,null,null,'আইসিটি টাওয়ার (১০ম তলা), ই-১৪/এক্স, আগারগাও, ঢাকা-১২০৭',null,null,'Active','3','3','13','1','3','No');
insert into "cmn".office (oid, name_en, name_bn, code_en, code_bn, address_en, address_bn, email, web, status, sort_order, parent_oid, office_layer_oid, ministry_oid, root_office_oid, is_root_office) values ('11','Bangladesh Telecommunication Regulatory Commission','বাংলাদেশ টেলিযোগাযোগ নিয়ন্ত্রণ কমিশন',null,null,null,'আইইবি ভবন (৫ম,৬ষ্ট ও ৭ম তলা), রমনা, ঢাকা-১০০০',null,null,'Active','3','2','12','1','2','No');
commit;

-- office_unit
insert into "cmn".office_unit (oid, name_en, name_bn, display_order, business_order, parent_oid, office_unit_category_oid, office_oid, ministry_oid) values ('1','Executive Branch','কার্যনির্বাহী শাখা','0','0',null,'2','4','1');
insert into "cmn".office_unit (oid, name_en, name_bn, display_order, business_order, parent_oid, office_unit_category_oid, office_oid, ministry_oid) values ('2','Systems Branch','সিস্টেমস শাখা','1','1','1','2','4','1');
insert into "cmn".office_unit (oid, name_en, name_bn, display_order, business_order, parent_oid, office_unit_category_oid, office_oid, ministry_oid) values ('3','Training Branch','প্রশিক্ষণ শাখা','1','1','1','2','4','1');
insert into "cmn".office_unit (oid, name_en, name_bn, display_order, business_order, parent_oid, office_unit_category_oid, office_oid, ministry_oid) values ('4','Administration Branch','প্রশাসন শাখা','1','1','1','2','4','1');
insert into "cmn".office_unit (oid, name_en, name_bn, display_order, business_order, parent_oid, office_unit_category_oid, office_oid, ministry_oid) values ('5','BKICT','বিকেআইসিটি','1','1','1','2','4','1');
commit;

-- office_unit_post
insert into "cmn".office_unit_post (oid, post_oid, display_order, business_order, parent_oid, office_unit_oid, phone_no) values ('1','1','0','0',null,'1','01556963236');
insert into "cmn".office_unit_post (oid, post_oid, display_order, business_order, parent_oid, office_unit_oid, phone_no) values ('2','2','1','1','1','1','01556963237');
commit;


