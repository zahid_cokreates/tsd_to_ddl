-- vendor
insert into "cmn".vendor (oid, code, name_en, name_bn, contact_address, contact_no, email, billing_address, description, status) values ('1','V-1','Abul Kasam','আবুল কাসেম','Shaymoli','16354823818','Kasam@gmail.com','Shaymoli',null,'Available');
insert into "cmn".vendor (oid, code, name_en, name_bn, contact_address, contact_no, email, billing_address, description, status) values ('2','V-2','Kamal Uddin','কামাল উদ্দিন','Karwanbazar','1635482381','kamal@gmail.com','Karwanbazar',null,'Not available');
insert into "cmn".vendor (oid, code, name_en, name_bn, contact_address, contact_no, email, billing_address, description, status) values ('3','V-3','Monjur Mahmud','মঞ্জুর মাহমুদ','Mohammadpur','16578962355','monjur@gmail.com','Mohammadpur',null,'Available');
commit;


