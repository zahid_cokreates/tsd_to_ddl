-- item_category
insert into "cmn".item_category (oid, name_en, name_bn, created_by, created_on, office_oid) values ('1','BG Press (Forms, Printing and Publishing)','বিজি প্রেস (ফরমস , মূদ্রণ ও প্রকাশনা)','ishmum','2016-06-22 19:10:25-07','143');
insert into "cmn".item_category (oid, name_en, name_bn, created_by, created_on, office_oid) values ('2','Computer Accessories','কম্পিউটার সামগ্রী','ishmum','2016-06-22 19:10:25-07','143');
insert into "cmn".item_category (oid, name_en, name_bn, created_by, created_on, office_oid) values ('3','Computer Related Accessories','কম্পিউটার আনুষঙ্গিক মালামাল','ishmum','2016-06-22 19:10:25-07','143');
insert into "cmn".item_category (oid, name_en, name_bn, created_by, created_on, office_oid) values ('4','Crockery','ক্রোকারিজ','ishmum','2016-06-22 19:10:25-07','143');
insert into "cmn".item_category (oid, name_en, name_bn, created_by, created_on, office_oid) values ('5','Furniture','ফার্নিচার','ishmum','2016-06-22 19:10:25-07','143');
insert into "cmn".item_category (oid, name_en, name_bn, created_by, created_on, office_oid) values ('6','Goods of Finished Project','সমাপ্ত প্রকল্পের মালামাল','ishmum','2016-06-22 19:10:25-07','143');
insert into "cmn".item_category (oid, name_en, name_bn, created_by, created_on, office_oid) values ('7','Lock and Key','তালাচাবি','ishmum','2016-06-22 19:10:25-07','143');
insert into "cmn".item_category (oid, name_en, name_bn, created_by, created_on, office_oid) values ('8','Office Equipment (Photocopier)','অফিস সরঞ্জাম (ফটোকপিয়ার)','ishmum','2016-06-22 19:10:25-07','143');
insert into "cmn".item_category (oid, name_en, name_bn, created_by, created_on, office_oid) values ('9','Open Market','খোলা বাজার','ishmum','2016-06-22 19:10:25-07','143');
insert into "cmn".item_category (oid, name_en, name_bn, created_by, created_on, office_oid) values ('10','Seal, Auto Seal and Nameplate','সিল, অটোসিল ও নামফলক','ishmum','2016-06-22 19:10:25-07','143');
insert into "cmn".item_category (oid, name_en, name_bn, created_by, created_on, office_oid) values ('11','Stationery','স্টেশনারি /মনোহারি','ishmum','2016-06-22 19:10:25-07','143');
insert into "cmn".item_category (oid, name_en, name_bn, created_by, created_on, office_oid) values ('12','Telephone Set','টেলিফোন সেট / স্টেনো টেলিফোনে সেট','ishmum','2016-06-22 19:10:25-07','143');
insert into "cmn".item_category (oid, name_en, name_bn, created_by, created_on, office_oid) values ('13','Electronic','ইলেকট্রনিক','shaon','2016-06-22 19:10:25-07','4');
commit;


