-- entitlement_type
insert into "cmn".entitlement_type (oid, name_en, name_bn, created_by, created_on) values ('01','Fixed','স্থায়ী','jakir','2019-09-17 3:00:00');
insert into "cmn".entitlement_type (oid, name_en, name_bn, created_by, created_on) values ('02','Unlimited','প্রয়োজন অনুসারে','jakir','2019-09-17 3:00:00');
insert into "cmn".entitlement_type (oid, name_en, name_bn, created_by, created_on) values ('03','Monthly','মাসিক','jakir','2019-09-17 3:00:00');
insert into "cmn".entitlement_type (oid, name_en, name_bn, created_by, created_on) values ('04','Quarterly','ত্রৈমাসিক','jakir','2019-09-17 3:00:00');
insert into "cmn".entitlement_type (oid, name_en, name_bn, created_by, created_on) values ('05','Yearly','বার্ষিক','jakir','2019-09-17 3:00:00');
commit;


