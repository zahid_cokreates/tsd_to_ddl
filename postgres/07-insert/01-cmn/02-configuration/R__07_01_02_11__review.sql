-- review_job_type
insert into "cmn".review_job_type (oid, job_name, table_name, is_deleted, created_by, created_on) values ('01','ItemRequisition','item_requisition','No','jakir','2019-09-17 3:00:00');
commit;

-- review_action_type
insert into "cmn".review_action_type (oid, name_en, name_bn, priority, is_deleted, created_by, created_on) values ('01','FORWARD','অগ্রবর্তী','6','No','jakir','2019-09-17 3:00:00');
insert into "cmn".review_action_type (oid, name_en, name_bn, priority, is_deleted, created_by, created_on) values ('02','RETURN','প্রত্যাবর্তন','5','No','jakir','2019-09-17 3:00:00');
insert into "cmn".review_action_type (oid, name_en, name_bn, priority, is_deleted, created_by, created_on) values ('03','VERIFY','যাচাই','4','No','jakir','2019-09-17 3:00:00');
insert into "cmn".review_action_type (oid, name_en, name_bn, priority, is_deleted, created_by, created_on) values ('04','REVIEW','পুনঃমূল্যায়ন','3','No','jakir','2019-09-17 3:00:00');
insert into "cmn".review_action_type (oid, name_en, name_bn, priority, is_deleted, created_by, created_on) values ('05','RECOMMEND','সুপারিশ করা','2','No','jakir','2019-09-17 3:00:00');
insert into "cmn".review_action_type (oid, name_en, name_bn, priority, is_deleted, created_by, created_on) values ('06','APPROVE','অনুমোদন করা','1','No','jakir','2019-09-17 3:00:00');
commit;

-- review_action
insert into "cmn".review_action (oid, request_time, review_time, return_comment, is_complete, is_rejected, is_disabled, is_deleted, created_by, created_on, source_tbl, source_row_oid, requested_by_oid, assigned_to_oid, review_action_type_oid, forwarded_oid) values ('01','2019-09-17 3:00:00','2019-09-17 3:00:00','aa','No','No','No','No','jakir','2019-09-17 3:00:00','ItemRequisition','d33fb81a-c615-4b53-a187-f834859f4bbb','10','13','06','16');
insert into "cmn".review_action (oid, request_time, review_time, return_comment, is_complete, is_rejected, is_disabled, is_deleted, created_by, created_on, source_tbl, source_row_oid, requested_by_oid, assigned_to_oid, review_action_type_oid, forwarded_oid) values ('02','2019-09-17 3:00:00','2019-09-17 3:00:00','fdgjdhfjdshf bsdfhsdngfbdfjkdjjgj fjdghghghghghghghghghghghghghghghf  jkjjkkjkl  kdfjgkdfjgkfjdgj l,fdkglllllllllllllllllllllllllllllllllllllf lkjfdgkljfkgjkfjgfkdjgkfdjgkfjgjfhk','Yes','No','No','No','jakir','2019-09-17 3:00:00','ItemRequisition','deee45a7-e9f1-4531-809e-cf2e755ff814','10','13','06','16');
insert into "cmn".review_action (oid, request_time, review_time, return_comment, is_complete, is_rejected, is_disabled, is_deleted, created_by, created_on, source_tbl, source_row_oid, requested_by_oid, assigned_to_oid, review_action_type_oid, forwarded_oid) values ('03','2019-09-17 3:00:00','2019-09-17 3:00:00','approve','Yes','No','No','No','jakir','2019-09-17 3:00:00','ItemRequisition','aa318d0c-4527-451a-a2f0-4fef672887e8','10','13','06','16');
insert into "cmn".review_action (oid, request_time, review_time, return_comment, is_complete, is_rejected, is_disabled, is_deleted, created_by, created_on, source_tbl, source_row_oid, requested_by_oid, assigned_to_oid, review_action_type_oid, forwarded_oid) values ('04','2019-09-17 3:00:00','2019-09-17 3:00:00','2nd time','Yes','No','No','No','jakir','2019-09-17 3:00:00','ItemRequisition','aa318d0c-4527-451a-a2f0-4fef672887e8','10','13','06','16');
insert into "cmn".review_action (oid, request_time, review_time, return_comment, is_complete, is_rejected, is_disabled, is_deleted, created_by, created_on, source_tbl, source_row_oid, requested_by_oid, assigned_to_oid, review_action_type_oid, forwarded_oid) values ('05','2019-09-17 3:00:00','2019-09-17 3:00:00','please approve','Yes','No','No','No','jakir','2019-09-17 3:00:00','ItemRequisition','aa318d0c-4527-451a-a2f0-4fef672887e8','10','13','06','16');
insert into "cmn".review_action (oid, request_time, review_time, return_comment, is_complete, is_rejected, is_disabled, is_deleted, created_by, created_on, source_tbl, source_row_oid, requested_by_oid, assigned_to_oid, review_action_type_oid, forwarded_oid) values ('06','2019-09-17 3:00:00','2019-09-17 3:00:00','approved','Yes','No','No','No','jakir','2019-09-17 3:00:00','ItemRequisition','aa318d0c-4527-451a-a2f0-4fef672887e8','10','13','06','16');
commit;

-- review_action_detail
insert into "cmn".review_action_detail (oid, audited_qty, status, remarks, is_deleted, created_by, created_on) values ('01','1','Yes','-','No','jakir','2019-09-17 3:00:00');
insert into "cmn".review_action_detail (oid, audited_qty, status, remarks, is_deleted, created_by, created_on) values ('02','2','Yes','-','No','jakir','2019-09-17 3:00:00');
insert into "cmn".review_action_detail (oid, audited_qty, status, remarks, is_deleted, created_by, created_on) values ('03','3','Yes','-','No','jakir','2019-09-17 3:00:00');
insert into "cmn".review_action_detail (oid, audited_qty, status, remarks, is_deleted, created_by, created_on) values ('04','5','Yes','-','No','jakir','2019-09-17 3:00:00');
insert into "cmn".review_action_detail (oid, audited_qty, status, remarks, is_deleted, created_by, created_on) values ('05','8','Yes','-','No','jakir','2019-09-17 3:00:00');
insert into "cmn".review_action_detail (oid, audited_qty, status, remarks, is_deleted, created_by, created_on) values ('06','2','Yes','-','No','jakir','2019-09-17 3:00:00');
commit;


