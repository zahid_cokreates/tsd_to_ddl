-- fiscal_year
insert into "cmn".fiscal_year (oid, name_en, name_bn, status, start_date, end_date) values ('1','2018-2019','২০১৮-২০১৯','Previous','2018-07-01','2019-06-30');
insert into "cmn".fiscal_year (oid, name_en, name_bn, status, start_date, end_date) values ('2','2019-2020','২০১৯-২০২০','Current','2019-07-01','2020-06-30');
insert into "cmn".fiscal_year (oid, name_en, name_bn, status, start_date, end_date) values ('3','2020-2021','২০২০-২০২১','Next','2020-07-01','2021-06-30');
insert into "cmn".fiscal_year (oid, name_en, name_bn, status, start_date, end_date) values ('4','2021-2022','২০২১-২০২২','Upcoming','2021-07-01','2022-06-30');
insert into "cmn".fiscal_year (oid, name_en, name_bn, status, start_date, end_date) values ('5','2022-2023','২০২২-২০২৩','Upcoming','2022-07-01','2023-06-30');
insert into "cmn".fiscal_year (oid, name_en, name_bn, status, start_date, end_date) values ('6','2023-2024','২০২৩-২০২৪','Upcoming','2023-07-01','2024-06-30');
commit;


