--
-- PostgreSQL database dump
--

-- Dumped from database version 11.4 (Ubuntu 11.4-1.pgdg16.04+1)
-- Dumped by pg_dump version 11.4 (Ubuntu 11.4-1.pgdg16.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: ast; Type: SCHEMA; Schema: -; Owner: grp
--

CREATE SCHEMA ast;


ALTER SCHEMA ast OWNER TO grp;

--
-- Name: cmn; Type: SCHEMA; Schema: -; Owner: grp
--

CREATE SCHEMA cmn;


ALTER SCHEMA cmn OWNER TO grp;

--
-- Name: hrm; Type: SCHEMA; Schema: -; Owner: grp
--

CREATE SCHEMA hrm;


ALTER SCHEMA hrm OWNER TO grp;

--
-- Name: prc; Type: SCHEMA; Schema: -; Owner: grp
--

CREATE SCHEMA prc;


ALTER SCHEMA prc OWNER TO grp;

--
-- Name: sec; Type: SCHEMA; Schema: -; Owner: grp
--

CREATE SCHEMA sec;


ALTER SCHEMA sec OWNER TO grp;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: acquisition_billing; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.acquisition_billing (
    oid character varying(128) NOT NULL,
    acquisition_billing_code character varying(128) NOT NULL,
    supplier_oid character varying(256) NOT NULL,
    approved_by character varying(256),
    approval_date date,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE ast.acquisition_billing OWNER TO grp;

--
-- Name: acquisition_billing_line; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.acquisition_billing_line (
    oid character varying(128) NOT NULL,
    asset_oid character varying(128) NOT NULL,
    acquisition_billing_oid character varying(256) NOT NULL,
    price numeric(20,6) NOT NULL
);


ALTER TABLE ast.acquisition_billing_line OWNER TO grp;

--
-- Name: asset_allocation; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.asset_allocation (
    oid character varying(128) NOT NULL,
    asset_allocation_code character varying(256) NOT NULL,
    asset_oid character varying(256) NOT NULL,
    allocation_date date NOT NULL,
    distributor_oid character varying(256) NOT NULL,
    store_oid character varying(256) NOT NULL,
    end_user_oid character varying(256) NOT NULL,
    asset_requisition_oid character varying(256) NOT NULL,
    document_oid character varying(256)
);


ALTER TABLE ast.asset_allocation OWNER TO grp;

--
-- Name: asset_complaint_letter; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.asset_complaint_letter (
    oid character varying(128) NOT NULL,
    complaint_letter_code character varying(128) NOT NULL,
    vendor_oid character varying(128) NOT NULL,
    description_en character varying(256) NOT NULL,
    description_bn character varying(256) NOT NULL,
    committee_oid character varying(128) NOT NULL,
    committee_note_en text NOT NULL,
    committee_note_bn text NOT NULL
);


ALTER TABLE ast.asset_complaint_letter OWNER TO grp;

--
-- Name: asset_disposal; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.asset_disposal (
    oid character varying(128) NOT NULL,
    asset_disposal_request_code character varying(256) NOT NULL,
    asset_oid character varying(128) NOT NULL,
    requester_oid character varying(128) NOT NULL,
    requested_date date NOT NULL,
    cause_details text NOT NULL,
    store_oid character varying(128) NOT NULL,
    committee_oid character varying(128) NOT NULL,
    committee_notes text,
    disposal_process character varying(256) NOT NULL,
    cost numeric(20,6) NOT NULL,
    decision_by character varying(128) NOT NULL,
    decision_date date NOT NULL,
    status character varying(32) NOT NULL,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE ast.asset_disposal OWNER TO grp;

--
-- Name: asset_information; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.asset_information (
    oid character varying(128) NOT NULL,
    code character varying(256) NOT NULL,
    description text,
    status character varying(32) NOT NULL,
    tagged_by character varying(128),
    tagged_on timestamp without time zone,
    temporary_item_oid character varying(128) NOT NULL,
    item_oid character varying(128) NOT NULL,
    office_oid character varying(128) DEFAULT ''::character varying,
    created_by character varying(128) DEFAULT 'System'::character varying NOT NULL,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone,
    CONSTRAINT ck_status_asset_information CHECK ((((status)::text = 'In-use'::text) OR ((status)::text = 'Available'::text) OR ((status)::text = 'Under maintenance'::text) OR ((status)::text = 'Disposed'::text) OR ((status)::text = 'Draft'::text) OR ((status)::text = 'Ready for tagging'::text)))
);


ALTER TABLE ast.asset_information OWNER TO grp;

--
-- Name: asset_lost_info; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.asset_lost_info (
    oid character varying(128) NOT NULL,
    asset_lost_code character varying(256) NOT NULL,
    asset_oid character varying(128) NOT NULL,
    info_about_incident character varying(256) NOT NULL,
    incident_date date NOT NULL,
    status character varying(32) NOT NULL,
    committee_oid character varying(128),
    committee_notes text,
    investigation_time timestamp without time zone,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE ast.asset_lost_info OWNER TO grp;

--
-- Name: asset_maintenance; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.asset_maintenance (
    oid character varying(128) NOT NULL,
    maintenance_code character varying(256) NOT NULL,
    requester_oid character varying(128) NOT NULL,
    requestdate date NOT NULL,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE ast.asset_maintenance OWNER TO grp;

--
-- Name: asset_maintenance_line; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.asset_maintenance_line (
    oid character varying(128) NOT NULL,
    maintenance_for_each_asset_code character varying(256) NOT NULL,
    maintenance_oid character varying(128) NOT NULL,
    asset_category character varying(128) NOT NULL,
    asset_oid character varying(128) NOT NULL,
    maintenance_purpose text NOT NULL,
    maintenance_type_oid character varying(128) NOT NULL,
    approver_note text,
    decision_by character varying(128),
    decision_date date,
    assigned_vendor character varying(128),
    requester_note text,
    status character varying(32) NOT NULL,
    hand_over_date date,
    CONSTRAINT ck_status_asset_maintenance_line CHECK ((((status)::text = 'Processing'::text) OR ((status)::text = 'Approved'::text) OR ((status)::text = 'Rejected'::text) OR ((status)::text = 'Maintained'::text) OR ((status)::text = 'Consigned'::text)))
);


ALTER TABLE ast.asset_maintenance_line OWNER TO grp;

--
-- Name: asset_maintenance_type; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.asset_maintenance_type (
    oid character varying(128) NOT NULL,
    maintenance_type_code character varying(256) NOT NULL,
    maintenance_type_name character varying(256) NOT NULL,
    asset_category_oid character varying(128) NOT NULL,
    service_description text,
    priority character varying(32) NOT NULL,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL,
    notes text NOT NULL
);


ALTER TABLE ast.asset_maintenance_type OWNER TO grp;

--
-- Name: asset_requisition; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.asset_requisition (
    oid character varying(128) NOT NULL,
    asset_requisition_code character varying(256) NOT NULL,
    requester_oid character varying(256) NOT NULL,
    requested_date date NOT NULL,
    purpose character varying(32) NOT NULL,
    decided_by character varying(256),
    decision_date date NOT NULL,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE ast.asset_requisition OWNER TO grp;

--
-- Name: asset_requisition_line; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.asset_requisition_line (
    oid character varying(128) NOT NULL,
    asset_category_oid character varying(256) NOT NULL,
    requested_quantity numeric(20,6) NOT NULL,
    status character varying(32) NOT NULL,
    approved_quantity numeric(20,6),
    asset_requisition_oid character varying(256) NOT NULL,
    CONSTRAINT ck_status_asset_requisition_line CHECK ((((status)::text = 'Processing'::text) OR ((status)::text = 'Approved'::text) OR ((status)::text = 'Rejected'::text)))
);


ALTER TABLE ast.asset_requisition_line OWNER TO grp;

--
-- Name: asset_return; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.asset_return (
    oid character varying(128) NOT NULL,
    asset_return_code character varying(256) NOT NULL,
    asset_oid character varying(128) NOT NULL,
    receiver_oid character varying(128) NOT NULL,
    store_oid character varying(128) NOT NULL,
    return_date date NOT NULL,
    asset_requisition_oid character varying(128) NOT NULL
);


ALTER TABLE ast.asset_return OWNER TO grp;

--
-- Name: asset_tracking; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.asset_tracking (
    oid character varying(128) NOT NULL,
    asset_tracking_code character varying(256) NOT NULL,
    asset_oid character varying(128) NOT NULL,
    table_name character varying(256) NOT NULL,
    primary_key_value character varying(128) NOT NULL,
    changed_by character varying(128) NOT NULL,
    changed_date date NOT NULL
);


ALTER TABLE ast.asset_tracking OWNER TO grp;

--
-- Name: asset_tracking_line; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.asset_tracking_line (
    oid character varying(128) NOT NULL,
    asset_tracking_oid character varying(128) NOT NULL,
    column_name character varying(256) NOT NULL,
    new_value text NOT NULL,
    previous_value text
);


ALTER TABLE ast.asset_tracking_line OWNER TO grp;

--
-- Name: asset_transfer; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.asset_transfer (
    oid character varying(128) NOT NULL,
    transfer_code character varying(128) NOT NULL,
    transfer_date date NOT NULL,
    store_from character varying(256) NOT NULL,
    store_to character varying(256) NOT NULL,
    asset_oid character varying(256) NOT NULL,
    note text,
    status character varying(32),
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE ast.asset_transfer OWNER TO grp;

--
-- Name: auto_maintenance; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.auto_maintenance (
    oid character varying(128) NOT NULL,
    auto_maintenance_code character varying(256) NOT NULL,
    set_by character varying(128) NOT NULL,
    to_notify character varying(128) NOT NULL,
    notification_date date NOT NULL,
    period date NOT NULL,
    status character varying(32) NOT NULL,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL,
    CONSTRAINT ck_status_auto_maintenance CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE ast.auto_maintenance OWNER TO grp;

--
-- Name: contract_info; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.contract_info (
    oid character varying(128) NOT NULL,
    contract_code character varying(256) NOT NULL,
    rent_or_lease_info_oid character varying(128) NOT NULL,
    tenant_oid character varying(128) NOT NULL,
    asset_oid character varying(128) NOT NULL,
    start_from date NOT NULL,
    ending_time date NOT NULL,
    rent_type character varying(256) NOT NULL,
    rate numeric(20,6) NOT NULL,
    notification_time timestamp without time zone NOT NULL,
    renew_with_in date,
    status character varying(32) NOT NULL,
    note text,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL,
    CONSTRAINT ck_status_contract_info CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE ast.contract_info OWNER TO grp;

--
-- Name: contract_renewal; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.contract_renewal (
    oid character varying(128) NOT NULL,
    contract_renewal_code character varying(256) NOT NULL,
    contract_info_oid character varying(128) NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    renew_within date NOT NULL,
    rent_type character varying(256) NOT NULL,
    rate numeric(20,6) NOT NULL,
    approved_by character varying(128) NOT NULL,
    note text
);


ALTER TABLE ast.contract_renewal OWNER TO grp;

--
-- Name: contractual_driver; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.contractual_driver (
    oid character varying(128) NOT NULL,
    driver_code character varying(256) NOT NULL,
    driver_name character varying(256) NOT NULL,
    address character varying(512),
    contact_no character varying(256) NOT NULL,
    nid character varying(256),
    licence_no character varying(256),
    description text,
    driver_type character varying(256) NOT NULL,
    status character varying(32) NOT NULL,
    vehicle_oid character varying(128),
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL,
    CONSTRAINT ck_status_contractual_driver CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE ast.contractual_driver OWNER TO grp;

--
-- Name: flat_info; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.flat_info (
    oid character varying(128) NOT NULL,
    flat_code character varying(256) NOT NULL,
    floor_info_oid character varying(128) NOT NULL,
    flat_no numeric(20,6) NOT NULL,
    position_of_flat character varying(256) NOT NULL,
    total_room numeric(3,0) NOT NULL,
    status character varying(32),
    CONSTRAINT ck_status_flat_info CHECK ((((status)::text = 'In-use'::text) OR ((status)::text = 'Available'::text)))
);


ALTER TABLE ast.flat_info OWNER TO grp;

--
-- Name: floor_info; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.floor_info (
    oid character varying(128) NOT NULL,
    floor_code character varying(256) NOT NULL,
    building_id character varying(128) NOT NULL,
    floor_no numeric(20,6) NOT NULL,
    total_flat numeric(20,6) NOT NULL
);


ALTER TABLE ast.floor_info OWNER TO grp;

--
-- Name: fuel_reconciliation; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.fuel_reconciliation (
    oid character varying(128) NOT NULL,
    fuel_reconciliation_code character varying(256) NOT NULL,
    fuel_type character varying(256) NOT NULL,
    vehicle_oid character varying(128) NOT NULL,
    driver_oid character varying(128) NOT NULL,
    station_name character varying(256) NOT NULL,
    quantity numeric(20,6) NOT NULL,
    rate numeric(10,6) NOT NULL,
    unit numeric(10,6) NOT NULL,
    total numeric(20,6) NOT NULL,
    date date NOT NULL,
    document_oid character varying(128) NOT NULL,
    status character varying(32) NOT NULL,
    approved_by character varying(128),
    approved_date date,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE ast.fuel_reconciliation OWNER TO grp;

--
-- Name: fuel_requisition; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.fuel_requisition (
    oid character varying(128) NOT NULL,
    fuel_requisition_code character varying(256) NOT NULL,
    fuel_type character varying(256) NOT NULL,
    requested_quantity numeric(20,6) NOT NULL,
    vehicle_oid character varying(128) NOT NULL,
    driver_oid character varying(128) NOT NULL,
    requisition_date date NOT NULL,
    status character varying(32) NOT NULL,
    approved_quantity numeric(20,6),
    approved_by character varying(128),
    approved_date date,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL,
    CONSTRAINT ck_status_fuel_requisition CHECK ((((status)::text = 'Processing'::text) OR ((status)::text = 'Approved'::text) OR ((status)::text = 'Rejected'::text)))
);


ALTER TABLE ast.fuel_requisition OWNER TO grp;

--
-- Name: fuel_service_station; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.fuel_service_station (
    oid character varying(128) NOT NULL,
    station_code character varying(256) NOT NULL,
    station_name character varying(256) NOT NULL,
    fuel_type_oid character varying(128) NOT NULL,
    location character varying(512) NOT NULL,
    contact_number character varying(256),
    contract_signing_date date,
    contract_closing_date date,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL,
    status character varying(32)
);


ALTER TABLE ast.fuel_service_station OWNER TO grp;

--
-- Name: fuel_type; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.fuel_type (
    oid character varying(128) NOT NULL,
    fuel_type_code character varying(256) NOT NULL,
    fuel_type_name character varying(256) NOT NULL,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE ast.fuel_type OWNER TO grp;

--
-- Name: garage; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.garage (
    oid character varying(128) NOT NULL,
    garage_code character varying(256) NOT NULL,
    garage_name character varying(256) NOT NULL,
    location character varying(512) NOT NULL,
    garage_contact_no character varying(256),
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE ast.garage OWNER TO grp;

--
-- Name: maintenance_asset_receive; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.maintenance_asset_receive (
    oid character varying(128) NOT NULL,
    maintenance_received_code character varying(256) NOT NULL,
    asset_maintenance_oid character varying(128) NOT NULL,
    received_date date NOT NULL,
    received_by character varying(128) NOT NULL,
    qc_committee character varying(128),
    qc_note text,
    qc_time timestamp without time zone,
    qc_status character varying(32),
    CONSTRAINT ck_qc_status_maintenance_asset_receive CHECK ((((qc_status)::text = 'Pending'::text) OR ((qc_status)::text = 'Checking'::text) OR ((qc_status)::text = 'Passed'::text) OR ((qc_status)::text = 'Failed'::text)))
);


ALTER TABLE ast.maintenance_asset_receive OWNER TO grp;

--
-- Name: maintenance_payment; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.maintenance_payment (
    oid character varying(128) NOT NULL,
    payment_code character varying(256) NOT NULL,
    asset_maintenance_oid character varying(128) NOT NULL,
    bill_type character varying(256) NOT NULL,
    invoice_no character varying(256) NOT NULL,
    net_amount numeric(20,6) NOT NULL,
    payment_date date NOT NULL,
    received_by character varying(128) NOT NULL,
    bill_attachment_file character varying(1028) NOT NULL
);


ALTER TABLE ast.maintenance_payment OWNER TO grp;

--
-- Name: method_rule_category; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.method_rule_category (
    oid character varying(128) NOT NULL,
    method_rule_category_code character varying(256) NOT NULL,
    method_rule_category_name character varying(256) NOT NULL,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE ast.method_rule_category OWNER TO grp;

--
-- Name: method_setup; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.method_setup (
    oid character varying(128) NOT NULL,
    method_code character varying(256) NOT NULL,
    method_name character varying(256) NOT NULL,
    method_rule_category_oid character varying(128) NOT NULL,
    description text,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE ast.method_setup OWNER TO grp;

--
-- Name: rent_lease_info; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.rent_lease_info (
    oid character varying(128) NOT NULL,
    rent_lease_lnfo_code character varying(256) NOT NULL,
    requester_oid character varying(256) NOT NULL,
    tender_oid character varying(256) NOT NULL,
    description text NOT NULL,
    proposal_document character varying(1028) NOT NULL,
    approved_by character varying(256),
    decision_date date,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL,
    status character varying(32) NOT NULL,
    CONSTRAINT ck_status_rent_lease_info CHECK ((((status)::text = 'Processing'::text) OR ((status)::text = 'Approved'::text) OR ((status)::text = 'Rejected'::text)))
);


ALTER TABLE ast.rent_lease_info OWNER TO grp;

--
-- Name: rent_receive; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.rent_receive (
    oid character varying(128) NOT NULL,
    rent_receive_code character varying(256) NOT NULL,
    contract_info_oid character varying(128) NOT NULL,
    year numeric(20,6) NOT NULL,
    month numeric(20,6) NOT NULL,
    net_amount numeric(20,6) NOT NULL,
    payment_date date NOT NULL,
    received_by_oid character varying(128) NOT NULL,
    note text
);


ALTER TABLE ast.rent_receive OWNER TO grp;

--
-- Name: route_config; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.route_config (
    oid character varying(128) NOT NULL,
    route_config_code character varying(256) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    pickup_location character varying(512) NOT NULL,
    route_setup_oid character varying(128) NOT NULL,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE ast.route_config OWNER TO grp;

--
-- Name: route_setup; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.route_setup (
    oid character varying(128) NOT NULL,
    route_oid character varying(256) NOT NULL,
    route_name character varying(256) NOT NULL,
    start_area character varying(256) NOT NULL,
    end_area character varying(256) NOT NULL,
    vehicle_oid character varying(128) NOT NULL,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE ast.route_setup OWNER TO grp;

--
-- Name: rules_regulation; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.rules_regulation (
    oid character varying(128) NOT NULL,
    rules_regulation_code character varying(256) NOT NULL,
    name character varying(256) NOT NULL,
    description text,
    method_category_oid character varying(256) NOT NULL,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE ast.rules_regulation OWNER TO grp;

--
-- Name: temporary_item; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.temporary_item (
    oid character varying(128) NOT NULL,
    code character varying(256),
    received_at date,
    received_by character varying(128),
    chalan_no character varying(128),
    chalan_date date,
    workorder_no character varying(128),
    workorder_date date,
    status character varying(32) NOT NULL,
    description text,
    qc_by character varying(128),
    qc_on timestamp without time zone,
    asset_added_by character varying(128),
    asset_added_on timestamp without time zone,
    vendor_oid character varying(128),
    office_oid character varying(128),
    created_by character varying(128) DEFAULT 'System'::character varying,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_by character varying(128),
    updated_on timestamp without time zone,
    CONSTRAINT ck_status_temporary_item CHECK ((((status)::text = 'Processing'::text) OR ((status)::text = 'Ready for QC'::text) OR ((status)::text = 'QC done'::text) OR ((status)::text = 'Asset added'::text) OR ((status)::text = 'Draft'::text) OR ((status)::text = 'QC ongoing'::text) OR ((status)::text = 'Asset adding'::text)))
);


ALTER TABLE ast.temporary_item OWNER TO grp;

--
-- Name: temporary_item_detail; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.temporary_item_detail (
    oid character varying(128) NOT NULL,
    received_quantity numeric(20,8) DEFAULT 0,
    qualified_quantity numeric(20,8) DEFAULT 0,
    disqualified_quantity numeric(20,8) DEFAULT 0,
    extra_quantity numeric(20,8) DEFAULT 0,
    remarks character varying(512),
    item_oid character varying(128) NOT NULL,
    temporary_item_oid character varying(128) NOT NULL
);


ALTER TABLE ast.temporary_item_detail OWNER TO grp;

--
-- Name: tenant; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.tenant (
    oid character varying(128) NOT NULL,
    tenant_code character varying(256) NOT NULL,
    tenant_name character varying(256) NOT NULL,
    rent_type character varying(256) NOT NULL,
    address character varying(512),
    contact_no character varying(256) NOT NULL,
    emergency_contact character varying(256) NOT NULL,
    nid_number character varying(256),
    tin_number character varying(256),
    bin_number character varying(256),
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE ast.tenant OWNER TO grp;

--
-- Name: tender; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.tender (
    oid character varying(128) NOT NULL,
    tender_code character varying(256) NOT NULL,
    asset_oid character varying(128) NOT NULL,
    circulation_date character varying(256) NOT NULL,
    description text,
    document_oid character varying(128)
);


ALTER TABLE ast.tender OWNER TO grp;

--
-- Name: ticket_generation; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.ticket_generation (
    oid character varying(128) NOT NULL,
    ticket_gen_code character varying(256) NOT NULL,
    fuel_requisition_oid character varying(128) NOT NULL,
    station_oid character varying(128) NOT NULL,
    quantity numeric(20,6) NOT NULL
);


ALTER TABLE ast.ticket_generation OWNER TO grp;

--
-- Name: vehicle_allocation; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.vehicle_allocation (
    oid character varying(128) NOT NULL,
    vehicle_allocation_code character varying(256) NOT NULL,
    vehicle_requisition_oid character varying(128) NOT NULL,
    distributor_oid character varying(128) NOT NULL,
    allocation_date date NOT NULL
);


ALTER TABLE ast.vehicle_allocation OWNER TO grp;

--
-- Name: vehicle_info; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.vehicle_info (
    oid character varying(128) NOT NULL,
    vehicle_code character varying(256) NOT NULL,
    garage_oid character varying(128) NOT NULL,
    engine_no character varying(256) NOT NULL,
    insurance_no character varying(256) NOT NULL,
    fitness_registration_no character varying(256) NOT NULL,
    registration_no character varying(256) NOT NULL,
    registration_date date,
    number_plate character varying(256),
    insurance_expiry_date date NOT NULL,
    fitness_expiry_date date,
    registration_expiry_date date NOT NULL,
    engine_cc character varying NOT NULL,
    color character varying(256) NOT NULL,
    fuel_type character varying(128) NOT NULL,
    fuel_capacity numeric(20,6) NOT NULL,
    horse_power character varying(256) NOT NULL,
    tex_token character varying(256) NOT NULL,
    vehicle_type character varying(256) NOT NULL,
    transmission_type character varying(256) NOT NULL,
    motor_type character varying(256) NOT NULL,
    no_of_seats numeric(256,0) NOT NULL,
    country_of_origin character varying(256) NOT NULL,
    lifetime numeric(20,6) NOT NULL,
    status character varying(32) NOT NULL,
    description text,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL,
    CONSTRAINT ck_motor_type_vehicle_info CHECK ((((motor_type)::text = 'Heavy'::text) OR ((motor_type)::text = 'Light'::text))),
    CONSTRAINT ck_status_vehicle_info CHECK ((((status)::text = 'In-use'::text) OR ((status)::text = 'Available'::text) OR ((status)::text = 'Under maintenance'::text) OR ((status)::text = 'Disposed'::text)))
);


ALTER TABLE ast.vehicle_info OWNER TO grp;

--
-- Name: vehicle_requisition; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.vehicle_requisition (
    oid character varying(128) NOT NULL,
    vehicle_requisition_code character varying(256) NOT NULL,
    vehicle_type character varying(256) NOT NULL,
    requester_oid character varying(128) NOT NULL,
    requsition_date date NOT NULL,
    start_date timestamp without time zone NOT NULL,
    end_date timestamp without time zone NOT NULL,
    purpose text,
    organization character varying(128) NOT NULL,
    decision_by character varying(128) NOT NULL,
    decision_date date NOT NULL,
    status character varying(32),
    vehicle_oid character varying(128) NOT NULL,
    driver_oid character varying(128) NOT NULL,
    description text,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE ast.vehicle_requisition OWNER TO grp;

--
-- Name: vehicle_return; Type: TABLE; Schema: ast; Owner: grp
--

CREATE TABLE ast.vehicle_return (
    oid character varying(128) NOT NULL,
    vehicle_return_code character varying(256) NOT NULL,
    receiver_oid character varying(128) NOT NULL,
    garage_oid character varying(128),
    return_date date NOT NULL,
    vehicle_requisition_oid character varying(128) NOT NULL
);


ALTER TABLE ast.vehicle_return OWNER TO grp;

--
-- Name: allowance_setup; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.allowance_setup (
    oid character varying(128) NOT NULL,
    grade character varying(256) NOT NULL,
    house_rent_allowance numeric(20,2) NOT NULL,
    medical_allowance numeric(20,2) NOT NULL,
    mobile_allowance numeric(20,2) NOT NULL,
    other_allowance numeric(20,2) NOT NULL,
    percentage numeric(20,2) NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL
);


ALTER TABLE cmn.allowance_setup OWNER TO grp;

--
-- Name: city_corporation; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.city_corporation (
    oid character varying(128) NOT NULL,
    name_en character varying(64) NOT NULL,
    name_bn character varying(128) NOT NULL,
    bbs_code character varying(32),
    district_oid character varying(128) NOT NULL
);


ALTER TABLE cmn.city_corporation OWNER TO grp;

--
-- Name: city_corporation_ward; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.city_corporation_ward (
    oid character varying(128) NOT NULL,
    name_en character varying(64) NOT NULL,
    name_bn character varying(128) NOT NULL,
    bbs_code character varying(32),
    district_oid character varying(128) NOT NULL,
    city_corporation_oid character varying(128) NOT NULL
);


ALTER TABLE cmn.city_corporation_ward OWNER TO grp;

--
-- Name: committee_setup; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.committee_setup (
    oid character varying(128) NOT NULL,
    committee_code character varying(128) NOT NULL,
    committee_type_oid character varying(128) NOT NULL,
    committee_name character varying(256) NOT NULL,
    max_member character varying(3) NOT NULL,
    min_member character varying(3) NOT NULL,
    forming_date date NOT NULL,
    purpose character varying(256) NOT NULL,
    comments text NOT NULL,
    status character varying(32) NOT NULL,
    organization_info_oid character varying(128) NOT NULL,
    CONSTRAINT ck_status_committee_setup CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'In-Active'::text)))
);


ALTER TABLE cmn.committee_setup OWNER TO grp;

--
-- Name: committee_setup_line; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.committee_setup_line (
    oid character varying(128) NOT NULL,
    committee_setup_line_code character varying(128) NOT NULL,
    committee_setup_oid character varying(128) NOT NULL,
    employee_info_oid character varying(128) NOT NULL,
    member_role character varying(256) NOT NULL,
    orgaization_oid character varying(128) NOT NULL
);


ALTER TABLE cmn.committee_setup_line OWNER TO grp;

--
-- Name: committee_type_setup; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.committee_type_setup (
    oid character varying(128) NOT NULL,
    committee_type_code character varying(128) NOT NULL,
    committee_type character varying(256) NOT NULL,
    approved_by character varying(128) NOT NULL
);


ALTER TABLE cmn.committee_type_setup OWNER TO grp;

--
-- Name: deduction_setup; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.deduction_setup (
    oid character varying(128) NOT NULL,
    grade character varying(256) NOT NULL,
    house_rent_deduction numeric(20,2) NOT NULL,
    tax_deduction numeric(20,2) NOT NULL,
    gpf_deduction numeric(20,2) NOT NULL,
    other_deduction numeric(20,2) NOT NULL,
    percentage numeric(20,2) NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL
);


ALTER TABLE cmn.deduction_setup OWNER TO grp;

--
-- Name: document; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.document (
    oid character varying(128) NOT NULL,
    document_code character varying(256) NOT NULL,
    document_name character varying(256) NOT NULL,
    document_type_oid character varying(128) NOT NULL,
    category character varying(256) NOT NULL,
    path character varying(1028) NOT NULL,
    description text
);


ALTER TABLE cmn.document OWNER TO grp;

--
-- Name: document_type; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.document_type (
    oid character varying(128) NOT NULL,
    document_type_code character varying(256) NOT NULL,
    document_type_name character varying(256) NOT NULL
);


ALTER TABLE cmn.document_type OWNER TO grp;

--
-- Name: donee_info; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.donee_info (
    oid character varying(128) NOT NULL,
    donee_code character varying(128) NOT NULL,
    donee_name character varying(256) NOT NULL,
    party_type_oid character varying(128) NOT NULL,
    address character varying(512),
    contact_no character varying(256) NOT NULL,
    organization character varying(256) NOT NULL,
    description text,
    status character varying(32) NOT NULL,
    CONSTRAINT ck_status_donee_info CHECK ((((status)::text = 'Available'::text) OR ((status)::text = 'Not available'::text)))
);


ALTER TABLE cmn.donee_info OWNER TO grp;

--
-- Name: donor_info; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.donor_info (
    oid character varying(128) NOT NULL,
    donor_code character varying(128) NOT NULL,
    donor_name character varying(256) NOT NULL,
    party_type_oid character varying(128) NOT NULL,
    address character varying(512) NOT NULL,
    contact_no character varying(256) NOT NULL,
    organization character varying(256) NOT NULL,
    description text,
    status character varying(32) NOT NULL,
    CONSTRAINT ck_status_donor_info CHECK ((((status)::text = 'Available'::text) OR ((status)::text = 'Not available'::text)))
);


ALTER TABLE cmn.donor_info OWNER TO grp;

--
-- Name: entitlement; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.entitlement (
    oid character varying(128) NOT NULL,
    entitlement_code character varying(128) NOT NULL,
    designation_oid character varying(128) NOT NULL,
    post_code character varying(128) NOT NULL,
    fiscal_year character varying NOT NULL,
    item_oid character varying(128) NOT NULL,
    quantity numeric(20,6) NOT NULL
);


ALTER TABLE cmn.entitlement OWNER TO grp;

--
-- Name: fiscal_year; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.fiscal_year (
    oid character varying(128) NOT NULL,
    name_en character varying(128) NOT NULL,
    name_bn character varying(128) NOT NULL,
    status character varying(128) NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    CONSTRAINT ck_status_fiscal_year CHECK ((((status)::text = 'Previous'::text) OR ((status)::text = 'Current'::text) OR ((status)::text = 'Next'::text) OR ((status)::text = 'Upcoming'::text)))
);


ALTER TABLE cmn.fiscal_year OWNER TO grp;

--
-- Name: geo_country; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.geo_country (
    oid character varying(128) NOT NULL,
    name_en character varying(64) NOT NULL,
    name_bn character varying(128) NOT NULL
);


ALTER TABLE cmn.geo_country OWNER TO grp;

--
-- Name: geo_district; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.geo_district (
    oid character varying(128) NOT NULL,
    name_en character varying(64) NOT NULL,
    name_bn character varying(128) NOT NULL,
    bbs_code character varying(32),
    division_oid character varying(128) NOT NULL
);


ALTER TABLE cmn.geo_district OWNER TO grp;

--
-- Name: geo_division; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.geo_division (
    oid character varying(128) NOT NULL,
    name_en character varying(64) NOT NULL,
    name_bn character varying(128) NOT NULL,
    bbs_code character varying(32),
    country_oid character varying(128) NOT NULL
);


ALTER TABLE cmn.geo_division OWNER TO grp;

--
-- Name: geo_post_office; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.geo_post_office (
    oid character varying(128) NOT NULL,
    name_en character varying(64) NOT NULL,
    name_bn character varying(128) NOT NULL,
    bbs_code character varying(32),
    union_oid character varying(128) NOT NULL
);


ALTER TABLE cmn.geo_post_office OWNER TO grp;

--
-- Name: geo_thana; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.geo_thana (
    oid character varying(128) NOT NULL,
    name_en character varying(64) NOT NULL,
    name_bn character varying(128) NOT NULL,
    bbs_code character varying(32),
    district_oid character varying(128) NOT NULL
);


ALTER TABLE cmn.geo_thana OWNER TO grp;

--
-- Name: geo_union; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.geo_union (
    oid character varying(128) NOT NULL,
    name_en character varying(64) NOT NULL,
    name_bn character varying(128) NOT NULL,
    bbs_code character varying(32),
    upazila_oid character varying(128) NOT NULL
);


ALTER TABLE cmn.geo_union OWNER TO grp;

--
-- Name: geo_union_ward; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.geo_union_ward (
    oid character varying(128) NOT NULL,
    name_en character varying(64) NOT NULL,
    name_bn character varying(128) NOT NULL,
    bbs_code character varying(32),
    union_oid character varying(128) NOT NULL
);


ALTER TABLE cmn.geo_union_ward OWNER TO grp;

--
-- Name: geo_upazila; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.geo_upazila (
    oid character varying(128) NOT NULL,
    name_en character varying(64) NOT NULL,
    name_bn character varying(128) NOT NULL,
    bbs_code character varying(32),
    district_oid character varying(128) NOT NULL
);


ALTER TABLE cmn.geo_upazila OWNER TO grp;

--
-- Name: item; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.item (
    oid character varying(128) NOT NULL,
    code character varying(128),
    name_en character varying(256) NOT NULL,
    name_bn character varying(256) NOT NULL,
    attribute_schema text,
    uom_oid character varying(128) NOT NULL,
    item_category_oid character varying(128) NOT NULL
);


ALTER TABLE cmn.item OWNER TO grp;

--
-- Name: item_category; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.item_category (
    oid character varying(128) NOT NULL,
    code character varying(128),
    name_en character varying(256) NOT NULL,
    name_bn character varying(256) NOT NULL,
    description_en character varying(256),
    description_bn character varying(256),
    status character varying(32) NOT NULL,
    attribute_schema_en text DEFAULT '{}'::text,
    attribute_schema_bn text DEFAULT '{}'::text,
    CONSTRAINT ck_status_item_category CHECK ((((status)::text = 'Available'::text) OR ((status)::text = 'Not available'::text)))
);


ALTER TABLE cmn.item_category OWNER TO grp;

--
-- Name: item_uom; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.item_uom (
    oid character varying(128) NOT NULL,
    code character varying(128),
    name_en character varying(256) NOT NULL,
    name_bn character varying(256) NOT NULL,
    mnemonic_en character varying(128) NOT NULL,
    mnemonic_bn character varying(128) NOT NULL,
    status character varying(32) DEFAULT 'Active'::character varying NOT NULL,
    CONSTRAINT ck_status_item_uom CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE cmn.item_uom OWNER TO grp;

--
-- Name: loan_sanction; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.loan_sanction (
    oid character varying(128) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    last_date_of_loan date NOT NULL,
    date_of_retirement date NOT NULL,
    loan_type character varying(256) NOT NULL,
    loan_amount numeric(20,2) NOT NULL,
    no_of_installment numeric(20,2) NOT NULL,
    loan_letter_upload character varying(1024) NOT NULL,
    notes text
);


ALTER TABLE cmn.loan_sanction OWNER TO grp;

--
-- Name: ministry; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.ministry (
    oid character varying(128) NOT NULL,
    name_en character varying(128) NOT NULL,
    name_bn character varying(256) NOT NULL,
    code_en character varying(128),
    code_bn character varying(128),
    status character varying(32) NOT NULL,
    sort_order numeric(5,0) NOT NULL,
    ministry_type_oid character varying(128) NOT NULL,
    CONSTRAINT ck_status_ministry CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE cmn.ministry OWNER TO grp;

--
-- Name: ministry_type; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.ministry_type (
    oid character varying(128) NOT NULL,
    name_en character varying(128) NOT NULL,
    name_bn character varying(256) NOT NULL,
    sort_order numeric(5,0) NOT NULL
);


ALTER TABLE cmn.ministry_type OWNER TO grp;

--
-- Name: municipality; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.municipality (
    oid character varying(128) NOT NULL,
    name_en character varying(64) NOT NULL,
    name_bn character varying(128) NOT NULL,
    bbs_code character varying(32),
    upazila_oid character varying(128) NOT NULL
);


ALTER TABLE cmn.municipality OWNER TO grp;

--
-- Name: municipality_ward; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.municipality_ward (
    oid character varying(128) NOT NULL,
    name_en character varying(64) NOT NULL,
    name_bn character varying(128) NOT NULL,
    bbs_code character varying(32),
    upazila_oid character varying(128) NOT NULL,
    municipality_oid character varying(128) NOT NULL
);


ALTER TABLE cmn.municipality_ward OWNER TO grp;

--
-- Name: office; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.office (
    oid character varying(128) NOT NULL,
    name_en character varying(128) NOT NULL,
    name_bn character varying(256) NOT NULL,
    code_en character varying(128),
    code_bn character varying(128),
    address_en text,
    address_bn text,
    email character varying(128),
    web character varying(128),
    status character varying(32) NOT NULL,
    sort_order numeric(5,0) NOT NULL,
    parent_oid character varying(128),
    office_origin_oid character varying(128) NOT NULL,
    office_layer_oid character varying(128) NOT NULL,
    ministry_oid character varying(128) NOT NULL,
    CONSTRAINT ck_status_office CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE cmn.office OWNER TO grp;

--
-- Name: office_layer; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.office_layer (
    oid character varying(128) NOT NULL,
    name_en character varying(128) NOT NULL,
    name_bn character varying(256) NOT NULL,
    status character varying(32) NOT NULL,
    level_order numeric(5,0) NOT NULL,
    sort_order numeric(5,0) NOT NULL,
    parent_oid character varying(128),
    ministry_oid character varying(128) NOT NULL,
    CONSTRAINT ck_status_office_layer CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE cmn.office_layer OWNER TO grp;

--
-- Name: office_origin; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.office_origin (
    oid character varying(128) NOT NULL,
    name_en character varying(128) NOT NULL,
    name_bn character varying(256) NOT NULL,
    status character varying(32) NOT NULL,
    level_order numeric(5,0) NOT NULL,
    sort_order numeric(5,0) NOT NULL,
    parent_oid character varying(128),
    office_layer_oid character varying(128) NOT NULL,
    ministry_oid character varying(128) NOT NULL,
    CONSTRAINT ck_status_office_origin CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE cmn.office_origin OWNER TO grp;

--
-- Name: office_origin_unit; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.office_origin_unit (
    oid character varying(128) NOT NULL,
    name_en character varying(128),
    name_bn character varying(256) NOT NULL,
    status character varying(32) NOT NULL,
    level_order numeric(5,0),
    sort_order numeric(5,0) NOT NULL,
    parent_oid character varying(128),
    office_unit_category_oid character varying(128),
    office_origin_oid character varying(128) NOT NULL,
    office_layer_oid character varying(128) NOT NULL,
    ministry_oid character varying(128) NOT NULL,
    CONSTRAINT ck_status_office_origin_unit CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE cmn.office_origin_unit OWNER TO grp;

--
-- Name: office_origin_unit_post; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.office_origin_unit_post (
    oid character varying(128) NOT NULL,
    name_en character varying(128) NOT NULL,
    name_bn character varying(256) NOT NULL,
    status character varying(32) NOT NULL,
    level_order numeric(5,0) NOT NULL,
    sort_order numeric(5,0) NOT NULL,
    parent_oid character varying(128),
    office_origin_unit_oid character varying(128),
    CONSTRAINT ck_status_office_origin_unit_post CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE cmn.office_origin_unit_post OWNER TO grp;

--
-- Name: office_unit; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.office_unit (
    oid character varying(128) NOT NULL,
    name_en character varying(128),
    name_bn character varying(256) NOT NULL,
    status character varying(32) NOT NULL,
    sort_order numeric(5,0) NOT NULL,
    parent_oid character varying(128),
    office_unit_category_oid character varying(128),
    parent_office_origin_unit_oid character varying(128),
    office_origin_unit_oid character varying(128),
    office_oid character varying(128),
    office_layer_oid character varying(128) NOT NULL,
    ministry_oid character varying(128) NOT NULL,
    CONSTRAINT ck_status_office_unit CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE cmn.office_unit OWNER TO grp;

--
-- Name: office_unit_category; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.office_unit_category (
    oid character varying(128) NOT NULL,
    name_en character varying(128) NOT NULL,
    name_bn character varying(256) NOT NULL,
    sort_order numeric(5,0) NOT NULL
);


ALTER TABLE cmn.office_unit_category OWNER TO grp;

--
-- Name: office_unit_post; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.office_unit_post (
    oid character varying(128) NOT NULL,
    name_en character varying(128) NOT NULL,
    name_bn character varying(256) NOT NULL,
    status character varying(32) NOT NULL,
    level_order numeric(5,0) NOT NULL,
    sort_order numeric(5,0) NOT NULL,
    parent_oid character varying(128),
    parent_office_unit_oid character varying(128),
    office_origin_unit_post_oid character varying(128),
    office_oid character varying(128),
    office_unit_oid character varying(128),
    CONSTRAINT ck_status_office_unit_post CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE cmn.office_unit_post OWNER TO grp;

--
-- Name: party_type; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.party_type (
    oid character varying(128) NOT NULL,
    party_type_code character varying(128) NOT NULL,
    party_type_name character varying(256) NOT NULL
);


ALTER TABLE cmn.party_type OWNER TO grp;

--
-- Name: payroll_process; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.payroll_process (
    oid character varying(128) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    house_rent numeric(20,2) NOT NULL,
    total_allowance numeric(20,2) NOT NULL,
    total_deduction numeric(20,2) NOT NULL,
    final_amount numeric(20,2) NOT NULL
);


ALTER TABLE cmn.payroll_process OWNER TO grp;

--
-- Name: prepare_salary_bill; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.prepare_salary_bill (
    oid character varying(128) NOT NULL,
    grade character varying(256) NOT NULL,
    employee_type character varying(256) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    attachment character varying(1024) NOT NULL
);


ALTER TABLE cmn.prepare_salary_bill OWNER TO grp;

--
-- Name: store; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.store (
    oid character varying(128) NOT NULL,
    store_code character varying(128) NOT NULL,
    store_name character varying(256) NOT NULL,
    store_location character varying(512) NOT NULL,
    organization_oid character varying(128) NOT NULL,
    store_keeper_oid character varying(128) NOT NULL,
    store_contact_number character varying(256) NOT NULL,
    description text,
    status character varying(32) NOT NULL,
    startingdate date NOT NULL,
    CONSTRAINT ck_status_store CHECK ((((status)::text = 'Yes'::text) OR ((status)::text = 'No'::text)))
);


ALTER TABLE cmn.store OWNER TO grp;

--
-- Name: vendor; Type: TABLE; Schema: cmn; Owner: grp
--

CREATE TABLE cmn.vendor (
    oid character varying(128) NOT NULL,
    code character varying(128) NOT NULL,
    name_en character varying(256) NOT NULL,
    name_bn character varying(256) NOT NULL,
    contact_address character varying(512) NOT NULL,
    contact_no character varying(256) NOT NULL,
    email character varying(256) NOT NULL,
    billing_address character varying(256) NOT NULL,
    description text,
    status character varying(32) NOT NULL,
    created_by character varying(128) DEFAULT 'System'::character varying NOT NULL,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone,
    CONSTRAINT ck_status_vendor CHECK ((((status)::text = 'Available'::text) OR ((status)::text = 'Not available'::text)))
);


ALTER TABLE cmn.vendor OWNER TO grp;

--
-- Name: acr_main_report; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.acr_main_report (
    oid character varying(128) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    moral numeric(20,0) NOT NULL,
    intellectual numeric(20,0) NOT NULL,
    materialistic numeric(20,0) NOT NULL,
    recommend_for_training numeric(20,0) NOT NULL,
    qualification_for_promotion numeric(20,0) NOT NULL,
    other_recommendation character varying(256) NOT NULL,
    esign character varying(256) NOT NULL,
    designation character varying(256) NOT NULL
);


ALTER TABLE hrm.acr_main_report OWNER TO grp;

--
-- Name: acr_medical_report; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.acr_medical_report (
    oid character varying(128) NOT NULL,
    employee_oid character varying(256) NOT NULL,
    height numeric(20,2) NOT NULL,
    weight numeric(20,2) NOT NULL,
    eye_power numeric(20,0) NOT NULL,
    blood_group character varying(256) NOT NULL,
    blood_pressure character varying(256) NOT NULL,
    medical_class_division character varying(256) NOT NULL,
    attachment character varying(1024) NOT NULL,
    date date NOT NULL,
    esign character varying(256) NOT NULL
);


ALTER TABLE hrm.acr_medical_report OWNER TO grp;

--
-- Name: acr_summary; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.acr_summary (
    oid character varying(128) NOT NULL,
    employee_oid character varying(256) NOT NULL,
    date_of_submission timestamp without time zone NOT NULL,
    subject_of_evaluation character varying(256) NOT NULL,
    obtained_marks numeric(20,2) NOT NULL,
    total_obtained_marks numeric(20,2) NOT NULL,
    esign character varying(256) NOT NULL
);


ALTER TABLE hrm.acr_summary OWNER TO grp;

--
-- Name: acr_under_controlling_auth; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.acr_under_controlling_auth (
    oid character varying(128) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    submission_date_ca timestamp without time zone NOT NULL,
    subject_of_evaluation character varying(256) NOT NULL,
    marks_from_ca numeric(20,2) NOT NULL,
    total_marks_ca numeric(20,2) NOT NULL,
    acr_form_upload character varying(1024) NOT NULL,
    e_sign_of_authority character varying(256) NOT NULL
);


ALTER TABLE hrm.acr_under_controlling_auth OWNER TO grp;

--
-- Name: acr_under_controlling_ministry; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.acr_under_controlling_ministry (
    oid character varying(128) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    submission_date timestamp without time zone NOT NULL,
    submission_date_ca timestamp without time zone NOT NULL,
    total_marks_ca numeric(20,2) NOT NULL,
    submission_date_csa timestamp without time zone NOT NULL,
    total_marks_csa numeric(20,2) NOT NULL,
    date_of_filled_up_form timestamp without time zone NOT NULL,
    e_sign_of_auth character varying(256) NOT NULL
);


ALTER TABLE hrm.acr_under_controlling_ministry OWNER TO grp;

--
-- Name: acr_under_counter_signing_auth; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.acr_under_counter_signing_auth (
    oid character varying(128) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    submission_date_ca timestamp without time zone NOT NULL,
    total_marks_ca character varying(128) NOT NULL,
    submission_date_csa numeric(20,2) NOT NULL,
    total_marks_csa numeric(20,2) NOT NULL,
    acr_form_upload character varying(1024) NOT NULL,
    comments text,
    e_sign_of_auth character varying(256) NOT NULL
);


ALTER TABLE hrm.acr_under_counter_signing_auth OWNER TO grp;

--
-- Name: admit_card; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.admit_card (
    oid character varying(128) NOT NULL,
    exam_oid character varying(128) NOT NULL,
    applicant_oid character varying(128) NOT NULL,
    roll_no numeric(20,2) NOT NULL,
    exam_date date NOT NULL,
    exam_venue character varying(256) NOT NULL,
    exam_type character varying(256) NOT NULL
);


ALTER TABLE hrm.admit_card OWNER TO grp;

--
-- Name: application_form_for_recruitment; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.application_form_for_recruitment (
    oid character varying(128) NOT NULL,
    circular_oid character varying(128) NOT NULL,
    applicant_code character varying(128) NOT NULL,
    application_code character varying(128) NOT NULL,
    application_date timestamp without time zone NOT NULL,
    name character varying(256) NOT NULL,
    nid_no character varying(256) NOT NULL,
    date_of_birth date NOT NULL,
    age numeric(20,2) NOT NULL,
    father_name character varying(256) NOT NULL,
    mother_name character varying(256) NOT NULL,
    address character varying(512) NOT NULL,
    contact_number character varying(256) NOT NULL,
    gender character varying(32) NOT NULL,
    occupation character varying(256) NOT NULL,
    religion character varying(32) NOT NULL,
    nationality character varying(256) NOT NULL,
    board character varying(256) NOT NULL,
    educational_qualification text NOT NULL,
    experience text NOT NULL,
    quota character varying(256) NOT NULL,
    CONSTRAINT ck_gender_application_form_for_recruitment CHECK ((((gender)::text = 'Male'::text) OR ((gender)::text = 'Female'::text) OR ((gender)::text = 'Other'::text))),
    CONSTRAINT ck_quota_application_form_for_recruitment CHECK ((((quota)::text = 'Physically challenges'::text) OR ((quota)::text = 'Woman'::text) OR ((quota)::text = 'Family of freedom fighter'::text) OR ((quota)::text = 'Ethnic minorities'::text) OR ((quota)::text = 'Based on merit'::text) OR ((quota)::text = 'others'::text))),
    CONSTRAINT ck_religion_application_form_for_recruitment CHECK ((((religion)::text = 'Islam'::text) OR ((religion)::text = 'Hinduism'::text) OR ((religion)::text = 'Buddhism'::text) OR ((religion)::text = 'Christianity'::text) OR ((religion)::text = 'Other'::text)))
);


ALTER TABLE hrm.application_form_for_recruitment OWNER TO grp;

--
-- Name: apply_for_leave; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.apply_for_leave (
    oid character varying(128) NOT NULL,
    application_code character varying(128) NOT NULL,
    leave_oid character varying(128) NOT NULL,
    applicant_oid character varying(128) NOT NULL,
    total_no_of_days_leave numeric(20,2) NOT NULL,
    total_no_of_using_leave numeric(20,2) NOT NULL,
    leave_remaining numeric(20,2) NOT NULL,
    leave_from date NOT NULL,
    leave_to date NOT NULL,
    duration_of_leave numeric(20,2) NOT NULL,
    attachment_url character varying(1024),
    substitute_oid character varying(128)
);


ALTER TABLE hrm.apply_for_leave OWNER TO grp;

--
-- Name: apply_for_prl; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.apply_for_prl (
    oid character varying(128) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    duration_from date NOT NULL,
    duration_to date NOT NULL,
    prl_duration numeric(20,0) NOT NULL,
    file_title character varying(256) NOT NULL,
    file_code character varying(128) NOT NULL,
    file_type character varying(256) NOT NULL,
    attachment character varying(1024) NOT NULL,
    decision_status character varying(256) NOT NULL,
    application_code character varying(128) NOT NULL,
    decision_by character varying(256) NOT NULL
);


ALTER TABLE hrm.apply_for_prl OWNER TO grp;

--
-- Name: at_leave; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.at_leave (
    oid character varying(128) NOT NULL,
    employee_oid character varying(256) NOT NULL,
    leave_status_oid character varying(256) NOT NULL
);


ALTER TABLE hrm.at_leave OWNER TO grp;

--
-- Name: at_meeting_or_events; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.at_meeting_or_events (
    oid character varying(128) NOT NULL,
    employee_oid character varying(256) NOT NULL,
    meeting_or_event_venue character varying(256) NOT NULL,
    meeting_time timestamp without time zone NOT NULL,
    attendance_id character varying(256) NOT NULL
);


ALTER TABLE hrm.at_meeting_or_events OWNER TO grp;

--
-- Name: attendance_penalty_committee; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.attendance_penalty_committee (
    oid character varying(128) NOT NULL,
    committee_oid character varying(128) NOT NULL,
    committee_name character varying(256) NOT NULL,
    short_description text NOT NULL,
    member_oid character varying(128) NOT NULL,
    member_name character varying(256) NOT NULL,
    designation character varying(256) NOT NULL,
    role character varying(256) NOT NULL
);


ALTER TABLE hrm.attendance_penalty_committee OWNER TO grp;

--
-- Name: award_application; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.award_application (
    oid character varying(128) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    award_title character varying(256) NOT NULL,
    attachments character varying(256) NOT NULL,
    date_of_receiving_award date NOT NULL,
    notes text,
    decision_status character varying(256) NOT NULL,
    application_code character varying(128) NOT NULL,
    decision_by character varying(256) NOT NULL
);


ALTER TABLE hrm.award_application OWNER TO grp;

--
-- Name: award_setup; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.award_setup (
    oid character varying(128) NOT NULL,
    award_title character varying(256) NOT NULL,
    award_code character varying(128) NOT NULL,
    description text NOT NULL,
    department character varying(256) NOT NULL,
    notes text
);


ALTER TABLE hrm.award_setup OWNER TO grp;

--
-- Name: batch; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.batch (
    oid character varying(128) NOT NULL,
    batch_no_en character varying(16) NOT NULL,
    batch_no_bn date NOT NULL,
    batch_year_en character varying(16),
    batch_year_bn character varying(16)
);


ALTER TABLE hrm.batch OWNER TO grp;

--
-- Name: bundle; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.bundle (
    oid character varying(128) NOT NULL,
    bundle_type character varying(16) NOT NULL,
    role_oid character varying(16) NOT NULL,
    bundle_name character varying(16) NOT NULL
);


ALTER TABLE hrm.bundle OWNER TO grp;

--
-- Name: cadre; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.cadre (
    oid character varying(128) NOT NULL,
    name_en character varying(16) NOT NULL,
    name_bn character varying(16) NOT NULL,
    cadre_date date NOT NULL,
    sort_order numeric(5,0) NOT NULL
);


ALTER TABLE hrm.cadre OWNER TO grp;

--
-- Name: call_for_exam; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.call_for_exam (
    oid character varying(128) NOT NULL,
    circular_oid character varying(128) NOT NULL,
    exam_configuration_oid character varying(128) NOT NULL,
    exam_code character varying(128) NOT NULL,
    exam_date date NOT NULL,
    exam_type character varying(256) NOT NULL,
    exam_start timestamp without time zone NOT NULL,
    exam_end timestamp without time zone NOT NULL,
    CONSTRAINT ck_exam_type_call_for_exam CHECK ((((exam_type)::text = 'viva'::text) OR ((exam_type)::text = 'written'::text) OR ((exam_type)::text = 'presentation'::text) OR ((exam_type)::text = 'interview'::text)))
);


ALTER TABLE hrm.call_for_exam OWNER TO grp;

--
-- Name: create_short_list; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.create_short_list (
    oid character varying(128) NOT NULL,
    short_list_code character varying(128) NOT NULL,
    application_code character varying(128) NOT NULL,
    application_name character varying(256) NOT NULL,
    educational_qualification text NOT NULL,
    experience text NOT NULL,
    short_list_created_by_oid character varying(128) NOT NULL,
    district character varying(256) NOT NULL
);


ALTER TABLE hrm.create_short_list OWNER TO grp;

--
-- Name: daily_attendance; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.daily_attendance (
    oid character varying(128) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    date date NOT NULL,
    is_present character varying(256) NOT NULL,
    CONSTRAINT ck_is_present_daily_attendance CHECK ((((is_present)::text = 'Yes'::text) OR ((is_present)::text = 'No'::text)))
);


ALTER TABLE hrm.daily_attendance OWNER TO grp;

--
-- Name: deputation_info; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.deputation_info (
    oid character varying(128) NOT NULL,
    type_ingoing character varying(256) NOT NULL,
    type_outgoing character varying(256) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    previous_ministry character varying(256) NOT NULL,
    deputed_ministry character varying(256) NOT NULL,
    previous_organization character varying(256) NOT NULL,
    deputed_organization character varying(256) NOT NULL,
    no_of_go character varying(256) NOT NULL,
    order_date date NOT NULL,
    start_date date NOT NULL,
    time_period numeric(20,2) NOT NULL
);


ALTER TABLE hrm.deputation_info OWNER TO grp;

--
-- Name: education_qualification; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.education_qualification (
    oid character varying(128) NOT NULL,
    employee_info_oid character varying(128) NOT NULL,
    degree_title character varying(256) NOT NULL,
    institution_name character varying(256) NOT NULL,
    principal_subject character varying(256) NOT NULL,
    result character varying(256) NOT NULL,
    year_of_passing character varying(256) NOT NULL,
    permission_acceptance_from_godata character varying(256) NOT NULL
);


ALTER TABLE hrm.education_qualification OWNER TO grp;

--
-- Name: employee; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.employee (
    oid character varying(128) NOT NULL,
    grp_username character varying(128) NOT NULL,
    name_en character varying(256) NOT NULL,
    name_bn character varying(256) NOT NULL,
    nick_name_en character varying(256) NOT NULL,
    nick_name_bn character varying(256) NOT NULL,
    father_name_en character varying(256) NOT NULL,
    father_name_bn character varying(256) NOT NULL,
    mother_name_en character varying(256) NOT NULL,
    mother_name_bn character varying(256) NOT NULL,
    gender character varying(32) NOT NULL,
    date_of_birth date NOT NULL,
    mobile_no character varying(256) NOT NULL,
    email character varying(256) NOT NULL,
    address character varying(512) NOT NULL,
    blood_group character varying(32) NOT NULL,
    nid character varying(32) NOT NULL,
    religion character varying(32) NOT NULL,
    marital_status character varying(32) NOT NULL,
    etin character varying(256) NOT NULL,
    bank_account_number character varying(256) NOT NULL,
    emergency_contact_number character varying(256),
    joining_at_present_post_on date NOT NULL,
    joining_at_present_workstation_on date NOT NULL,
    status character varying(32) NOT NULL,
    present_office_unit_post_oid character varying(128) NOT NULL,
    created_by character varying(128) DEFAULT 'System'::character varying NOT NULL,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone,
    CONSTRAINT ck_gender_employee CHECK ((((gender)::text = 'Male'::text) OR ((gender)::text = 'Female'::text) OR ((gender)::text = 'Other'::text))),
    CONSTRAINT ck_status_employee CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE hrm.employee OWNER TO grp;

--
-- Name: employee_evaluation_category_setup; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.employee_evaluation_category_setup (
    oid character varying(128) NOT NULL,
    acr_form_part_number numeric(20,2) NOT NULL,
    acr_form_part_title character varying(256) NOT NULL,
    acr_form_evaluation_category character varying(256) NOT NULL
);


ALTER TABLE hrm.employee_evaluation_category_setup OWNER TO grp;

--
-- Name: employee_in_queue_for_promotion_or_posting; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.employee_in_queue_for_promotion_or_posting (
    oid character varying(128) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    no_of_go character varying(256) NOT NULL
);


ALTER TABLE hrm.employee_in_queue_for_promotion_or_posting OWNER TO grp;

--
-- Name: employee_perf_summary; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.employee_perf_summary (
    oid character varying(128) NOT NULL,
    unit_weight_of_perf_metrics numeric(20,0) NOT NULL,
    performance_metrics character varying(256) NOT NULL
);


ALTER TABLE hrm.employee_perf_summary OWNER TO grp;

--
-- Name: exam_configuration; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.exam_configuration (
    oid character varying(128) NOT NULL,
    exam_type character varying(256) NOT NULL,
    total_marks numeric(20,2) NOT NULL,
    pass_marks numeric(20,2) NOT NULL,
    assigned_by character varying(256) NOT NULL
);


ALTER TABLE hrm.exam_configuration OWNER TO grp;

--
-- Name: exam_venue; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.exam_venue (
    oid character varying(128) NOT NULL,
    applicant_oid character varying(128) NOT NULL,
    shortlist_oid character varying(128) NOT NULL,
    district character varying(256) NOT NULL,
    venue character varying(256) NOT NULL,
    exam_oid character varying(128) NOT NULL
);


ALTER TABLE hrm.exam_venue OWNER TO grp;

--
-- Name: final_selection; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.final_selection (
    oid character varying(128) NOT NULL,
    candidate_oid character varying(128) NOT NULL,
    police_verification_attachment_url character varying(1024) NOT NULL,
    medical_verification_url character varying(1024) NOT NULL,
    remarks character varying(1024) NOT NULL
);


ALTER TABLE hrm.final_selection OWNER TO grp;

--
-- Name: foreign_training; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.foreign_training (
    oid character varying(128) NOT NULL,
    training_type_oid character varying(128) NOT NULL,
    training_title character varying(256) NOT NULL,
    institution character varying(256) NOT NULL,
    country character varying(256) NOT NULL,
    starting_from date NOT NULL,
    end_to date NOT NULL,
    sponsoring_agency character varying(256) NOT NULL
);


ALTER TABLE hrm.foreign_training OWNER TO grp;

--
-- Name: grade; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.grade (
    oid character varying(128) NOT NULL,
    name_en character varying(16) NOT NULL,
    name_bn date NOT NULL,
    sort_order numeric(5,0) NOT NULL,
    cadre_oid character varying(128) NOT NULL
);


ALTER TABLE hrm.grade OWNER TO grp;

--
-- Name: in_service_training; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.in_service_training (
    oid character varying(128) NOT NULL,
    training_type_oid character varying(128) NOT NULL,
    training_title character varying(256) NOT NULL,
    institution character varying(256) NOT NULL,
    location character varying(256) NOT NULL,
    starting_from date NOT NULL,
    end_to date NOT NULL,
    sponsoring_agency character varying(256)
);


ALTER TABLE hrm.in_service_training OWNER TO grp;

--
-- Name: investigation_team_feedback; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.investigation_team_feedback (
    oid character varying(128) NOT NULL,
    employee_oid character varying(256) NOT NULL,
    date_of_submission timestamp without time zone NOT NULL,
    evaluation_by_investigation_team numeric(20,0) NOT NULL,
    notes text,
    esign character varying(256) NOT NULL
);


ALTER TABLE hrm.investigation_team_feedback OWNER TO grp;

--
-- Name: investigation_team_setup; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.investigation_team_setup (
    oid character varying(128) NOT NULL,
    committee_oid character varying(128) NOT NULL,
    employee_oid character varying(256) NOT NULL,
    controlling_authority character varying(256) NOT NULL,
    appointing_authority character varying(256) NOT NULL,
    assign_investigator character varying(256) NOT NULL,
    esign character varying(256) NOT NULL
);


ALTER TABLE hrm.investigation_team_setup OWNER TO grp;

--
-- Name: investigation_under_appointing_auth; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.investigation_under_appointing_auth (
    oid character varying(128) NOT NULL,
    employee_oid character varying(256) NOT NULL,
    date_of_submission timestamp without time zone NOT NULL,
    evaluation_by_aa numeric(20,0) NOT NULL,
    decision_status character varying(256) NOT NULL,
    application_code character varying(128) NOT NULL,
    decision_by character varying(256) NOT NULL,
    notes text,
    esign character varying(256) NOT NULL
);


ALTER TABLE hrm.investigation_under_appointing_auth OWNER TO grp;

--
-- Name: investigation_under_controlling_auth; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.investigation_under_controlling_auth (
    oid character varying(128) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    date_of_submission timestamp without time zone NOT NULL,
    evaluation_by_ca numeric(20,0) NOT NULL,
    decision_status character varying(256) NOT NULL,
    application_code character varying(128) NOT NULL,
    decision_by character varying(256) NOT NULL,
    notes text,
    esign character varying(256) NOT NULL
);


ALTER TABLE hrm.investigation_under_controlling_auth OWNER TO grp;

--
-- Name: job_circular; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.job_circular (
    oid character varying(128) NOT NULL,
    post_oid character varying(128) NOT NULL,
    position_name character varying(256) NOT NULL,
    project character varying(256),
    number_of_position character varying(32) NOT NULL,
    job_type character varying(256) NOT NULL,
    educational_qualification character varying(256) NOT NULL,
    age_duration character varying(256) NOT NULL,
    application_fee character varying(256) NOT NULL,
    other_qualification character varying(256) NOT NULL,
    least_experience character varying(256) NOT NULL,
    preferable_district_to_attend_exam character varying(256) NOT NULL,
    date_of_exam timestamp without time zone NOT NULL
);


ALTER TABLE hrm.job_circular OWNER TO grp;

--
-- Name: job_circular_approval; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.job_circular_approval (
    oid character varying(128) NOT NULL,
    circular_oid character varying(128) NOT NULL,
    approved_by_oid character varying(128) NOT NULL,
    approved_by_post_oid character varying(256) NOT NULL,
    approval_code character varying(128) NOT NULL,
    approval_status character varying(32) NOT NULL,
    comments text,
    CONSTRAINT ck_approval_status_job_circular_approval CHECK ((((approval_status)::text = 'yes'::text) OR ((approval_status)::text = 'no'::text)))
);


ALTER TABLE hrm.job_circular_approval OWNER TO grp;

--
-- Name: justify_against_penalty; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.justify_against_penalty (
    oid character varying(128) NOT NULL,
    penalty_oid character varying(128) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    justification text NOT NULL,
    attachment_url character varying(1024) NOT NULL
);


ALTER TABLE hrm.justify_against_penalty OWNER TO grp;

--
-- Name: leave_approval_by_admin; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.leave_approval_by_admin (
    oid character varying(128) NOT NULL,
    leave_oid character varying(128) NOT NULL,
    application_oid character varying(128) NOT NULL,
    approval_status character varying(32) NOT NULL,
    approved_by_post character varying(256) NOT NULL,
    approved_by_oid character varying(128) NOT NULL,
    approval_code character varying(128) NOT NULL,
    comments text
);


ALTER TABLE hrm.leave_approval_by_admin OWNER TO grp;

--
-- Name: leave_approval_by_substitute; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.leave_approval_by_substitute (
    oid character varying(128) NOT NULL,
    leave_oid character varying(128) NOT NULL,
    substitute_oid character varying(128) NOT NULL,
    approved_from date NOT NULL,
    approved_to date NOT NULL,
    approval_status character varying(32) NOT NULL,
    reason text,
    substitute_approval_code character varying(128) NOT NULL,
    CONSTRAINT ck_approval_status_leave_approval_by_substitute CHECK ((((approval_status)::text = 'Yes'::text) OR ((approval_status)::text = 'No'::text)))
);


ALTER TABLE hrm.leave_approval_by_substitute OWNER TO grp;

--
-- Name: leave_setup; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.leave_setup (
    oid character varying(128) NOT NULL,
    leave_name character varying(128) NOT NULL,
    gender character varying(32) NOT NULL,
    no_of_time numeric(20,2) NOT NULL,
    duration numeric(20,2) NOT NULL,
    eligibility character varying(256) NOT NULL,
    description text,
    CONSTRAINT ck_eligibility_leave_setup CHECK ((((eligibility)::text = 'Yes'::text) OR ((eligibility)::text = 'No'::text))),
    CONSTRAINT ck_gender_leave_setup CHECK ((((gender)::text = 'Male'::text) OR ((gender)::text = 'Female'::text)))
);


ALTER TABLE hrm.leave_setup OWNER TO grp;

--
-- Name: offense_setup; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.offense_setup (
    oid character varying(128) NOT NULL,
    offense_title character varying(256) NOT NULL,
    offense_code numeric(20,0) NOT NULL,
    description text NOT NULL,
    start_date_of_offense date NOT NULL,
    end_date_of_offense date NOT NULL,
    go_number numeric(20,0) NOT NULL,
    esign character varying(256) NOT NULL
);


ALTER TABLE hrm.offense_setup OWNER TO grp;

--
-- Name: organisational_training; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.organisational_training (
    oid character varying(128) NOT NULL,
    is_mandatory character varying(32) NOT NULL,
    duration numeric(20,2) NOT NULL,
    training_type_oid character varying(128) NOT NULL,
    objective text,
    descriptions text NOT NULL,
    training_name character varying(256) NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    organization character varying(256) NOT NULL,
    designation_required text NOT NULL,
    education_required text NOT NULL,
    CONSTRAINT ck_is_mandatory_organisational_training CHECK ((((is_mandatory)::text = 'Yes'::text) OR ((is_mandatory)::text = 'No'::text)))
);


ALTER TABLE hrm.organisational_training OWNER TO grp;

--
-- Name: penalty_committee_list; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.penalty_committee_list (
    oid character varying(128) NOT NULL,
    attendance_committee_oid character varying(128) NOT NULL,
    committee_name character varying(256) NOT NULL
);


ALTER TABLE hrm.penalty_committee_list OWNER TO grp;

--
-- Name: penalty_for_attendance; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.penalty_for_attendance (
    oid character varying(128) NOT NULL,
    date date NOT NULL,
    employee_oid character varying(128) NOT NULL,
    cause text NOT NULL,
    action text NOT NULL,
    remarks text,
    penalty_code character varying(128) NOT NULL
);


ALTER TABLE hrm.penalty_for_attendance OWNER TO grp;

--
-- Name: pending_participation_confirmation; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.pending_participation_confirmation (
    oid character varying(128) NOT NULL,
    training_oid character varying(128) NOT NULL,
    trainee_oid character varying(128) NOT NULL,
    is_attendant character varying(32) NOT NULL,
    reason character varying(1024),
    CONSTRAINT ck_is_attendant_pending_participation_confirmation CHECK ((((is_attendant)::text = 'Yes'::text) OR ((is_attendant)::text = 'No'::text)))
);


ALTER TABLE hrm.pending_participation_confirmation OWNER TO grp;

--
-- Name: pension_procedure_for_employee; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.pension_procedure_for_employee (
    oid character varying(128) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    employee_post_oid character varying(256) NOT NULL,
    noc_upload character varying(1024) NOT NULL,
    ipc_upload character varying(1024) NOT NULL,
    date_of_retirement date NOT NULL,
    employed_from date NOT NULL,
    employed_upto date NOT NULL,
    bank_account_no numeric(20,0) NOT NULL,
    bank_name character varying(256) NOT NULL,
    bank_branch character varying(256) NOT NULL,
    application_code character varying(128) NOT NULL,
    decision_by character varying(256) NOT NULL,
    current_post_joining_date date NOT NULL,
    current_post_retirement_date date NOT NULL,
    prl_status character varying(256) NOT NULL,
    rate_of_retirement_allowance numeric(20,2) NOT NULL,
    total_amount_of_retirement_allowance numeric(20,2) NOT NULL,
    monthly_amount_of_retirement_allowance numeric(20,2) NOT NULL,
    total_gratuity numeric(20,2) NOT NULL,
    loan_history character varying(256) NOT NULL,
    notes character varying(256),
    pension_code character varying(128) NOT NULL,
    decision_status character varying(256) NOT NULL
);


ALTER TABLE hrm.pension_procedure_for_employee OWNER TO grp;

--
-- Name: posting_and_promotion_committee_setup; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.posting_and_promotion_committee_setup (
    oid character varying(128) NOT NULL,
    committee_name character varying(256) NOT NULL,
    short_description text NOT NULL,
    member_oid character varying(128) NOT NULL,
    role character varying(256) NOT NULL
);


ALTER TABLE hrm.posting_and_promotion_committee_setup OWNER TO grp;

--
-- Name: posting_info; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.posting_info (
    oid character varying(128) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    division character varying(200) NOT NULL,
    district character varying(200) NOT NULL,
    starting_from date NOT NULL,
    end_to date NOT NULL,
    last_posting date NOT NULL,
    pay_scale numeric(20,2) NOT NULL,
    no_of_go character varying(128) NOT NULL
);


ALTER TABLE hrm.posting_info OWNER TO grp;

--
-- Name: promotion_detail; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.promotion_detail (
    oid character varying(128) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    date_of_promotion date NOT NULL,
    rank character varying(256) NOT NULL,
    nature_of_promotion character varying(256) NOT NULL,
    pay_scale character varying(256) NOT NULL,
    no_of_go character varying(256) NOT NULL
);


ALTER TABLE hrm.promotion_detail OWNER TO grp;

--
-- Name: publication_application; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.publication_application (
    oid character varying(128) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    publication_title character varying(256) NOT NULL,
    attachments character varying(1024) NOT NULL,
    date_of_publication date NOT NULL,
    notes text,
    decision_status character varying(256) NOT NULL,
    application_code character varying(128) NOT NULL,
    decision_by character varying(256) NOT NULL
);


ALTER TABLE hrm.publication_application OWNER TO grp;

--
-- Name: publication_setup; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.publication_setup (
    oid character varying(128) NOT NULL,
    publication_title character varying(256) NOT NULL,
    publication_code text NOT NULL,
    description character varying(256) NOT NULL,
    department character varying(256) NOT NULL,
    notes text
);


ALTER TABLE hrm.publication_setup OWNER TO grp;

--
-- Name: recruitment_committee; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.recruitment_committee (
    oid character varying(128) NOT NULL,
    committee_oid character varying(128) NOT NULL,
    committee_name character varying(256) NOT NULL,
    short_description text NOT NULL,
    member_oid character varying(128) NOT NULL,
    member_name character varying(256) NOT NULL,
    designation character varying(256) NOT NULL,
    role character varying(256) NOT NULL
);


ALTER TABLE hrm.recruitment_committee OWNER TO grp;

--
-- Name: recruitment_result; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.recruitment_result (
    oid character varying(128) NOT NULL,
    attachment_url character varying(1024) NOT NULL,
    circular_oid character varying(128) NOT NULL,
    written_marks numeric(20,2) NOT NULL,
    viva_marks numeric(20,2) NOT NULL,
    applicant_oid character varying(128) NOT NULL
);


ALTER TABLE hrm.recruitment_result OWNER TO grp;

--
-- Name: role; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.role (
    oid character varying(128) NOT NULL,
    role_type character varying(16) NOT NULL,
    role_name_bn character varying(16) NOT NULL,
    role_name_en character varying(16) NOT NULL
);


ALTER TABLE hrm.role OWNER TO grp;

--
-- Name: rules_of_award_publication; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.rules_of_award_publication (
    oid character varying(128) NOT NULL,
    rules_and_regulations text,
    assigned_by character varying(128),
    assigned_on date
);


ALTER TABLE hrm.rules_of_award_publication OWNER TO grp;

--
-- Name: rules_of_vesting_magisterial_power; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.rules_of_vesting_magisterial_power (
    oid character varying(128) NOT NULL,
    rules_and_regulations text,
    assigned_by character varying(128),
    assigned_on date
);


ALTER TABLE hrm.rules_of_vesting_magisterial_power OWNER TO grp;

--
-- Name: rules_regarding_acr_apa; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.rules_regarding_acr_apa (
    oid character varying(128) NOT NULL,
    rules_and_regulations text,
    assigned_by character varying(128),
    assigned_on date
);


ALTER TABLE hrm.rules_regarding_acr_apa OWNER TO grp;

--
-- Name: rules_regarding_attendance; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.rules_regarding_attendance (
    oid character varying(128) NOT NULL,
    rules_and_regulations text,
    assigned_by character varying(128),
    assigned_on date
);


ALTER TABLE hrm.rules_regarding_attendance OWNER TO grp;

--
-- Name: rules_regarding_discipline; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.rules_regarding_discipline (
    oid character varying(128) NOT NULL,
    rules_and_regulations text,
    assigned_by character varying(128),
    assigned_on date
);


ALTER TABLE hrm.rules_regarding_discipline OWNER TO grp;

--
-- Name: rules_regarding_leave; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.rules_regarding_leave (
    oid character varying(128) NOT NULL,
    leave_type character varying(256),
    rules_and_regulations text,
    assigned_by character varying(128),
    assigned_on date
);


ALTER TABLE hrm.rules_regarding_leave OWNER TO grp;

--
-- Name: rules_regarding_posting; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.rules_regarding_posting (
    oid character varying(128) NOT NULL,
    rules_and_regulations text,
    assigned_by character varying(128),
    assigned_on date
);


ALTER TABLE hrm.rules_regarding_posting OWNER TO grp;

--
-- Name: rules_regarding_promotion; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.rules_regarding_promotion (
    oid character varying(128) NOT NULL,
    rules_and_regulations text,
    assigned_by character varying(128),
    assigned_on date
);


ALTER TABLE hrm.rules_regarding_promotion OWNER TO grp;

--
-- Name: rules_regarding_recruitment; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.rules_regarding_recruitment (
    oid character varying(128) NOT NULL,
    rules_and_regulations text,
    assigned_by character varying(128),
    assigned_on date
);


ALTER TABLE hrm.rules_regarding_recruitment OWNER TO grp;

--
-- Name: rules_regarding_training; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.rules_regarding_training (
    oid character varying(128) NOT NULL,
    training_type character varying(256),
    rules_and_regulations text,
    assigned_by character varying(128),
    assigned_on date
);


ALTER TABLE hrm.rules_regarding_training OWNER TO grp;

--
-- Name: select_candidate; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.select_candidate (
    oid character varying(128) NOT NULL,
    applicant_oid character varying(128) NOT NULL,
    circular_oid character varying(128) NOT NULL,
    is_selected character varying(200) NOT NULL,
    CONSTRAINT ck_is_selected_select_candidate CHECK ((((is_selected)::text = 'yes'::text) OR ((is_selected)::text = 'no'::text)))
);


ALTER TABLE hrm.select_candidate OWNER TO grp;

--
-- Name: service_history; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.service_history (
    oid character varying(128) NOT NULL,
    employee_info_oid character varying(128) NOT NULL,
    entry_to_govt_service_on date,
    gazetted_on date,
    encadrement_on date,
    type_of_cadre character varying(256)
);


ALTER TABLE hrm.service_history OWNER TO grp;

--
-- Name: survey_for_trainee; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.survey_for_trainee (
    oid character varying(128) NOT NULL,
    training_type_oid character varying(128) NOT NULL,
    trainer_detail_oid character varying(128) NOT NULL,
    trainee_id character varying(128) NOT NULL,
    survey_question_code character varying(128) NOT NULL,
    answer character varying(1000) NOT NULL
);


ALTER TABLE hrm.survey_for_trainee OWNER TO grp;

--
-- Name: survey_for_trainer; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.survey_for_trainer (
    oid character varying(128) NOT NULL,
    training_type_oid character varying(128) NOT NULL,
    trainer_detail_oid character varying(128) NOT NULL,
    trainer_code character varying(128) NOT NULL,
    survey_question_code character varying(128) NOT NULL,
    answer character varying(1024) NOT NULL
);


ALTER TABLE hrm.survey_for_trainer OWNER TO grp;

--
-- Name: survey_questionnaire_for_trainee; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.survey_questionnaire_for_trainee (
    oid character varying(128) NOT NULL,
    training_oid character varying(128) NOT NULL,
    question character varying(1000) NOT NULL
);


ALTER TABLE hrm.survey_questionnaire_for_trainee OWNER TO grp;

--
-- Name: survey_questionnaire_for_trainer; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.survey_questionnaire_for_trainer (
    oid character varying(128) NOT NULL,
    training_type_oid character varying(128) NOT NULL,
    trainer_detail_oid character varying(128) NOT NULL,
    question character varying(1024) NOT NULL
);


ALTER TABLE hrm.survey_questionnaire_for_trainer OWNER TO grp;

--
-- Name: termination; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.termination (
    oid character varying(128) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    employee_post_oid character varying(256) NOT NULL,
    date_of_termination date NOT NULL,
    reason character varying(256) NOT NULL,
    remarks text,
    go_code character varying(128) NOT NULL,
    goverment_order character varying(1024) NOT NULL,
    decision_status character varying(256) NOT NULL,
    application_code character varying(128) NOT NULL,
    decision_by character varying(256) NOT NULL
);


ALTER TABLE hrm.termination OWNER TO grp;

--
-- Name: trainee_result; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.trainee_result (
    oid character varying(128) NOT NULL,
    trainer character varying(128) NOT NULL,
    trainee character varying(128) NOT NULL,
    training character varying(128) NOT NULL,
    result character varying(100) NOT NULL
);


ALTER TABLE hrm.trainee_result OWNER TO grp;

--
-- Name: trainer_detail; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.trainer_detail (
    oid character varying(128) NOT NULL,
    trainer_oid character varying(256) NOT NULL,
    training_history character varying NOT NULL
);


ALTER TABLE hrm.trainer_detail OWNER TO grp;

--
-- Name: training_type; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.training_type (
    oid character varying(128) NOT NULL,
    type_name character varying(256) NOT NULL,
    type_code character varying(256) NOT NULL,
    type_description text NOT NULL,
    principal_department character varying(256) NOT NULL
);


ALTER TABLE hrm.training_type OWNER TO grp;

--
-- Name: vesting_of_magisterial_power; Type: TABLE; Schema: hrm; Owner: grp
--

CREATE TABLE hrm.vesting_of_magisterial_power (
    oid character varying(128) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    area_of_work character varying(256) NOT NULL,
    work_description text NOT NULL,
    reason_of_vesting character varying(256) NOT NULL,
    substitute_department character varying(256) NOT NULL,
    substitute_code character varying(128) NOT NULL,
    duration numeric(20,0) NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    go_code character varying(128) NOT NULL,
    government_order character varying(1024) NOT NULL,
    notes text
);


ALTER TABLE hrm.vesting_of_magisterial_power OWNER TO grp;

--
-- Name: annual_procurement_plan; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.annual_procurement_plan (
    oid character varying(128) NOT NULL,
    app_code character varying(128),
    status character varying(128),
    fiscal_year_oid character varying(128),
    project_oid character varying(128),
    budget_type_oid character varying(128),
    office_oid character varying(128),
    created_by character varying(128) DEFAULT 'System'::character varying,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_by character varying(128),
    updated_on timestamp without time zone,
    CONSTRAINT ck_status_annual_procurement_plan CHECK ((((status)::text = 'Draft'::text) OR ((status)::text = 'Final'::text)))
);


ALTER TABLE prc.annual_procurement_plan OWNER TO grp;

--
-- Name: annual_procurement_plan_detail; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.annual_procurement_plan_detail (
    oid character varying(128) NOT NULL,
    app_detail_code character varying(256) NOT NULL,
    approving_authority character varying(256) NOT NULL,
    app_oid character varying(128) NOT NULL,
    fiscal_year_oid character varying(128) NOT NULL,
    budget_type_oid character varying(128) NOT NULL,
    project_name character varying(256) NOT NULL,
    app_code character varying(128) NOT NULL,
    package_no character varying(256) NOT NULL,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE prc.annual_procurement_plan_detail OWNER TO grp;

--
-- Name: annual_procurement_plan_reviewer; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.annual_procurement_plan_reviewer (
    oid character varying(128) NOT NULL,
    employee_info_oid character varying(128) NOT NULL,
    procurement_role character varying(256) NOT NULL,
    description text,
    created_by character varying(128) DEFAULT 'System'::character varying NOT NULL,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE prc.annual_procurement_plan_reviewer OWNER TO grp;

--
-- Name: app_package; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.app_package (
    oid character varying(128) NOT NULL,
    package_type_oid character varying(128),
    package_no character varying(128),
    package_description character varying(512),
    package_unit character varying,
    package_quantity numeric(20,0) DEFAULT 0,
    procurement_method_oid character varying(128),
    procurement_nature_oid character varying(128),
    procurement_type_oid character varying(128),
    contract_approving_authority_oid character varying(128),
    source_of_fund character varying(256),
    estimate_cost numeric(20,6) DEFAULT 0,
    tender_advertise_date date,
    tender_opening_date date,
    tender_evaluation_date date,
    approval_to_award_date date,
    notification_of_award_date date,
    signing_of_contract_date date,
    contract_signature_day numeric(20,0) DEFAULT 0,
    contract_completion_day numeric(20,0) DEFAULT 0,
    created_by character varying(128) DEFAULT 'System'::character varying,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_by character varying(128),
    updated_on timestamp without time zone,
    annual_procurement_plan_oid character varying(128),
    item_category_oid character varying(256)
);


ALTER TABLE prc.app_package OWNER TO grp;

--
-- Name: approving_authority; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.approving_authority (
    oid character varying(128) NOT NULL,
    code character varying(256),
    name_en character varying(256),
    name_bn character varying(256)
);


ALTER TABLE prc.approving_authority OWNER TO grp;

--
-- Name: attachment; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.attachment (
    oid character varying(128) NOT NULL,
    file_code character varying(256) NOT NULL,
    file_name_en character varying(256) NOT NULL,
    file_name_bn character varying(256) NOT NULL,
    file_path character varying(1024) NOT NULL,
    is_delete character varying(32) NOT NULL,
    created_by character varying(128) DEFAULT 'System'::character varying NOT NULL,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL,
    CONSTRAINT ck_is_delete_attachment CHECK ((((is_delete)::text = 'Yes'::text) OR ((is_delete)::text = 'No'::text)))
);


ALTER TABLE prc.attachment OWNER TO grp;

--
-- Name: budget_type; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.budget_type (
    oid character varying(128) NOT NULL,
    code character varying(128) NOT NULL,
    name_en character varying(256) NOT NULL,
    name_bn character varying(256) NOT NULL,
    description text,
    created_by character varying(128) DEFAULT 'System'::character varying NOT NULL,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone,
    CONSTRAINT ck_name_en_budget_type CHECK ((((name_en)::text = 'Development'::text) OR ((name_en)::text = 'Revenue'::text) OR ((name_en)::text = 'Own Fund'::text)))
);


ALTER TABLE prc.budget_type OWNER TO grp;

--
-- Name: chalan; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.chalan (
    oid character varying(128) NOT NULL,
    remarks text,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL,
    pakage_lot_oid character varying(128) NOT NULL
);


ALTER TABLE prc.chalan OWNER TO grp;

--
-- Name: committee_member; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.committee_member (
    oid character varying(128) NOT NULL,
    code character varying(128) NOT NULL,
    name_en character varying(256) NOT NULL,
    name_bn character varying(256) NOT NULL,
    organization character varying(256) NOT NULL,
    responsibility character varying(256) NOT NULL,
    employee_oid character varying(128) NOT NULL,
    created_by character varying(128) DEFAULT 'System'::character varying NOT NULL,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone
);


ALTER TABLE prc.committee_member OWNER TO grp;

--
-- Name: consolidate_requisition; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.consolidate_requisition (
    oid character varying(128) NOT NULL,
    status character varying(256) NOT NULL,
    created_by character varying(128) DEFAULT 'System'::character varying NOT NULL,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone,
    office_oid character varying(128),
    fiscal_year_oid character varying(128),
    requisition_declaration_oid character varying(128),
    CONSTRAINT ck_status_consolidate_requisition CHECK ((((status)::text = 'Draft'::text) OR ((status)::text = 'Consolidated'::text)))
);


ALTER TABLE prc.consolidate_requisition OWNER TO grp;

--
-- Name: consolidate_requisition_detail; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.consolidate_requisition_detail (
    oid character varying(128) NOT NULL,
    required_quantity numeric(20,6) DEFAULT 0,
    approved_quantity numeric(20,6) DEFAULT 0,
    unit character varying(256),
    remarks text,
    item_category_oid character varying(128),
    item_oid character varying(128),
    consolidate_requisition_oid character varying(128)
);


ALTER TABLE prc.consolidate_requisition_detail OWNER TO grp;

--
-- Name: debarment; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.debarment (
    oid character varying(128) NOT NULL,
    vendor_oid character varying(128) NOT NULL,
    reported_by character varying(128) NOT NULL,
    approved_by character varying(128) NOT NULL,
    debarment_from date NOT NULL,
    debarment_to date NOT NULL
);


ALTER TABLE prc.debarment OWNER TO grp;

--
-- Name: item_purchase; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.item_purchase (
    oid character varying(128) NOT NULL,
    contract_amount numeric(20,6) DEFAULT 0 NOT NULL,
    completion_date date NOT NULL,
    contract_period numeric(20,6) DEFAULT 0 NOT NULL,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL,
    schedule_sale_oid character varying(128) NOT NULL,
    employee_info_oid character varying(128) NOT NULL,
    vendor_oid character varying(128) NOT NULL
);


ALTER TABLE prc.item_purchase OWNER TO grp;

--
-- Name: material_receive_info; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.material_receive_info (
    oid character varying(128) NOT NULL,
    material_receive_info_code character varying(128) NOT NULL,
    vendor_oid character varying(128) NOT NULL,
    temporary_store_oid character varying(128) NOT NULL,
    chalan_info character varying(128) NOT NULL,
    item_purchase_oid character varying(128) NOT NULL
);


ALTER TABLE prc.material_receive_info OWNER TO grp;

--
-- Name: notification_of_award; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.notification_of_award (
    oid character varying(128) NOT NULL,
    ministry_name character varying(128) NOT NULL,
    agency_name character varying(256) NOT NULL,
    tender_id character varying(128) NOT NULL,
    package_no character varying(256) NOT NULL,
    reference_description character varying(128) NOT NULL,
    consultant_name character varying(256) NOT NULL,
    consultant_address character varying(512) NOT NULL,
    date_of_publication date NOT NULL,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE prc.notification_of_award OWNER TO grp;

--
-- Name: package_lot; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.package_lot (
    oid character varying(128) NOT NULL,
    package_lot_code character varying(128) NOT NULL,
    app_package_oid character varying(128) NOT NULL,
    item_oid character varying(128) NOT NULL,
    lot_no character varying(256) NOT NULL,
    lot_description character varying(256),
    unit character varying(256) NOT NULL,
    quantity numeric(20,2) DEFAULT 0,
    estimate_cost numeric(20,6) DEFAULT 0 NOT NULL,
    remarks text,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE prc.package_lot OWNER TO grp;

--
-- Name: package_type; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.package_type (
    oid character varying(128) NOT NULL,
    code character varying(256),
    name_en character varying(256),
    name_bn character varying(256),
    description text
);


ALTER TABLE prc.package_type OWNER TO grp;

--
-- Name: procurement_contract; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.procurement_contract (
    completion_date date NOT NULL,
    contract_period numeric(20,6) NOT NULL,
    comments text,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE prc.procurement_contract OWNER TO grp;

--
-- Name: procurement_item; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.procurement_item (
    oid character varying(128) NOT NULL,
    code character varying(128) NOT NULL,
    name_en character varying(256) NOT NULL,
    name_bn character varying(256) NOT NULL,
    hs_code character varying(256) NOT NULL,
    is_avilable character varying(256) NOT NULL,
    required_quantity numeric(20,6) DEFAULT 0 NOT NULL,
    item_category_oid character varying(128) NOT NULL,
    item_oid character varying(128) NOT NULL,
    uom_oid character varying(128) NOT NULL,
    created_by character varying(128) DEFAULT 'System'::character varying NOT NULL,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL,
    CONSTRAINT ck_is_avilable_procurement_item CHECK ((((is_avilable)::text = 'Available'::text) OR ((is_avilable)::text = 'Not available'::text)))
);


ALTER TABLE prc.procurement_item OWNER TO grp;

--
-- Name: procurement_method; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.procurement_method (
    oid character varying(128) NOT NULL,
    name_en character varying(256) NOT NULL,
    name_bn character varying(256) NOT NULL,
    description text,
    CONSTRAINT ck_name_en_procurement_method CHECK ((((name_en)::text = 'OTM'::text) OR ((name_en)::text = 'LTM'::text) OR ((name_en)::text = 'QCBS'::text) OR ((name_en)::text = 'RFQ'::text) OR ((name_en)::text = 'OSTETM'::text) OR ((name_en)::text = 'DPM'::text) OR ((name_en)::text = 'TSTM'::text) OR ((name_en)::text = 'RFP'::text) OR ((name_en)::text = 'SBCQ'::text) OR ((name_en)::text = 'SSS'::text)))
);


ALTER TABLE prc.procurement_method OWNER TO grp;

--
-- Name: procurement_nature; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.procurement_nature (
    oid character varying(128) NOT NULL,
    name_en character varying(256) NOT NULL,
    name_bn character varying(256) NOT NULL,
    description text,
    CONSTRAINT ck_name_en_procurement_nature CHECK ((((name_en)::text = 'Goods'::text) OR ((name_en)::text = 'Works'::text) OR ((name_en)::text = 'Services'::text)))
);


ALTER TABLE prc.procurement_nature OWNER TO grp;

--
-- Name: procurement_project; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.procurement_project (
    oid character varying(128) NOT NULL,
    project_code character varying(128) NOT NULL,
    name_en character varying(256) NOT NULL,
    name_bn character varying(256) NOT NULL,
    description character varying(256),
    start_date date NOT NULL,
    end_date date NOT NULL,
    commitment_amount numeric(20,6) DEFAULT 0 NOT NULL,
    status character varying NOT NULL,
    created_by character varying(128) DEFAULT 'System'::character varying NOT NULL,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone,
    approval_date date NOT NULL,
    CONSTRAINT ck_status_procurement_project CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Closed'::text)))
);


ALTER TABLE prc.procurement_project OWNER TO grp;

--
-- Name: procurement_type; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.procurement_type (
    oid character varying(128) NOT NULL,
    name_en character varying(256) NOT NULL,
    name_bn character varying(256) NOT NULL,
    description text,
    CONSTRAINT ck_name_en_procurement_type CHECK ((((name_en)::text = 'NCT'::text) OR ((name_en)::text = 'ICT'::text)))
);


ALTER TABLE prc.procurement_type OWNER TO grp;

--
-- Name: qc_committee; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.qc_committee (
    oid character varying(128) NOT NULL,
    code character varying(128) NOT NULL,
    authorized_by character varying(128) NOT NULL,
    name_en character varying(256) NOT NULL,
    name_bn character varying(256) NOT NULL,
    min_no_member numeric(20,6) NOT NULL,
    max_no_member numeric(20,6) NOT NULL,
    forming_date date NOT NULL,
    purpose text NOT NULL,
    created_by character varying(128) DEFAULT 'System'::character varying NOT NULL,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE prc.qc_committee OWNER TO grp;

--
-- Name: qc_info; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.qc_info (
    oid character varying(128) NOT NULL,
    qc_detail_code character varying(256) NOT NULL,
    qc_info_oid character varying(256) NOT NULL,
    quantity numeric(20,6) DEFAULT 0 NOT NULL,
    "position" character varying(256) NOT NULL,
    comment character varying(256) NOT NULL,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE prc.qc_info OWNER TO grp;

--
-- Name: qc_list; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.qc_list (
    oid character varying(128) NOT NULL,
    qc_code character varying(256) NOT NULL,
    item_category_oid character varying(128) NOT NULL,
    item_oid character varying(256) NOT NULL,
    item_uom_oid character varying(128) NOT NULL,
    quantity numeric(20,6) DEFAULT 0 NOT NULL,
    "position" character varying(256) NOT NULL,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE prc.qc_list OWNER TO grp;

--
-- Name: qc_report; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.qc_report (
    oid character varying(128) NOT NULL,
    qc_code character varying(256) NOT NULL,
    temporary_store_oid character varying(128) NOT NULL,
    qc_committee_oid character varying(128) NOT NULL,
    inspection_time date NOT NULL,
    created_by character varying(128) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE prc.qc_report OWNER TO grp;

--
-- Name: requisition_declaration; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.requisition_declaration (
    oid character varying(128) NOT NULL,
    start_date date,
    end_date date,
    status character varying(32) NOT NULL,
    description text,
    declared_by character varying(128) DEFAULT 'System'::character varying,
    office_oid character varying(128),
    fiscal_year_oid character varying(256),
    created_by character varying(128) DEFAULT 'System'::character varying NOT NULL,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone,
    CONSTRAINT ck_status_requisition_declaration CHECK ((((status)::text = 'Draft'::text) OR ((status)::text = 'Published'::text) OR ((status)::text = 'Closed'::text) OR ((status)::text = 'Consolidating'::text) OR ((status)::text = 'Consolidated'::text)))
);


ALTER TABLE prc.requisition_declaration OWNER TO grp;

--
-- Name: requisition_detail; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.requisition_detail (
    oid character varying(128) NOT NULL,
    quantity numeric(20,6) DEFAULT 0,
    unit character varying(256),
    remarks text,
    specification text,
    item_oid character varying(256) NOT NULL,
    item_category_oid character varying(128) NOT NULL,
    requisition_oid character varying(128) NOT NULL
);


ALTER TABLE prc.requisition_detail OWNER TO grp;

--
-- Name: requisition_information; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.requisition_information (
    oid character varying(128) NOT NULL,
    requisition_date date,
    remarks text,
    status character varying(128) NOT NULL,
    created_by character varying(128) DEFAULT 'System'::character varying NOT NULL,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone,
    requisition_declaration_oid character varying(128) NOT NULL,
    office_oid character varying(128),
    CONSTRAINT ck_status_requisition_information CHECK ((((status)::text = 'Draft'::text) OR ((status)::text = 'Final'::text)))
);


ALTER TABLE prc.requisition_information OWNER TO grp;

--
-- Name: reviewer; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.reviewer (
    oid character varying(128) NOT NULL,
    name_en character varying(128) NOT NULL,
    name_bn character varying(128) NOT NULL,
    description character varying(256),
    employee_oid character varying(128) NOT NULL,
    created_by character varying(128) DEFAULT 'System'::character varying NOT NULL,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE prc.reviewer OWNER TO grp;

--
-- Name: schedule_info; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.schedule_info (
    oid character varying(128) NOT NULL,
    app_package_oid character varying(128) NOT NULL,
    expected_date_of_advert date NOT NULL,
    date_of_submission date NOT NULL,
    submission_date_of_evolution_report date NOT NULL,
    approval_date_of_award_of_contract date NOT NULL,
    issue_date_of_notification_of_award date NOT NULL,
    date_of_completion_contract date NOT NULL,
    total_days_of_completion numeric(20,6) DEFAULT 0 NOT NULL,
    description text
);


ALTER TABLE prc.schedule_info OWNER TO grp;

--
-- Name: schedule_sale; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.schedule_sale (
    oid character varying(128) NOT NULL,
    schedule_sale_code character varying(128) NOT NULL,
    schedule_info_oid character varying(128) NOT NULL,
    employee_info_oid character varying(128) NOT NULL,
    vendor_oid character varying(128) NOT NULL,
    payment_received_by character varying(128) NOT NULL,
    paid_amount numeric(20,6) DEFAULT 0 NOT NULL,
    purchase_date date NOT NULL,
    security_deposit numeric(20,6) DEFAULT 0 NOT NULL,
    payment_method character varying(32) NOT NULL,
    pay_order_no character varying(256) NOT NULL,
    pay_order_bank_name character varying(256) NOT NULL,
    description text,
    CONSTRAINT ck_payment_method_schedule_sale CHECK ((((payment_method)::text = 'Cash'::text) OR ((payment_method)::text = 'PayOrder'::text)))
);


ALTER TABLE prc.schedule_sale OWNER TO grp;

--
-- Name: temporary_store; Type: TABLE; Schema: prc; Owner: grp
--

CREATE TABLE prc.temporary_store (
    oid character varying(128) NOT NULL,
    store_code character varying(128) NOT NULL,
    office_name_en character varying(256) NOT NULL,
    office_name_bn character varying(256) NOT NULL,
    store_name_en character varying(256) NOT NULL,
    store_name_bn character varying(256) NOT NULL,
    store_location character varying(256) NOT NULL,
    is_active character varying(256) NOT NULL,
    store_type character varying(256) NOT NULL,
    created_by character varying(128) DEFAULT 'System'::character varying NOT NULL,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by character varying(128),
    updated_on timestamp without time zone NOT NULL,
    CONSTRAINT ck_is_active_temporary_store CHECK ((((is_active)::text = 'Yes'::text) OR ((is_active)::text = 'No'::text)))
);


ALTER TABLE prc.temporary_store OWNER TO grp;

--
-- Name: flyway_schema_history; Type: TABLE; Schema: public; Owner: grp
--

CREATE TABLE public.flyway_schema_history (
    installed_rank integer NOT NULL,
    version character varying(50),
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


ALTER TABLE public.flyway_schema_history OWNER TO grp;

--
-- Name: oauth_access_token; Type: TABLE; Schema: public; Owner: grp
--

CREATE TABLE public.oauth_access_token (
    token_id character varying(255),
    token bytea,
    authentication_id character varying(255),
    user_name character varying(255),
    client_id character varying(255),
    authentication bytea,
    refresh_token character varying(255)
);


ALTER TABLE public.oauth_access_token OWNER TO grp;

--
-- Name: oauth_client_details; Type: TABLE; Schema: public; Owner: grp
--

CREATE TABLE public.oauth_client_details (
    client_id character varying(256) NOT NULL,
    resource_ids character varying(256),
    client_secret character varying(256),
    scope character varying(256),
    authorized_grant_types character varying(256),
    web_server_redirect_uri character varying(256),
    authorities character varying(256),
    access_token_validity numeric(5,0),
    refresh_token_validity numeric(5,0),
    additional_information text,
    autoapprove character varying(256)
);


ALTER TABLE public.oauth_client_details OWNER TO grp;

--
-- Name: oauth_client_token; Type: TABLE; Schema: public; Owner: grp
--

CREATE TABLE public.oauth_client_token (
    token_id character varying(255),
    token bytea,
    authentication_id character varying(255),
    user_name character varying(255),
    client_id character varying(255)
);


ALTER TABLE public.oauth_client_token OWNER TO grp;

--
-- Name: oauth_code; Type: TABLE; Schema: public; Owner: grp
--

CREATE TABLE public.oauth_code (
    code character varying(255),
    authentication bytea
);


ALTER TABLE public.oauth_code OWNER TO grp;

--
-- Name: oauth_refresh_token; Type: TABLE; Schema: public; Owner: grp
--

CREATE TABLE public.oauth_refresh_token (
    token_id character varying(255),
    token bytea,
    authentication bytea
);


ALTER TABLE public.oauth_refresh_token OWNER TO grp;

--
-- Name: component; Type: TABLE; Schema: sec; Owner: grp
--

CREATE TABLE sec.component (
    oid character varying(128) NOT NULL,
    name_en character varying(128) NOT NULL,
    name_bn character varying(256) NOT NULL,
    status character varying(32) DEFAULT 'Active'::character varying NOT NULL,
    url character varying(256),
    subfeature_oid character varying(128),
    CONSTRAINT ck_status_component CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE sec.component OWNER TO grp;

--
-- Name: component_resource; Type: TABLE; Schema: sec; Owner: grp
--

CREATE TABLE sec.component_resource (
    oid character varying(128) NOT NULL,
    status character varying(32) DEFAULT 'Active'::character varying NOT NULL,
    resource_oid character varying(128) NOT NULL,
    component_oid character varying(128) NOT NULL,
    CONSTRAINT ck_status_component_resource CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE sec.component_resource OWNER TO grp;

--
-- Name: grp_feature; Type: TABLE; Schema: sec; Owner: grp
--

CREATE TABLE sec.grp_feature (
    oid character varying(128) NOT NULL,
    name_en character varying(128) NOT NULL,
    name_bn character varying(256) NOT NULL,
    status character varying(32) DEFAULT 'Active'::character varying NOT NULL,
    sort_order numeric(5,0) NOT NULL,
    sub_module_oid character varying(128) NOT NULL,
    CONSTRAINT ck_status_grp_feature CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE sec.grp_feature OWNER TO grp;

--
-- Name: grp_module; Type: TABLE; Schema: sec; Owner: grp
--

CREATE TABLE sec.grp_module (
    oid character varying(128) NOT NULL,
    name_en character varying(128) NOT NULL,
    name_bn character varying(256) NOT NULL,
    status character varying(32) DEFAULT 'Active'::character varying NOT NULL,
    module_type character varying(32) NOT NULL,
    sort_order numeric(5,0) NOT NULL,
    CONSTRAINT ck_module_type_grp_module CHECK ((((module_type)::text = 'General'::text) OR ((module_type)::text = 'Business'::text))),
    CONSTRAINT ck_status_grp_module CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE sec.grp_module OWNER TO grp;

--
-- Name: grp_role; Type: TABLE; Schema: sec; Owner: grp
--

CREATE TABLE sec.grp_role (
    oid character varying(128) NOT NULL,
    name_en character varying(256) NOT NULL,
    name_bn character varying(128) NOT NULL,
    role_type character varying(64) DEFAULT 'Business'::character varying,
    description_en text,
    description_bn text,
    menu_json text DEFAULT '{}'::text NOT NULL,
    CONSTRAINT ck_role_type_grp_role CHECK ((((role_type)::text = 'System'::text) OR ((role_type)::text = 'Service'::text) OR ((role_type)::text = 'Business'::text)))
);


ALTER TABLE sec.grp_role OWNER TO grp;

--
-- Name: grp_role_permission; Type: TABLE; Schema: sec; Owner: grp
--

CREATE TABLE sec.grp_role_permission (
    oid character varying(128) NOT NULL,
    component_oid character varying(128) NOT NULL,
    grp_role_oid character varying(128) NOT NULL
);


ALTER TABLE sec.grp_role_permission OWNER TO grp;

--
-- Name: grp_subfeature; Type: TABLE; Schema: sec; Owner: grp
--

CREATE TABLE sec.grp_subfeature (
    oid character varying(128) NOT NULL,
    name_en character varying(128) NOT NULL,
    name_bn character varying(256) NOT NULL,
    status character varying(32) DEFAULT 'Active'::character varying NOT NULL,
    sort_order numeric(5,0),
    feature_oid character varying(128),
    CONSTRAINT ck_status_grp_subfeature CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE sec.grp_subfeature OWNER TO grp;

--
-- Name: grp_submodule; Type: TABLE; Schema: sec; Owner: grp
--

CREATE TABLE sec.grp_submodule (
    oid character varying(128) NOT NULL,
    name_en character varying(128) NOT NULL,
    name_bn character varying(256) NOT NULL,
    status character varying(32) DEFAULT 'Active'::character varying NOT NULL,
    sort_order numeric(5,0) NOT NULL,
    module_oid character varying(128) NOT NULL,
    CONSTRAINT ck_status_grp_submodule CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE sec.grp_submodule OWNER TO grp;

--
-- Name: grp_user; Type: TABLE; Schema: sec; Owner: grp
--

CREATE TABLE sec.grp_user (
    oid character varying(128) NOT NULL,
    username character varying(256) NOT NULL,
    password character varying(128) NOT NULL,
    status character varying(32) NOT NULL,
    account_expired character varying(32) DEFAULT 'No'::character varying NOT NULL,
    credentials_expired character varying(32) DEFAULT 'No'::character varying NOT NULL,
    account_locked character varying(32) DEFAULT 'No'::character varying NOT NULL,
    CONSTRAINT ck_account_expired_grp_user CHECK ((((account_expired)::text = 'Yes'::text) OR ((account_expired)::text = 'No'::text))),
    CONSTRAINT ck_account_locked_grp_user CHECK ((((account_locked)::text = 'Yes'::text) OR ((account_locked)::text = 'No'::text))),
    CONSTRAINT ck_credentials_expired_grp_user CHECK ((((credentials_expired)::text = 'Yes'::text) OR ((credentials_expired)::text = 'No'::text))),
    CONSTRAINT ck_status_grp_user CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE sec.grp_user OWNER TO grp;

--
-- Name: grp_user_role; Type: TABLE; Schema: sec; Owner: grp
--

CREATE TABLE sec.grp_user_role (
    oid character varying(128) NOT NULL,
    menu_json text NOT NULL,
    default_status character varying(128) NOT NULL,
    grp_role_oid character varying(128) NOT NULL,
    grp_user_oid character varying(128) NOT NULL
);


ALTER TABLE sec.grp_user_role OWNER TO grp;

--
-- Name: office_module; Type: TABLE; Schema: sec; Owner: grp
--

CREATE TABLE sec.office_module (
    oid character varying(128) NOT NULL,
    status character varying(32) DEFAULT 'Active'::character varying NOT NULL,
    module_oid character varying(128) NOT NULL,
    office_oid character varying(128) NOT NULL,
    CONSTRAINT ck_status_office_module CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE sec.office_module OWNER TO grp;

--
-- Name: resource; Type: TABLE; Schema: sec; Owner: grp
--

CREATE TABLE sec.resource (
    oid character varying(128) NOT NULL,
    end_point character varying(128) NOT NULL,
    status character varying(32) DEFAULT 'Active'::character varying NOT NULL,
    service_oid character varying(128) NOT NULL,
    CONSTRAINT ck_status_resource CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE sec.resource OWNER TO grp;

--
-- Name: service; Type: TABLE; Schema: sec; Owner: grp
--

CREATE TABLE sec.service (
    oid character varying(128) NOT NULL,
    name_en character varying(128) NOT NULL,
    name_bn character varying(256),
    status character varying(32) DEFAULT 'Active'::character varying NOT NULL,
    CONSTRAINT ck_status_service CHECK ((((status)::text = 'Active'::text) OR ((status)::text = 'Inactive'::text)))
);


ALTER TABLE sec.service OWNER TO grp;

--
-- Data for Name: acquisition_billing; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.acquisition_billing (oid, acquisition_billing_code, supplier_oid, approved_by, approval_date, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: acquisition_billing_line; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.acquisition_billing_line (oid, asset_oid, acquisition_billing_oid, price) FROM stdin;
\.


--
-- Data for Name: asset_allocation; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.asset_allocation (oid, asset_allocation_code, asset_oid, allocation_date, distributor_oid, store_oid, end_user_oid, asset_requisition_oid, document_oid) FROM stdin;
\.


--
-- Data for Name: asset_complaint_letter; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.asset_complaint_letter (oid, complaint_letter_code, vendor_oid, description_en, description_bn, committee_oid, committee_note_en, committee_note_bn) FROM stdin;
\.


--
-- Data for Name: asset_disposal; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.asset_disposal (oid, asset_disposal_request_code, asset_oid, requester_oid, requested_date, cause_details, store_oid, committee_oid, committee_notes, disposal_process, cost, decision_by, decision_date, status, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: asset_information; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.asset_information (oid, code, description, status, tagged_by, tagged_on, temporary_item_oid, item_oid, office_oid, created_by, created_on, updated_by, updated_on) FROM stdin;
20190728-174929-fOQTjQu7rh55P79	1564314569947	\N	Ready for tagging	\N	\N	20190724-164730-rztLidOhLnvL0aG	14	143	shiropa	2019-07-29 11:41:42.042	shiropa	2019-07-29 11:41:42.042
20190725-162916-zwbL35gydyOEmHi	1564050556776	\N	Available	shiropa	2019-07-25 16:33:17.282	20190724-124840-6mDRg7PSmb8XAwD	12	143	shiropa	2019-07-25 16:31:20.508	shiropa	2019-07-25 16:33:17.282
20190729-112050-GzzfTz62EePq6wa	1564377650737	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.214	shiropa	2019-07-29 11:44:53.214
20190729-112050-xNAGfpjBFK1U1Qh	1564377650737	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.215	shiropa	2019-07-29 11:44:53.215
20190728-164536-mWoaZf7LNUNXJRa	1564310736870	\N	Available	shiropa	2019-07-28 16:47:39.105	20190728-164437-BDSgr7HEiguTArf	14	143	shiropa	2019-07-28 16:46:27.428	shiropa	2019-07-28 16:47:39.105
20190728-164536-tgXXsdk7XQOpnlR	1564310736871	\N	Available	shiropa	2019-07-28 16:47:44.758	20190728-164437-BDSgr7HEiguTArf	14	143	shiropa	2019-07-28 16:46:27.429	shiropa	2019-07-28 16:47:44.758
20190729-120342-Y34rgPd56bhQCLX	1564380222255	\N	Draft	\N	\N	20190724-165346-4teRYvHk1LPwnoW	17	143	shaon	2019-07-29 12:03:42.255	\N	\N
20190730-131203-gM2oWJIBbrREo8E	1564470723033	\N	Ready for tagging	\N	\N	20190724-164939-mEVAYn3J6EouFbA	17	143	shiropa	2019-07-30 16:06:11.659	shiropa	2019-07-30 16:06:11.659
20190730-131221-mobNb3x6aX3OQD1	1564470741313	\N	Ready for tagging	\N	\N	20190730-114951-tR10pznW8jvcJ3r	15	143	shiropa	2019-07-30 16:09:28.989	shiropa	2019-07-30 16:09:28.989
20190730-131706-gXMOGLeRasztSDl	1564471026801	dont buy this	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.292	tanim	2019-07-31 12:09:04.292
20190730-131221-UrW8H054L8WSPs3	1564470741312	\N	Available	shiropa	2019-07-30 16:11:00.771	20190730-114951-tR10pznW8jvcJ3r	15	143	shiropa	2019-07-30 16:09:28.99	shiropa	2019-07-30 16:11:00.771
20190731-120643-4l7UD5mzXXueSEd	1564553203815	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.815	\N	\N
20190731-120643-6NGAWKHdEa5KqaC	1564553203815	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.815	\N	\N
20190731-120643-xQ8NFvNYHdiqdcK	1564553203815	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.815	\N	\N
20190729-112050-RGIDNlJw7iY6foo	1564377650738	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.215	shiropa	2019-07-29 11:44:53.215
20190729-112050-PeNSjv2YFiXLs7z	1564377650738	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.215	shiropa	2019-07-29 11:44:53.215
20190731-120643-CWvFYqdHqmETtWn	1564553203815	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.815	\N	\N
20190731-120643-pvuqDvH3sZWidCS	1564553203816	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.816	\N	\N
20190731-120643-Pwwgurn5XJxfnG9	1564553203816	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.816	\N	\N
20190731-120643-TJ14gUW5ue253eE	1564553203816	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.816	\N	\N
20190729-110317-5QgED95HXA8BtQi	1564376597098	\N	Available	shiropa	2019-07-29 11:51:53.357	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.821	shiropa	2019-07-29 11:51:53.357
20190729-110317-5dIJhnQTh0r6vvC	1564376597098	\N	Available	shiropa	2019-07-29 11:51:53.359	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.821	shiropa	2019-07-29 11:51:53.359
20190729-110317-babArO1rJvhxYpy	1564376597097	\N	Available	shiropa	2019-07-29 11:51:53.361	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.821	shiropa	2019-07-29 11:51:53.361
20190729-110317-bAye0URSiuLnQhH	1564376597096	\N	Available	shiropa	2019-07-29 11:51:53.362	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.82	shiropa	2019-07-29 11:51:53.362
20190731-120643-hZtCxTYy2lgtUfH	1564553203816	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.816	\N	\N
20190730-131706-7KzFe7MvTs8gZ9E	1564471026802	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.292	tanim	2019-07-31 12:09:04.292
20190730-131706-2Eb3YtD7Gqoca6u	1564471026802	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.293	tanim	2019-07-31 12:09:04.293
20190730-133139-EvdDDt3wGwCFhNX	1564471899537	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.537	\N	\N
20190730-133139-NYJOE3XgzndKGwY	1564471899537	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.537	\N	\N
20190730-133139-CSmhBkOkQ7jlkml	1564471899538	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.538	\N	\N
20190730-133139-OKs3KUOyrwIUorC	1564471899538	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.538	\N	\N
20190730-133139-Ir5R3uXn8rney3l	1564471899538	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.538	\N	\N
20190730-133139-jVseKUBE9u7PbfF	1564471899538	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.538	\N	\N
20190730-133139-6bWkWuOG3sNC78L	1564471899538	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.538	\N	\N
20190730-133139-2r83mrRMDFV1WkY	1564471899539	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.539	\N	\N
20190730-133139-KbDSwsKBfvgyd3Q	1564471899539	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.539	\N	\N
20190730-133139-YzBMqUCPpi5fRlL	1564471899539	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.539	\N	\N
20190730-133139-7scdUTp63cT3j2W	1564471899539	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.539	\N	\N
20190730-133139-mV06aj5N1nvYfEP	1564471899539	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.539	\N	\N
20190730-133139-snA0mEmaWBdJ1hC	1564471899542	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.542	\N	\N
20190730-133139-oj4xxbWYTMeascG	1564471899542	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.542	\N	\N
20190730-133139-f5hlb4oGAZ5jhD0	1564471899542	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.542	\N	\N
20190730-133139-NPKnmrAVF6LQivd	1564471899542	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.542	\N	\N
20190730-133139-d21kTbUT93gJRv6	1564471899543	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.543	\N	\N
20190730-133139-TKeJyvy7pVJNjvO	1564471899543	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.543	\N	\N
20190730-133139-EUl0EVsIJs3TaMs	1564471899543	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.543	\N	\N
20190730-133139-1VybpKSkKwo2q1l	1564471899543	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.543	\N	\N
20190729-112050-evjIv9VA62XHkMC	1564377650738	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.215	shiropa	2019-07-29 11:44:53.215
20190729-112050-vquH8RHSYujnqrx	1564377650738	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.216	shiropa	2019-07-29 11:44:53.216
20190729-112050-9r1kSsl145LnlPA	1564377650739	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.216	shiropa	2019-07-29 11:44:53.216
20190729-112050-yiZ35iC7OANSoSQ	1564377650739	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.216	shiropa	2019-07-29 11:44:53.216
20190729-112050-VD80YBRAZVevvwA	1564377650739	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.216	shiropa	2019-07-29 11:44:53.216
20190729-112050-g46UX6qmNIhU4Y9	1564377650739	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.216	shiropa	2019-07-29 11:44:53.216
20190729-112050-iHnGdLWsavj3QOv	1564377650739	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.217	shiropa	2019-07-29 11:44:53.217
20190729-112050-N9e3TOza0uWYWgW	1564377650740	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.217	shiropa	2019-07-29 11:44:53.217
20190729-112050-29hYigAZd5KRpUs	1564377650740	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.217	shiropa	2019-07-29 11:44:53.217
20190729-112050-YZafvccRIzhZPYt	1564377650740	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.217	shiropa	2019-07-29 11:44:53.217
20190729-112050-hqXZBdmjaaLjrUS	1564377650740	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.217	shiropa	2019-07-29 11:44:53.217
20190729-112050-BmqdXdYPfZJmLnn	1564377650741	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.217	shiropa	2019-07-29 11:44:53.217
20190729-112050-WSyzMNZ3ZmVT3qe	1564377650741	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.218	shiropa	2019-07-29 11:44:53.218
20190729-112050-evbQqI9YPYhLexC	1564377650741	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.218	shiropa	2019-07-29 11:44:53.218
20190729-112050-gTPj3EZPpPiB8Hq	1564377650741	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.218	shiropa	2019-07-29 11:44:53.218
20190729-112050-CHZKqs1Ljssgj7z	1564377650742	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.218	shiropa	2019-07-29 11:44:53.218
20190730-133139-xoYYmRyqFFd7dKy	1564471899544	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.544	\N	\N
20190730-133139-2kZkws3ePrVQZLB	1564471899544	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.544	\N	\N
20190730-133139-1lJ5FgjeW8b38lO	1564471899544	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.544	\N	\N
20190730-133139-zFq4JTgmVOMhz9d	1564471899544	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.544	\N	\N
20190730-133139-XgtWT79U0h6UMnH	1564471899545	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.545	\N	\N
20190730-133139-rQICymm3NnPtlQH	1564471899545	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.545	\N	\N
20190730-133139-0OBEywU8MZg9GP9	1564471899545	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.545	\N	\N
20190730-133139-rhbiF36xaks3cry	1564471899545	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.545	\N	\N
20190730-133139-xgUfuBlLUW8e2W6	1564471899546	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.546	\N	\N
20190730-133139-xwAYggiS4fJzbRa	1564471899546	\N	Draft	\N	\N	20190730-132459-RhQWIpeOc7LcGae	16	143	shaon	2019-07-30 13:31:39.546	\N	\N
20190731-120643-SFk3uipduttxWZI	1564553203816	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.816	\N	\N
20190731-120643-rHPr2LAxxpA6UZ6	1564553203817	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.817	\N	\N
20190731-120643-wua2F6pjKGykzY7	1564553203817	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.817	\N	\N
20190731-120643-eTPtLM3Tw6GFQBq	1564553203817	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.817	\N	\N
20190731-120643-TpQCoc2LykFH6Vp	1564553203817	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.817	\N	\N
20190731-120643-XpPbOytEagG2uLH	1564553203818	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.818	\N	\N
20190729-112050-f1M1FDerdIkYSqn	1564377650742	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.218	shiropa	2019-07-29 11:44:53.218
20190729-112050-diDUC1OgQg9BCTQ	1564377650742	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.219	shiropa	2019-07-29 11:44:53.219
20190729-112050-TlNpDGU5rDBwtt6	1564377650742	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.219	shiropa	2019-07-29 11:44:53.219
20190729-112050-Odq2iu3pHQZSA6U	1564377650742	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.219	shiropa	2019-07-29 11:44:53.219
20190729-112050-YAtSt7TxP1u1oFl	1564377650743	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.219	shiropa	2019-07-29 11:44:53.219
20190729-112050-KT1vibU6sK02hbj	1564377650743	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.219	shiropa	2019-07-29 11:44:53.219
20190729-112050-1WlZzCC7LbXFhcr	1564377650743	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.22	shiropa	2019-07-29 11:44:53.22
20190729-112050-OtUarI8NQbjyQWE	1564377650743	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.22	shiropa	2019-07-29 11:44:53.22
20190729-112050-EjjXGHFuYYNeeJG	1564377650743	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.22	shiropa	2019-07-29 11:44:53.22
20190729-112050-J4mkz8t2J8IaesE	1564377650744	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.22	shiropa	2019-07-29 11:44:53.22
20190729-112050-v8nmytGTNUY1TUm	1564377650744	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.22	shiropa	2019-07-29 11:44:53.22
20190729-112050-leYrVPvBFH6ZXH1	1564377650744	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.22	shiropa	2019-07-29 11:44:53.22
20190729-112050-PyWPrHr0hs97ySc	1564377650744	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.221	shiropa	2019-07-29 11:44:53.221
20190729-112050-O4GounCDkEXTOXW	1564377650744	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.221	shiropa	2019-07-29 11:44:53.221
20190729-112050-smxUoxjX0Rg1H9m	1564377650744	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.221	shiropa	2019-07-29 11:44:53.221
20190729-112050-IIImTCKeaf17Yye	1564377650745	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.221	shiropa	2019-07-29 11:44:53.221
20190729-112050-4lcY0aT7LCgc8iX	1564377650745	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.221	shiropa	2019-07-29 11:44:53.221
20190729-112050-ZEA7QzJJJiOvakI	1564377650745	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.222	shiropa	2019-07-29 11:44:53.222
20190729-112050-Kadl09o7YVWonZP	1564377650745	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.222	shiropa	2019-07-29 11:44:53.222
20190729-112050-EvcRUsmNEfKW1Yb	1564377650746	\N	Ready for tagging	\N	\N	20190729-111511-TEEUc5bYCIzeaKp	17	143	shiropa	2019-07-29 11:44:53.222	shiropa	2019-07-29 11:44:53.222
20190731-120643-cRr5ceqpZssrY2w	1564553203799	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.799	\N	\N
20190731-120643-BALFBVjjDwsYKPP	1564553203800	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.8	\N	\N
20190729-110317-xGSR6Nvg82cUZqk	1564376597108	\N	Available	shiropa	2019-07-29 11:51:53.314	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.826	shiropa	2019-07-29 11:51:53.314
20190729-110317-SG5kHmoLFCeVNqX	1564376597108	\N	Available	shiropa	2019-07-29 11:51:53.316	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.826	shiropa	2019-07-29 11:51:53.316
20190729-110317-dN1HJwUXJgJDJee	1564376597108	\N	Available	shiropa	2019-07-29 11:51:53.317	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.826	shiropa	2019-07-29 11:51:53.317
20190729-110317-CTolG8NdtW0DW2T	1564376597108	\N	Available	shiropa	2019-07-29 11:51:53.319	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.826	shiropa	2019-07-29 11:51:53.319
20190729-110317-EgM3mSgM4rlzIwn	1564376597108	\N	Available	shiropa	2019-07-29 11:51:53.321	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.826	shiropa	2019-07-29 11:51:53.321
20190729-110317-UnMiLbyJbRHJecC	1564376597107	\N	Available	shiropa	2019-07-29 11:51:53.322	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.825	shiropa	2019-07-29 11:51:53.322
20190729-110317-pTNwuxyqm4NES7C	1564376597107	\N	Available	shiropa	2019-07-29 11:51:53.324	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.825	shiropa	2019-07-29 11:51:53.324
20190729-110317-mZpdtzVqG5PmL2c	1564376597107	\N	Available	shiropa	2019-07-29 11:51:53.327	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.825	shiropa	2019-07-29 11:51:53.327
20190729-110317-QihewkkmxbR0mbR	1564376597106	\N	Available	shiropa	2019-07-29 11:51:53.329	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.825	shiropa	2019-07-29 11:51:53.329
20190729-110317-UKzBILXFp45ft34	1564376597106	\N	Available	shiropa	2019-07-29 11:51:53.331	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.825	shiropa	2019-07-29 11:51:53.331
20190729-110317-KDXwplT9mEcyhRT	1564376597106	\N	Available	shiropa	2019-07-29 11:51:53.332	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.824	shiropa	2019-07-29 11:51:53.332
20190729-110317-lmPiPzMlqsJD6RS	1564376597106	\N	Available	shiropa	2019-07-29 11:51:53.334	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.824	shiropa	2019-07-29 11:51:53.334
20190729-110317-WckY0inSbIc4zr4	1564376597105	\N	Available	shiropa	2019-07-29 11:51:53.336	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.824	shiropa	2019-07-29 11:51:53.336
20190729-110317-gnrKRzsVNTGaw18	1564376597103	\N	Available	shiropa	2019-07-29 11:51:53.337	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.824	shiropa	2019-07-29 11:51:53.337
20190729-110317-IaqpderPvVl9svl	1564376597103	\N	Available	shiropa	2019-07-29 11:51:53.339	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.824	shiropa	2019-07-29 11:51:53.339
20190729-110317-vtrDktmwXod39pz	1564376597102	\N	Available	shiropa	2019-07-29 11:51:53.341	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.823	shiropa	2019-07-29 11:51:53.341
20190729-110317-EMRvKKMMxr2b8Lb	1564376597102	\N	Available	shiropa	2019-07-29 11:51:53.343	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.823	shiropa	2019-07-29 11:51:53.343
20190729-110317-aSksGfVbykj101L	1564376597102	\N	Available	shiropa	2019-07-29 11:51:53.344	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.823	shiropa	2019-07-29 11:51:53.344
20190729-110317-v5KQSRtxHYGPp4F	1564376597102	\N	Available	shiropa	2019-07-29 11:51:53.346	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.823	shiropa	2019-07-29 11:51:53.346
20190729-110317-Kcy2Kcz0W2qKrWu	1564376597101	\N	Available	shiropa	2019-07-29 11:51:53.348	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.823	shiropa	2019-07-29 11:51:53.348
20190729-110317-ndnGxjk63KSrPoe	1564376597100	\N	Available	shiropa	2019-07-29 11:51:53.349	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.822	shiropa	2019-07-29 11:51:53.349
20190729-110317-Sx0QwlCpZwis9yq	1564376597099	\N	Available	shiropa	2019-07-29 11:51:53.352	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.822	shiropa	2019-07-29 11:51:53.352
20190729-110317-fIUQv7Z229Nlf2j	1564376597099	\N	Available	shiropa	2019-07-29 11:51:53.354	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.822	shiropa	2019-07-29 11:51:53.354
20190729-110317-4HqGDXc2nYG7K07	1564376597099	\N	Available	shiropa	2019-07-29 11:51:53.356	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.821	shiropa	2019-07-29 11:51:53.356
20190729-110317-Er7gvXBOMOW72Dx	1564376597111	\N	Available	shiropa	2019-07-29 11:51:53.294	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.828	shiropa	2019-07-29 11:51:53.294
20190729-110317-ioLJjCQflMTi6Mo	1564376597110	\N	Available	shiropa	2019-07-29 11:51:53.296	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.828	shiropa	2019-07-29 11:51:53.296
20190729-110317-HHuVvjytkZCcwq2	1564376597110	\N	Available	shiropa	2019-07-29 11:51:53.298	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.828	shiropa	2019-07-29 11:51:53.298
20190729-110317-WdAv81ABHW8EErz	1564376597110	\N	Available	shiropa	2019-07-29 11:51:53.3	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.828	shiropa	2019-07-29 11:51:53.3
20190729-110317-mC8WYQyt3WZuvyB	1564376597110	\N	Available	shiropa	2019-07-29 11:51:53.302	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.828	shiropa	2019-07-29 11:51:53.302
20190729-110317-l8XLncvYedqD6lY	1564376597110	\N	Available	shiropa	2019-07-29 11:51:53.304	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.827	shiropa	2019-07-29 11:51:53.304
20190729-110317-XxURFlVLl5sBbgD	1564376597109	\N	Available	shiropa	2019-07-29 11:51:53.306	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.827	shiropa	2019-07-29 11:51:53.306
20190729-110317-XJDTB4Ilsf8qrWJ	1564376597109	\N	Available	shiropa	2019-07-29 11:51:53.308	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.827	shiropa	2019-07-29 11:51:53.308
20190729-110317-QyHp0HOfeLseJfr	1564376597109	\N	Available	shiropa	2019-07-29 11:51:53.31	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.827	shiropa	2019-07-29 11:51:53.31
20190729-110317-nCIkOyazt8o7Pkc	1564376597109	\N	Available	shiropa	2019-07-29 11:51:53.312	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.827	shiropa	2019-07-29 11:51:53.312
20190729-110317-rPGmohUfUGvL2qk	1564376597107	\N	Available	shiropa	2019-07-29 11:51:53.325	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.825	shiropa	2019-07-29 11:51:53.325
20190729-110317-Hv9lnTKDOZ22HlG	1564376597100	\N	Available	shiropa	2019-07-29 11:51:53.351	20190729-110124-8Bt6Dflq4STsoYr	14	143	shiropa	2019-07-29 11:45:13.822	shiropa	2019-07-29 11:51:53.351
20190731-120643-b7eDVKQOvgX4G6M	1564553203800	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.8	\N	\N
20190731-120643-f2Fk3WfvfD5ivVh	1564553203801	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.801	\N	\N
20190731-120643-SMVG5WxdMUgvrLb	1564553203801	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.801	\N	\N
20190731-120643-tdX1dc71SIp4X0r	1564553203801	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.801	\N	\N
20190731-120643-47rreIQrQH0ddhX	1564553203801	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.801	\N	\N
20190731-120643-5btT2aLQrFQmA6l	1564553203802	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.802	\N	\N
20190731-120643-dQ5BnMYYhNScs9p	1564553203802	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.802	\N	\N
20190731-120643-FW8N3D58gXNEDVr	1564553203802	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.802	\N	\N
20190731-120643-NyziJiJ8WiSHL2o	1564553203802	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.802	\N	\N
20190731-120643-rk90JMK7tKU2SQK	1564553203803	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.803	\N	\N
20190731-120643-3xyOWUU4lkmyEJ0	1564553203808	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.808	\N	\N
20190731-120643-Ic880g6wTg4OLp8	1564553203808	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.808	\N	\N
20190731-120643-kYOSKT7ULcWTGam	1564553203808	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.808	\N	\N
20190731-120643-Pzwvoa6kNK7JBu6	1564553203808	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.808	\N	\N
20190731-120643-OBEDyDWFlnmHj4X	1564553203809	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.809	\N	\N
20190731-120643-cPRpEzHvzq5vgeP	1564553203809	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.809	\N	\N
20190731-120643-XEyC7OcoKn2dPJH	1564553203809	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.809	\N	\N
20190731-120643-P5RAB3uKK0A3OTP	1564553203809	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.809	\N	\N
20190731-120643-Edc7SJuGVetWm5T	1564553203810	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.81	\N	\N
20190731-120643-r8RJzGGJrCMUvGw	1564553203810	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.81	\N	\N
20190731-120643-3RjJCa6wjQS3xYx	1564553203810	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.81	\N	\N
20190731-120643-DwBgBvsNj3z3ENJ	1564553203810	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.81	\N	\N
20190731-120643-hTZXWJaj4t4TBdn	1564553203810	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.81	\N	\N
20190731-120643-MkTPK0ZiknzDhMt	1564553203811	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.811	\N	\N
20190731-120643-P3C4ngAeYsZWBI0	1564553203811	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.811	\N	\N
20190731-120643-j1cwFlkFv85hUDy	1564553203811	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.811	\N	\N
20190731-120643-2IX2uACeziq6ZWE	1564553203811	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.811	\N	\N
20190731-120643-5YHIgVbk7XG4QkS	1564553203812	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.812	\N	\N
20190731-120643-dZQWKbFqwnfDftU	1564553203812	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.812	\N	\N
20190731-120643-8vlXLPCBPBJhiNj	1564553203812	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.812	\N	\N
20190731-120643-vTDM5hBKW4ebDpB	1564553203812	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.812	\N	\N
20190731-120643-gJMITcjH9laojkJ	1564553203812	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.812	\N	\N
20190731-120643-gMLWFyvawcJ0ii0	1564553203813	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.813	\N	\N
20190731-120643-9b4oO7W6m377Ryt	1564553203813	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.813	\N	\N
20190731-120643-sRCtLGlF90Y1EIi	1564553203813	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.813	\N	\N
20190731-120643-AsgxK4AI9tcacBQ	1564553203813	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.813	\N	\N
20190731-120643-akLACwbG7D37ND6	1564553203814	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.814	\N	\N
20190731-120643-1Ybi7mhY6Yy8XDK	1564553203814	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.814	\N	\N
20190731-120643-RVZPGyjtQ4xjkwQ	1564553203814	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.814	\N	\N
20190731-120643-qgKWam0QDIX0k94	1564553203814	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.814	\N	\N
20190731-120643-Lr4Ei1mvOo2Y8T5	1564553203818	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.818	\N	\N
20190731-120643-6UgjTROzxe18mj3	1564553203818	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.818	\N	\N
20190731-120643-WR8BOYNVSJQFFA7	1564553203818	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.818	\N	\N
20190731-120643-puYMdTqikrzj2Fr	1564553203819	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.819	\N	\N
20190731-120643-k8J0U2AxEqw6Jfo	1564553203819	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.819	\N	\N
20190731-120643-fFY6H8Zjlrifz9S	1564553203819	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.819	\N	\N
20190731-120643-b8eYSMyM3n3dLIY	1564553203819	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.819	\N	\N
20190731-120643-zSrlA3LcVTFEStx	1564553203819	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.819	\N	\N
20190731-120643-MM2aI9E7CZ3Kinc	1564553203820	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.82	\N	\N
20190731-120643-7mpgdwjNvihlQqR	1564553203820	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.82	\N	\N
20190731-120643-pWFHwqEkHzOnsKa	1564553203820	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.82	\N	\N
20190731-120643-Hu0PVTGAG1hzJrl	1564553203820	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.82	\N	\N
20190731-120643-pCGkc5tsAkeFL5c	1564553203821	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.821	\N	\N
20190731-120643-HYNPz38rzD9awdB	1564553203821	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.821	\N	\N
20190731-120643-ohEYRFSq5T0dLJM	1564553203821	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.821	\N	\N
20190731-120643-MbCBNYw61PFEeFF	1564553203821	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.821	\N	\N
20190731-120643-tlpoUld2hG0sfs9	1564553203821	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.821	\N	\N
20190731-120643-f5yfgD2YMS0zy6j	1564553203822	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.822	\N	\N
20190731-120643-H11gt05s1Uzsc0k	1564553203822	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.822	\N	\N
20190731-120643-Ya0sN8H63edZd87	1564553203822	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.822	\N	\N
20190731-120643-qP4rXohpWsdJ5VE	1564553203822	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.822	\N	\N
20190731-120643-eAMDFaFw2oGkKVU	1564553203823	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.823	\N	\N
20190731-120643-niuD8JG4B1TtNma	1564553203823	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.823	\N	\N
20190731-120643-mU421G6hVTSKrkq	1564553203823	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.823	\N	\N
20190731-120643-VHbLmE6e5CNg0AG	1564553203823	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.823	\N	\N
20190731-120643-vgPJlsJIk69wWe5	1564553203823	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.823	\N	\N
20190731-120643-D97dcUF8vWi4z79	1564553203824	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.824	\N	\N
20190731-120643-wv478jsncMTuoyf	1564553203824	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.824	\N	\N
20190731-120643-SvxbleBssOEHjN3	1564553203824	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.824	\N	\N
20190731-120643-d8gVIn9HOSJf3U6	1564553203824	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.824	\N	\N
20190731-120643-VRny4panoBF1B9Z	1564553203824	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.824	\N	\N
20190731-120643-TcLpUn9hez6Xf8x	1564553203825	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.825	\N	\N
20190731-120643-dCeFplLbJn7D6oX	1564553203825	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.825	\N	\N
20190731-120643-BI000xYNAA0LdSf	1564553203825	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.825	\N	\N
20190731-120643-ATEZWWHCH5IGxi0	1564553203825	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.825	\N	\N
20190731-120643-pjLFYMCVf76OsBf	1564553203826	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.826	\N	\N
20190731-120643-nyakg4hdrkgv3QH	1564553203826	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.826	\N	\N
20190731-120643-CMekiG0QnRcz1uf	1564553203826	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.826	\N	\N
20190731-120643-aZhnetu9czexlzQ	1564553203826	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.826	\N	\N
20190731-120643-9z24GCbDsXXzqJf	1564553203826	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.826	\N	\N
20190731-120643-2lxurj0mt0pi6kk	1564553203827	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.827	\N	\N
20190731-120643-9a1JAlCedFqXWx6	1564553203827	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.827	\N	\N
20190731-120643-LBkKxbVnVodenbl	1564553203827	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.827	\N	\N
20190731-120643-8sEKO5dWJK1zRsQ	1564553203827	\N	Draft	\N	\N	20190730-153825-YI3YIbbTKezh5aH	14	143	tanim	2019-07-31 12:06:43.827	\N	\N
20190731-120704-JyIhVM31ntzBrXp	1564553224272	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.272	\N	\N
20190731-120704-Ls7MgO1HKXyj1oh	1564553224273	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.273	\N	\N
20190731-120704-YNrhzXjipe1uSUN	1564553224273	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.273	\N	\N
20190731-120704-7yNKNJ3Ge3weZcy	1564553224273	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.273	\N	\N
20190731-120704-PioGMwhjqFj7mFX	1564553224274	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.274	\N	\N
20190731-120704-n7WDe34b3BzVsiN	1564553224274	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.274	\N	\N
20190731-120704-YsaFbaJtdfkgbfP	1564553224274	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.274	\N	\N
20190731-120704-beXnZqi6UhuclUE	1564553224274	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.274	\N	\N
20190731-120704-qXyHQYKe6r25h7M	1564553224274	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.274	\N	\N
20190731-120704-Xdz4NPmpMf3wBoC	1564553224277	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.277	\N	\N
20190731-120704-DNKNUXuXYBT7H0K	1564553224278	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.278	\N	\N
20190731-120704-4d8BsSiuD6zz9Fk	1564553224278	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.278	\N	\N
20190731-120704-CcwFjSfPUF5zuBS	1564553224278	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.278	\N	\N
20190731-120704-e2HwrBczDRRRUdC	1564553224278	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.278	\N	\N
20190731-120704-8oNSgAa2fbYWOLj	1564553224279	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.279	\N	\N
20190731-120704-AJdGfcuHEuYja20	1564553224279	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.279	\N	\N
20190731-120704-JsRCDwLgBGz79Qu	1564553224279	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.279	\N	\N
20190731-120704-tutX1r0JIa3SoCC	1564553224279	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.279	\N	\N
20190731-120704-WKUrVt9yWfsFAZp	1564553224279	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.279	\N	\N
20190731-120704-r9vK86whGNg13E6	1564553224280	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.28	\N	\N
20190731-120704-DGXnW9xWlU3I6a7	1564553224280	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.28	\N	\N
20190731-120704-7PEO3jadrKursfD	1564553224280	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.28	\N	\N
20190731-120704-HJKKAhjQIntFxfY	1564553224280	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.28	\N	\N
20190731-120704-iAw232VeCUQoUo7	1564553224280	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.28	\N	\N
20190731-120704-AiyJpstYCVz3wRy	1564553224281	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.281	\N	\N
20190731-120704-AIxcqIi1ACZM7Cq	1564553224281	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.281	\N	\N
20190731-120704-ALmeAY13vaeYYbI	1564553224281	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.281	\N	\N
20190731-120704-knzF4uDlauUfZFg	1564553224281	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.281	\N	\N
20190731-120704-tQmyACy8elIy0LX	1564553224282	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.282	\N	\N
20190731-120704-gRz1Z51PNVV9A0m	1564553224282	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.282	\N	\N
20190731-120704-0X5p15zVNRBYBAO	1564553224282	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.282	\N	\N
20190731-120704-JDuCqPCK3ewLbkZ	1564553224282	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.282	\N	\N
20190731-120704-1COSKMrLrsndCa3	1564553224282	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.282	\N	\N
20190731-120704-f2pHsWbq7eLAmKf	1564553224283	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.283	\N	\N
20190731-120704-V9lOsGSGnsuNvQP	1564553224283	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.283	\N	\N
20190731-120704-OkwRjnchlEdxFxB	1564553224283	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.283	\N	\N
20190731-120704-Ufn3f4lhxt8meuh	1564553224283	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.283	\N	\N
20190731-120704-7GQq8EEaaXuFNvc	1564553224283	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.283	\N	\N
20190731-120704-d67ThHwqkNRbt71	1564553224283	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.283	\N	\N
20190731-120704-B6eBcNU2PW0jg12	1564553224284	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.284	\N	\N
20190731-120704-uBsHPMMNV54XYGS	1564553224284	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.284	\N	\N
20190731-120704-AfDrt89Jj5tGS2n	1564553224284	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.284	\N	\N
20190731-120704-lyQoWMTEFwGn0QR	1564553224284	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.284	\N	\N
20190731-120704-lta3kUOTPezlhSB	1564553224284	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.284	\N	\N
20190731-120704-guoUFWSqEHs7pOp	1564553224285	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.285	\N	\N
20190731-120704-iOJTzayYhnokdFD	1564553224285	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.285	\N	\N
20190731-120704-dFJ84IgJpMIiPa4	1564553224285	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.285	\N	\N
20190731-120704-UUIFH4BqmYLFlWW	1564553224285	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.285	\N	\N
20190731-120704-5FlN2VQ29pIOtza	1564553224285	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.285	\N	\N
20190731-120704-KWgiqHBNhX7JAWH	1564553224286	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.286	\N	\N
20190731-120704-98EuwcBpSSxSMtW	1564553224286	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.286	\N	\N
20190731-120704-mFKViFni76xlCkC	1564553224286	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.286	\N	\N
20190731-120704-HHbnVtrZSaID9XW	1564553224286	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.286	\N	\N
20190731-120704-dscor49CBmVUUkt	1564553224286	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.286	\N	\N
20190731-120704-G5b3D2AZjEgVNF7	1564553224287	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.287	\N	\N
20190731-120704-30JPlAkYilRsV4L	1564553224287	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.287	\N	\N
20190731-120704-KdcNcuut2dvYiwn	1564553224287	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.287	\N	\N
20190731-120704-ELC7rblFWJCoRtA	1564553224287	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.287	\N	\N
20190731-120704-wuy5h9epixz3S7w	1564553224287	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.287	\N	\N
20190731-120704-73wuomIc6Q1BGce	1564553224288	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.288	\N	\N
20190731-120704-pJlui7MRWGeGQwJ	1564553224288	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.288	\N	\N
20190731-120704-SyieXFzYbfQsGjO	1564553224288	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.288	\N	\N
20190731-120704-HBqlGU7asX4Oib0	1564553224288	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.288	\N	\N
20190731-120704-1XkWbR8yEjOVylW	1564553224288	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.288	\N	\N
20190731-120704-0froVSlhib8HwEk	1564553224289	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.289	\N	\N
20190731-120704-kLagt9B4tqovFaV	1564553224289	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.289	\N	\N
20190731-120704-AmS8FQoKGO5hlZ0	1564553224289	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.289	\N	\N
20190731-120704-hiaxszsnh9DmdFX	1564553224289	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.289	\N	\N
20190731-120704-8puDCFidAXWuVnc	1564553224289	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.289	\N	\N
20190731-120704-nUpiJhzcAB3C3uN	1564553224289	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.289	\N	\N
20190731-120704-6vssDW3ZxqWgGft	1564553224290	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.29	\N	\N
20190731-120704-hPMqkKKiku9a3bm	1564553224290	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.29	\N	\N
20190731-120704-1fLfhAxD55r7gwg	1564553224290	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.29	\N	\N
20190731-120704-wNshBkSwAas5PGX	1564553224290	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.29	\N	\N
20190731-120704-kThjXMV3aXGPhAw	1564553224290	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.29	\N	\N
20190731-120704-YCWNkLNBk8Jg3E1	1564553224291	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.291	\N	\N
20190731-120704-0lsY0b5vOn0h5EA	1564553224291	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.291	\N	\N
20190731-120704-FWmKKSqya8WOGgH	1564553224291	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.291	\N	\N
20190731-120704-tICOwYtnMEs0fT5	1564553224291	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.291	\N	\N
20190731-120704-pjHjQfJ2sWGUMKm	1564553224291	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.291	\N	\N
20190731-120704-Vi6eKkMpFm4uGhh	1564553224292	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.292	\N	\N
20190731-120704-RCglmyAltgQ8Vk5	1564553224292	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.292	\N	\N
20190731-120704-aK6A2IIDv5DPNPJ	1564553224292	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.292	\N	\N
20190731-120704-pp0laqut3vXV44I	1564553224292	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.292	\N	\N
20190731-120704-fLy9lxNgRwXqL4g	1564553224292	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.292	\N	\N
20190731-120704-2xIK0wQg5AjXcL2	1564553224293	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.293	\N	\N
20190731-120704-7PTIKHZdzT69lG7	1564553224293	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.293	\N	\N
20190731-120704-8VqiUC60227xWcB	1564553224293	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.293	\N	\N
20190731-120704-7SWAFZeveod9xvb	1564553224293	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.293	\N	\N
20190731-120704-kGExKNpUbgzgfhx	1564553224293	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.293	\N	\N
20190731-120704-g79qW8JObXm6qB0	1564553224293	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.293	\N	\N
20190731-120704-IumZDak8iMZUnHT	1564553224294	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.294	\N	\N
20190731-120704-XsRhY20btHJLsAC	1564553224294	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.294	\N	\N
20190731-120704-OKwgRn1HvhCNBSz	1564553224294	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.294	\N	\N
20190731-120704-8PxVBRbq0iIl79r	1564553224294	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.294	\N	\N
20190731-120704-adsTIZkCgml3F5Q	1564553224294	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.294	\N	\N
20190731-120704-jhJ550MCyy8Y6cS	1564553224295	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.295	\N	\N
20190731-120704-BGKWDTytoalnINN	1564553224295	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.295	\N	\N
20190731-120704-RlcHtLUsmvoxznQ	1564553224295	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.295	\N	\N
20190731-120704-nY2RVNy7gCd066Y	1564553224295	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.295	\N	\N
20190731-120704-DG5ZlXiMfgvZzSD	1564553224295	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.295	\N	\N
20190731-120704-pNcank4Ojiyio4X	1564553224296	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.296	\N	\N
20190731-120704-XxfBHBrKRIUNlRB	1564553224296	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.296	\N	\N
20190731-120704-r2zXsZCQOe1O0zi	1564553224296	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.296	\N	\N
20190731-120704-RQWmm0BIsvzsffk	1564553224296	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.296	\N	\N
20190731-120704-d1PoycW57KrZ2hT	1564553224296	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.296	\N	\N
20190731-120704-T76E0hOXrqVFZ1s	1564553224296	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.296	\N	\N
20190731-120704-5CSiA47sGnDvTnE	1564553224297	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.297	\N	\N
20190731-120704-OkixxGQe09bGzVi	1564553224297	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.297	\N	\N
20190731-120704-iQ0fTFJA5c2mrHA	1564553224297	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.297	\N	\N
20190731-120704-DPpb9znXPjrwbt2	1564553224297	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.297	\N	\N
20190731-120704-jtME4oBuZrhmQ5b	1564553224297	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.297	\N	\N
20190731-120704-nNx89wqrshUA9yz	1564553224298	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.298	\N	\N
20190731-120704-lRlpbM97FVJjFg4	1564553224298	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.298	\N	\N
20190731-120704-IUZQiaJdKEA5e0y	1564553224298	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.298	\N	\N
20190731-120704-fD7u8CgUsoRJdNO	1564553224298	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.298	\N	\N
20190731-120704-w1kAkUhXvE3HJge	1564553224298	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.298	\N	\N
20190731-120704-14IHDtp5xPLMMjy	1564553224299	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.299	\N	\N
20190731-120704-z6EODlsLXNPiIyB	1564553224299	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.299	\N	\N
20190731-120704-EG36naOSvuK0kaV	1564553224299	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.299	\N	\N
20190731-120704-lxTln6KAsMJ89lx	1564553224299	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.299	\N	\N
20190731-120704-iQAdUUSGUTbSOhF	1564553224299	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.299	\N	\N
20190731-120704-4bsmCjhuI36e8Sq	1564553224300	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.3	\N	\N
20190731-120704-z5DFZBYMASJnPlh	1564553224300	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.3	\N	\N
20190731-120704-S46D5GPwB5DOxHP	1564553224300	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.3	\N	\N
20190731-120704-QWP1NZBTj0J4iWY	1564553224300	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.3	\N	\N
20190731-120704-m2xQhu2Kp6AAUmB	1564553224300	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.3	\N	\N
20190731-120704-KHnNXH7nFTMnkhv	1564553224300	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.3	\N	\N
20190731-120704-MobU3o8pLnPI5Mv	1564553224301	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.301	\N	\N
20190731-120704-kvK9sFoF0cUIBqr	1564553224301	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.301	\N	\N
20190731-120704-B3WCHnT1DgxlpyA	1564553224301	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.301	\N	\N
20190731-120704-ickUYqI82zyrWKe	1564553224301	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.301	\N	\N
20190731-120704-PwNqGZA91Dc3lu2	1564553224302	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.302	\N	\N
20190731-120704-GrSOEmSOQCYUI3O	1564553224302	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.302	\N	\N
20190731-120704-tTZUYiW1HDZQo0v	1564553224302	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.302	\N	\N
20190731-120704-jufu2vWjxarmlbZ	1564553224302	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.302	\N	\N
20190731-120704-21QUOqOK2eVDfmA	1564553224302	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.302	\N	\N
20190731-120704-ehdKRj30PcqnFu4	1564553224302	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.302	\N	\N
20190731-120704-XKxPu2dlR4uK3ZM	1564553224303	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.303	\N	\N
20190731-120704-GyBWfmdgrVcs51A	1564553224303	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.303	\N	\N
20190731-120704-WPfU7ZwJeWXlXLu	1564553224303	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.303	\N	\N
20190731-120704-XJ3MO1CDBqBSVoB	1564553224303	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.303	\N	\N
20190731-120704-JklOJm2C3RL0kxs	1564553224303	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.303	\N	\N
20190731-120704-7HZwuOOTnTKvfVU	1564553224304	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.304	\N	\N
20190731-120704-Wx5T7EyVqvqUHMs	1564553224304	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.304	\N	\N
20190731-120704-rzUp2UooEGnvFpR	1564553224304	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.304	\N	\N
20190731-120704-QCoFhO4jJJNR8nZ	1564553224304	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.304	\N	\N
20190731-120704-kti6wMev8wpnX3E	1564553224304	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.304	\N	\N
20190731-120704-MLf5VCYru7cd5gl	1564553224305	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.305	\N	\N
20190731-120704-J7eRQrxcktfpCiT	1564553224305	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.305	\N	\N
20190731-120704-cgfv38jRc2RryOn	1564553224305	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.305	\N	\N
20190731-120704-SjwZ16kAFgjfzdT	1564553224305	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.305	\N	\N
20190731-120704-FHPENyCs7I8xBX9	1564553224305	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.305	\N	\N
20190731-120704-O98f1Q737Te4GNE	1564553224306	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.306	\N	\N
20190731-120704-MTQnDSIz9raWpeZ	1564553224306	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.306	\N	\N
20190731-120704-rgoHP36pLXSpqCe	1564553224306	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.306	\N	\N
20190731-120704-OS7tr0oepYWgQWU	1564553224306	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.306	\N	\N
20190731-120704-RRPzmKgAyOVnuUx	1564553224306	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.306	\N	\N
20190731-120704-7bneWlYAcRgVDC5	1564553224306	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.307	\N	\N
20190731-120704-Q4N9ijoWWWO3RXD	1564553224307	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.307	\N	\N
20190731-120704-xVTUzM0WIPLqdCJ	1564553224307	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.307	\N	\N
20190731-120704-nEf8IT8aXjzUS38	1564553224307	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.307	\N	\N
20190731-120704-mg7w9hEn9uMjtP5	1564553224307	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.307	\N	\N
20190731-120704-3uvWAhcuZCt1fEL	1564553224307	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.307	\N	\N
20190731-120704-mmQheBG20ktnAbx	1564553224308	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.308	\N	\N
20190731-120704-7VoJqUh5NLQ5vSl	1564553224308	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.308	\N	\N
20190731-120704-FhqPVmTUfjTJkcC	1564553224308	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.308	\N	\N
20190731-120704-C2yf2LG2tTfi7FS	1564553224308	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.308	\N	\N
20190731-120704-38I4mj4aILWZRM6	1564553224308	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.308	\N	\N
20190731-120704-upIRxLbSlKPhV2S	1564553224309	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.309	\N	\N
20190731-120704-q3Sog5zSVk4T98F	1564553224309	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.309	\N	\N
20190731-120704-wRhI2E6qCIwjqIx	1564553224309	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.309	\N	\N
20190731-120704-TOmonJKEFfIeylr	1564553224309	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.309	\N	\N
20190731-120704-kvN9HfHEU2Rpne6	1564553224309	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.309	\N	\N
20190731-120704-XbjzXxeYrieLTbw	1564553224309	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.31	\N	\N
20190731-120704-yp5csePp4z2FT3J	1564553224310	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.31	\N	\N
20190731-120704-GKXlFNzKuZYCX00	1564553224310	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.31	\N	\N
20190731-120704-qIS0dIUB5x3ama4	1564553224310	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.31	\N	\N
20190731-120704-8SPZupNr3BrBK5E	1564553224310	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.31	\N	\N
20190731-120704-8N2fB559P0N9Wdc	1564553224310	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.31	\N	\N
20190731-120704-iMyXlZVkmPpctsO	1564553224311	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.311	\N	\N
20190731-120704-qxLKYyBmcnbSYTN	1564553224311	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.311	\N	\N
20190731-120704-LfPPjG9vhAr5MY2	1564553224311	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.311	\N	\N
20190731-120704-ot5lvBpD5xaGH3s	1564553224311	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.311	\N	\N
20190731-120704-mB7jpbFzV6R7Hei	1564553224311	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.311	\N	\N
20190731-120704-eyPQz5hlblb5mNd	1564553224312	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.312	\N	\N
20190731-120704-HqrGSwn1tCMFEVV	1564553224312	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.312	\N	\N
20190731-120704-AnMo8Dw73kpUCDZ	1564553224312	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.312	\N	\N
20190731-120704-NF7cO4VejaC6jY5	1564553224312	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.312	\N	\N
20190731-120704-weVMLXt7G3tHnHg	1564553224312	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.312	\N	\N
20190731-120704-OxV7h904jtvanls	1564553224313	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.313	\N	\N
20190731-120704-zoRqU3jDFkNcQ8r	1564553224313	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.313	\N	\N
20190731-120704-TC2ZFotgbNZdglK	1564553224313	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.313	\N	\N
20190731-120704-n6Txnjk4n28tkG6	1564553224313	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.313	\N	\N
20190731-120704-DfEpQ08ivzY0IM8	1564553224313	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.313	\N	\N
20190731-120704-DMnJxUpVCFPV5mb	1564553224313	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.313	\N	\N
20190731-120704-S9VY2ZHmyW0rMCH	1564553224314	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.314	\N	\N
20190731-120704-tGCZm2GjAAXjZAb	1564553224314	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.314	\N	\N
20190731-120704-gNSWy7EzG35JNp5	1564553224314	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.314	\N	\N
20190731-120704-LjqFXJlkRvRwYb0	1564553224314	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.314	\N	\N
20190731-120704-NwST6gVLHUTBMUq	1564553224314	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.314	\N	\N
20190731-120704-axAD7lfxVVKFgtS	1564553224315	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.315	\N	\N
20190731-120704-PwoPlkTW5RZh4gN	1564553224315	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.315	\N	\N
20190731-120704-gD3Dk3xnjDtlnYx	1564553224315	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.315	\N	\N
20190731-120704-m8R329FpsaO0t4i	1564553224315	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.315	\N	\N
20190731-120704-9T7EBnoC7WT5eDs	1564553224315	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.315	\N	\N
20190731-120704-afXEC07KcqzNkOq	1564553224316	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.316	\N	\N
20190731-120704-CmA5ydDMpGR6zrv	1564553224316	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.316	\N	\N
20190731-120704-iKaYmy7KxooQCWL	1564553224316	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.316	\N	\N
20190731-120704-8eSbfscNjMH0EPf	1564553224316	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.316	\N	\N
20190731-120704-VPuXYUKqc1uRxDE	1564553224316	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.316	\N	\N
20190731-120704-JdR14MchEb2mrmG	1564553224316	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.316	\N	\N
20190731-120704-36XHGyomBWo5z96	1564553224317	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.317	\N	\N
20190731-120704-VxZYwQ9cgtWNVo7	1564553224317	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.317	\N	\N
20190731-120704-tPcDvBKZL2zoC8u	1564553224317	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.317	\N	\N
20190731-120704-qKejtYcmIgZYyOk	1564553224317	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.317	\N	\N
20190731-120704-K3re7rvUvXfXB5g	1564553224317	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.317	\N	\N
20190731-120704-DgU6NoKpFzZaYGm	1564553224318	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.318	\N	\N
20190731-120704-56b0Khi2fxs1qbI	1564553224318	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.318	\N	\N
20190731-120704-cXL1t5E8GXIORwO	1564553224318	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.318	\N	\N
20190731-120704-fhnrVLp0hATwjoJ	1564553224318	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.318	\N	\N
20190731-120704-8970Wii4tsDJuBN	1564553224318	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.318	\N	\N
20190731-120704-3kBtOTQVStTjERt	1564553224319	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.319	\N	\N
20190731-120704-Mj018zgM8HwdfVg	1564553224319	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.319	\N	\N
20190731-120704-bH3H4SNUWK9sb3s	1564553224319	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.319	\N	\N
20190731-120704-RnHpnk8bePujdm3	1564553224319	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.319	\N	\N
20190731-120704-u3FKzoeRxe1ZyXj	1564553224319	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.319	\N	\N
20190731-120704-DgASQAcI3acgIaH	1564553224320	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.32	\N	\N
20190731-120704-qi8KHdLpQ6scB3x	1564553224320	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.32	\N	\N
20190731-120704-4dUyoeu3NrLyhRF	1564553224320	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.32	\N	\N
20190731-120704-jmIZCa7eAlMXsCk	1564553224320	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.32	\N	\N
20190731-120704-LDQc9D3ty1oVDFW	1564553224320	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.32	\N	\N
20190731-120704-cCaQ726vYuKluQj	1564553224320	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.32	\N	\N
20190731-120704-PoT4orUb28slwJj	1564553224321	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.321	\N	\N
20190731-120704-lLuzEryy8GhU4dx	1564553224321	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.321	\N	\N
20190731-120704-ct6l7kN25HPthOd	1564553224321	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.321	\N	\N
20190731-120704-9hGpaJnRVeoewd7	1564553224321	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.321	\N	\N
20190731-120704-qmgkgttWGjuSG8k	1564553224322	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.322	\N	\N
20190731-120704-0K1TCtRB3gEU7TI	1564553224322	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.322	\N	\N
20190731-120704-E8ejFKIPHX5zS2X	1564553224322	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.322	\N	\N
20190731-120704-SGK9ePVmf0SQTsB	1564553224322	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.322	\N	\N
20190731-120704-QfoivMR3EjGnw8g	1564553224322	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.322	\N	\N
20190731-120704-NSiY2Mz6XjWYywq	1564553224322	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.322	\N	\N
20190731-120704-nTTe4mZRVpLjdFR	1564553224323	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.323	\N	\N
20190731-120704-ID7orwF1cH4rpia	1564553224323	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.323	\N	\N
20190731-120704-kTaTIwgvYHUsYcm	1564553224323	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.323	\N	\N
20190731-120704-1I6deZ7wfqovsRV	1564553224323	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.323	\N	\N
20190731-120704-7jGMZxBDSAxH0AI	1564553224323	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.323	\N	\N
20190731-120704-NnNHpQGssv3XRaS	1564553224324	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.324	\N	\N
20190731-120704-PBjnD6vp62zymrh	1564553224324	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.324	\N	\N
20190731-120704-bnfzIFl4dr906vC	1564553224324	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.324	\N	\N
20190731-120704-SMWrtgOvVwSdZwO	1564553224324	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.324	\N	\N
20190731-120704-7GGaZSWz8nM1yu3	1564553224324	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.324	\N	\N
20190731-120704-Kcfldt6Sp1LgXq7	1564553224325	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.325	\N	\N
20190731-120704-FCY8HXpkxHuYnfY	1564553224325	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.325	\N	\N
20190731-120704-cCWmlJKCkgVtxKM	1564553224325	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.325	\N	\N
20190731-120704-5uZdm8J3hy4OkKr	1564553224325	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.325	\N	\N
20190731-120704-sCQzkhktEbAxGsI	1564553224325	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.325	\N	\N
20190731-120704-mQMnPgjl5sZt8Jh	1564553224326	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.326	\N	\N
20190731-120704-5cKsAXDoMRPI7lt	1564553224326	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.326	\N	\N
20190731-120704-QAEcZ2fNdKjey1M	1564553224326	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.326	\N	\N
20190731-120704-wfrgafdDviiN1vj	1564553224326	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.326	\N	\N
20190731-120704-fSZz9ftzeX0OQxj	1564553224326	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.326	\N	\N
20190731-120704-O1nuUdP7DxTDMlJ	1564553224326	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.326	\N	\N
20190731-120704-BNpkrrRHjnC6S5o	1564553224327	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.327	\N	\N
20190731-120704-gLj46XJQajkbPW6	1564553224327	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.327	\N	\N
20190731-120704-IsfywkrRJPrR0qd	1564553224327	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.327	\N	\N
20190731-120704-74ZAtA63uVUMneb	1564553224327	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.327	\N	\N
20190731-120704-qtjs3ARqETEEhth	1564553224327	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.327	\N	\N
20190731-120704-VLjFd8398Ezs5Eg	1564553224328	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.328	\N	\N
20190731-120704-X0UbElCIvQURatr	1564553224328	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.328	\N	\N
20190731-120704-LahMxGD4wXAC5rO	1564553224328	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.328	\N	\N
20190731-120704-Bj7OoS5LBXSjf28	1564553224328	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.328	\N	\N
20190731-120704-vzdnFSE39oyxnh0	1564553224328	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.328	\N	\N
20190731-120704-aeeQykQtDdnReAK	1564553224329	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.329	\N	\N
20190731-120704-Y7UF2QzxgcSwWfz	1564553224329	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.329	\N	\N
20190731-120704-00ReedZ96k1QoJC	1564553224329	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.329	\N	\N
20190731-120704-FJllxW6rzmdpx5r	1564553224329	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.329	\N	\N
20190731-120704-Efcl85vdUgko3cF	1564553224329	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.329	\N	\N
20190731-120704-uRhWCChFyQd5Gz7	1564553224329	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.329	\N	\N
20190731-120704-GJ8Grzv1cBEYFdt	1564553224330	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.33	\N	\N
20190731-120704-lrzNRNbKtiFYEe9	1564553224330	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.33	\N	\N
20190731-120704-rZOo8gl5YTNV3L8	1564553224330	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.33	\N	\N
20190731-120704-iPvxCCzOcfmvviI	1564553224330	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.33	\N	\N
20190731-120704-Ca90uaCMoGzmHkW	1564553224330	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.33	\N	\N
20190731-120704-0T4ItaMzc3iWPSq	1564553224331	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.331	\N	\N
20190731-120704-9hnP8hO61SpQMBj	1564553224331	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.331	\N	\N
20190731-120704-VUd3cCirezKLrgl	1564553224331	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.331	\N	\N
20190731-120704-3VCp20kYWIUjbbd	1564553224331	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.331	\N	\N
20190731-120704-jr8GY6KCA46t60X	1564553224331	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.331	\N	\N
20190731-120704-z2SWeKSu2CtzEGx	1564553224332	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.332	\N	\N
20190731-120704-vY76apZwnqF5FVr	1564553224332	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.332	\N	\N
20190731-120704-sMCUIvNApCQmKDS	1564553224332	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.332	\N	\N
20190731-120704-2HTsAQWSp6QjIDY	1564553224332	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.332	\N	\N
20190731-120704-YAjPAh3bCjo0b7Q	1564553224332	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.332	\N	\N
20190731-120704-HdG3Jzul64iFRp3	1564553224333	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.333	\N	\N
20190731-120704-BDn1am9kctQcyQX	1564553224333	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.333	\N	\N
20190731-120704-hHgq4dxGCw3sphF	1564553224333	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.333	\N	\N
20190731-120704-WzB5K5cFO3Wbyd5	1564553224333	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.333	\N	\N
20190731-120704-Y0zoWePrGNP0EB7	1564553224333	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.333	\N	\N
20190731-120704-QmQPM7YMjRr1JMw	1564553224334	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.334	\N	\N
20190731-120704-wVOmfRngVvarN2A	1564553224334	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.334	\N	\N
20190731-120704-QRf13LyRLykInBK	1564553224334	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.334	\N	\N
20190731-120704-L1gAZvvYfESL1QZ	1564553224334	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.334	\N	\N
20190731-120704-CtMm2izYLOKLtSo	1564553224334	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.334	\N	\N
20190731-120704-ejKhhSNxxAVY8vN	1564553224335	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.335	\N	\N
20190731-120704-26yw9AozUfpzavG	1564553224335	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.335	\N	\N
20190731-120704-O0XfOPucgWkfDLw	1564553224335	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.335	\N	\N
20190731-120704-4ZPVfgAsHrsfODk	1564553224335	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.335	\N	\N
20190731-120704-tkqo1zCCUQlAmVE	1564553224335	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.335	\N	\N
20190731-120704-dSI7WLsvpiLebZy	1564553224335	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.335	\N	\N
20190731-120704-ALJPkXGhvXXvmlW	1564553224336	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.336	\N	\N
20190731-120704-8kGHo38Z0sF2Z8d	1564553224336	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.336	\N	\N
20190731-120704-yvD4dcJLplEofbO	1564553224336	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.336	\N	\N
20190731-120704-PIkwiWkK6qph63z	1564553224336	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.336	\N	\N
20190731-120704-2SRZBq3jPINO153	1564553224336	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.336	\N	\N
20190731-120704-4jHnyN44yuTCld5	1564553224337	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.337	\N	\N
20190731-120704-XSCvmWYQfsef43m	1564553224337	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.337	\N	\N
20190731-120704-A3XX9YvWKWfpzgb	1564553224337	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.337	\N	\N
20190731-120704-5YPk4CUVA1iDxqD	1564553224337	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.337	\N	\N
20190731-120704-eeEPuT41zyM4HnM	1564553224337	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.337	\N	\N
20190731-120704-f9s7wTyaCsi0GyU	1564553224338	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.338	\N	\N
20190731-120704-POWUup4YcrCvHLC	1564553224338	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.338	\N	\N
20190731-120704-CnMID7nKLTOestv	1564553224338	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.338	\N	\N
20190731-120704-0dcvN3CNC96vAFe	1564553224338	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.338	\N	\N
20190731-120704-LFY2sFqoZQeQTng	1564553224338	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.338	\N	\N
20190731-120704-Fh2JTDGSxFIz6aF	1564553224338	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.338	\N	\N
20190731-120704-LGmtwPf88anzeVv	1564553224339	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.339	\N	\N
20190731-120704-IE4yBw11sBbJ6lT	1564553224339	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.339	\N	\N
20190731-120704-BP2IlSNNnIdoh3d	1564553224339	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.339	\N	\N
20190731-120704-HscsTwTuu3rlbk2	1564553224339	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.339	\N	\N
20190731-120704-skcdpxyCXH6bnPM	1564553224339	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.339	\N	\N
20190731-120704-Rlp7bbYWbbp5JQ7	1564553224340	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.34	\N	\N
20190731-120704-UfSLKhhUGkNvusO	1564553224340	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.34	\N	\N
20190731-120704-5Zr8D9UrG2iTUpL	1564553224340	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.34	\N	\N
20190731-120704-XnBU3IS6MWkRN3Z	1564553224340	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.34	\N	\N
20190731-120704-4zTmshTeSCaIJP0	1564553224340	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.34	\N	\N
20190731-120704-Y4tQwREL3FKY1XZ	1564553224341	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.341	\N	\N
20190731-120704-Hd5eVDm5pWtZJmj	1564553224341	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.341	\N	\N
20190731-120704-EHH6uCy8p0sSgqk	1564553224341	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.341	\N	\N
20190731-120704-mC0jhQSFDf3jjVq	1564553224341	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.341	\N	\N
20190731-120704-OtMBCi4jpCDTGAG	1564553224341	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.341	\N	\N
20190731-120704-vf8QcqVSR4exj6R	1564553224342	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.342	\N	\N
20190731-120704-MKH6duxoDeqVmg6	1564553224342	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.342	\N	\N
20190731-120704-0mNvml09MyLa0zr	1564553224342	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.342	\N	\N
20190731-120704-i6FGt3dmotosPBA	1564553224342	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.342	\N	\N
20190731-120704-4egS18rFaHIfQlL	1564553224342	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.342	\N	\N
20190731-120704-tUz27L8dqvkSPGb	1564553224342	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.342	\N	\N
20190731-120704-oWWPyPt49z8ewnb	1564553224343	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.343	\N	\N
20190731-120704-tRHtLoryc6FbADW	1564553224343	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.343	\N	\N
20190731-120704-POWiWCbkyVOlKXc	1564553224343	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.343	\N	\N
20190731-120704-XtDbMash7Y1dz4M	1564553224343	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.343	\N	\N
20190731-120704-rDyhwaR3cWhL7LR	1564553224343	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.343	\N	\N
20190731-120704-9X1r5z8uN0FeBUx	1564553224344	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.344	\N	\N
20190731-120704-y0qWsx3ImRbjxbL	1564553224344	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.344	\N	\N
20190731-120704-kRlYFUtQRFRQTzB	1564553224344	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.344	\N	\N
20190731-120704-HFyFFalGmQzcggy	1564553224344	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.344	\N	\N
20190731-120704-heUH0hRldcCaKAn	1564553224344	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.344	\N	\N
20190731-120704-wNt7pTciSUFDDEG	1564553224345	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.345	\N	\N
20190731-120704-CYRYP9AtK2d5rd0	1564553224345	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.345	\N	\N
20190731-120704-Ik1V55jHKPFehW4	1564553224345	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.345	\N	\N
20190731-120704-ocriTNkL8i5JPAH	1564553224345	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.345	\N	\N
20190731-120704-rK7F7MFErK47tBt	1564553224345	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.345	\N	\N
20190731-120704-pmvi2LFYhkaiNVQ	1564553224346	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.346	\N	\N
20190731-120704-hOGnUGqXj56OagU	1564553224346	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.346	\N	\N
20190731-120704-jIKFPfnGyeIK84W	1564553224346	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.346	\N	\N
20190731-120704-N8vq6HvQfnIPn4N	1564553224346	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.346	\N	\N
20190731-120704-cddT5sRAdmyp6A6	1564553224346	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.346	\N	\N
20190731-120704-PErV961AxOm0a6I	1564553224346	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.346	\N	\N
20190731-120704-IZAJh3OX8dEZ3jD	1564553224347	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.347	\N	\N
20190731-120704-Qcwo151qhpPhs75	1564553224347	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.347	\N	\N
20190731-120704-kWCYSlC3CpP2yFJ	1564553224347	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.347	\N	\N
20190731-120704-6vxV3qaQrUwRztA	1564553224347	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.347	\N	\N
20190731-120704-tmTEq30hYIJnLQ4	1564553224347	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.347	\N	\N
20190731-120704-hsDUxznD9or4Ebb	1564553224348	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.348	\N	\N
20190731-120704-vHxcSAwHhBM1BJx	1564553224348	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.348	\N	\N
20190731-120704-qubwLZ8Bw8zgygp	1564553224348	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.348	\N	\N
20190731-120704-mGOwtx5qpKegFCt	1564553224348	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.348	\N	\N
20190731-120704-G72G0KeFOunwupT	1564553224348	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.348	\N	\N
20190731-120704-ZdlB6COXeQ37rvx	1564553224349	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.349	\N	\N
20190731-120704-qRAGh6kSHvXfLar	1564553224349	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.349	\N	\N
20190731-120704-WQEKoiiuhC0KHTT	1564553224349	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.349	\N	\N
20190731-120704-pQKAPDGJiUsft3L	1564553224349	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.349	\N	\N
20190731-120704-zvJZQej9n9j3FMs	1564553224349	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.349	\N	\N
20190731-120704-A5ZPbG5dFRaRVnG	1564553224350	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.35	\N	\N
20190731-120704-wOhxESoiXVNuvrx	1564553224350	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.35	\N	\N
20190731-120704-8vHmlO67c1niP8h	1564553224350	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.35	\N	\N
20190731-120704-FSvPzrCQlnEL2kR	1564553224350	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.35	\N	\N
20190731-120704-GEA1Uxfbn6dkirb	1564553224350	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.35	\N	\N
20190731-120704-zug1PYMsrQiZBOL	1564553224350	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.35	\N	\N
20190731-120704-8WGFMKfwmUSnS4I	1564553224351	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.351	\N	\N
20190731-120704-DI7D9IYUsC54ofC	1564553224351	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.351	\N	\N
20190731-120704-sYqQGIKoGETlNiw	1564553224351	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.351	\N	\N
20190731-120704-QKfx4OaA54Y09Fx	1564553224351	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.351	\N	\N
20190731-120704-pwLXRbNLGdhjNKI	1564553224351	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.351	\N	\N
20190731-120704-hjrBlXY3Zl6KhvI	1564553224352	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.352	\N	\N
20190731-120704-zHPXMO7LBB58ie8	1564553224352	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.352	\N	\N
20190731-120704-9bCOx0QlUN9gyrM	1564553224352	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.352	\N	\N
20190731-120704-e6UPAQcdj5EyTBI	1564553224352	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.352	\N	\N
20190731-120704-QzJpA5O3lIYDF3X	1564553224352	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.352	\N	\N
20190731-120704-DvmiTtJvFSf1mJd	1564553224353	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.353	\N	\N
20190731-120704-QGHBOcx43eAmBBV	1564553224353	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.353	\N	\N
20190731-120704-zbPCx88D88vzcWO	1564553224353	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.353	\N	\N
20190731-120704-04e8AHWWLdcUgh2	1564553224353	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.353	\N	\N
20190731-120704-j7EDkEXhuq5M8kv	1564553224353	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.353	\N	\N
20190731-120704-ZLHnRKS3cg1OOOM	1564553224354	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.354	\N	\N
20190731-120704-Qo6PHH0D9PrWnhK	1564553224354	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.354	\N	\N
20190731-120704-nX9DEqsUKr7mRlK	1564553224354	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.354	\N	\N
20190731-120704-a9WW2oCARRNJDZb	1564553224354	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.354	\N	\N
20190731-120704-7TRxbIjoYMcwLXW	1564553224354	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.354	\N	\N
20190731-120704-wgHXGsq4lGOaHWp	1564553224355	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.355	\N	\N
20190731-120704-4ezijghj9TXUbqX	1564553224355	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.355	\N	\N
20190731-120704-mEpojlk3IqWlSc4	1564553224355	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.355	\N	\N
20190731-120704-mcV3An5jQhPCGhQ	1564553224355	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.355	\N	\N
20190731-120704-D9z1ksMfPhEbZ1F	1564553224356	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.356	\N	\N
20190731-120704-hmh4MB0aGfYe3aI	1564553224356	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.356	\N	\N
20190731-120704-shfEU6C8YwPsVgK	1564553224356	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.356	\N	\N
20190731-120704-PORAXZSpJVKBJg7	1564553224356	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.356	\N	\N
20190731-120704-RnRFkAHXeKThT4X	1564553224356	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.356	\N	\N
20190731-120704-GxxVRBwgHJOaUkv	1564553224357	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.357	\N	\N
20190731-120704-xNxHi93S50qvLct	1564553224357	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.357	\N	\N
20190731-120704-3dcSgTCiV3zOTgB	1564553224357	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.357	\N	\N
20190731-120704-T1WrGdzNyhnt1Oz	1564553224357	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.357	\N	\N
20190731-120704-54CQ8XXNz3ykVnk	1564553224358	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.358	\N	\N
20190731-120704-zA5l20Fl8cDxd6V	1564553224358	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.358	\N	\N
20190731-120704-VCmTTuBL3RFklwX	1564553224358	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.358	\N	\N
20190731-120704-aGakrJXqvulJr4l	1564553224358	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.358	\N	\N
20190731-120704-c1TDx7CJisWmsQ0	1564553224358	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.358	\N	\N
20190731-120704-ERw3Yfvdo2CJ9OC	1564553224359	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.359	\N	\N
20190731-120704-fjYzP0crbFxxocO	1564553224359	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.359	\N	\N
20190731-120704-wKyDlyzLydJ6XlO	1564553224359	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.359	\N	\N
20190731-120704-YJhyaKkvBYGPEwJ	1564553224359	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.359	\N	\N
20190731-120704-RvNP4mVTo5kYjGk	1564553224359	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.359	\N	\N
20190731-120704-NTkU0FKUDLwVWAX	1564553224360	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.36	\N	\N
20190731-120704-CjEvENpO6PEa9MJ	1564553224360	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.36	\N	\N
20190731-120704-o6yfk83SaRkZhf3	1564553224360	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.36	\N	\N
20190731-120704-WYJwqDau25mPy1U	1564553224360	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.36	\N	\N
20190731-120704-bgmZAqhrELANgPQ	1564553224361	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.361	\N	\N
20190731-120704-frj661QAuM36jNl	1564553224361	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.361	\N	\N
20190731-120704-dDVlEiwTUACbNsS	1564553224361	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.361	\N	\N
20190731-120704-vG9y5ziDGQE2abV	1564553224361	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.361	\N	\N
20190731-120704-Gy6YMke5hLSafrJ	1564553224361	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.361	\N	\N
20190731-120704-IFwensj0Tna2EpP	1564553224362	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.362	\N	\N
20190731-120704-Qml7JYqVH6NmeW8	1564553224362	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.362	\N	\N
20190731-120704-Daz4b2utMbgrVyp	1564553224362	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.362	\N	\N
20190731-120704-HbaudHX0PhMoZkM	1564553224362	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.362	\N	\N
20190731-120704-jcjHrHU7M8mkCL8	1564553224362	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.362	\N	\N
20190731-120704-Z13hFRrRYZWNWyE	1564553224363	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.363	\N	\N
20190731-120704-qywrbKUFnrk3WCD	1564553224363	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.363	\N	\N
20190731-120704-0nnDwRRBqvQyP1y	1564553224363	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.363	\N	\N
20190731-120704-dZY1KReyOpqrFWm	1564553224363	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.363	\N	\N
20190731-120704-tl1D072x3yb60QS	1564553224363	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.363	\N	\N
20190731-120704-XEyN5uMl9jOIEyp	1564553224364	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.364	\N	\N
20190731-120704-VYxYAR02uxZs4an	1564553224364	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.364	\N	\N
20190731-120704-YU6kdhaeRouR09S	1564553224364	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.364	\N	\N
20190731-120704-1XItWQLLGagGBs2	1564553224364	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.364	\N	\N
20190731-120704-xu4aMQACHRY1Djt	1564553224364	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.364	\N	\N
20190731-120704-Ezy36ZcaxrnUFpH	1564553224365	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.365	\N	\N
20190731-120704-ky5gmMzg385cnDB	1564553224365	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.365	\N	\N
20190731-120704-kFuyegoFYRZIoP2	1564553224365	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.365	\N	\N
20190731-120704-s0l4OEtLQWKnAKr	1564553224365	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.365	\N	\N
20190731-120704-HnoBw7spZrVtFD6	1564553224365	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.365	\N	\N
20190731-120704-cGJvWZKsNhdqulz	1564553224366	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.366	\N	\N
20190731-120704-tMF2fTPvS1mhh1L	1564553224366	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.366	\N	\N
20190731-120704-QkIjdHv1enq5n7T	1564553224366	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.366	\N	\N
20190731-120704-2L0NBxbUwgzFEMm	1564553224366	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.366	\N	\N
20190731-120704-LoG22yzartLnJLL	1564553224366	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.366	\N	\N
20190731-120704-BOaMsx6zMFNtgaK	1564553224367	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.367	\N	\N
20190731-120704-Z6emlSPT5CrJiKE	1564553224367	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.367	\N	\N
20190731-120704-KAA6TOEI2nAmDFr	1564553224367	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.367	\N	\N
20190731-120704-RuIj0v4ogAhtrDh	1564553224367	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.367	\N	\N
20190731-120704-MIoDplWqG77mDjo	1564553224367	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.367	\N	\N
20190731-120704-yJOwZOynO2i9B6j	1564553224367	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.367	\N	\N
20190731-120704-QYCMCvknKpXzUgG	1564553224368	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.368	\N	\N
20190731-120704-cQDvF2fFadoLpG6	1564553224368	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.368	\N	\N
20190731-120704-sbAwWlNEonQ54Hj	1564553224368	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.368	\N	\N
20190731-120704-57HJn99Qn8Ow3mE	1564553224368	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.368	\N	\N
20190731-120704-Q9ebXrPE5STNlvo	1564553224369	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.369	\N	\N
20190731-120704-IpWJydzMy5vC44l	1564553224369	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.369	\N	\N
20190731-120704-yPLUzbOGyq0kE6L	1564553224369	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.369	\N	\N
20190731-120704-UUD8ccTXNTYRSjY	1564553224369	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.369	\N	\N
20190731-120704-awPLh7iNia5qDsI	1564553224369	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.369	\N	\N
20190731-120704-kNkWJTYyart34Wj	1564553224370	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.37	\N	\N
20190731-120704-8SycJgwpEaBXlvC	1564553224370	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.37	\N	\N
20190731-120704-6K8yoFFlG07k5SW	1564553224370	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.37	\N	\N
20190731-120704-IH1nyCgHC1urRWE	1564553224370	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.37	\N	\N
20190731-120704-nrukLBcTa0MWrBA	1564553224370	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.37	\N	\N
20190731-120704-NJ5hGkF8CewfdQF	1564553224370	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.37	\N	\N
20190731-120704-Xfn2SsTzwnVVXsD	1564553224371	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.371	\N	\N
20190731-120704-wf7sDLIyToTmuXX	1564553224371	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.371	\N	\N
20190731-120704-eoBRwEq9YDVy9hi	1564553224371	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.371	\N	\N
20190731-120704-KveC6ASf8QRkbfi	1564553224371	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.371	\N	\N
20190731-120704-VnsHiBoi8hCliVJ	1564553224371	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.371	\N	\N
20190731-120704-6VgspYbEt8hW4rY	1564553224372	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.372	\N	\N
20190731-120704-wYm5mWreMnWmiwh	1564553224372	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.372	\N	\N
20190731-120704-rGEwBWrP3BOG6i6	1564553224372	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.372	\N	\N
20190731-120704-FqIQhqfMlQ7SOOo	1564553224372	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.372	\N	\N
20190731-120704-t9UUsq8BN1eApFS	1564553224372	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.372	\N	\N
20190731-120704-dqc58ySS3CsDhw4	1564553224373	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.373	\N	\N
20190731-120704-2oeDt2sPzlCK9WL	1564553224373	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.373	\N	\N
20190731-120704-wirxnVxtN3eurkl	1564553224373	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.373	\N	\N
20190731-120704-QKIp3XEaRV6dI41	1564553224373	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.373	\N	\N
20190731-120704-SUQWtIkbYvBRapq	1564553224373	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.373	\N	\N
20190731-120704-CYbTwTZ4R8xRjoW	1564553224374	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.374	\N	\N
20190731-120704-N2xybeeSxXGIQLj	1564553224374	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.374	\N	\N
20190731-120704-s9inhhxGU4oTQgc	1564553224374	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.374	\N	\N
20190731-120704-CBqonT9F8knodGA	1564553224374	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.374	\N	\N
20190731-120704-UFjldk029ajDHzO	1564553224374	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.374	\N	\N
20190731-120704-wn4ysDaPXnUnDAa	1564553224375	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.375	\N	\N
20190731-120704-BhrujXjeSb3QzOB	1564553224375	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.375	\N	\N
20190731-120704-e0VDDaeSccjrH1I	1564553224375	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.375	\N	\N
20190731-120704-tBapDS3mZBlN94i	1564553224375	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.375	\N	\N
20190731-120704-McFOgLfQulk6x5L	1564553224375	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.375	\N	\N
20190731-120704-dYY55gdDGWREWCT	1564553224375	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.375	\N	\N
20190731-120704-8ZZZ37pFgWUzXk6	1564553224376	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.376	\N	\N
20190731-120704-NOwOcSsmMfvpWWD	1564553224376	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.376	\N	\N
20190731-120704-nFnqaZTEtHjLorR	1564553224376	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.376	\N	\N
20190731-120704-4DiwuNcfRoIYt8Z	1564553224376	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.376	\N	\N
20190731-120704-CnEdSVOpEGOeuQR	1564553224376	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.376	\N	\N
20190731-120704-jhq4qRKztYWo1aL	1564553224377	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.377	\N	\N
20190731-120704-Ig9lkm4bzPlfuE6	1564553224377	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.377	\N	\N
20190731-120704-Mi7tkj5NlQ6CgQe	1564553224377	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.377	\N	\N
20190731-120704-tryB8eKMYt2VtQM	1564553224377	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.377	\N	\N
20190731-120704-vr8eXEnZDwS3P9j	1564553224377	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.377	\N	\N
20190731-120704-G2ngaSp0jWcdlkE	1564553224378	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.378	\N	\N
20190731-120704-HhNlN4czZ4N2weu	1564553224378	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.378	\N	\N
20190731-120704-mFevR92F5dKcLxk	1564553224378	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.378	\N	\N
20190731-120704-rXmDWNlgafo5PB2	1564553224378	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.378	\N	\N
20190731-120704-sqMCny7lXrgj14J	1564553224378	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.378	\N	\N
20190731-120704-RD78Z4v3liBtYt9	1564553224379	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.379	\N	\N
20190731-120704-iHuw0MispQYNoGO	1564553224379	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.379	\N	\N
20190731-120704-vb3SpMUJvi0qgmC	1564553224379	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.379	\N	\N
20190731-120704-Hmj3eRPcx43LJs9	1564553224379	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.379	\N	\N
20190731-120704-OwvLtld3Sy2hc9K	1564553224379	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.379	\N	\N
20190731-120704-PbYEHPVkMuQuySs	1564553224380	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.38	\N	\N
20190731-120704-5wPZOXajRsD9SIN	1564553224380	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.38	\N	\N
20190731-120704-qeT8jDAszDgjdnZ	1564553224380	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.38	\N	\N
20190731-120704-PCWPyGC6q3Ej5VJ	1564553224380	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.38	\N	\N
20190731-120704-Z8Wo7dTtXR17mUq	1564553224380	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.38	\N	\N
20190731-120704-E8cCfcTFAiwHs1Y	1564553224380	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.38	\N	\N
20190731-120704-xlW1NhWfHxMP6xV	1564553224381	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.381	\N	\N
20190731-120704-m7PfuMMdgSSShjf	1564553224381	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.381	\N	\N
20190731-120704-gOBjvNjxH7KkXCu	1564553224381	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.381	\N	\N
20190731-120704-9Hqvxwj7PoZqCLg	1564553224381	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.381	\N	\N
20190731-120704-bfTGjHi3gn3KkTs	1564553224381	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.381	\N	\N
20190731-120704-fLGua7gZKe6rn0X	1564553224382	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.382	\N	\N
20190731-120704-tU5LLgKqT7mSNqQ	1564553224382	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.382	\N	\N
20190731-120704-C4V89lozNifbNiU	1564553224382	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.382	\N	\N
20190731-120704-kRaOf6fe0meq1dh	1564553224382	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.382	\N	\N
20190731-120704-QYoHb8gSLlORmA3	1564553224382	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.382	\N	\N
20190731-120704-jRMESgyhEEJlHIk	1564553224383	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.383	\N	\N
20190731-120704-6YE00a6hfBKczZO	1564553224383	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.383	\N	\N
20190731-120704-uu1j5Vb7VWrcUQa	1564553224383	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.383	\N	\N
20190731-120704-PrdPTJwprG3A1ft	1564553224383	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.383	\N	\N
20190731-120704-3ZuAHE7sZg2OfqK	1564553224383	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.383	\N	\N
20190731-120704-hXFfKJGA8lAhwy3	1564553224384	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.384	\N	\N
20190731-120704-cI9kICe4JjGbLMH	1564553224384	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.384	\N	\N
20190731-120704-8XsBw53IP9fYwxQ	1564553224384	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.384	\N	\N
20190731-120704-L2f4qZZVoXT9nKm	1564553224384	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.384	\N	\N
20190731-120704-6az1ouz5hoLYvvK	1564553224384	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.384	\N	\N
20190731-120704-opxJqsJgWuW0zAH	1564553224385	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.385	\N	\N
20190731-120704-QboaJVctYBfQtIi	1564553224385	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.385	\N	\N
20190731-120704-c2xl16iGKhdHip0	1564553224385	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.385	\N	\N
20190731-120704-IqPgKCsYygKWecx	1564553224385	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.385	\N	\N
20190731-120704-RIxeE3KwmcwtGJV	1564553224385	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.385	\N	\N
20190731-120704-E4VAwcM2VM2MGp2	1564553224386	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.386	\N	\N
20190731-120704-ghr1kHc5LRMUNEw	1564553224386	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.386	\N	\N
20190731-120704-ZCwwy7I9BtAJ8hH	1564553224386	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.386	\N	\N
20190731-120704-FORgh4Adv1jgsP1	1564553224386	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.386	\N	\N
20190731-120704-oM53wtX8HazbBic	1564553224386	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.386	\N	\N
20190731-120704-vqcvCoIGH9nQRAT	1564553224386	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.386	\N	\N
20190731-120704-RS5qUvC9EZggm9S	1564553224387	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.387	\N	\N
20190731-120704-XWu1uCZAs1qa8ba	1564553224387	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.387	\N	\N
20190731-120704-PhOsDVoUriEmi7K	1564553224387	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.387	\N	\N
20190731-120704-GlYYCvSZWyLB6mS	1564553224387	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.387	\N	\N
20190731-120704-6iR0ZbBo9PH8Dxg	1564553224387	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.387	\N	\N
20190731-120704-Cmh4MH3dhI2heqr	1564553224388	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.388	\N	\N
20190731-120704-IiOPp569Sc0Ya8L	1564553224388	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.388	\N	\N
20190731-120704-dhQvYtdzxwJNJ0J	1564553224388	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.388	\N	\N
20190731-120704-QSaaQclljgWi8mN	1564553224388	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.388	\N	\N
20190731-120704-4feKNNTodsM5AIv	1564553224388	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.388	\N	\N
20190731-120704-hEwuOv7J1MP0LQn	1564553224389	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.389	\N	\N
20190731-120704-wnRfNQLR1kCoM6M	1564553224389	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.389	\N	\N
20190731-120704-S3RC21YYFg3W4D7	1564553224389	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.389	\N	\N
20190731-120704-x0Ru6POxvZFsqu1	1564553224389	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.389	\N	\N
20190731-120704-aowh5yAb8aTqbUs	1564553224389	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.389	\N	\N
20190731-120704-luqOiMU6xsJAKTN	1564553224390	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.39	\N	\N
20190731-120704-kH8zee70rwTt0dx	1564553224390	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.39	\N	\N
20190731-120704-TmIveiWDEwCcIXL	1564553224390	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.39	\N	\N
20190731-120704-xMlwxHZaoSvALjq	1564553224390	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.39	\N	\N
20190731-120704-sX1822dz8q1jZt5	1564553224390	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.39	\N	\N
20190731-120704-vQx1FH6W3p0pm1X	1564553224391	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.391	\N	\N
20190731-120704-VIyEO64OEkGtVZt	1564553224391	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.391	\N	\N
20190731-120704-YOpoVG6ELIV0IJN	1564553224391	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.391	\N	\N
20190731-120704-J4jx6a9qEkFeXpS	1564553224391	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.391	\N	\N
20190731-120704-dnJaVD5BGALbwLh	1564553224391	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.391	\N	\N
20190731-120704-vCRHORFluoHJVxJ	1564553224392	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.392	\N	\N
20190731-120704-MZqtl9N3zP21f5j	1564553224392	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.392	\N	\N
20190731-120704-CJ8RUXZhCPP85Xp	1564553224392	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.392	\N	\N
20190731-120704-eJQt10EOssb5hcY	1564553224392	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.392	\N	\N
20190731-120704-CFA42x0WUK4GRUR	1564553224392	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.392	\N	\N
20190731-120704-z7mJp0shqOwY2Ph	1564553224393	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.393	\N	\N
20190731-120704-bziYcwFqK9SiG3X	1564553224393	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.393	\N	\N
20190731-120704-drT9FkWixjkhR0G	1564553224393	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.393	\N	\N
20190731-120704-4WeO2sZFvQCLDcX	1564553224393	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.393	\N	\N
20190731-120704-sdK15kOIbHOCYxR	1564553224393	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.393	\N	\N
20190731-120704-zNWYvqGc2YDcOVQ	1564553224394	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.394	\N	\N
20190731-120704-Z2TjAZzhsQj0cD9	1564553224394	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.394	\N	\N
20190731-120704-or4BZNj1LdpXviz	1564553224394	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.394	\N	\N
20190731-120704-S086AFlPSfecP6M	1564553224394	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.394	\N	\N
20190731-120704-Xl2qjq4EBtmIoUd	1564553224394	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.394	\N	\N
20190731-120704-Uh2RxmA2jvecAJK	1564553224395	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.395	\N	\N
20190731-120704-bt2kQlF5ejEyHeO	1564553224395	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.395	\N	\N
20190731-120704-C2Vx6foFZNHLEKc	1564553224395	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.395	\N	\N
20190731-120704-WdvdlXwCUuZDDKr	1564553224395	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.395	\N	\N
20190731-120704-9r0dvfR7lv6sGNy	1564553224395	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.395	\N	\N
20190731-120704-kE08H8h81q1PDEa	1564553224396	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.396	\N	\N
20190731-120704-ANMW6h8VaiFHUJU	1564553224396	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.396	\N	\N
20190731-120704-JW5zqSMGSUlMBkX	1564553224396	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.396	\N	\N
20190731-120704-Wh2Bk25DyDKgMLR	1564553224396	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.396	\N	\N
20190731-120704-brzdbNxpMjiRCei	1564553224396	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.396	\N	\N
20190731-120704-r30QtL3FlrGoLWV	1564553224396	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.396	\N	\N
20190731-120704-yXc5ZfQguRNQXE9	1564553224397	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.397	\N	\N
20190731-120704-VOnpkuEYcUQVaZK	1564553224397	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.397	\N	\N
20190731-120704-22OJuQupEL0ePpF	1564553224397	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.397	\N	\N
20190731-120704-uT86reeyfPCZbZ6	1564553224397	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.397	\N	\N
20190731-120704-tQJQQZ3YmRDBYH7	1564553224397	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.397	\N	\N
20190731-120704-67dWrUrZySx0JVu	1564553224398	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.398	\N	\N
20190731-120704-bD5A0ejfNzkAF3y	1564553224398	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.398	\N	\N
20190731-120704-kaiwg5VC62Wvi1c	1564553224398	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.398	\N	\N
20190731-120704-LDYby7TGaT4AAFD	1564553224398	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.398	\N	\N
20190731-120704-zZH8wOEKjpsP3dm	1564553224398	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.398	\N	\N
20190731-120704-TeW6CU6GL1i7xtJ	1564553224399	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.399	\N	\N
20190731-120704-AXOdCb0CKWoL0My	1564553224399	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.399	\N	\N
20190731-120704-W2xohJQRQMnkKAG	1564553224399	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.399	\N	\N
20190731-120704-r7CKL3p5XTrkyhn	1564553224399	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.399	\N	\N
20190731-120704-2F3b1UoGA0Kz25t	1564553224399	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.399	\N	\N
20190731-120704-WlKW2jmdfHmD7Gv	1564553224400	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.4	\N	\N
20190731-120704-HmQOMfqOrpglXKq	1564553224400	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.4	\N	\N
20190731-120704-tKEnnWNA2OqKuRr	1564553224400	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.4	\N	\N
20190731-120704-7CuqyFU4LtETlHR	1564553224400	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.4	\N	\N
20190731-120704-Kt5YdcdLKImM6A5	1564553224400	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.4	\N	\N
20190731-120704-Pt1suUCm3nnCNMj	1564553224400	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.4	\N	\N
20190731-120704-spHelaA8HPte3Kr	1564553224401	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.401	\N	\N
20190731-120704-43DVwk2s18jevio	1564553224401	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.401	\N	\N
20190731-120704-QDncmjS7C442D2h	1564553224401	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.401	\N	\N
20190731-120704-WfA2PGFjd4CR0b7	1564553224401	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.401	\N	\N
20190731-120704-ZlrQ5GQOlP1V8MM	1564553224401	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.401	\N	\N
20190731-120704-Cmlwamz7p3ODbtm	1564553224406	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.406	\N	\N
20190731-120704-91G9a1wqwuXNmRA	1564553224406	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.406	\N	\N
20190731-120704-3MXLW0xOKeUB4XY	1564553224406	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.406	\N	\N
20190731-120704-ZRuva0rlfhvk7Fx	1564553224407	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.407	\N	\N
20190731-120704-YDD1Qtkwz3AqT2a	1564553224407	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.407	\N	\N
20190731-120704-kjjCcOh6sJUrAT3	1564553224407	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.407	\N	\N
20190731-120704-G8lrqJTzq2vvqGo	1564553224407	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.407	\N	\N
20190731-120704-nPkNWDArqQZH22i	1564553224407	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.407	\N	\N
20190731-120704-pXmiIS074mjWE4J	1564553224408	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.408	\N	\N
20190731-120704-LgYmp3mRh8SEEPr	1564553224408	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.408	\N	\N
20190731-120704-PT462PwV73yCkJF	1564553224408	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.408	\N	\N
20190731-120704-ydSopKWrbclFybM	1564553224408	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.408	\N	\N
20190731-120704-oWVkTdVqntAikgx	1564553224408	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.408	\N	\N
20190731-120704-YvlHckPuzoZqWLt	1564553224409	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.409	\N	\N
20190731-120704-63jdEMShy4gERdj	1564553224409	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.409	\N	\N
20190731-120704-lPPZizN1Fj4meO1	1564553224409	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.409	\N	\N
20190731-120704-1BeuRyWMI9Z1IXo	1564553224409	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.409	\N	\N
20190731-120704-XTtYHLJ5CO2B1Px	1564553224409	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.409	\N	\N
20190731-120704-Oyvw0jRvvvEHwv6	1564553224409	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.409	\N	\N
20190731-120704-psvafjqlmlA7aHs	1564553224410	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.41	\N	\N
20190731-120704-C5zXCtDimi6HaZU	1564553224410	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.41	\N	\N
20190731-120704-fLBBZvAKDZ0iaaM	1564553224410	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.41	\N	\N
20190731-120704-1qbmgPHjuLunNPv	1564553224410	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.41	\N	\N
20190731-120704-OiBfdQARI6UZr2F	1564553224410	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.41	\N	\N
20190731-120704-QF0VykoYKpU7JC3	1564553224411	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.411	\N	\N
20190731-120704-YhdqBiTp3s2xAXu	1564553224411	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.411	\N	\N
20190731-120704-g7EES70DNHLUO5E	1564553224411	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.411	\N	\N
20190731-120704-scFRrSD2N2Qa32E	1564553224411	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.411	\N	\N
20190731-120704-3jibXhcmw7ZuH27	1564553224411	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.411	\N	\N
20190731-120704-zFbxJC1lJ5E9ydJ	1564553224412	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.412	\N	\N
20190731-120704-twDAX4lx9sQPz2x	1564553224412	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.412	\N	\N
20190731-120704-bk1yKAUgXWVrcnj	1564553224412	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.412	\N	\N
20190731-120704-xXomu9XV8CUyiXt	1564553224412	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.412	\N	\N
20190731-120704-iRmVBXGpHXgE3Hh	1564553224412	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.412	\N	\N
20190731-120704-VaQUhRVAu9Wxywq	1564553224413	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.413	\N	\N
20190731-120704-kceqNFBCVQHqFRm	1564553224413	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.413	\N	\N
20190731-120704-RwiFPV2974Y6ELS	1564553224413	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.413	\N	\N
20190731-120704-EdWHpemU2ppJdOq	1564553224413	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.413	\N	\N
20190731-120704-O0tf3slytdb6fJh	1564553224413	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.413	\N	\N
20190731-120704-C1663yrP5VgoMeI	1564553224413	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.413	\N	\N
20190731-120704-Fwh3igQID83F0QN	1564553224414	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.414	\N	\N
20190731-120704-Fe28AT8oaXb0HAD	1564553224414	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.414	\N	\N
20190731-120704-b4EQfnCO9ZDCZNn	1564553224414	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.414	\N	\N
20190731-120704-8mbrXkMNBSycxyH	1564553224414	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.414	\N	\N
20190731-120704-A24YjWCpFrVIjCg	1564553224414	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.414	\N	\N
20190731-120704-l60UI6x2Xai2FkK	1564553224415	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.415	\N	\N
20190731-120704-fv7HHVDo96P1ifZ	1564553224415	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.415	\N	\N
20190731-120704-V3Q80v41SPpSDR2	1564553224415	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.415	\N	\N
20190731-120704-IW1ri9cCB69oetz	1564553224415	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.415	\N	\N
20190731-120704-oQpeh21Piyc44gM	1564553224415	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.415	\N	\N
20190731-120704-nhX2V0WKrIz7BKM	1564553224416	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.416	\N	\N
20190731-120704-Zkp3Xasr1Gnv5CQ	1564553224416	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.416	\N	\N
20190731-120704-nibdWD5bvCh7Ih0	1564553224416	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.416	\N	\N
20190731-120704-RFPuTzetZmZZmlT	1564553224416	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.416	\N	\N
20190731-120704-3f3Q5L5lDX6wSA8	1564553224416	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.416	\N	\N
20190731-120704-rHUipJKn9qw5VEi	1564553224417	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.417	\N	\N
20190731-120704-ePWKJenS0ZbT7sV	1564553224417	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.417	\N	\N
20190731-120704-L7zHvWgR9glVxId	1564553224417	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.417	\N	\N
20190731-120704-cUHfJlQSq5i6TvM	1564553224417	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.417	\N	\N
20190731-120704-q6OcmfvqcqH3kdd	1564553224417	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.417	\N	\N
20190731-120704-4VbP9FGQfmDUEaS	1564553224418	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.418	\N	\N
20190731-120704-uKRgb6JMDLdmtxx	1564553224418	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.418	\N	\N
20190731-120704-uYBoexuSqx5eB4n	1564553224418	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.418	\N	\N
20190731-120704-qW8dZJ8EZtsSEX9	1564553224418	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.418	\N	\N
20190731-120704-SkU8oIX7f9lmsp6	1564553224419	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.419	\N	\N
20190731-120704-q8bXJ8eGmAuFC5j	1564553224419	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.419	\N	\N
20190731-120704-Bipwi1bNAerW1tM	1564553224419	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.419	\N	\N
20190731-120704-bWRHKCjejwjenam	1564553224419	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.419	\N	\N
20190731-120704-Ny2Ee4UxS4lAY83	1564553224420	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.42	\N	\N
20190731-120704-B2nxQy7wdJmZKdL	1564553224420	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.42	\N	\N
20190731-120704-WcYmsj8swTqRCaU	1564553224420	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.42	\N	\N
20190731-120704-MesCXFRfKO95VJS	1564553224420	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.42	\N	\N
20190731-120704-OAKSIH3h4JdTDiN	1564553224420	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.42	\N	\N
20190731-120704-O2ZV2DsQxlDiMqR	1564553224421	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.421	\N	\N
20190731-120704-qKEtEqIr7KiM6ro	1564553224421	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.421	\N	\N
20190731-120704-XU2GSZ5HkERxhNf	1564553224421	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.421	\N	\N
20190731-120704-3XkbNW1OKJ8BZ4D	1564553224421	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.421	\N	\N
20190731-120704-Boj71ofHjqT7C5M	1564553224421	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.421	\N	\N
20190731-120704-iXyWrb9u4n5b0Yd	1564553224422	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.422	\N	\N
20190731-120704-omFQoOy8iiXzbCr	1564553224422	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.422	\N	\N
20190731-120704-v22bVvVDFwVVZ1i	1564553224422	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.422	\N	\N
20190731-120704-KOiQ2ezptouCNbq	1564553224422	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.422	\N	\N
20190731-120704-XOyYH1mloslDuFW	1564553224423	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.423	\N	\N
20190731-120704-v2esIRaeEg3zW8P	1564553224423	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.423	\N	\N
20190731-120704-6woyB1hyd3ikg4s	1564553224423	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.423	\N	\N
20190731-120704-Ie2RRlVrTIPPHql	1564553224423	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.423	\N	\N
20190731-120704-nfAUWIYg3QmCnAo	1564553224423	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.423	\N	\N
20190731-120704-jtJ7vhOHEHfI3YG	1564553224424	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.424	\N	\N
20190731-120704-ViBV8FBrapE9UsU	1564553224424	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.424	\N	\N
20190731-120704-OyY9D6F7TgYbyTk	1564553224424	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.424	\N	\N
20190731-120704-Q7H1LAIXiuxRznf	1564553224424	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.424	\N	\N
20190731-120704-tVB6YmKmnNjS12U	1564553224424	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.424	\N	\N
20190731-120704-vnDlwCFiYCEJcPl	1564553224425	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.425	\N	\N
20190731-120704-vrZZ3rzqdU9jxju	1564553224425	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.425	\N	\N
20190731-120704-EY1dMS2MTlP8Yle	1564553224425	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.425	\N	\N
20190731-120704-PPdVy7eKbCqQ9xd	1564553224425	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.425	\N	\N
20190731-120704-wWuiv4ylkVuTgLC	1564553224426	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.426	\N	\N
20190731-120704-SbGgpIFrJcGuZtY	1564553224426	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.426	\N	\N
20190731-120704-GgP9YG9bvThPqSj	1564553224426	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.426	\N	\N
20190731-120704-aOOQdyiMqvQFJat	1564553224426	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.426	\N	\N
20190731-120704-ySNIfYvm8gybfCQ	1564553224427	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.427	\N	\N
20190731-120704-peTKDfD37NVBUzp	1564553224427	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.427	\N	\N
20190731-120704-j6w9bYxiY09mgkN	1564553224427	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.427	\N	\N
20190731-120704-pl22DaPcgSghNfx	1564553224427	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.427	\N	\N
20190731-120704-t1gSkLDMJI8Jwz1	1564553224427	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.427	\N	\N
20190731-120704-iKbWJ3i7fEPpibm	1564553224428	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.428	\N	\N
20190731-120704-rDQd2BTHPwj2qHX	1564553224428	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.428	\N	\N
20190731-120704-r348bCgREsdQQFr	1564553224428	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.428	\N	\N
20190731-120704-EX9BgSXJMnPpIIL	1564553224428	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.428	\N	\N
20190731-120704-SNJavJ5ajm8jehG	1564553224429	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.429	\N	\N
20190731-120704-wJaOP7k5qGymPNB	1564553224429	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.429	\N	\N
20190731-120704-SKy4b2IaStCgWlz	1564553224429	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.429	\N	\N
20190731-120704-zk5K7pvcieQgAJO	1564553224429	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.429	\N	\N
20190731-120704-oqtKxKDHmM9Maok	1564553224429	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.429	\N	\N
20190731-120704-PTMuOQEOrU5iAna	1564553224430	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.43	\N	\N
20190731-120704-HHLO9CzaYfud2Ah	1564553224430	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.43	\N	\N
20190731-120704-DePcLyEgZ1veGwU	1564553224430	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.43	\N	\N
20190731-120704-0h86M9OefxCHVn0	1564553224430	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.43	\N	\N
20190731-120704-LNSga7rY6MO5txP	1564553224431	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.431	\N	\N
20190731-120704-0amJfK12dj7lLsH	1564553224431	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.431	\N	\N
20190731-120704-xaWagNWns8dAm7x	1564553224431	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.431	\N	\N
20190731-120704-ttVJkS0FkYovj63	1564553224431	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.431	\N	\N
20190731-120704-yGBIbyzeAVTcCsA	1564553224431	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.431	\N	\N
20190731-120704-oLd6qu7B2HsVzJu	1564553224432	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.432	\N	\N
20190731-120704-3fAmf9dUX5aQRUm	1564553224432	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.432	\N	\N
20190731-120704-SSXn64dQ91XUC2w	1564553224432	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.432	\N	\N
20190731-120704-p3a3uQVOA8PA1Wx	1564553224432	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.432	\N	\N
20190731-120704-GKhOFmVK1egyoAf	1564553224432	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.432	\N	\N
20190731-120704-9ceMR9X08Ov2V7N	1564553224432	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.432	\N	\N
20190731-120704-ziE9BJtEfHvsYnc	1564553224433	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.433	\N	\N
20190731-120704-GEKtU08qVNwBIsD	1564553224433	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.433	\N	\N
20190731-120704-A4xxSGyakXFA8ab	1564553224433	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.433	\N	\N
20190731-120704-sK4VAujJT4dKVK3	1564553224433	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.433	\N	\N
20190731-120704-kAqWJbjTn1cLYbw	1564553224433	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.433	\N	\N
20190731-120704-o288tDwliDQbzKw	1564553224434	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.434	\N	\N
20190731-120704-yFGulrw3CmHKS14	1564553224434	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.434	\N	\N
20190731-120704-gG8WOdMUjWE19e3	1564553224434	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.434	\N	\N
20190731-120704-I0vpBZV1091g5eA	1564553224434	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.434	\N	\N
20190731-120704-EgcV6VNhd6t96oC	1564553224434	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.434	\N	\N
20190731-120704-RjPv2Or1Zy3tdTb	1564553224435	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.435	\N	\N
20190731-120704-HIM1IopeIOX9izz	1564553224435	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.435	\N	\N
20190731-120704-2rGBxSRNyezG5OI	1564553224435	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.435	\N	\N
20190731-120704-RVbjpLobk9lkIhB	1564553224435	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.435	\N	\N
20190731-120704-hIzGmDbKpxvAYsN	1564553224435	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.435	\N	\N
20190731-120704-cvUo9jFRbLdZ8fq	1564553224436	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.436	\N	\N
20190731-120704-dPeuf3HTklpFVmk	1564553224436	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.436	\N	\N
20190731-120704-AqnTFnM6ubUgwmE	1564553224436	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.436	\N	\N
20190731-120704-A5r5G2ZNZbAU4Py	1564553224436	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.436	\N	\N
20190731-120704-6ZNcDbnLgqXFj1y	1564553224436	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.436	\N	\N
20190731-120704-Lj1PgXkJzurrCFz	1564553224437	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.437	\N	\N
20190731-120704-z1cHNbPHVUHIJcv	1564553224437	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.437	\N	\N
20190731-120704-PRjNzT1Rhrn6LD6	1564553224437	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.437	\N	\N
20190731-120704-G8m6bGne1yNqXOG	1564553224437	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.437	\N	\N
20190731-120704-FQB7LvCZB2TIqgT	1564553224437	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.437	\N	\N
20190731-120704-hUbvfjNbfvZZpTz	1564553224438	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.438	\N	\N
20190731-120704-4dKeRvC4OAtFpRx	1564553224438	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.438	\N	\N
20190731-120704-dUMVsZz1E3I8fk0	1564553224438	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.438	\N	\N
20190731-120704-HcZP3QeMQSYzo0y	1564553224438	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.438	\N	\N
20190731-120704-clpWi6TC4kPYpbk	1564553224438	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.438	\N	\N
20190731-120704-8F32PAs57Pib4AC	1564553224438	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.438	\N	\N
20190731-120704-NKudCxKPXnjX1CK	1564553224439	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.439	\N	\N
20190731-120704-LfKb9diD9fsXWMg	1564553224439	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.439	\N	\N
20190731-120704-7zjgLLKNmqB7vZq	1564553224439	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.439	\N	\N
20190731-120704-fk4vsNeIWC6j7kQ	1564553224439	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.439	\N	\N
20190731-120704-dI4eLjbIaWPxAEr	1564553224439	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.439	\N	\N
20190731-120704-4spcJY5gTwnjCnT	1564553224440	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.44	\N	\N
20190731-120704-uI9R60xLTZFLrr8	1564553224440	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.44	\N	\N
20190731-120704-M1S6dUtuwdQpNXK	1564553224440	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.44	\N	\N
20190731-120704-miauCds1EPxr5z2	1564553224440	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.44	\N	\N
20190731-120704-rFaQgXBMbvfcVtG	1564553224440	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.44	\N	\N
20190731-120704-WQ38GjiCs45CHvi	1564553224441	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.441	\N	\N
20190731-120704-UizJegU9Yq7XdQf	1564553224441	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.441	\N	\N
20190731-120704-Mj7HStKDIbOjake	1564553224441	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.441	\N	\N
20190731-120704-N3Kmv73GkIYSCCv	1564553224441	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.441	\N	\N
20190731-120704-VKHgA0GQtyDnd47	1564553224441	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.441	\N	\N
20190731-120704-uxqntLw0ahs3Z5Q	1564553224442	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.442	\N	\N
20190731-120704-zkq45tRb9wTA1lQ	1564553224442	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.442	\N	\N
20190731-120704-LxTbktpbGrfSCK8	1564553224442	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.442	\N	\N
20190731-120704-q5cSdBmRIUG2djg	1564553224442	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.442	\N	\N
20190731-120704-JpzTqroa5cdMXgg	1564553224442	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.442	\N	\N
20190731-120704-lpdc4mhDiZvGsax	1564553224443	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.443	\N	\N
20190731-120704-Cl4NG3Mrdb4zaBB	1564553224443	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.443	\N	\N
20190731-120704-LJLkZssh9sWsQjd	1564553224443	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.443	\N	\N
20190731-120704-Bd10Sj90NVoMwhV	1564553224443	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.443	\N	\N
20190731-120704-mFg1tjvxYt4Rck0	1564553224443	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.443	\N	\N
20190731-120704-75kIaCOLZtP2Lfd	1564553224443	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.443	\N	\N
20190731-120704-qhBwdCoe58Grgu6	1564553224444	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.444	\N	\N
20190731-120704-vtubZWHr0QfqXcj	1564553224444	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.444	\N	\N
20190731-120704-vvPs4Vo4hckibIZ	1564553224444	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.444	\N	\N
20190731-120704-pePETgpzgv8nbZS	1564553224444	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.444	\N	\N
20190731-120704-jp5Gop6yDVrq0U8	1564553224444	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.444	\N	\N
20190731-120704-sMSU8b4etgVHOdw	1564553224445	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.445	\N	\N
20190731-120704-5FgfQ2811s9ekNF	1564553224445	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.445	\N	\N
20190731-120704-teGcX2hEev22pAJ	1564553224445	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.445	\N	\N
20190731-120704-xHjDfOVCf66sIvF	1564553224445	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.445	\N	\N
20190731-120704-qoadd6F6Z4WngTg	1564553224445	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.445	\N	\N
20190731-120704-C7X5vuHeZBqN4CW	1564553224446	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.446	\N	\N
20190731-120704-4IkYjh7jMEBp3gV	1564553224446	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.446	\N	\N
20190731-120704-bqJUotWU7jejuKl	1564553224446	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.446	\N	\N
20190731-120704-rXbDIy3LBD44HiB	1564553224446	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.446	\N	\N
20190731-120704-I7oTn50rUqivHWo	1564553224446	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.446	\N	\N
20190731-120704-b2i8edtxp4WqzKL	1564553224447	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.447	\N	\N
20190731-120704-qDA5rNv4GMUFysI	1564553224447	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.447	\N	\N
20190731-120704-E8aswGQNxRnWiFR	1564553224447	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.447	\N	\N
20190731-120704-npbN0bVr6776fSo	1564553224447	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.447	\N	\N
20190731-120704-ybUpboTUbocQT5I	1564553224447	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.447	\N	\N
20190731-120704-eC9ZhZhfu3eFqhp	1564553224448	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.448	\N	\N
20190731-120704-QM3lyUDLspOXZai	1564553224448	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.448	\N	\N
20190731-120704-TrM7rBe4qWU5IIF	1564553224448	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.448	\N	\N
20190731-120704-cOZMEkCzQDUQa7u	1564553224448	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.448	\N	\N
20190731-120704-r0nSf0KoY9cvSBQ	1564553224449	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.449	\N	\N
20190731-120704-kzi52QbRJs1Xv2e	1564553224449	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.449	\N	\N
20190731-120704-QITy41g5hiv2vxa	1564553224449	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.449	\N	\N
20190731-120704-Zg2urRSLOeXhjMi	1564553224449	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.449	\N	\N
20190731-120704-ifvpON4GFipqlMR	1564553224449	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.449	\N	\N
20190731-120704-gxKre8ugHxG9MYp	1564553224450	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.45	\N	\N
20190731-120704-tTX36dYIv3TzWji	1564553224450	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.45	\N	\N
20190731-120704-A3r1OtSZkLwACV2	1564553224450	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.45	\N	\N
20190731-120704-LMexsTJkM7t78fm	1564553224450	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.45	\N	\N
20190731-120704-4UhcjTOCGnyCebG	1564553224451	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.451	\N	\N
20190731-120704-ln6GC0EThcn1ImW	1564553224451	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.451	\N	\N
20190731-120704-aLL8i8YcF9ZRzUB	1564553224451	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.451	\N	\N
20190731-120704-JzjjiOoylzpINXq	1564553224451	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.451	\N	\N
20190731-120704-iU6GYRMzrNCx1Bn	1564553224451	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.451	\N	\N
20190731-120704-0QVHLWXzmtewrtE	1564553224452	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.452	\N	\N
20190731-120704-4dnpd08Hr6D39qC	1564553224452	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.452	\N	\N
20190731-120704-lLCG9xsva6uc9I5	1564553224452	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.452	\N	\N
20190731-120704-BTRJN8JdTpAcT5r	1564553224452	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.452	\N	\N
20190731-120704-OJHjqd8vkfpU9OD	1564553224452	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.452	\N	\N
20190731-120704-iBsD19mAz1bzO7h	1564553224453	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.453	\N	\N
20190731-120704-kQQ6urfMOI8hjdt	1564553224453	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.453	\N	\N
20190731-120704-QQNYYtI5yFfrpaU	1564553224453	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.453	\N	\N
20190731-120704-ItbJYOpICAbNeFS	1564553224453	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.453	\N	\N
20190731-120704-Y3SDYFVX18HPP6j	1564553224454	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.454	\N	\N
20190731-120704-sfWAHFLZi3W1Cqn	1564553224454	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.454	\N	\N
20190731-120704-DTCnc9W2QLNZSPV	1564553224454	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.454	\N	\N
20190731-120704-WV8HPsawZprQo6d	1564553224454	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.454	\N	\N
20190731-120704-FH8Tm6hBRHPZLAw	1564553224454	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.454	\N	\N
20190731-120704-uTbesRDX15cgWA3	1564553224455	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.455	\N	\N
20190731-120704-snrEZufIorneX5W	1564553224455	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.455	\N	\N
20190731-120704-0zvaPSuOzpCP3Id	1564553224455	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.455	\N	\N
20190731-120704-UFoyy9ZmRZ2NZ8C	1564553224455	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.455	\N	\N
20190731-120704-LmYPaAhH27YwmYB	1564553224456	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.456	\N	\N
20190731-120704-MFheseHUfXPdqhy	1564553224456	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.456	\N	\N
20190731-120704-UwP6nBKXAb0vynp	1564553224456	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.456	\N	\N
20190731-120704-wIZviwkZnAARIQJ	1564553224456	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.456	\N	\N
20190731-120704-VJwyIxsflDCHnGI	1564553224457	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.457	\N	\N
20190731-120704-vU2e9DpL24JldcX	1564553224457	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.457	\N	\N
20190731-120704-ZHQgNjd6NN8m9XA	1564553224457	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.457	\N	\N
20190731-120704-Xrf39i6EfrkzY8k	1564553224457	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.457	\N	\N
20190731-120704-i7rCJypuM0YLZWE	1564553224457	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.457	\N	\N
20190731-120704-NeOrApxwy5xODu2	1564553224458	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.458	\N	\N
20190731-120704-LhJ6AS1XQYMGoSN	1564553224458	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.458	\N	\N
20190731-120704-o6uUjSESbfVGCYk	1564553224458	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.458	\N	\N
20190731-120704-sG05zRz35bxIN2e	1564553224458	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.458	\N	\N
20190731-120704-9HSAyCKoToTJ8Q7	1564553224459	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.459	\N	\N
20190731-120704-k1R2SgJTB3cCwvA	1564553224459	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.459	\N	\N
20190731-120704-z2Fw3rvBLxpXIcr	1564553224459	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.459	\N	\N
20190731-120704-3UEBdgpvMoAkaK2	1564553224459	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.459	\N	\N
20190731-120704-e7kcWRaYC1LE0o8	1564553224459	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.459	\N	\N
20190731-120704-5pRUTpzQsE2Us0W	1564553224460	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.46	\N	\N
20190731-120704-urchjUHdK5xt0ig	1564553224460	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.46	\N	\N
20190731-120704-G0WGlM2Dd48VCZm	1564553224460	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.46	\N	\N
20190731-120704-IWuj6AfDxdNIERV	1564553224460	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.46	\N	\N
20190731-120704-I3B3I6l91aCVJVY	1564553224461	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.461	\N	\N
20190731-120704-ffThnWDOGgTqL8G	1564553224461	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.461	\N	\N
20190731-120704-YT5OotGhkkVTlo7	1564553224461	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.461	\N	\N
20190731-120704-mMeotRnMar8dFx6	1564553224461	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.461	\N	\N
20190731-120704-8H8uYyuMn1KADat	1564553224461	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.461	\N	\N
20190731-120704-4agHVc3TakURWmz	1564553224462	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.462	\N	\N
20190731-120704-ee5zYflBhZMRh5d	1564553224462	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.462	\N	\N
20190731-120704-yUabTVNbzUr33OV	1564553224462	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.462	\N	\N
20190731-120704-uRQryM9wM2YQ2sl	1564553224462	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.462	\N	\N
20190731-120704-4F28LvorW1eT7Sp	1564553224462	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.462	\N	\N
20190731-120704-eTUNr5mBVbnFusv	1564553224463	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.463	\N	\N
20190731-120704-8A4sFSMeZPjQfLJ	1564553224463	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.463	\N	\N
20190731-120704-ldo0qwmFLMYwI3C	1564553224463	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.463	\N	\N
20190731-120704-6GvN8bdpHWOt9HF	1564553224463	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.463	\N	\N
20190731-120704-okzjzCvNXpbpYvz	1564553224464	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.464	\N	\N
20190731-120704-ZKl1Tu5comkDszH	1564553224464	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.464	\N	\N
20190731-120704-Z2m74u6sWYhl7vv	1564553224464	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.464	\N	\N
20190731-120704-BNCheOW8XtnyrC7	1564553224464	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.464	\N	\N
20190731-120704-PUu1XnO2z1rQq9A	1564553224464	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.464	\N	\N
20190731-120704-yz8SBHRVo5hbIdY	1564553224465	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.465	\N	\N
20190731-120704-Ko7xT7t2KJyjURl	1564553224465	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.465	\N	\N
20190731-120704-FEZqHoEObHeXCDi	1564553224465	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.465	\N	\N
20190731-120704-3ypIKdc4Gjl5SUG	1564553224465	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.465	\N	\N
20190731-120704-62MFsdjFN52BYPX	1564553224465	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.465	\N	\N
20190731-120704-YZZvTtmSJzTshIZ	1564553224466	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.466	\N	\N
20190731-120704-fiI4cq7N30J7g5F	1564553224466	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.466	\N	\N
20190731-120704-RpcvCRNIS9GH0GE	1564553224466	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.466	\N	\N
20190731-120704-5Ugg0MxbGsGWjTo	1564553224466	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.466	\N	\N
20190731-120704-d3QFRrhkmvW9pln	1564553224467	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.467	\N	\N
20190731-120704-j3thqx1Homtxcyg	1564553224467	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.467	\N	\N
20190731-120704-u7kV3HaQKUe3ilu	1564553224467	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.467	\N	\N
20190731-120704-zj2kgjMKNGymofw	1564553224467	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.467	\N	\N
20190731-120704-rbxtH7WsO2WzRcP	1564553224467	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.467	\N	\N
20190731-120704-ug3wYUsxEda0vYs	1564553224467	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.467	\N	\N
20190731-120704-uJNE11iRmH9GYKz	1564553224468	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.468	\N	\N
20190731-120704-WbU6nlN3zfvWbAG	1564553224468	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.468	\N	\N
20190731-120704-UZrCjEP591yD0p1	1564553224468	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.468	\N	\N
20190731-120704-x3aF2lkZZLtlQ0X	1564553224468	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.468	\N	\N
20190731-120704-U8KAnOY0EH9nsRY	1564553224468	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.468	\N	\N
20190731-120704-DV2KvgBV1cMl5pY	1564553224469	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.469	\N	\N
20190731-120704-6pYaj9t9QgHhmS4	1564553224469	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.469	\N	\N
20190731-120704-fgrthseQOAd1kTx	1564553224469	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.469	\N	\N
20190731-120704-bqGardoZuCJGT0M	1564553224469	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.469	\N	\N
20190731-120704-T42byjeRzWWUrlO	1564553224469	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.469	\N	\N
20190731-120704-6wxl1w7Js8vGIos	1564553224470	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.47	\N	\N
20190731-120704-0CCmxYEAuuRpDG0	1564553224470	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.47	\N	\N
20190731-120704-duj0PrLu69vuQ6w	1564553224470	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.47	\N	\N
20190731-120704-Bl7TvVvkOh3k3z4	1564553224470	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.47	\N	\N
20190731-120704-l9YIQ1k1ITl3hZD	1564553224470	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.47	\N	\N
20190731-120704-R3G4LRnCZeXdHXb	1564553224471	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.471	\N	\N
20190731-120704-unI4h8o2tyYDmqI	1564553224471	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.471	\N	\N
20190731-120704-uI3Em6Wi5tDOATs	1564553224471	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.471	\N	\N
20190731-120704-rcIqJMHtKcej9XS	1564553224471	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.471	\N	\N
20190731-120704-5YxpJchZFZHvU21	1564553224471	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.471	\N	\N
20190731-120704-FICJVpueBRImMxf	1564553224472	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.472	\N	\N
20190731-120704-Km4AkgnlPIA65hd	1564553224472	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.472	\N	\N
20190731-120704-EH4r3d6YdYiDpzM	1564553224472	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.472	\N	\N
20190731-120704-wiczebaJVpO2Fpd	1564553224472	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.472	\N	\N
20190731-120704-dp3hs4RWUhKbqno	1564553224472	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.472	\N	\N
20190731-120704-02FO2WWf3eNtU0q	1564553224472	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.472	\N	\N
20190731-120704-FLeT6vApAijpRnS	1564553224473	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.473	\N	\N
20190731-120704-ZmW95ft44C7FL0N	1564553224473	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.473	\N	\N
20190731-120704-rZRvijiKHaatkLV	1564553224473	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.473	\N	\N
20190731-120704-fMB6Q5ZIDWAU1pd	1564553224473	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.473	\N	\N
20190731-120704-MLw0rPoOxZqIeGe	1564553224473	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.473	\N	\N
20190731-120704-RCd7ylhKa3vfZKZ	1564553224474	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.474	\N	\N
20190731-120704-UwzmnUxdeBoIoga	1564553224474	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.474	\N	\N
20190731-120704-eT5Xjfl8x03jMC4	1564553224474	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.474	\N	\N
20190731-120704-T5HdsJYva0JzSuj	1564553224474	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.474	\N	\N
20190731-120704-nu4nIdE5EhSisfE	1564553224474	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.474	\N	\N
20190731-120704-Mz1i8hCeFJoxJRM	1564553224475	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.475	\N	\N
20190731-120704-MbTjaRgm4t74Iiy	1564553224475	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.475	\N	\N
20190731-120704-l8yfK487ylatrcE	1564553224475	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.475	\N	\N
20190731-120704-uJ6C9QjGIlLTqax	1564553224475	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.475	\N	\N
20190731-120704-b2ZR1y0BZHnLu4J	1564553224475	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.475	\N	\N
20190731-120704-DM8npoAVKN7Lzk6	1564553224476	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.476	\N	\N
20190731-120704-QxbP0xubI7ouaHT	1564553224476	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.476	\N	\N
20190731-120704-scC1UxSjJOjohmH	1564553224476	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.476	\N	\N
20190731-120704-Qtm6FKl1hOXJSjT	1564553224476	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.476	\N	\N
20190731-120704-pQg0Cp5v80C0kIR	1564553224476	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.476	\N	\N
20190731-120704-iT1xn3jPkMHUlIb	1564553224477	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.477	\N	\N
20190731-120704-5yMfiVo74dLkm77	1564553224477	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.477	\N	\N
20190731-120704-j9OXWOasWDOmUKJ	1564553224477	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.477	\N	\N
20190731-120704-5tJapDaXHlaJQok	1564553224477	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.477	\N	\N
20190731-120704-S2kEJ2yc5By9WOv	1564553224477	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.477	\N	\N
20190731-120704-mtGGItBikC5sPqk	1564553224478	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.478	\N	\N
20190731-120704-fvemieBZdUspRK0	1564553224478	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.478	\N	\N
20190731-120704-oiQHu0AVLD7Ovai	1564553224478	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.478	\N	\N
20190731-120704-soFnBHNTnpyhBB9	1564553224478	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.478	\N	\N
20190731-120704-xO6WzmA2C5PDdx3	1564553224479	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.479	\N	\N
20190731-120704-Z4afYZ50SA75LpS	1564553224479	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.479	\N	\N
20190731-120704-Yr6VEsz8af9nfbe	1564553224479	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.479	\N	\N
20190731-120704-2LCErh1xWfj3H17	1564553224479	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.479	\N	\N
20190731-120704-jgc9nCdR88if0CA	1564553224480	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.48	\N	\N
20190731-120704-GCznB1noNa30kYU	1564553224480	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.48	\N	\N
20190731-120704-xRsXc6YVjmmINlJ	1564553224480	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.48	\N	\N
20190731-120704-fYvN661EEZzmsat	1564553224480	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.48	\N	\N
20190731-120704-uFneGhNQFZT6VwK	1564553224480	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.48	\N	\N
20190731-120704-yPMoKqmvBX3VRr9	1564553224481	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.481	\N	\N
20190731-120704-NOidRcSGBymymcZ	1564553224481	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.481	\N	\N
20190731-120704-hbiW23Z3scjpfZ2	1564553224481	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.481	\N	\N
20190731-120704-yKhnOMv2VmbS8bV	1564553224481	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.481	\N	\N
20190731-120704-72ygwO7xQ0CVS9g	1564553224481	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.481	\N	\N
20190731-120704-OD5zmRx6rChlwMO	1564553224482	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.482	\N	\N
20190731-120704-7B0b1RipInZh0dP	1564553224482	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.482	\N	\N
20190731-120704-e9KljiJnniXGKLv	1564553224482	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.482	\N	\N
20190731-120704-8A4ME42CWIKD5Yn	1564553224482	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.482	\N	\N
20190731-120704-kYbIxGjqa2KJyPn	1564553224483	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.483	\N	\N
20190731-120704-nggiLuIrC5BHtYD	1564553224483	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.483	\N	\N
20190731-120704-PDeRqmWwM7pTUlz	1564553224483	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.483	\N	\N
20190731-120704-hZeZgAe4JoJMxV5	1564553224483	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.483	\N	\N
20190731-120704-q6WKzu6AYSpJP1x	1564553224483	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.483	\N	\N
20190731-120704-LeAULPqDRw43U0z	1564553224484	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.484	\N	\N
20190731-120704-sHe2MZp3n4d0kXk	1564553224484	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.484	\N	\N
20190731-120704-isO07PDcedowX7b	1564553224484	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.484	\N	\N
20190731-120704-JjdqKaBWrRriICm	1564553224484	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.484	\N	\N
20190731-120704-zqIxaK7ivquRpKb	1564553224484	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.484	\N	\N
20190731-120704-AAeRNqMGFCOhUzU	1564553224485	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.485	\N	\N
20190731-120704-6fFsBL3Q7vltfq2	1564553224485	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.485	\N	\N
20190731-120704-73OAzNIYTDfqi9T	1564553224485	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.485	\N	\N
20190731-120704-QlmxESFpgfyXrTn	1564553224485	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.485	\N	\N
20190731-120704-1Sn0jknZ1Tbboi2	1564553224485	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.485	\N	\N
20190731-120704-IJH8twJMIB6GG4k	1564553224485	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.485	\N	\N
20190731-120704-AHblsuSUPIlN41e	1564553224486	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.486	\N	\N
20190731-120704-qZlLhiDR0xYLRvU	1564553224486	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.486	\N	\N
20190731-120704-N9O1he5n14getel	1564553224486	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.486	\N	\N
20190731-120704-2syEYnm4mgfzvy0	1564553224486	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.486	\N	\N
20190731-120704-Nql8GShqmKWl4Ye	1564553224486	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.486	\N	\N
20190731-120704-HoJVvjM7lJRR3a3	1564553224487	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.487	\N	\N
20190731-120704-CS77HjXHWgQ7jvW	1564553224487	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.487	\N	\N
20190731-120704-kKDIVBy6XkrFyEX	1564553224487	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.487	\N	\N
20190731-120704-MNYpHGkrA81ATgy	1564553224487	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.487	\N	\N
20190731-120704-8GGuiDvNHfJGAOS	1564553224487	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.487	\N	\N
20190731-120704-tJYT1ZSsCgm0aWW	1564553224488	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.488	\N	\N
20190731-120704-gLZca8C64paZo4P	1564553224488	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.488	\N	\N
20190731-120704-l19B2EfxfRGEZdC	1564553224488	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.488	\N	\N
20190731-120704-0iacd4JnGOXIyVr	1564553224488	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.488	\N	\N
20190731-120704-PmZmsYeALIc4Gyd	1564553224488	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.488	\N	\N
20190731-120704-FHrlUsqkX4DwVeh	1564553224489	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.489	\N	\N
20190731-120704-8YWP5tMAz8aiO6C	1564553224489	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.489	\N	\N
20190731-120704-Bn0iPwUlCVbsUwo	1564553224489	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.489	\N	\N
20190731-120704-JHucwvONNzD2BDe	1564553224489	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.489	\N	\N
20190731-120704-SEfcWL2SX1M7elR	1564553224489	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.489	\N	\N
20190731-120704-p6EnAPOJQlROsSP	1564553224490	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.49	\N	\N
20190731-120704-wJjR0P540SpbKPu	1564553224490	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.49	\N	\N
20190731-120704-bYQGJsoXW2YsE4v	1564553224490	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.49	\N	\N
20190731-120704-MFSNEx15Vnqsivn	1564553224490	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.49	\N	\N
20190731-120704-TL1eWqMu7M7EVZU	1564553224490	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.49	\N	\N
20190731-120704-J8qqP8sWEdmF2Qb	1564553224491	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.491	\N	\N
20190731-120704-497jE8jMLTUKcVl	1564553224491	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.491	\N	\N
20190731-120704-D2ZJmMSMnCCtsW9	1564553224491	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.491	\N	\N
20190731-120704-GaRWeO3RQv6WEnh	1564553224491	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.491	\N	\N
20190731-120704-EadGPlswgSXT3DN	1564553224492	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.492	\N	\N
20190731-120704-xodQCpUcG9i7dSP	1564553224492	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.492	\N	\N
20190731-120704-QdAtsGogKDoKUsR	1564553224492	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.492	\N	\N
20190731-120704-W0OBrRrurNKmhjk	1564553224492	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.492	\N	\N
20190731-120704-3nTf0yPF5UmAyIs	1564553224492	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.492	\N	\N
20190731-120704-PpdJesy1Tkt5b7q	1564553224493	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.493	\N	\N
20190731-120704-rwNvNS6ltMqI0FH	1564553224493	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.493	\N	\N
20190731-120704-PADohZxQ5xSFGUP	1564553224493	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.493	\N	\N
20190731-120704-1Q7RzFyhQYDL1ea	1564553224493	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.493	\N	\N
20190731-120704-HsQLdGMYgQJ9PgX	1564553224493	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.493	\N	\N
20190731-120704-dqkxO1VaXvbbTvC	1564553224494	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.494	\N	\N
20190731-120704-mgAdML035dPWxl3	1564553224494	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.494	\N	\N
20190731-120704-f9dtwiD1rAcZ4sK	1564553224494	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.494	\N	\N
20190731-120704-YBJquqG716pMByT	1564553224494	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.494	\N	\N
20190731-120704-YXY0g8yAX8Ea6DT	1564553224495	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.495	\N	\N
20190731-120704-VGoRb1J5mJ2vyCl	1564553224495	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.495	\N	\N
20190731-120704-DUPG6wVRrVr2Xi2	1564553224495	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.495	\N	\N
20190731-120704-IkeSQKyRmoLEiCt	1564553224495	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.495	\N	\N
20190731-120704-n7UBPskTSjl0Kww	1564553224495	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.495	\N	\N
20190731-120704-2nR2lucZkpdoUAH	1564553224496	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.496	\N	\N
20190731-120704-SzmiYkzA2DUHPCF	1564553224496	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.496	\N	\N
20190731-120704-Q5nI7uK5ASLAZYZ	1564553224496	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.496	\N	\N
20190731-120704-67YHgT9ax6I55BQ	1564553224496	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.496	\N	\N
20190731-120704-e4WOfw1CZ7UrMaI	1564553224496	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.496	\N	\N
20190731-120704-6gXXIZSrhGaMfxu	1564553224497	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.497	\N	\N
20190731-120704-hS6Vp0pCR3NMYNs	1564553224497	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.497	\N	\N
20190731-120704-g2P8OmIHyJUjk4b	1564553224497	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.497	\N	\N
20190731-120704-xrVFtJZ4CaIu0PV	1564553224497	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.497	\N	\N
20190731-120704-pdo6TNbyteAnGf8	1564553224498	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.498	\N	\N
20190731-120704-nHGnn88qd0LnR9s	1564553224498	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.498	\N	\N
20190731-120704-PZLr38hvWgJyybl	1564553224498	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.498	\N	\N
20190731-120704-5blrK314PVciKIX	1564553224498	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.498	\N	\N
20190731-120704-kutDT8KPMSESQ1s	1564553224498	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.498	\N	\N
20190731-120704-8sTt59ia4JomVtR	1564553224499	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.499	\N	\N
20190731-120704-QrEB47YOU594WSF	1564553224499	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.499	\N	\N
20190731-120704-ZKA1WjSsGVHsvCv	1564553224499	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.499	\N	\N
20190731-120704-UG73WFffinbTYhm	1564553224499	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.499	\N	\N
20190731-120704-3tu9KR1hEZgy30h	1564553224499	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.499	\N	\N
20190731-120704-HChwQ2n9XLZhdnZ	1564553224500	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.5	\N	\N
20190731-120704-zkP4cGsDaVGUhJ4	1564553224500	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.5	\N	\N
20190731-120704-ZnqFn83VBUsK6Gk	1564553224500	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.5	\N	\N
20190731-120704-WmiLsex5l5rAGHV	1564553224500	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.5	\N	\N
20190731-120704-kD97YAprPTOfALG	1564553224501	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.501	\N	\N
20190731-120704-3HOj0AqmyBbky7o	1564553224501	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.501	\N	\N
20190731-120704-pUD1zUCoPouvVos	1564553224501	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.501	\N	\N
20190731-120704-8VABvOFWrii8wIP	1564553224501	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.501	\N	\N
20190731-120704-vUemlWEEukFmcdQ	1564553224501	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.501	\N	\N
20190731-120704-0OtrPiCmprZjaUO	1564553224502	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.502	\N	\N
20190731-120704-VaoMihy16Ssc5ae	1564553224502	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.502	\N	\N
20190731-120704-iGybYFDtK5UW8CZ	1564553224502	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.502	\N	\N
20190731-120704-fD95CmYYNr3uQTP	1564553224502	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.502	\N	\N
20190731-120704-M6p0iFHm9OAUQof	1564553224503	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.503	\N	\N
20190731-120704-EV0gMGR0twgjI5E	1564553224503	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.503	\N	\N
20190731-120704-0I9Vx8oxf5MQcM7	1564553224503	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.503	\N	\N
20190731-120704-hGS0oFk3FVHYIi6	1564553224503	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.503	\N	\N
20190731-120704-gl60OJLDCdcXG9v	1564553224503	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.503	\N	\N
20190731-120704-lueoTwBDywdvCwv	1564553224503	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.503	\N	\N
20190731-120704-TIvcAHZegt6mirX	1564553224504	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.504	\N	\N
20190731-120704-ISuBVFlbUhTLikR	1564553224504	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.504	\N	\N
20190731-120704-yND8HaD6ikbFJjr	1564553224504	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.504	\N	\N
20190731-120704-1ot2aqjQC7SSlEH	1564553224504	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.504	\N	\N
20190731-120704-KGwskkrqrWFbIwk	1564553224504	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.504	\N	\N
20190731-120704-pZv3iMpcjRcm1Zk	1564553224505	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.505	\N	\N
20190731-120704-sm6fVjlh0pDP6Vc	1564553224505	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.505	\N	\N
20190731-120704-i0VhJ93uv0APMD8	1564553224505	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.505	\N	\N
20190731-120704-IgxkEpU5rYdl3oQ	1564553224505	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.505	\N	\N
20190731-120704-Y8GZGvV3697pNY3	1564553224505	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.505	\N	\N
20190731-120704-erEJb7UBYyQQCxr	1564553224506	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.506	\N	\N
20190731-120704-z05qepvdTqLc237	1564553224506	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.506	\N	\N
20190731-120704-KihNER7mQloAfFN	1564553224506	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.506	\N	\N
20190731-120704-20ZKODaV1PtuqKu	1564553224506	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.506	\N	\N
20190731-120704-b50IowldA3gPNdy	1564553224506	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.506	\N	\N
20190731-120704-L3ZuS6W6TlJPMpx	1564553224507	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.507	\N	\N
20190731-120704-W76M287nukbbz2g	1564553224507	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.507	\N	\N
20190731-120704-TtL5uWv49sCDYY0	1564553224507	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.507	\N	\N
20190731-120704-V6BUIiA9ujbkWFc	1564553224507	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.507	\N	\N
20190731-120704-cNhHw68B2uBM9jt	1564553224507	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.507	\N	\N
20190731-120704-kN9jFcMoaaU0Rhh	1564553224508	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.508	\N	\N
20190731-120704-arq6ERQRN3Vh3hx	1564553224508	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.508	\N	\N
20190731-120704-7K7i7qvVtQIU6jQ	1564553224508	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.508	\N	\N
20190731-120704-9kBoZRSH7gwuK7B	1564553224508	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.508	\N	\N
20190731-120704-y19UxGd4uiwF4QB	1564553224508	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.508	\N	\N
20190731-120704-N8ya1Syrs2ULitu	1564553224509	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.509	\N	\N
20190731-120704-0Y7v7BglWrFWHYA	1564553224509	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.509	\N	\N
20190731-120704-nhTFIb1z8MMxqo8	1564553224509	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.509	\N	\N
20190731-120704-woTHdS74YNuaP3M	1564553224509	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.509	\N	\N
20190731-120704-ZxBr1xyqkeiY3DP	1564553224509	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.509	\N	\N
20190731-120704-x3vZ0A5iBtMSUbq	1564553224510	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.51	\N	\N
20190731-120704-1YlPRKBR49h2rZH	1564553224510	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.51	\N	\N
20190731-120704-dKybC8Y3YHNrgR0	1564553224510	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.51	\N	\N
20190731-120704-VZLUUaYTXDjrd0Q	1564553224510	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.51	\N	\N
20190731-120704-Wbq6JQf2V0FlpHI	1564553224510	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.51	\N	\N
20190731-120704-ZMIryTIVj69e6VN	1564553224511	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.511	\N	\N
20190731-120704-xMChWDnVyTBpgSx	1564553224511	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.511	\N	\N
20190731-120704-WQGswPOUgjwovND	1564553224511	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.511	\N	\N
20190731-120704-lpaXBhYBm2y2Ei8	1564553224511	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.511	\N	\N
20190731-120704-sc4OesP0kxfYOt9	1564553224511	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.511	\N	\N
20190731-120704-qTEefAxX8ZmTrHK	1564553224512	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.512	\N	\N
20190731-120704-HCKVdJkzWKd3mom	1564553224512	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.512	\N	\N
20190731-120704-ExaxHwZx8Rn0YT3	1564553224512	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.512	\N	\N
20190731-120704-SQWdTMkdNemIvpK	1564553224512	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.512	\N	\N
20190731-120704-nWgBC9oGgS0jOgp	1564553224512	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.512	\N	\N
20190731-120704-mw6oPgRGSMuZtVw	1564553224512	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.512	\N	\N
20190731-120704-XqsM32NJGsYOHHk	1564553224513	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.513	\N	\N
20190731-120704-2YjshB3bnjz824F	1564553224513	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.513	\N	\N
20190731-120704-aDnjjCf58ePD52g	1564553224513	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.513	\N	\N
20190731-120704-xk91RYlJNtnMp54	1564553224513	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.513	\N	\N
20190731-120704-bfI1Dk65oW2Lzj3	1564553224513	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.513	\N	\N
20190731-120704-gSDjd7fm8ulPRi3	1564553224514	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.514	\N	\N
20190731-120704-R8kgbOq6Y8UYsQs	1564553224514	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.514	\N	\N
20190731-120704-Algvjiifbq07Ssz	1564553224514	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.514	\N	\N
20190731-120704-AYv3QYw38DjNEM4	1564553224514	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.514	\N	\N
20190731-120704-ZjlZ97rjTo5UrSA	1564553224514	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.514	\N	\N
20190731-120704-5Tfp39BWdfyChkT	1564553224515	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.515	\N	\N
20190731-120704-ItDDad7cHItcAcQ	1564553224515	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.515	\N	\N
20190731-120704-tosCFKHCm9CyiLT	1564553224515	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.515	\N	\N
20190731-120704-5o9Ubk3ffGQskXN	1564553224515	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.515	\N	\N
20190731-120704-6ZgmAS8Sne1HFSx	1564553224515	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.515	\N	\N
20190731-120704-POdWV6xbNldnyjM	1564553224516	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.516	\N	\N
20190731-120704-I5bOO72Rt97yylq	1564553224516	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.516	\N	\N
20190731-120704-8wu4b1wvUPXIDF4	1564553224516	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.516	\N	\N
20190731-120704-06wrZJc3F6FS65s	1564553224516	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.516	\N	\N
20190731-120704-z6F8Z0FR6UDe5Iq	1564553224516	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.516	\N	\N
20190731-120704-zWkjejMprUI2pLE	1564553224517	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.517	\N	\N
20190731-120704-HMM31hE6zCx5I1g	1564553224517	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.517	\N	\N
20190731-120704-7YqvRWR5k95KEn6	1564553224517	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.517	\N	\N
20190731-120704-PbLfY4whhzsU367	1564553224517	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.517	\N	\N
20190731-120704-3Er4AVkP1c9lxvA	1564553224517	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.517	\N	\N
20190731-120704-SebVZeeUtkbvXFa	1564553224518	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.518	\N	\N
20190731-120704-H3SIqQ1sVwCFy0E	1564553224518	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.518	\N	\N
20190731-120704-16E2gxBeYDZR8dA	1564553224518	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.518	\N	\N
20190731-120704-faXHxvIsOCsYyjF	1564553224518	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.518	\N	\N
20190731-120704-c8gD1A7xum2IvIg	1564553224518	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.518	\N	\N
20190731-120704-kHxxnUZ4swhrSKe	1564553224519	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.519	\N	\N
20190731-120704-qMlfnCWT3CwLiNZ	1564553224519	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.519	\N	\N
20190731-120704-psnIRpbUrjgIExO	1564553224519	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.519	\N	\N
20190731-120704-vebLE96TXO5XMEn	1564553224519	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.519	\N	\N
20190731-120704-QFtRDsDGYhFtYNI	1564553224519	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.519	\N	\N
20190731-120704-ui9snaxIgPEZ67k	1564553224520	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.52	\N	\N
20190731-120704-1SwGr9IWAaEatXT	1564553224520	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.52	\N	\N
20190731-120704-SfU1Uf4OLRh3A6N	1564553224520	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.52	\N	\N
20190731-120704-3uQaA92qp2cCC7l	1564553224520	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.52	\N	\N
20190731-120704-mMeNRUvKdzVm9u0	1564553224520	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.52	\N	\N
20190731-120704-B5XIZwGYo9eOlus	1564553224521	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.521	\N	\N
20190731-120704-FYsl8xJg7GIqyn0	1564553224521	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.521	\N	\N
20190731-120704-jFGpIgWq3hMQpBi	1564553224521	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.521	\N	\N
20190731-120704-PvDcdjwRSa028up	1564553224521	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.521	\N	\N
20190731-120704-bf1JGBE5ZXyIRrc	1564553224521	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.521	\N	\N
20190731-120704-HPFezxgimhvrC8E	1564553224522	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.522	\N	\N
20190731-120704-QLJIpEyuHM0L01v	1564553224522	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.522	\N	\N
20190731-120704-JL2SPwsc5abA7T6	1564553224522	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.522	\N	\N
20190731-120704-UhZqpOZ39iNK4Vi	1564553224522	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.522	\N	\N
20190731-120704-GLnQaeVwJ6nQZEI	1564553224522	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.522	\N	\N
20190731-120704-oVfAT2H2WRaAPdF	1564553224523	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.523	\N	\N
20190731-120704-vdWCTgvpZAyU1cw	1564553224523	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.523	\N	\N
20190731-120704-jTyjLg995mp1hML	1564553224523	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.523	\N	\N
20190731-120704-yoMRw1kJNhjHl5e	1564553224523	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.523	\N	\N
20190731-120704-XXYiRPA0FdvQTPC	1564553224523	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.523	\N	\N
20190731-120704-PmGpnPfmZjzjIYP	1564553224523	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.523	\N	\N
20190731-120704-dEIMjJjAqZOWlCz	1564553224524	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.524	\N	\N
20190731-120704-Jv2TwH42Vuz5hDd	1564553224524	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.524	\N	\N
20190731-120704-M1djj5CncuU72Wa	1564553224524	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.524	\N	\N
20190731-120704-wBJS0DlFh9eaAS5	1564553224524	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.524	\N	\N
20190731-120704-1ROFPrSqniHrXzg	1564553224524	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.524	\N	\N
20190731-120704-8tgKglRvOwxRaYe	1564553224525	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.525	\N	\N
20190731-120704-wvovP4QzuBhovmy	1564553224525	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.525	\N	\N
20190731-120704-51nwwDxqD1inqCW	1564553224525	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.525	\N	\N
20190731-120704-xkSGebcY3Tr6FMl	1564553224525	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.525	\N	\N
20190731-120704-8BIpOleFDqTG4Zn	1564553224525	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.525	\N	\N
20190731-120704-fVyRWhRzoZqA9Vk	1564553224526	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.526	\N	\N
20190731-120704-9ev5kyrm6fP5uWZ	1564553224526	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.526	\N	\N
20190731-120704-Uz2UTuHqdl557WG	1564553224526	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.526	\N	\N
20190731-120704-lNW2YfkafpZFPdu	1564553224526	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.526	\N	\N
20190731-120704-VLxLHblQBYIcdne	1564553224526	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.526	\N	\N
20190731-120704-nOHGDCifip0DCOb	1564553224527	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.527	\N	\N
20190731-120704-3hoVQOUmRD1LD1P	1564553224527	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.527	\N	\N
20190731-120704-wxMJO954bK429rr	1564553224527	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.527	\N	\N
20190731-120704-Lvu877qgvB9nOvh	1564553224527	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.527	\N	\N
20190731-120704-KsPz9sbHCMV1jzx	1564553224528	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.528	\N	\N
20190731-120704-ivPYdwvptboGOxO	1564553224528	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.528	\N	\N
20190731-120704-vgeOdBUNByFAHWH	1564553224528	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.528	\N	\N
20190731-120704-0oWy9DsYRQvc6PN	1564553224528	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.528	\N	\N
20190731-120704-zlLYPFaMsMGmGIS	1564553224528	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.528	\N	\N
20190731-120704-kKwW35NZMSzJgS5	1564553224529	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.529	\N	\N
20190731-120704-TwDGmpZdXmk5b4F	1564553224529	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.529	\N	\N
20190731-120704-Bnw5DazUKmKN3AH	1564553224529	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.529	\N	\N
20190731-120704-vDteCwYhJseWDwq	1564553224529	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.529	\N	\N
20190731-120704-ItRTitgzriHA8tW	1564553224529	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.529	\N	\N
20190731-120704-xuCnz8erxf3dWP6	1564553224529	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.529	\N	\N
20190731-120704-G30YRCtZqVSLopb	1564553224530	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.53	\N	\N
20190731-120704-Hyl7tNVnGiuGSan	1564553224530	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.53	\N	\N
20190731-120704-wqRtLpEIwsZIhFr	1564553224530	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.53	\N	\N
20190731-120704-7KFJgx8P0rrMCsi	1564553224530	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.53	\N	\N
20190731-120704-mKf6L6pPeKuGv1n	1564553224530	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.53	\N	\N
20190731-120704-4Mi62d8OiVHQwHR	1564553224531	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.531	\N	\N
20190731-120704-4zYGuOAjecuQ3Mz	1564553224531	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.531	\N	\N
20190731-120704-WlIHoFXakV1WznY	1564553224531	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.531	\N	\N
20190731-120704-yFbPhtNY2PwVd8Q	1564553224531	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.531	\N	\N
20190731-120704-YkhM38nBjg5I4tW	1564553224531	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.531	\N	\N
20190731-120704-3IF5eS4au9N4S1S	1564553224532	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.532	\N	\N
20190731-120704-DQJY53v0E3dLRZX	1564553224532	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.532	\N	\N
20190731-120704-9mLyPJoIlfwrueL	1564553224532	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.532	\N	\N
20190731-120704-yEsoFW5lMGAzACE	1564553224532	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.532	\N	\N
20190731-120704-IYk8yWCaI6bonEv	1564553224532	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.532	\N	\N
20190731-120704-xUQSAy9RjNn8AX3	1564553224533	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.533	\N	\N
20190731-120704-bqxPV37kGnTQ1Fi	1564553224533	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.533	\N	\N
20190731-120704-MeEDTVVTZhtPeLH	1564553224533	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.533	\N	\N
20190731-120704-fZUW9Q17OC4JNZi	1564553224533	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.533	\N	\N
20190731-120704-13U73NJDRbiRqba	1564553224533	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.533	\N	\N
20190731-120704-zHsFSj4rnUbrKEy	1564553224533	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.533	\N	\N
20190731-120704-1bTpCYUfI19wnOE	1564553224534	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.534	\N	\N
20190731-120704-ldYRRGkLpLvoROI	1564553224534	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.534	\N	\N
20190731-120704-aUMNcjQiZbSscyA	1564553224534	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.534	\N	\N
20190731-120704-ytFfiWRBECSu8r5	1564553224534	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.534	\N	\N
20190731-120704-gu7OpEJ8Z5EJijY	1564553224534	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.534	\N	\N
20190731-120704-NvpCIbxcAMRhP5L	1564553224535	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.535	\N	\N
20190731-120704-nsdgVaUvAhO17Zp	1564553224535	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.535	\N	\N
20190731-120704-VTwO1sujYpt8phz	1564553224535	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.535	\N	\N
20190731-120704-gNJlump02f4xnGd	1564553224535	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.535	\N	\N
20190731-120704-fx8InEnhRy5UJF1	1564553224535	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.535	\N	\N
20190731-120704-wOz9HyxlX3ynUQW	1564553224536	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.536	\N	\N
20190731-120704-Wu4VdcGAHDK5dGi	1564553224536	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.536	\N	\N
20190731-120704-yxq0pHwGxpLXKIw	1564553224536	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.536	\N	\N
20190731-120704-Mxmu3pZDy9YvDQB	1564553224536	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.536	\N	\N
20190731-120704-tdNqAj2ruKeDsEF	1564553224536	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.536	\N	\N
20190731-120704-cQqSUAQDPIsWfBN	1564553224537	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.537	\N	\N
20190731-120704-p8W7Vb0cd9emv8R	1564553224537	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.537	\N	\N
20190731-120704-jgghhYuYGwGALgO	1564553224537	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.537	\N	\N
20190731-120704-AMMxXh9QIvNUdg9	1564553224537	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.537	\N	\N
20190731-120704-lYUltY0CWmcwAtT	1564553224538	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.538	\N	\N
20190731-120704-lR5Mx8HR6TzCE7l	1564553224538	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.538	\N	\N
20190731-120704-xVm9RCapA8EkH8W	1564553224538	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.538	\N	\N
20190731-120704-Pg7X37MgXrDzXAR	1564553224538	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.538	\N	\N
20190731-120704-wRXNWz8gR6lD3YN	1564553224538	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.538	\N	\N
20190731-120704-8DHyJCDDD4x5Pwz	1564553224539	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.539	\N	\N
20190731-120704-Jho861N2qIx48zl	1564553224539	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.539	\N	\N
20190731-120704-Yuceum9UubyVryb	1564553224539	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.539	\N	\N
20190731-120704-RAX2LQIERwQgzMy	1564553224539	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.539	\N	\N
20190731-120704-7WFuYWkN2tJ9Lum	1564553224539	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.539	\N	\N
20190731-120704-WaqyLIqjBomDteE	1564553224539	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.539	\N	\N
20190731-120704-lotU3kOFMwtTVU7	1564553224540	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.54	\N	\N
20190731-120704-7ZxvFBHogQ5IXNL	1564553224540	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.54	\N	\N
20190731-120704-yd5OYhAbUWBRO17	1564553224540	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.54	\N	\N
20190731-120704-kF9AajILLboqAr4	1564553224540	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.54	\N	\N
20190731-120704-JFJ54ctNEMSwJ5M	1564553224540	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.54	\N	\N
20190731-120704-HZvRjgMLMjaZ4h6	1564553224541	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.541	\N	\N
20190731-120704-oKmmXBzplCap2sE	1564553224541	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.541	\N	\N
20190731-120704-3bNs4i4Mns48Qkj	1564553224541	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.541	\N	\N
20190731-120704-4Tx7HYZBZDA1ZfZ	1564553224541	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.541	\N	\N
20190731-120704-tbVkjYXabMkFYsN	1564553224541	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.541	\N	\N
20190731-120704-OVEtAbd37gNZSwP	1564553224542	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.542	\N	\N
20190731-120704-7JQN8SP3UieyM0A	1564553224542	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.542	\N	\N
20190731-120704-hJmrxwkb8DIN9xu	1564553224542	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.542	\N	\N
20190731-120704-NrLvDYzibwdSgHQ	1564553224542	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.542	\N	\N
20190731-120704-UEnz5MfM6t8iGLv	1564553224542	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.542	\N	\N
20190731-120704-49VvwNpGXPkWfLj	1564553224543	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.543	\N	\N
20190731-120704-EJXSXvfiLBSUiNf	1564553224543	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.543	\N	\N
20190731-120704-pC22Cfd8rkscwTe	1564553224543	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.543	\N	\N
20190731-120704-o3llytCZzQi9bqc	1564553224543	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.543	\N	\N
20190731-120704-zRxINkEYr863qID	1564553224543	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.543	\N	\N
20190731-120704-hMnOabJpwKxciBN	1564553224543	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.543	\N	\N
20190731-120704-Hc4QAiE5jBwm1At	1564553224544	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.544	\N	\N
20190731-120704-TVvOK2qH5uq8pu9	1564553224544	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.544	\N	\N
20190731-120704-dpF1D0TD0RbUEpm	1564553224544	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.544	\N	\N
20190731-120704-jCTAVYFgmI1mWBZ	1564553224544	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.544	\N	\N
20190731-120704-WQLYVxvZxbZlGpn	1564553224544	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.544	\N	\N
20190731-120704-xG2NjfBN38RzcpW	1564553224545	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.545	\N	\N
20190731-120704-g2IO8NsQ9TdPqOY	1564553224545	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.545	\N	\N
20190731-120704-zC3iMmBS81y7jcZ	1564553224545	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.545	\N	\N
20190731-120704-kTxITQTL0iL2vHI	1564553224545	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.545	\N	\N
20190731-120704-LYw0SLkU3H5HiCE	1564553224545	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.545	\N	\N
20190731-120704-MoXq8nHyR3F9oyw	1564553224546	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.546	\N	\N
20190731-120704-09cuCDMaVymOOSd	1564553224546	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.546	\N	\N
20190731-120704-X5CNsZYCZinKbbD	1564553224546	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.546	\N	\N
20190731-120704-VJnG482d1yFRKhV	1564553224546	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.546	\N	\N
20190731-120704-r4HhxmTAorE7XoM	1564553224547	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.547	\N	\N
20190731-120704-tsfdwyA01EKXUzE	1564553224547	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.547	\N	\N
20190731-120704-lNQ3m7d94wlPvWo	1564553224547	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.547	\N	\N
20190731-120704-99Fwz1PepKfSsID	1564553224547	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.547	\N	\N
20190731-120704-VVendV2ZlTUv2FX	1564553224547	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.547	\N	\N
20190731-120704-Pg8nhrN3F9Tlyfz	1564553224548	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.548	\N	\N
20190731-120704-ivWKnmX2oHGr1lr	1564553224548	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.548	\N	\N
20190731-120704-BsybY77EWaJRmDa	1564553224548	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.548	\N	\N
20190731-120704-kHF8aW5YQJ1EQVN	1564553224548	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.548	\N	\N
20190731-120704-rLpp93Y4i7OtJWL	1564553224549	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.549	\N	\N
20190731-120704-28SiI0aY95UmRZS	1564553224549	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.549	\N	\N
20190731-120704-KKNNtRGceh30U1o	1564553224549	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.549	\N	\N
20190731-120704-djw9tbzwLrJ9kiv	1564553224549	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.549	\N	\N
20190731-120704-EZSajxHXoviT6EO	1564553224549	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.549	\N	\N
20190731-120704-XGTJxLfKVH66R0k	1564553224550	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.55	\N	\N
20190731-120704-4ERznrfFuOvFj8Y	1564553224550	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.55	\N	\N
20190731-120704-TZzp1S0OwOa37ID	1564553224550	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.55	\N	\N
20190731-120704-LnsH7zc8Pv0qN8P	1564553224550	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.55	\N	\N
20190731-120704-2NFW0VGWRv6Ryh3	1564553224551	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.551	\N	\N
20190731-120704-21OWedzuHjDQQUS	1564553224551	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.551	\N	\N
20190731-120704-7Tv7Dx9qGHRjGCa	1564553224551	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.551	\N	\N
20190731-120704-GsHFkG8toWaeyjN	1564553224551	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.551	\N	\N
20190731-120704-HIJmX3zeTeRYtQ6	1564553224551	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.551	\N	\N
20190731-120704-z2MTAhvlcj4GKon	1564553224552	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.552	\N	\N
20190731-120704-9LaFJFoAp1ovQqc	1564553224552	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.552	\N	\N
20190731-120704-AuwPJBMNNWMGNN4	1564553224552	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.552	\N	\N
20190731-120704-SN8s65zT3k9i8bN	1564553224552	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.552	\N	\N
20190731-120704-6kffVPAsTefNclY	1564553224552	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.553	\N	\N
20190731-120704-aJgmIuLD4TrXyIN	1564553224553	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.553	\N	\N
20190731-120704-ka0AeRCzs4gkww4	1564553224553	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.553	\N	\N
20190731-120704-TxzmLHVFMiv8Pyp	1564553224553	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.553	\N	\N
20190731-120704-MOxxxcM3bGLdzXm	1564553224553	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.553	\N	\N
20190731-120704-l6oDDmru1Fq14xo	1564553224554	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.554	\N	\N
20190731-120704-BTu0WF5EbZye8hR	1564553224554	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.554	\N	\N
20190731-120704-ct2RoZLnCVDIU4Q	1564553224554	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.554	\N	\N
20190731-120704-RJGqDyao9aprvuy	1564553224554	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.554	\N	\N
20190731-120704-ayaOeySxBxu2HWT	1564553224554	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.554	\N	\N
20190731-120704-RidTQt6eMUXx7O8	1564553224555	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.555	\N	\N
20190731-120704-bXarzDwjf4I8jdX	1564553224555	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.555	\N	\N
20190731-120704-joK60QOX9fWUmJ7	1564553224555	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.555	\N	\N
20190731-120704-Nx2h9Vezz7rPGkO	1564553224555	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.555	\N	\N
20190731-120704-O0LvdraAUyoPxOp	1564553224555	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.555	\N	\N
20190731-120704-9PxcWVZ5NIvnX4s	1564553224556	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.556	\N	\N
20190731-120704-g5DgwwS0CvuMCA9	1564553224556	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.556	\N	\N
20190731-120704-f9vyTDfs66RC231	1564553224556	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.556	\N	\N
20190731-120704-5d8CYcBjYZisuFT	1564553224556	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.556	\N	\N
20190731-120704-3Ur35HuVW0gtncI	1564553224557	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.557	\N	\N
20190731-120704-uUz4x0pEUtO1d9b	1564553224557	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.557	\N	\N
20190731-120704-xUlYHCUKF4CPHlT	1564553224557	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.557	\N	\N
20190731-120704-UaW2F07SEn2fwfX	1564553224557	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.557	\N	\N
20190731-120704-ee9QnZ6PpXiVyyc	1564553224557	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.557	\N	\N
20190731-120704-dxrzrpDZgWOv34Q	1564553224558	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.558	\N	\N
20190731-120704-UtzXWzhbgvwdHrJ	1564553224558	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.558	\N	\N
20190731-120704-mxcWMGLZOyZTovC	1564553224558	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.558	\N	\N
20190731-120704-E3AjiKMbThUJUv7	1564553224558	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.558	\N	\N
20190731-120704-ku88RA9pw2oLehF	1564553224559	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.559	\N	\N
20190731-120704-Yj2mvZhIO43kT71	1564553224559	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.559	\N	\N
20190731-120704-hmzqCYD4A0itkdV	1564553224559	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.559	\N	\N
20190731-120704-SZIsVSNGOEJ0IW9	1564553224559	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.559	\N	\N
20190731-120704-SCFdd5iqQ27B5Kt	1564553224559	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.559	\N	\N
20190731-120704-XHpmZH9pPqeeWdz	1564553224560	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.56	\N	\N
20190731-120704-TDmbySyEbK9YhkF	1564553224560	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.56	\N	\N
20190731-120704-YpVBMOOurV7rL6J	1564553224560	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.56	\N	\N
20190731-120704-50XsIvyX8mOlKw7	1564553224560	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.56	\N	\N
20190731-120704-6cUpuOidn77pDSL	1564553224561	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.561	\N	\N
20190731-120704-MI0mU7moJTV7IC2	1564553224561	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.561	\N	\N
20190731-120704-QLCYmLNTCUZI3QQ	1564553224561	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.561	\N	\N
20190731-120704-MMdzRc2LxoeIjQb	1564553224561	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.561	\N	\N
20190731-120704-yTgfan3xR1VJoP5	1564553224561	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.561	\N	\N
20190731-120704-eJtKUbwrIsNTKag	1564553224562	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.562	\N	\N
20190731-120704-RFcki9SEoYwrCM0	1564553224562	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.562	\N	\N
20190731-120704-SbZNbS9ImapKf4N	1564553224562	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.562	\N	\N
20190731-120704-lk1fy4Fem78v95N	1564553224562	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.562	\N	\N
20190731-120704-AYf4k19YsqdiV61	1564553224562	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.562	\N	\N
20190731-120704-2VwT6YinJ92ps2p	1564553224563	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.563	\N	\N
20190731-120704-x58keQONDkegNZW	1564553224563	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.563	\N	\N
20190731-120704-vq3KF3K8oC86RXR	1564553224563	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.563	\N	\N
20190731-120704-6CYMmeH2fFRWK6r	1564553224563	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.563	\N	\N
20190731-120704-4Q893D21Aj6mEcB	1564553224564	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.564	\N	\N
20190731-120704-bUe5sa8GAtwgEee	1564553224564	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.564	\N	\N
20190731-120704-47SFht0JqPawSPf	1564553224564	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.564	\N	\N
20190731-120704-jMqzabjN35SGybS	1564553224564	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.564	\N	\N
20190731-120704-Bef7dNVhw4zSOc2	1564553224564	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.564	\N	\N
20190731-120704-qulRz3fkO6DXLcR	1564553224565	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.565	\N	\N
20190731-120704-oSR8lJ5lRaHshGx	1564553224565	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.565	\N	\N
20190731-120704-XQf7TaS3LJVFhZ6	1564553224565	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.565	\N	\N
20190731-120704-bBhnK5eK3cFjSeu	1564553224565	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.565	\N	\N
20190731-120704-mIEDwKBh6Sq5jvN	1564553224565	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.565	\N	\N
20190731-120704-34gr5KyS7PxmMut	1564553224566	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.566	\N	\N
20190731-120704-NdiJGwAZKPCJIUz	1564553224566	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.566	\N	\N
20190731-120704-Zhr7kYEolomIWhB	1564553224566	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.566	\N	\N
20190731-120704-LihvIG1FeRy0ySv	1564553224566	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.566	\N	\N
20190731-120704-TC44fcQSPxKIpzo	1564553224567	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.567	\N	\N
20190731-120704-WVzqq3yHoyN6Dwq	1564553224567	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.567	\N	\N
20190731-120704-alX9zFR8TM8G9h4	1564553224567	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.567	\N	\N
20190731-120704-IqE8YOa3DF1CV5e	1564553224567	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.567	\N	\N
20190731-120704-Yr8jjBs6IlVB4zi	1564553224567	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.567	\N	\N
20190731-120704-oaRGADUkarTfEVp	1564553224568	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.568	\N	\N
20190731-120704-LOIeLm3VKRVR9FQ	1564553224568	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.568	\N	\N
20190731-120704-pHz2xRwniz9Fvv2	1564553224568	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.568	\N	\N
20190731-120704-hfV5GJA2TQrlmqy	1564553224568	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.568	\N	\N
20190731-120704-yqD0PT7VCyYl9IQ	1564553224568	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.568	\N	\N
20190731-120704-X6eOOv3Wz7z9hmH	1564553224569	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.569	\N	\N
20190731-120704-6Pxautn11YN4Xtg	1564553224569	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.569	\N	\N
20190731-120704-DUMNGMsoZ5vo8dj	1564553224569	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.569	\N	\N
20190731-120704-rINrzNIoehs0ckW	1564553224569	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.569	\N	\N
20190731-120704-vTreKjjq8dwLF80	1564553224570	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.57	\N	\N
20190731-120704-NpGzt747xnogl8X	1564553224570	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.57	\N	\N
20190731-120704-eKWeckuUrR8Nj6y	1564553224570	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.57	\N	\N
20190731-120704-gVSydo3otR4QU6W	1564553224570	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.57	\N	\N
20190731-120704-6mWCZRWw7FYmAsy	1564553224570	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.57	\N	\N
20190731-120704-tvIKN3hSc83ZXjH	1564553224571	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.571	\N	\N
20190731-120704-9shgSo6ctqlhQOI	1564553224571	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.571	\N	\N
20190731-120704-bNMCD2wCMoYzQRm	1564553224571	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.571	\N	\N
20190731-120704-PRplwCONTR5M7A4	1564553224571	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.571	\N	\N
20190731-120704-05IKiyZmcjFmEk4	1564553224571	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.571	\N	\N
20190731-120704-iidQOTOs8C0y73i	1564553224572	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.572	\N	\N
20190731-120704-aNoFHiplETLdL56	1564553224572	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.572	\N	\N
20190731-120704-B0zbtn10KlSIbfs	1564553224572	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.572	\N	\N
20190731-120704-3lOLn3FoFo73Rg8	1564553224572	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.572	\N	\N
20190731-120704-iozNwS58lGUug5h	1564553224572	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.572	\N	\N
20190731-120704-QYPTRFkk7REnRSN	1564553224573	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.573	\N	\N
20190731-120704-Aa9b0RsKoX6JTVW	1564553224573	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.573	\N	\N
20190731-120704-SOVF6bm92PBzsCP	1564553224573	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.573	\N	\N
20190731-120704-j33djb3S4yDX1J0	1564553224573	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.573	\N	\N
20190731-120704-ry8Ye14urqvbtdL	1564553224573	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.573	\N	\N
20190731-120704-EVktjUV13vgXDmd	1564553224574	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.574	\N	\N
20190731-120704-wgQkUqTU8ncT1K7	1564553224574	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.574	\N	\N
20190731-120704-ptKTRW6QdXJJ9YI	1564553224574	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.574	\N	\N
20190731-120704-TZ6AUDSCRFBcS3N	1564553224574	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.574	\N	\N
20190731-120704-9IGcXsql2dJzxRE	1564553224574	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.574	\N	\N
20190731-120704-pc8rPk1P5NOQdRC	1564553224575	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.575	\N	\N
20190731-120704-ub8bw2qrWryDbNW	1564553224575	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.575	\N	\N
20190731-120704-9nMrgRZ5zWDuU5a	1564553224575	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.575	\N	\N
20190731-120704-ppeCx0ppz2OdCUV	1564553224575	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.575	\N	\N
20190731-120704-fejgdj3NKOuHvTS	1564553224575	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.575	\N	\N
20190731-120704-FuFBhOJ9Q1DVD8c	1564553224575	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.575	\N	\N
20190731-120704-b8D9kegeX0ykVtH	1564553224576	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.576	\N	\N
20190731-120704-mj6XtSgMWSxVQDL	1564553224576	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.576	\N	\N
20190731-120704-PJPHB79Qntj0P0p	1564553224576	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.576	\N	\N
20190731-120704-f88huMjtlgfqwTW	1564553224576	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.576	\N	\N
20190731-120704-JHUnSuN94VgkiFc	1564553224576	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.576	\N	\N
20190731-120704-KeVT9n0gtPi0pS6	1564553224577	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.577	\N	\N
20190731-120704-AKcqMf0w3pEU3om	1564553224577	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.577	\N	\N
20190731-120704-kJt4lFOeSFoSW7i	1564553224577	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.577	\N	\N
20190731-120704-WdgHKq3j2Q5rnyf	1564553224577	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.577	\N	\N
20190731-120704-uxH5AWL166MGAmB	1564553224577	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.577	\N	\N
20190731-120704-t8uxefoyGktB4cX	1564553224578	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.578	\N	\N
20190731-120704-cBduMDYtcOGXFsa	1564553224578	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.578	\N	\N
20190731-120704-2PqslDATwRGzVoW	1564553224578	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.578	\N	\N
20190731-120704-BcnzJbcXkcU973L	1564553224578	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.578	\N	\N
20190731-120704-bkDm4WYbisQ9HsQ	1564553224578	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.578	\N	\N
20190731-120704-CqSKX2eoB8sJOPb	1564553224578	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.578	\N	\N
20190731-120704-ucON1ldE3fW7nja	1564553224579	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.579	\N	\N
20190731-120704-Wk6QtmDeMMHVtwn	1564553224579	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.579	\N	\N
20190731-120704-blt3Pe9BYs0o5bq	1564553224579	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.579	\N	\N
20190731-120704-EbSQONEBZ0D2Zwz	1564553224579	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.579	\N	\N
20190731-120704-k5goLu3wEbrMJbm	1564553224579	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.579	\N	\N
20190731-120704-JOU4ijybYL2Ah6p	1564553224580	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.58	\N	\N
20190731-120704-RGSnJXuHnTQfIkZ	1564553224580	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.58	\N	\N
20190731-120704-eXNMeCe7gE406sv	1564553224580	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.58	\N	\N
20190731-120704-UPYaUxVzeVtedUM	1564553224580	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.58	\N	\N
20190731-120704-v1HccoJ6ZcL4y8d	1564553224580	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.58	\N	\N
20190731-120704-Bz9PVR7xrhHUw04	1564553224581	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.581	\N	\N
20190731-120704-DkBZqWpkjRvpoka	1564553224581	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.581	\N	\N
20190731-120704-m149i4akXdfUn07	1564553224581	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.581	\N	\N
20190731-120704-gQi5xttKTyRCoLe	1564553224581	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.581	\N	\N
20190731-120704-zStW0zWvbHem86W	1564553224581	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.581	\N	\N
20190731-120704-lUJL8FAQksXayy2	1564553224581	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.581	\N	\N
20190731-120704-WLjDGbEH29oomUF	1564553224582	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.582	\N	\N
20190731-120704-xFCpqQmVLLrm1YT	1564553224582	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.582	\N	\N
20190731-120704-YtMweygH6314Fvr	1564553224582	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.582	\N	\N
20190731-120704-8zlr3Oxga8MstqU	1564553224582	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.582	\N	\N
20190731-120704-yHL5V5TdG5SOhzf	1564553224582	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.582	\N	\N
20190731-120704-hfjsvPaHdo3j5bM	1564553224583	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.583	\N	\N
20190731-120704-rRUIu6ERNgDzrQG	1564553224583	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.583	\N	\N
20190731-120704-FE6UNfFUkVXPaAa	1564553224583	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.583	\N	\N
20190731-120704-nYB7va7lywUGiIg	1564553224583	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.583	\N	\N
20190731-120704-NIUsngTKw4jgrr1	1564553224583	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.583	\N	\N
20190731-120704-J9ecV50gx8xY75R	1564553224584	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.584	\N	\N
20190731-120704-o4P9n4fhgMToPjP	1564553224584	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.584	\N	\N
20190731-120704-2fxCEG5OulKTX9Z	1564553224584	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.584	\N	\N
20190731-120704-n7jtUVZa8UIMr1H	1564553224584	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.584	\N	\N
20190731-120704-g48HxvKj2ubWJSv	1564553224585	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.585	\N	\N
20190731-120704-igcb2Oz49jW5WRZ	1564553224585	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.585	\N	\N
20190731-120704-FfHALHwDS1aWHCW	1564553224585	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.585	\N	\N
20190731-120704-lHRJVrkBW3lZhdt	1564553224585	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.585	\N	\N
20190731-120704-ilnbp6UsoWVw93Y	1564553224585	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.585	\N	\N
20190731-120704-RA31mgCH1PFkIcf	1564553224586	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.586	\N	\N
20190731-120704-BtlUIS472TMutBh	1564553224586	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.586	\N	\N
20190731-120704-FkpFcmimkiA8SNQ	1564553224586	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.586	\N	\N
20190731-120704-2gDZyKj0lTqk3bS	1564553224586	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.586	\N	\N
20190731-120704-WSYPLqGDrzkHxD7	1564553224586	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.586	\N	\N
20190731-120704-10drLMKjJDlxAG7	1564553224587	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.587	\N	\N
20190731-120704-hnBwoanLDk7ART0	1564553224587	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.587	\N	\N
20190731-120704-N8eBjTeO3XHzKRm	1564553224587	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.587	\N	\N
20190731-120704-GtzjGLc1Jdp9x7G	1564553224587	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.587	\N	\N
20190731-120704-cgVYMK2YBiFDIhq	1564553224587	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.587	\N	\N
20190731-120704-l9tatTTTyobN5Vz	1564553224588	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.588	\N	\N
20190731-120704-C680IJzlk629XRW	1564553224588	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.588	\N	\N
20190731-120704-5oZMh4rwjI6xCaZ	1564553224588	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.588	\N	\N
20190731-120704-AoxTwUdpd8flAsZ	1564553224588	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.588	\N	\N
20190731-120704-vpRxXCfOJAXs8H9	1564553224588	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.588	\N	\N
20190731-120704-RNTkASBJ7RCxZaO	1564553224589	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.589	\N	\N
20190731-120704-XyXOSwajCgfvB3p	1564553224589	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.589	\N	\N
20190731-120704-cl35RYz0tQjEJ1o	1564553224589	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.589	\N	\N
20190731-120704-uEyCfcnC8DaINJo	1564553224589	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.589	\N	\N
20190731-120704-8dNu1VPl6aCcOME	1564553224589	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.589	\N	\N
20190731-120704-sF3Mf5kFuxe3shk	1564553224590	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.59	\N	\N
20190731-120704-wzDngDdeAaRSxO5	1564553224590	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.59	\N	\N
20190731-120704-EB8Y3ZFiHvYfduV	1564553224590	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.59	\N	\N
20190731-120704-jKVp3DHtke7j0pH	1564553224590	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.59	\N	\N
20190731-120704-IlKDKC7YYDqLqXC	1564553224590	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.59	\N	\N
20190731-120704-uB6FIIio50yfMwO	1564553224591	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.591	\N	\N
20190731-120704-FndKy4ImxrUOmbM	1564553224591	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.591	\N	\N
20190731-120704-EY3f5AXQfgO5657	1564553224591	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.591	\N	\N
20190731-120704-4BI5NblAQAPvEsq	1564553224591	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.591	\N	\N
20190731-120704-b0n5yST85tGjbTQ	1564553224591	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.591	\N	\N
20190731-120704-AmDUeBh4R2ddK71	1564553224592	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.592	\N	\N
20190731-120704-kMyHOceeJNBeidZ	1564553224592	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.592	\N	\N
20190731-120704-UfyorT4GQ0b7jG8	1564553224592	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.592	\N	\N
20190731-120704-hCxvoo9UbAsGLVS	1564553224592	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.592	\N	\N
20190731-120704-Nebjaum7cCS1mEu	1564553224592	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.592	\N	\N
20190731-120704-zIZ4WXeZ7mqF6xl	1564553224593	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.593	\N	\N
20190731-120704-4smL51IoigJ8MOn	1564553224593	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.593	\N	\N
20190731-120704-ABqAUrdLJVLVUU3	1564553224593	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.593	\N	\N
20190731-120704-8l0QhOUxuoyzX97	1564553224593	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.593	\N	\N
20190731-120704-IFB25x1wNkzhhlo	1564553224593	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.593	\N	\N
20190731-120704-TUIEDWi2F0qkH7W	1564553224594	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.594	\N	\N
20190731-120704-d6y0TTe8OxgZKKV	1564553224594	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.594	\N	\N
20190731-120704-fXLtTHvIqW7oKcZ	1564553224594	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.594	\N	\N
20190731-120704-AVxHjZ2yxHvrZM9	1564553224594	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.594	\N	\N
20190731-120704-GZW9lWx0DyiWgfb	1564553224594	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.594	\N	\N
20190731-120704-fAc4AZw2amYXLXd	1564553224594	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.594	\N	\N
20190731-120704-2IDB1eP8aRDAX47	1564553224595	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.595	\N	\N
20190731-120704-orunTkybyRmLCCv	1564553224595	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.595	\N	\N
20190731-120704-Z03zozTIiBiNxoC	1564553224595	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.595	\N	\N
20190731-120704-TNIckpPBoZMcevM	1564553224595	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.595	\N	\N
20190731-120704-AmHP3L7rkSR0lfU	1564553224595	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.595	\N	\N
20190731-120704-kpmXoalY0SqujvQ	1564553224596	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.596	\N	\N
20190731-120704-rKhIhscshdP7Apl	1564553224596	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.596	\N	\N
20190731-120704-POR9TB1VIkqMGrU	1564553224596	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.596	\N	\N
20190731-120704-xB1ll1ZJAZJtL1a	1564553224596	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.596	\N	\N
20190731-120704-Wr7VRp96dBr53ZZ	1564553224596	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.596	\N	\N
20190731-120704-Yn4KOwpmiTyT21Z	1564553224597	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.597	\N	\N
20190731-120704-E0Y9PAccdJjv4jt	1564553224597	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.597	\N	\N
20190731-120704-gyS1wzwZNcXzYMG	1564553224597	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.597	\N	\N
20190731-120704-1Cxw3bmPvdzmYHU	1564553224597	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.597	\N	\N
20190731-120704-2B6miGxsXbAEfSI	1564553224597	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.597	\N	\N
20190731-120704-S9MvFxf99wQzbAy	1564553224598	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.598	\N	\N
20190731-120704-XYKwaZMRxzNGXGq	1564553224598	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.598	\N	\N
20190731-120704-faDKKXm4XWcBKaE	1564553224598	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.598	\N	\N
20190731-120704-a4mufWftVtr7QaR	1564553224598	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.598	\N	\N
20190731-120704-aZtW2zmO27I69Zf	1564553224598	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.598	\N	\N
20190731-120704-JjIUZ2iw6vuu9Po	1564553224599	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.599	\N	\N
20190731-120704-t1odoBWa6pUL6hL	1564553224599	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.599	\N	\N
20190731-120704-Y5VuR56bMGPkJb7	1564553224599	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.599	\N	\N
20190731-120704-p6ijJNQHVgfdDxo	1564553224599	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.599	\N	\N
20190731-120704-oai2GDKrGLLwTsz	1564553224599	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.599	\N	\N
20190731-120704-K5n2Z63r7ujdZEM	1564553224600	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.6	\N	\N
20190731-120704-NOnamisFDj9OR9V	1564553224600	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.6	\N	\N
20190731-120704-XPYfmF1jux3u0oh	1564553224600	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.6	\N	\N
20190731-120704-PfAo1nubwf15T7u	1564553224600	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.6	\N	\N
20190731-120704-famcvf3LABC55dT	1564553224600	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.6	\N	\N
20190731-120704-BKVEp83v6Lu4d8z	1564553224601	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.601	\N	\N
20190731-120704-9NFveB6iY1NFYDl	1564553224601	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.601	\N	\N
20190731-120704-XrfETuvgit3GzgS	1564553224601	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.601	\N	\N
20190731-120704-RlUSxiwUG2X70dw	1564553224601	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.601	\N	\N
20190731-120704-4GZYt3Is1lFCVgi	1564553224601	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.601	\N	\N
20190731-120704-Ji106bzWmf6FlbQ	1564553224602	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.602	\N	\N
20190731-120704-2TPshL1jVhFyqNc	1564553224602	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.602	\N	\N
20190731-120704-lLLEbJ25rKOBPO0	1564553224602	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.602	\N	\N
20190731-120704-lf7l8AZ8lANTpdK	1564553224602	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.602	\N	\N
20190731-120704-rLNBcRmb8kH1Z9t	1564553224602	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.602	\N	\N
20190731-120704-IKKfLHUcy3fkyes	1564553224603	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.603	\N	\N
20190731-120704-7zE6HciBNX06Nn0	1564553224603	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.603	\N	\N
20190731-120704-IQRWqKbMtxobBBB	1564553224603	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.603	\N	\N
20190731-120704-3l2wkJTISRad5Nf	1564553224603	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.603	\N	\N
20190731-120704-2vWrgs95XQCsU4Y	1564553224603	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.603	\N	\N
20190731-120704-KrDrl8WBwMkqy0W	1564553224604	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.604	\N	\N
20190731-120704-8epVKUdtIEEKgiQ	1564553224604	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.604	\N	\N
20190731-120704-JZEJnjzyOmU7Pi7	1564553224604	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.604	\N	\N
20190731-120704-C9nuYprdi4f0DSW	1564553224604	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.604	\N	\N
20190731-120704-cjSPziqN97Q0gxe	1564553224604	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.604	\N	\N
20190731-120704-IyFGU86e1Lcxgtb	1564553224604	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.604	\N	\N
20190731-120704-OvTjPp4IUDghz6O	1564553224605	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.605	\N	\N
20190731-120704-VFfZ14kagdbcN8e	1564553224605	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.605	\N	\N
20190731-120704-WdZJW7DC6sx0vX4	1564553224605	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.605	\N	\N
20190731-120704-hPSeHPLZDXfhEbQ	1564553224605	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.605	\N	\N
20190731-120704-5f3NOgeVLb17zEX	1564553224605	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.605	\N	\N
20190731-120704-VHY03Qv6PCAZBlT	1564553224606	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.606	\N	\N
20190731-120704-G0Hp0nWlkwJTsqo	1564553224606	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.606	\N	\N
20190731-120704-LZv6uJZUyJAoZVp	1564553224606	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.606	\N	\N
20190731-120704-aEADPFtgRXL9vbr	1564553224606	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.606	\N	\N
20190731-120704-C2ruvXOEl88ehqA	1564553224606	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.606	\N	\N
20190731-120704-8YqmBNvPW3VFasW	1564553224607	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.607	\N	\N
20190731-120704-3gVM5MhL7jRBuco	1564553224607	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.607	\N	\N
20190731-120704-MmOT5CgLB9BRn4x	1564553224607	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.607	\N	\N
20190731-120704-pQBFSvnWmFiWSir	1564553224607	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.607	\N	\N
20190731-120704-EHO0m7HCKlaFqwW	1564553224607	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.607	\N	\N
20190731-120704-yhD2yIOYROS7ojf	1564553224608	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.608	\N	\N
20190731-120704-rtJrKpXs7mmdumH	1564553224608	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.608	\N	\N
20190731-120704-9z0myeGR04qFzDq	1564553224608	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.608	\N	\N
20190731-120704-UT2dq4aafZ0b1bJ	1564553224608	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.608	\N	\N
20190731-120704-2U6mZ9SCeWNTmbN	1564553224608	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.608	\N	\N
20190731-120704-qTOZk0Q4RKINXPs	1564553224609	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.609	\N	\N
20190731-120704-vEb863hu1DSdLYc	1564553224609	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.609	\N	\N
20190731-120704-74eqivkb4FpaALt	1564553224609	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.609	\N	\N
20190731-120704-M89qaAOzfsN68mP	1564553224609	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.609	\N	\N
20190731-120704-QgJpFq0CkeV8ecX	1564553224609	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.609	\N	\N
20190731-120704-O7qje7FIzRKf2L6	1564553224609	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.609	\N	\N
20190731-120704-DWpJnn1JJ7JIejx	1564553224610	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.61	\N	\N
20190731-120704-E6O27o2HxDr9CVv	1564553224610	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.61	\N	\N
20190731-120704-4wEd9Ma7OjN7gaZ	1564553224610	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.61	\N	\N
20190731-120704-FpPYnhtLt1IRLqv	1564553224610	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.61	\N	\N
20190731-120704-3SqpJzp3uyk3bDd	1564553224611	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.611	\N	\N
20190731-120704-GTrWPo8I5L9rAFJ	1564553224611	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.611	\N	\N
20190731-120704-4MtJLYCCn7Z14ob	1564553224611	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.611	\N	\N
20190731-120704-6woYByzAuQU3MUr	1564553224611	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.611	\N	\N
20190731-120704-ol5tLMhPP56rm6k	1564553224611	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.611	\N	\N
20190731-120704-6Gi6LBDnoaMCYmd	1564553224611	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.611	\N	\N
20190731-120704-trARhhGbVUvCv76	1564553224612	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.612	\N	\N
20190731-120704-uy28YaNgaG5jcUm	1564553224612	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.612	\N	\N
20190731-120704-iSrxeiQMZPGoy56	1564553224612	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.612	\N	\N
20190731-120704-uGr255FmR6wy4Q1	1564553224612	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.612	\N	\N
20190731-120704-GaUFrZvmqnm6OkB	1564553224612	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.612	\N	\N
20190731-120704-cG9l9VIxBjH97kE	1564553224613	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.613	\N	\N
20190731-120704-bftd7qeWHB608cT	1564553224613	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.613	\N	\N
20190731-120704-wdL8TtAYZwESN9J	1564553224613	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.613	\N	\N
20190731-120704-MjW760IVNgUYX1j	1564553224613	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.613	\N	\N
20190731-120704-byJSVOitiJnIbF8	1564553224613	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.613	\N	\N
20190731-120704-5wutoUwjD7ZNGyQ	1564553224614	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.614	\N	\N
20190731-120704-Z6xvAZq44dSPDx8	1564553224614	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.614	\N	\N
20190731-120704-nfv6a2lSLVCeVQl	1564553224614	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.614	\N	\N
20190731-120704-RjXKsGp7LJRgcny	1564553224614	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.614	\N	\N
20190731-120704-L6svxpylsEZpQXX	1564553224614	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.614	\N	\N
20190731-120704-HtV6nSH4QSb8e9e	1564553224615	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.615	\N	\N
20190731-120704-iaYunYngCZpcXEu	1564553224615	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.615	\N	\N
20190731-120704-tKQ0gzuqSMKcJAR	1564553224615	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.615	\N	\N
20190731-120704-0WgmuwshN9OcCGn	1564553224615	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.615	\N	\N
20190731-120704-rWJxZqRFeHJaob8	1564553224615	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.615	\N	\N
20190731-120704-dKQqmXRFcdYsqWj	1564553224615	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.616	\N	\N
20190731-120704-Rw0OnDNKf73kGO7	1564553224616	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.616	\N	\N
20190731-120704-vyf4blVLnJhcoOZ	1564553224616	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.616	\N	\N
20190731-120704-ooc1z1WawgqSW67	1564553224616	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.616	\N	\N
20190731-120704-lwbuFENDQsBkKy7	1564553224616	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.616	\N	\N
20190731-120704-nZtYHSFqU8aowMk	1564553224616	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.616	\N	\N
20190731-120704-Hl9KOpgdfiHu157	1564553224617	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.617	\N	\N
20190731-120704-y6IgHbfsHiMtEfa	1564553224617	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.617	\N	\N
20190731-120704-NfJ8LuQhenMwUre	1564553224617	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.617	\N	\N
20190731-120704-dVyEJTRSVLeL4xG	1564553224617	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.617	\N	\N
20190731-120704-9U3v1Ii3khRhX9F	1564553224617	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.617	\N	\N
20190731-120704-DxYvEgo19PCYh0p	1564553224618	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.618	\N	\N
20190731-120704-FkZ1EVFDHbh38VN	1564553224618	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.618	\N	\N
20190731-120704-89Cn3RkwpUeiJQG	1564553224618	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.618	\N	\N
20190731-120704-9D9osGyIo7DSNgc	1564553224618	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.618	\N	\N
20190731-120704-gBwwTqhqQ7HGyw5	1564553224618	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.618	\N	\N
20190731-120704-ze7D8xdlAV5ZB8X	1564553224619	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.619	\N	\N
20190731-120704-OH1YoOg2dWFoCLP	1564553224619	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.619	\N	\N
20190731-120704-N4FVkuanQt1Tffq	1564553224619	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.619	\N	\N
20190731-120704-rdb45Tb3RIOxrxh	1564553224619	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.619	\N	\N
20190731-120704-UCAT16Zvp3TtjQE	1564553224619	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.619	\N	\N
20190731-120704-h9clwQMZw9lK9p0	1564553224620	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.62	\N	\N
20190731-120704-xAEYWV4As3IAka6	1564553224620	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.62	\N	\N
20190731-120704-GgHbawHBFFxIS4c	1564553224620	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.62	\N	\N
20190731-120704-29U5FiftgHhEhjz	1564553224620	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.62	\N	\N
20190731-120704-bxwTLMIKr9lsibA	1564553224620	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.62	\N	\N
20190731-120704-4I0bTZlydsyj5P5	1564553224620	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.62	\N	\N
20190731-120704-MIaPKik5WEeoxgO	1564553224621	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.621	\N	\N
20190731-120704-nyyWgwvsldQq5Kj	1564553224621	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.621	\N	\N
20190731-120704-e9A7GdSlCgHM20d	1564553224621	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.621	\N	\N
20190731-120704-WLFaaPXJhvpBgI5	1564553224621	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.621	\N	\N
20190731-120704-SCofPwX1q3l3CIq	1564553224621	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.621	\N	\N
20190731-120704-EgEXralGRTE33vZ	1564553224622	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.622	\N	\N
20190731-120704-MUQpMHxrDf9SNJN	1564553224622	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.622	\N	\N
20190731-120704-kY47CHghWvei3tB	1564553224622	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.622	\N	\N
20190731-120704-rlw91ODBxVR8av4	1564553224622	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.622	\N	\N
20190731-120704-rEG55CpEcXJPcoJ	1564553224622	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.622	\N	\N
20190731-120704-qbLgiZKLRSFHEpe	1564553224623	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.623	\N	\N
20190731-120704-OnLwupR03uZg11V	1564553224623	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.623	\N	\N
20190731-120704-j12Li6sO9H422RS	1564553224623	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.623	\N	\N
20190731-120704-iVOFD93BqwssoFt	1564553224623	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.623	\N	\N
20190731-120704-mRhRyns1vLNfeOA	1564553224623	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.623	\N	\N
20190731-120704-CUpgyWM3rsTyly2	1564553224624	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.624	\N	\N
20190731-120704-Odo30iiCoofvtCH	1564553224624	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.624	\N	\N
20190731-120704-9dKdt4BfmWNwDBb	1564553224624	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.624	\N	\N
20190731-120704-l0xuVyCiOHlxjnE	1564553224624	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.624	\N	\N
20190731-120704-QXqNbSxD4eTSr8A	1564553224624	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.624	\N	\N
20190731-120704-c9NusiRADMQf1Du	1564553224625	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.625	\N	\N
20190731-120704-unIDOK7Afc4eIiV	1564553224625	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.625	\N	\N
20190731-120704-N8S2xokp3bSK4Ji	1564553224625	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.625	\N	\N
20190731-120704-cyQrS9YxoGYYqbC	1564553224625	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.625	\N	\N
20190731-120704-5ne2FvWOlT9qQHF	1564553224625	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.625	\N	\N
20190731-120704-iRce37jDUWtYlHX	1564553224626	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.626	\N	\N
20190731-120704-VZnEupx3gCJKRag	1564553224626	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.626	\N	\N
20190731-120704-75nggdPEZHY6shY	1564553224626	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.626	\N	\N
20190731-120704-GkebvqKnerX09yu	1564553224626	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.626	\N	\N
20190731-120704-cjn0SManKv1fjbO	1564553224627	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.627	\N	\N
20190731-120704-MMM9mbqrxRfMgW5	1564553224627	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.627	\N	\N
20190731-120704-hI6Pv8OIAkfVYgg	1564553224627	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.627	\N	\N
20190731-120704-5dgXrstRrJf0M65	1564553224627	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.627	\N	\N
20190731-120704-Drl0GXqJCCaQJ8F	1564553224627	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.627	\N	\N
20190731-120704-AfvXY7SWjjJQ9Qv	1564553224628	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.628	\N	\N
20190731-120704-y2P2yrBcf2jWKPP	1564553224628	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.628	\N	\N
20190731-120704-F1HwjDyOeoHItSQ	1564553224628	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.628	\N	\N
20190731-120704-yF0Rs4suYQRLJqO	1564553224628	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.628	\N	\N
20190731-120704-9HGDlHdfv40rHrk	1564553224629	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.629	\N	\N
20190731-120704-V4TjFONPBEfox8R	1564553224629	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.629	\N	\N
20190731-120704-2mRT2euRrGGFlg9	1564553224629	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.629	\N	\N
20190731-120704-wu14pYBzU27oDMX	1564553224629	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.629	\N	\N
20190731-120704-ZvLUnOADPjaCicD	1564553224629	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.629	\N	\N
20190731-120704-59zDGes8oiUF848	1564553224630	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.63	\N	\N
20190731-120704-F3KDNIcL9fFzAB8	1564553224630	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.63	\N	\N
20190731-120704-OUELT3Igpi2OomN	1564553224630	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.63	\N	\N
20190731-120704-T82I8gvmeVKoshX	1564553224630	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.63	\N	\N
20190731-120704-R4XBHO2oRLx1zbo	1564553224631	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.631	\N	\N
20190731-120704-dKIodORX3w0KznH	1564553224631	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.631	\N	\N
20190731-120704-yg7VHjlIH9I6Qjb	1564553224631	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.631	\N	\N
20190731-120704-lUpApDeh74z4l8U	1564553224631	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.631	\N	\N
20190731-120704-MpEB3A1tttZN01S	1564553224632	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.632	\N	\N
20190731-120704-1Vk8HyDQryWhaaN	1564553224632	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.632	\N	\N
20190731-120704-76MYtw4wAlMZOM3	1564553224632	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.632	\N	\N
20190731-120704-kuQ0enm2kTA5y7e	1564553224632	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.632	\N	\N
20190731-120704-5QsDeBP6okJWoWa	1564553224633	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.633	\N	\N
20190731-120704-eI7fTxOlIRuxX3d	1564553224633	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.633	\N	\N
20190731-120704-GPNBIJNdsvnZUbt	1564553224633	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.633	\N	\N
20190731-120704-tZ9JL9i1akZSPxi	1564553224633	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.633	\N	\N
20190731-120704-mufp9m0klQuLX31	1564553224633	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.633	\N	\N
20190731-120704-MTs4I1BV5jGhRpR	1564553224634	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.634	\N	\N
20190731-120704-DvQRm98zjPTq5Xa	1564553224634	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.634	\N	\N
20190731-120704-lvMK6V7YLm78aLz	1564553224634	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.634	\N	\N
20190731-120704-khxicwxlqXVLEdl	1564553224634	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.634	\N	\N
20190731-120704-olUhALEaUv9EC7h	1564553224635	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.635	\N	\N
20190731-120704-bizjes6RBPaBODv	1564553224635	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.635	\N	\N
20190731-120704-HkoSdrzQhWCwBhu	1564553224635	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.635	\N	\N
20190731-120704-BlIhumQdkWpAhxg	1564553224635	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.635	\N	\N
20190731-120704-lLNyPNFKhgFmxXY	1564553224635	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.635	\N	\N
20190731-120704-wXOR5uPjah1KJGp	1564553224636	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.636	\N	\N
20190731-120704-jS2hNdWgAKh7Bpz	1564553224636	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.636	\N	\N
20190731-120704-nfUyx23x69tzAlR	1564553224636	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.636	\N	\N
20190731-120704-1OyprMBiS5d2vqr	1564553224636	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.636	\N	\N
20190731-120704-0ATateE0NwOz7k8	1564553224637	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.637	\N	\N
20190731-120704-RIAYg0yzxdbZbPa	1564553224637	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.637	\N	\N
20190731-120704-vVP2CtqA2j4RiHB	1564553224637	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.637	\N	\N
20190731-120704-cCnQpXBnYLGnaBj	1564553224637	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.637	\N	\N
20190731-120704-RpPbMcVjOs3Y7j4	1564553224637	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.637	\N	\N
20190731-120704-EZ2WEugpvpufCiz	1564553224638	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.638	\N	\N
20190731-120704-PCsiosPtGa5zwou	1564553224638	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.638	\N	\N
20190731-120704-OXu9TmJ4UuNJiKG	1564553224638	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.638	\N	\N
20190731-120704-qFqD7Dnn0zDlPAs	1564553224638	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.638	\N	\N
20190731-120704-GImELmaZm8Npdwt	1564553224639	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.639	\N	\N
20190731-120704-uHYn38ZeuaQmyJX	1564553224639	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.639	\N	\N
20190731-120704-4evtc5Htuvp7ZPW	1564553224639	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.639	\N	\N
20190731-120704-ogHk06S06HtQh6C	1564553224639	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.639	\N	\N
20190731-120704-SaXpnNwpOzp2WFW	1564553224639	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.639	\N	\N
20190731-120704-ULBf8GJ14sBWeIC	1564553224640	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.64	\N	\N
20190731-120704-0HZc5WfrJq3hZrm	1564553224640	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.64	\N	\N
20190731-120704-pJh9bDZBbNlMcjq	1564553224640	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.64	\N	\N
20190731-120704-Vb4CxVUaf6dYIwd	1564553224640	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.64	\N	\N
20190731-120704-sey8uMztbiIlfXc	1564553224641	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.641	\N	\N
20190731-120704-Wc62IzduweGqAvB	1564553224641	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.641	\N	\N
20190731-120704-T2c6S5uNZFTFlPc	1564553224641	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.641	\N	\N
20190731-120704-VuhZbm9dwiN9qoD	1564553224641	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.641	\N	\N
20190731-120704-N2hLW9rfPOgarSz	1564553224642	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.642	\N	\N
20190731-120704-mWX6ATbBZgpMHn8	1564553224642	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.642	\N	\N
20190731-120704-gwI4EzVLJyivPXw	1564553224642	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.642	\N	\N
20190731-120704-vac1zOtmhOnvGk1	1564553224642	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.642	\N	\N
20190731-120704-2jTDkHxutEL8JWD	1564553224643	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.643	\N	\N
20190731-120704-1Ajwd3phMHTuioI	1564553224643	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.643	\N	\N
20190731-120704-23vTy0P8PNerg8m	1564553224643	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.643	\N	\N
20190731-120704-zwuSnQw8G88hWuy	1564553224643	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.643	\N	\N
20190731-120704-8z7FOVehL8utxn2	1564553224644	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.644	\N	\N
20190731-120704-Y1NufjCWpAONVd6	1564553224644	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.644	\N	\N
20190731-120704-zMcpEkxvsVykmqj	1564553224644	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.644	\N	\N
20190731-120704-GhsMsk7c1LElm3h	1564553224644	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.644	\N	\N
20190731-120704-pm2szOWRdRsjGBU	1564553224644	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.644	\N	\N
20190731-120704-qxY9bZHjuObN1og	1564553224645	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.645	\N	\N
20190731-120704-rZQu1r5NNHcX06B	1564553224645	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.645	\N	\N
20190731-120704-H94Edp7gJ1bAx01	1564553224645	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.645	\N	\N
20190731-120704-UutETMOPGZXZrYp	1564553224645	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.645	\N	\N
20190731-120704-ebFwyugYiZfs5xM	1564553224646	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.646	\N	\N
20190731-120704-FKrRzPhLNm0x3tn	1564553224646	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.646	\N	\N
20190731-120704-qrrRlF2N3YvWe78	1564553224646	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.646	\N	\N
20190731-120704-X4FAQ7Oskhk5sgW	1564553224646	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.646	\N	\N
20190731-120704-LZZg7f7y8U7jmdY	1564553224646	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.646	\N	\N
20190731-120704-tzhuSs8ppZOcHqJ	1564553224647	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.647	\N	\N
20190731-120704-dqMx753Evq4nAWr	1564553224647	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.647	\N	\N
20190731-120704-mekTRfQMjf8WUUA	1564553224647	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.647	\N	\N
20190731-120704-H1JpvTAjm3WFSag	1564553224647	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.647	\N	\N
20190731-120704-MrOlFia3KCUckql	1564553224648	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.648	\N	\N
20190731-120704-ds1D0uHAj2VCy8S	1564553224648	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.648	\N	\N
20190731-120704-Ao5CB8o9aln3tFO	1564553224648	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.648	\N	\N
20190731-120704-DM13AYNU13uaO8N	1564553224648	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.648	\N	\N
20190731-120704-YQPnvne0cziywH4	1564553224648	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.648	\N	\N
20190731-120704-awI6slikTTpRPGv	1564553224649	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.649	\N	\N
20190731-120704-CHVetFEPToWPqkt	1564553224649	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.649	\N	\N
20190731-120704-WoA6jFNDUQLyq3e	1564553224649	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.649	\N	\N
20190731-120704-Yg8nnCkFpNPaSLt	1564553224649	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.649	\N	\N
20190731-120704-VUNZiXvhSrneOYY	1564553224650	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.65	\N	\N
20190731-120704-DxVKKoFS7xND6s1	1564553224650	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.65	\N	\N
20190731-120704-8TfLAjHsPF9ZUSQ	1564553224650	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.65	\N	\N
20190731-120704-ReVy71indnWaisA	1564553224650	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.65	\N	\N
20190731-120704-qTV5gwxwez54j3Z	1564553224650	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.65	\N	\N
20190731-120704-ucDPcjYoWARvodU	1564553224651	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.651	\N	\N
20190731-120704-iXhfAsZA0dmCyDL	1564553224651	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.651	\N	\N
20190731-120704-3d8bPXiFRBWw7K3	1564553224651	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.651	\N	\N
20190731-120704-0OFmF8lrY5qm9yb	1564553224651	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.651	\N	\N
20190731-120704-Wbti2UO6g4ftAxt	1564553224652	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.652	\N	\N
20190731-120704-PpqxX2NOh9dx9t6	1564553224652	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.652	\N	\N
20190731-120704-aLgnSmEwcEYkC9g	1564553224652	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.652	\N	\N
20190731-120704-MYruoTe9kfA9u95	1564553224652	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.652	\N	\N
20190731-120704-gY5m3iO0c4wLjaG	1564553224653	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.653	\N	\N
20190731-120704-YwIh69ocZjPnCGs	1564553224653	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.653	\N	\N
20190731-120704-6POsovWABk0u9Lv	1564553224653	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.653	\N	\N
20190731-120704-scCrbqpiRshHTMT	1564553224653	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.653	\N	\N
20190731-120704-YR03QBe3di7I4rT	1564553224653	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.653	\N	\N
20190731-120704-eB56V30aJfzMFW8	1564553224654	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.654	\N	\N
20190731-120704-obfCP6GV9szpTFc	1564553224654	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.654	\N	\N
20190731-120704-SSA2HlmRks7BkXH	1564553224654	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.654	\N	\N
20190731-120704-axnnEYKKfB5BFSS	1564553224654	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.654	\N	\N
20190731-120704-tGZT138VDx72sYf	1564553224655	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.655	\N	\N
20190731-120704-N75PqGJCxFJaooE	1564553224655	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.655	\N	\N
20190731-120704-rkGwCVjyECcMH6c	1564553224655	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.655	\N	\N
20190731-120704-YuSDzbB3cNJvsbf	1564553224655	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.655	\N	\N
20190731-120704-KxGODXKA2BK752l	1564553224655	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.655	\N	\N
20190731-120704-HfCPonhY9sYjJSj	1564553224656	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.656	\N	\N
20190731-120704-DtpHjgMR9iYmSvl	1564553224656	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.656	\N	\N
20190731-120704-ZRGcChO9LJHttPd	1564553224656	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.656	\N	\N
20190731-120704-9mbn19cwXbAyEJJ	1564553224656	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.656	\N	\N
20190731-120704-sOfn307C1VHjc6q	1564553224657	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.657	\N	\N
20190731-120704-HBMUb46U6Vrjc5e	1564553224657	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.657	\N	\N
20190731-120704-zzEQ431KmidqgmK	1564553224657	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.657	\N	\N
20190731-120704-mKbdGVy891TRYEJ	1564553224657	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.657	\N	\N
20190731-120704-ImdQUSAKw5OX982	1564553224657	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.657	\N	\N
20190731-120704-ztaYkOdXhd70oHQ	1564553224658	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.658	\N	\N
20190731-120704-UEoB83ygv0cJhbf	1564553224658	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.658	\N	\N
20190731-120704-TUx2KKgA5iXowEN	1564553224658	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.658	\N	\N
20190731-120704-itDfYyhOq0JU1nt	1564553224658	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.658	\N	\N
20190731-120704-s5nqyCo2WTFAZpN	1564553224659	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.659	\N	\N
20190731-120704-lZAnzG7P1bhtkZh	1564553224659	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.659	\N	\N
20190731-120704-1hOKV8JKQXoDDHB	1564553224659	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.659	\N	\N
20190731-120704-x7Q6DwjNIvIiIdi	1564553224659	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.659	\N	\N
20190731-120704-gysMsqcC2K0UtGN	1564553224659	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.659	\N	\N
20190731-120704-iGntpJ5YwaBzB5B	1564553224660	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.66	\N	\N
20190731-120704-MTcsd7cQqaNI6Ya	1564553224660	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.66	\N	\N
20190731-120704-ufbphNlaPkvI3KB	1564553224660	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.66	\N	\N
20190731-120704-nKvFUJWJOlrqitq	1564553224660	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.66	\N	\N
20190731-120704-ub9MTJKjgDdPFr0	1564553224661	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.661	\N	\N
20190731-120704-lj9mm0cjLw3lZAp	1564553224661	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.661	\N	\N
20190731-120704-GkkkckIrpxpLhM0	1564553224661	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.661	\N	\N
20190731-120704-azAX1RPoT7nwYyc	1564553224661	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.661	\N	\N
20190731-120704-uvaXW5mhQr1cG2L	1564553224662	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.662	\N	\N
20190731-120704-pu27Hua1MaUvdGr	1564553224662	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.662	\N	\N
20190731-120704-K9mGvalSMnysZ8Y	1564553224662	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.662	\N	\N
20190731-120704-s9WFWmRO8JneHY5	1564553224662	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.662	\N	\N
20190731-120704-Fd4yjRt2QDTpIxZ	1564553224663	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.663	\N	\N
20190731-120704-j2p3x01szK4m23v	1564553224663	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.663	\N	\N
20190731-120704-DTWsaPJl0SyayCV	1564553224663	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.663	\N	\N
20190731-120704-NU2Jo8UuVY2Bre9	1564553224663	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.663	\N	\N
20190731-120704-kQagd9yPdj1wJD7	1564553224663	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.663	\N	\N
20190731-120704-aQj8cO0m63fD1cp	1564553224664	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.664	\N	\N
20190731-120704-RpvQfDNCSizMFk2	1564553224664	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.664	\N	\N
20190731-120704-JV1TyfwUalW2s7w	1564553224664	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.664	\N	\N
20190731-120704-y5CwISxEpAKV6kr	1564553224664	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.664	\N	\N
20190731-120704-lqq6LtJBXgamyNb	1564553224665	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.665	\N	\N
20190731-120704-N49tJcpXdVUcW08	1564553224665	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.665	\N	\N
20190731-120704-yOb8UcdpUw9G4qG	1564553224665	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.665	\N	\N
20190731-120704-4ha2A7XmIxb2uil	1564553224665	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.665	\N	\N
20190731-120704-VU5Cg2kEJT9UX8e	1564553224665	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.665	\N	\N
20190731-120704-3WzGw05wCZ9VnbS	1564553224666	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.666	\N	\N
20190731-120704-MNYFi1qsRJoHSBd	1564553224666	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.666	\N	\N
20190731-120704-xE3UrobFfmYikij	1564553224666	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.666	\N	\N
20190731-120704-qMDQ7RKb65hvFYO	1564553224666	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.666	\N	\N
20190731-120704-ze5MIbOdUHLiqGZ	1564553224666	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.666	\N	\N
20190731-120704-ZkjvbJfKjFZg3gn	1564553224667	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.667	\N	\N
20190731-120704-XZaPedzfhB6LazN	1564553224667	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.667	\N	\N
20190731-120704-6UTbTUNI9k0k2KP	1564553224667	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.667	\N	\N
20190731-120704-TSOat0LJjnqgRcI	1564553224667	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.667	\N	\N
20190731-120704-ArJ5JvGcKZRleVX	1564553224667	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.667	\N	\N
20190731-120704-CDIl3yqZw0WllG8	1564553224668	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.668	\N	\N
20190731-120704-cqKny4qhsuJ7fz8	1564553224668	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.668	\N	\N
20190731-120704-FbxehCWKCkC2KRc	1564553224668	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.668	\N	\N
20190731-120704-UAPPvzgqIzYMxpx	1564553224668	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.668	\N	\N
20190731-120704-AbMvqkR7S3YXOaU	1564553224668	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.668	\N	\N
20190731-120704-bKFrXnOOrZsSpX6	1564553224669	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.669	\N	\N
20190731-120704-q5mSAzfXpbp8ejH	1564553224669	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.669	\N	\N
20190731-120704-ycDJYrkhvWT0l1E	1564553224669	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.669	\N	\N
20190731-120704-f3riBBpCgtFnsHZ	1564553224669	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.669	\N	\N
20190731-120704-N6vnEIgd0SnXHzp	1564553224670	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.67	\N	\N
20190731-120704-CvvukAWRM3KezDb	1564553224670	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.67	\N	\N
20190731-120704-7Z4W4x7hS84KXPx	1564553224670	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.67	\N	\N
20190731-120704-JSXz6b5MwhSMBkD	1564553224670	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.67	\N	\N
20190731-120704-PPoAutQ8mSPEggZ	1564553224670	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.67	\N	\N
20190731-120704-FYqtB5z6067fYOI	1564553224671	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.671	\N	\N
20190731-120704-d0zcoCCjE9fafQ2	1564553224671	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.671	\N	\N
20190731-120704-LQByQIhDQjl4St2	1564553224671	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.671	\N	\N
20190731-120704-KjyhtEGvTmnMQDb	1564553224671	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.671	\N	\N
20190731-120704-wNapKoBezsglDNZ	1564553224672	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.672	\N	\N
20190731-120704-AKXVPS938BgEd8p	1564553224672	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.672	\N	\N
20190731-120704-eyOqpP54h4UZqIK	1564553224672	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.672	\N	\N
20190731-120704-Q8nt5q6jnxqZ9mv	1564553224672	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.672	\N	\N
20190731-120704-QNL2SB69sqSaoJj	1564553224673	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.673	\N	\N
20190731-120704-YcgqMajow7snakW	1564553224673	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.673	\N	\N
20190731-120704-pkneFWi3cqfAKHy	1564553224673	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.673	\N	\N
20190731-120704-qx9mqx56u1fxNk8	1564553224673	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.673	\N	\N
20190731-120704-5onwiFmDSbEAmp7	1564553224673	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.673	\N	\N
20190731-120704-QDG2jfv6yTy9MLY	1564553224674	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.674	\N	\N
20190731-120704-l1aeZnf2YuAGjqS	1564553224674	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.674	\N	\N
20190731-120704-Wp2lMvPLwh7BuAS	1564553224674	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.674	\N	\N
20190731-120704-jXF7JANc5BTe8GJ	1564553224674	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.674	\N	\N
20190731-120704-M2K0OTUFLu6M5bC	1564553224675	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.675	\N	\N
20190731-120704-TZvumFeYMUm1jy4	1564553224675	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.675	\N	\N
20190731-120704-H1SMoIp7OWBpSNa	1564553224675	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.675	\N	\N
20190731-120704-yvo1iT0jOGRWdnO	1564553224675	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.675	\N	\N
20190731-120704-pXdZ3yPVqxs2BSs	1564553224675	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.675	\N	\N
20190731-120704-EIIcpLl75103a6B	1564553224676	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.676	\N	\N
20190731-120704-aWtd1aL0VlFCpid	1564553224676	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.676	\N	\N
20190731-120704-lkd5908FaUwYL1E	1564553224676	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.676	\N	\N
20190731-120704-FOTVffhQxD0dZDi	1564553224676	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.676	\N	\N
20190731-120704-h0FI684GrhJyHau	1564553224677	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.677	\N	\N
20190731-120704-pcCu73n4gkss2z3	1564553224677	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.677	\N	\N
20190731-120704-cIQ0Z8s146bTVyJ	1564553224677	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.677	\N	\N
20190731-120704-4QQZDDFxNgMhUDG	1564553224677	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.677	\N	\N
20190731-120704-wlirYhfmJOXkUZH	1564553224677	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.677	\N	\N
20190731-120704-Kkwm9INAuOBreQb	1564553224678	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.678	\N	\N
20190731-120704-SoOaEQ2Vfn42EdB	1564553224678	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.678	\N	\N
20190731-120704-Z6MRfzoGPCKq4WL	1564553224678	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.678	\N	\N
20190731-120704-gMgauyU0msmDtne	1564553224678	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.678	\N	\N
20190731-120704-p8zBDQkxkSTtfwJ	1564553224679	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.679	\N	\N
20190731-120704-atcFC3wobFFuQ1J	1564553224679	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.679	\N	\N
20190731-120704-A7WCeDvS3jS18TD	1564553224679	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.679	\N	\N
20190731-120704-qRdZlQ2YMCbU4WE	1564553224679	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.679	\N	\N
20190731-120704-K6GD0vWA8Ql6wAG	1564553224679	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.679	\N	\N
20190731-120704-HqhxMeQbiX2iSSO	1564553224680	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.68	\N	\N
20190731-120704-k4vLWrwutx7A7gX	1564553224680	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.68	\N	\N
20190731-120704-EsYVP6Olewtb6XK	1564553224680	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.68	\N	\N
20190731-120704-cNGsCXLiNiWQEU6	1564553224680	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.68	\N	\N
20190731-120704-5QEj08JQ9KTN9k4	1564553224681	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.681	\N	\N
20190731-120704-HjkDIrrYOU6imdx	1564553224681	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.681	\N	\N
20190731-120704-YXskpa7zvu5C4PY	1564553224681	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.681	\N	\N
20190731-120704-uOGLqtkCPPf4bPR	1564553224681	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.681	\N	\N
20190731-120704-Ms4P0hEl7NXDJsv	1564553224682	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.682	\N	\N
20190731-120704-STIgtlyBxXSLnFF	1564553224682	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.682	\N	\N
20190731-120704-nwGnLNsGLR10X8m	1564553224682	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.682	\N	\N
20190731-120704-EBEDMqo0lxLQEkZ	1564553224682	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.682	\N	\N
20190731-120704-hBIlwzubear8cvV	1564553224683	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.683	\N	\N
20190731-120704-qINaJUYQ28H3ssc	1564553224683	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.683	\N	\N
20190731-120704-uzwNC9qopCCFbkK	1564553224683	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.683	\N	\N
20190731-120704-0HHawJRIRIW1Wx0	1564553224683	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.683	\N	\N
20190731-120704-by9PRazCLNFVLYM	1564553224683	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.683	\N	\N
20190731-120704-pfRX4L9mJwTPTFv	1564553224684	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.684	\N	\N
20190731-120704-Gd76Vph5iV1X7In	1564553224684	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.684	\N	\N
20190731-120704-wUFUb1C77h3IqKQ	1564553224684	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.684	\N	\N
20190731-120704-wDWZJNXLsySVE5p	1564553224684	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.684	\N	\N
20190731-120704-Uphe17RrwATzvwp	1564553224685	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.685	\N	\N
20190731-120704-dmcGDlsTgWRw2Qx	1564553224685	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.685	\N	\N
20190731-120704-sg7cz4N6uIHHX4p	1564553224685	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.685	\N	\N
20190731-120704-pkpzix61Cnec12V	1564553224685	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.685	\N	\N
20190731-120704-cc0wrS3h9f9U1fW	1564553224685	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.685	\N	\N
20190731-120704-mj2VENbtK7j48W8	1564553224686	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.686	\N	\N
20190731-120704-k1EKV0E01Qdw7kP	1564553224686	\N	Draft	\N	\N	20190729-163920-cnSFhQNr3bw8U3a	1	143	tanim	2019-07-31 12:07:04.686	\N	\N
20190730-131706-wk0CQd3A3knd22a	1564471026802	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.293	tanim	2019-07-31 12:09:04.293
20190730-131706-iuaFai9l4U9ULgk	1564471026802	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.293	tanim	2019-07-31 12:09:04.293
20190730-131706-ZH8FoNvviFGma5T	1564471026803	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.293	tanim	2019-07-31 12:09:04.293
20190730-131706-8dbppRrWlvX0aJS	1564471026804	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.293	tanim	2019-07-31 12:09:04.293
20190730-131706-4UCseSpcVou2viR	1564471026804	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.294	tanim	2019-07-31 12:09:04.294
20190730-131706-WvhelmvzAR3bQqo	1564471026805	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.294	tanim	2019-07-31 12:09:04.294
20190730-131706-iH3mHNFxbT6nMaP	1564471026805	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.294	tanim	2019-07-31 12:09:04.294
20190730-131706-qMx8w6dQyNYcI0V	1564471026805	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.294	tanim	2019-07-31 12:09:04.294
20190730-131706-B5HrtnWq7k0OLDO	1564471026805	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.294	tanim	2019-07-31 12:09:04.294
20190730-131706-BzB1NRbaJ4iOBFi	1564471026805	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.294	tanim	2019-07-31 12:09:04.294
20190730-131706-BaEMMV1D1c3hoNq	1564471026806	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.295	tanim	2019-07-31 12:09:04.295
20190730-131706-t7iKk0MGb4YJ64x	1564471026806	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.295	tanim	2019-07-31 12:09:04.295
20190730-131706-F0OL1Z2DVk2shTI	1564471026806	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.295	tanim	2019-07-31 12:09:04.295
20190730-131706-QaXq6p3mESN5eqE	1564471026806	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.295	tanim	2019-07-31 12:09:04.295
20190730-131706-zZXXsQ3Q9djBAru	1564471026806	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.295	tanim	2019-07-31 12:09:04.295
20190730-131706-nQCyfDn7tql7QZ0	1564471026807	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.295	tanim	2019-07-31 12:09:04.295
20190730-131706-niKFFUlXeCG1Gww	1564471026807	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.296	tanim	2019-07-31 12:09:04.296
20190730-131706-CgA9g4uMDHTytWh	1564471026807	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.296	tanim	2019-07-31 12:09:04.296
20190730-131706-bxesTPv2p8US3QG	1564471026807	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.296	tanim	2019-07-31 12:09:04.296
20190730-131706-zqPklvrQ0w5y1nH	1564471026807	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.296	tanim	2019-07-31 12:09:04.296
20190730-131706-BocbDmLQZUM50Yq	1564471026808	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.296	tanim	2019-07-31 12:09:04.296
20190730-131706-Lk7GHppJNOzyztr	1564471026808	\N	Ready for tagging	\N	\N	20190730-131410-o7BiII0CXTImVP2	8	143	tanim	2019-07-31 12:09:04.296	tanim	2019-07-31 12:09:04.296
\.


--
-- Data for Name: asset_lost_info; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.asset_lost_info (oid, asset_lost_code, asset_oid, info_about_incident, incident_date, status, committee_oid, committee_notes, investigation_time, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: asset_maintenance; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.asset_maintenance (oid, maintenance_code, requester_oid, requestdate, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: asset_maintenance_line; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.asset_maintenance_line (oid, maintenance_for_each_asset_code, maintenance_oid, asset_category, asset_oid, maintenance_purpose, maintenance_type_oid, approver_note, decision_by, decision_date, assigned_vendor, requester_note, status, hand_over_date) FROM stdin;
\.


--
-- Data for Name: asset_maintenance_type; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.asset_maintenance_type (oid, maintenance_type_code, maintenance_type_name, asset_category_oid, service_description, priority, created_by, created_on, updated_by, updated_on, notes) FROM stdin;
\.


--
-- Data for Name: asset_requisition; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.asset_requisition (oid, asset_requisition_code, requester_oid, requested_date, purpose, decided_by, decision_date, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: asset_requisition_line; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.asset_requisition_line (oid, asset_category_oid, requested_quantity, status, approved_quantity, asset_requisition_oid) FROM stdin;
\.


--
-- Data for Name: asset_return; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.asset_return (oid, asset_return_code, asset_oid, receiver_oid, store_oid, return_date, asset_requisition_oid) FROM stdin;
\.


--
-- Data for Name: asset_tracking; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.asset_tracking (oid, asset_tracking_code, asset_oid, table_name, primary_key_value, changed_by, changed_date) FROM stdin;
\.


--
-- Data for Name: asset_tracking_line; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.asset_tracking_line (oid, asset_tracking_oid, column_name, new_value, previous_value) FROM stdin;
\.


--
-- Data for Name: asset_transfer; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.asset_transfer (oid, transfer_code, transfer_date, store_from, store_to, asset_oid, note, status, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: auto_maintenance; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.auto_maintenance (oid, auto_maintenance_code, set_by, to_notify, notification_date, period, status, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: contract_info; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.contract_info (oid, contract_code, rent_or_lease_info_oid, tenant_oid, asset_oid, start_from, ending_time, rent_type, rate, notification_time, renew_with_in, status, note, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: contract_renewal; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.contract_renewal (oid, contract_renewal_code, contract_info_oid, start_date, end_date, renew_within, rent_type, rate, approved_by, note) FROM stdin;
\.


--
-- Data for Name: contractual_driver; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.contractual_driver (oid, driver_code, driver_name, address, contact_no, nid, licence_no, description, driver_type, status, vehicle_oid, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: flat_info; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.flat_info (oid, flat_code, floor_info_oid, flat_no, position_of_flat, total_room, status) FROM stdin;
\.


--
-- Data for Name: floor_info; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.floor_info (oid, floor_code, building_id, floor_no, total_flat) FROM stdin;
\.


--
-- Data for Name: fuel_reconciliation; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.fuel_reconciliation (oid, fuel_reconciliation_code, fuel_type, vehicle_oid, driver_oid, station_name, quantity, rate, unit, total, date, document_oid, status, approved_by, approved_date, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: fuel_requisition; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.fuel_requisition (oid, fuel_requisition_code, fuel_type, requested_quantity, vehicle_oid, driver_oid, requisition_date, status, approved_quantity, approved_by, approved_date, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: fuel_service_station; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.fuel_service_station (oid, station_code, station_name, fuel_type_oid, location, contact_number, contract_signing_date, contract_closing_date, created_by, created_on, updated_by, updated_on, status) FROM stdin;
\.


--
-- Data for Name: fuel_type; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.fuel_type (oid, fuel_type_code, fuel_type_name, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: garage; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.garage (oid, garage_code, garage_name, location, garage_contact_no, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: maintenance_asset_receive; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.maintenance_asset_receive (oid, maintenance_received_code, asset_maintenance_oid, received_date, received_by, qc_committee, qc_note, qc_time, qc_status) FROM stdin;
\.


--
-- Data for Name: maintenance_payment; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.maintenance_payment (oid, payment_code, asset_maintenance_oid, bill_type, invoice_no, net_amount, payment_date, received_by, bill_attachment_file) FROM stdin;
\.


--
-- Data for Name: method_rule_category; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.method_rule_category (oid, method_rule_category_code, method_rule_category_name, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: method_setup; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.method_setup (oid, method_code, method_name, method_rule_category_oid, description, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: rent_lease_info; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.rent_lease_info (oid, rent_lease_lnfo_code, requester_oid, tender_oid, description, proposal_document, approved_by, decision_date, created_by, created_on, updated_by, updated_on, status) FROM stdin;
\.


--
-- Data for Name: rent_receive; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.rent_receive (oid, rent_receive_code, contract_info_oid, year, month, net_amount, payment_date, received_by_oid, note) FROM stdin;
\.


--
-- Data for Name: route_config; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.route_config (oid, route_config_code, employee_oid, pickup_location, route_setup_oid, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: route_setup; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.route_setup (oid, route_oid, route_name, start_area, end_area, vehicle_oid, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: rules_regulation; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.rules_regulation (oid, rules_regulation_code, name, description, method_category_oid, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: temporary_item; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.temporary_item (oid, code, received_at, received_by, chalan_no, chalan_date, workorder_no, workorder_date, status, description, qc_by, qc_on, asset_added_by, asset_added_on, vendor_oid, office_oid, created_by, created_on, updated_by, updated_on) FROM stdin;
1	A-01	2019-06-09	tanim	\N	\N	\N	\N	Draft	\N	\N	\N	\N	\N	1	143	System	2019-07-23 20:57:27.873006	\N	\N
2	A-02	2019-06-09	shiropa	\N	\N	\N	\N	Draft	\N	\N	\N	\N	\N	2	143	System	2019-07-23 20:57:27.873006	\N	\N
3	A-03	2019-06-09	kabir	\N	\N	\N	\N	Draft	\N	\N	\N	\N	\N	1	143	System	2019-07-23 20:57:27.873006	\N	\N
20190724-124840-6mDRg7PSmb8XAwD	1563950920152	2019-07-24	shaon	test1	2019-07-23	t1	2019-07-24	Asset added	created	shaon	2019-07-25 16:29:16.775	shiropa	2019-07-25 16:31:20.509	1	143	shaon	2019-07-24 12:48:40.152	shiropa	2019-07-25 16:31:20.509
20190728-111439-FvqFqEmpAoftiVd	1564290879539	2019-07-28	shaon	t1	2019-07-27	test1	2019-07-27	Asset adding	Testcase1	tanim	2019-07-28 11:50:00.228	tanim	2019-07-28 11:52:11.131	3	143	shaon	2019-07-28 11:14:39.539	tanim	2019-07-28 11:52:11.131
20190731-121401-AbkRz82oVpPiiet	1564553641769	2019-08-08	shaon	চালান ৩১-০৭	2019-07-28	৩১০০০৭	2019-07-30	QC done	\N	shaon	2019-07-31 12:14:32.675	\N	\N	2	143	shaon	2019-07-31 12:14:01.769	shaon	2019-07-31 12:14:32.675
20190729-164117-7c4XcXxcsMdrW3Q	1564396877656	2019-06-02	tanim	1	2019-06-03	2	2019-06-09	QC done	Shiropa	shaon	2019-07-31 12:16:22.614	\N	\N	1	143	tanim	2019-07-29 16:41:17.656	shaon	2019-07-31 12:16:22.614
20190728-164437-BDSgr7HEiguTArf	1564310677473	2019-07-27	shaon	321	2019-07-27	321	2019-07-27	Asset added	\N	shaon	2019-07-28 16:45:36.868	shiropa	2019-07-28 16:46:27.431	1	143	shaon	2019-07-28 16:44:37.473	shiropa	2019-07-28 16:46:27.431
20190731-121825-gHokLYC2dHgqQvg	1564553905481	2019-07-19	shaon	চালান ৩১ ০০৭	2019-07-19	৩১০০০০৭	2019-07-29	Ready for QC	\N	\N	\N	\N	\N	2	143	shaon	2019-07-31 12:18:25.481	\N	\N
20190728-175347-rcrxkktj4GJVkV0	1564314827013	2019-07-28	shaon	chalan1	2019-07-27	chalan1234	2019-07-27	Asset adding	\N	shaon	2019-07-28 17:56:42.839	shiropa	2019-07-28 17:58:36.275	3	143	shaon	2019-07-28 17:53:47.013	shiropa	2019-07-28 17:58:36.275
20190731-135919-UzA3W0VimJo2jsH	1564559959706	2019-07-01	shaon	chalan 111	2019-07-01	11101	2019-07-01	Ready for QC	\N	\N	\N	\N	\N	2	143	shaon	2019-07-31 13:59:19.706	\N	\N
20190728-184034-pSxEqrQpNzQzxFP	1564317634798	2019-07-28	shaon	চালান ১ 	2019-07-27	১২৩৪৫	2019-07-27	Asset adding	\N	shaon	2019-07-28 18:41:24.821	shiropa	2019-07-28 18:42:35.306	1	143	shaon	2019-07-28 18:40:34.798	shiropa	2019-07-28 18:42:35.306
20190729-100523-p52kj9RUQcG8Fj7	1564373123058	2019-07-30	shaon	৫৫০	2019-07-29	চালান৫৫০	2019-07-27	Asset adding	\N	shaon	2019-07-29 10:52:15.208	shiropa	2019-07-29 10:54:23.362	2	143	shaon	2019-07-29 10:05:23.058	shiropa	2019-07-29 10:54:23.362
20190730-132459-RhQWIpeOc7LcGae	1564471499618	2019-08-06	shaon	30-08	2019-07-22	300008	2019-07-29	QC done	\N	shaon	2019-07-30 13:31:39.536	\N	\N	1	143	shaon	2019-07-30 13:24:59.618	shaon	2019-07-30 13:31:39.536
20190724-164730-rztLidOhLnvL0aG	1563965250481	2019-07-24	mashud	t1	2019-07-23	test01	2019-07-19	Asset adding	Assettest1	tanim	2019-07-28 17:49:29.946	shiropa	2019-07-29 11:41:42.043	1	143	mashud	2019-07-24 16:47:30.481	shiropa	2019-07-29 11:41:42.043
20190729-111511-TEEUc5bYCIzeaKp	1564377311086	2019-07-28	shaon	1234	2019-07-28	575	2019-07-28	Asset adding	\N	shaon	2019-07-29 11:20:50.736	shiropa	2019-07-29 11:44:53.222	3	143	shaon	2019-07-29 11:15:11.086	shiropa	2019-07-29 11:44:53.222
20190729-110124-8Bt6Dflq4STsoYr	1564376484360	2019-07-29	shaon	29	2019-07-28	454	2019-07-27	Asset added	\N	shaon	2019-07-29 11:03:17.096	shiropa	2019-07-29 11:45:13.828	1	143	shaon	2019-07-29 11:01:24.36	shiropa	2019-07-29 11:45:13.828
20190724-165346-4teRYvHk1LPwnoW	1563965626309	2019-07-25	mashud	t3	2019-07-24	test003	2019-07-15	QC done	test03	shaon	2019-07-29 12:03:42.254	\N	\N	2	143	mashud	2019-07-24 16:53:46.309	shaon	2019-07-29 12:03:42.254
20190729-163002-nJ4NczZg0OqYcif	\N	2019-06-02	tanim	1	\N	\N	\N	Draft	ok	\N	\N	\N	\N	1	143	tanim	2019-07-29 16:30:02.903	\N	\N
20190730-114400-W0GkZ9Zu3zlqSzQ	1564465440935	2019-07-30	shaon	30-07	2019-07-29	30007	2019-07-28	Ready for QC	No Comments	\N	\N	\N	\N	1	143	shaon	2019-07-30 11:44:00.935	\N	\N
20190730-154519-gZ1BuM1UOPYfaXu	1564479919318	2019-07-29	shaon	১২৩৪৫	2019-07-29	১২৩৪৫	2019-07-29	Ready for QC	\N	\N	\N	\N	\N	1	143	shaon	2019-07-30 15:45:19.318	\N	\N
20190724-164939-mEVAYn3J6EouFbA	1563965379374	2019-07-25	mashud	t2	2019-07-24	test2	2019-07-16	Asset adding	test2	shaon	2019-07-30 13:12:03.032	shiropa	2019-07-30 16:06:11.662	2	143	mashud	2019-07-24 16:49:39.374	shiropa	2019-07-30 16:06:11.662
20190730-114951-tR10pznW8jvcJ3r	20190730-120809-cV2VsNLaiS4pTiT	2019-08-02	shaon	30-08	2019-07-30	30008	2019-07-28	Asset adding	\N	shaon	2019-07-30 13:12:21.312	shiropa	2019-07-30 16:09:28.991	1	143	shaon	2019-07-30 12:08:09.991	shiropa	2019-07-30 16:09:28.991
20190731-105534-vhxqAv5bh8J3bBm	1564548934265	2019-07-30	shaon	31-07	2019-07-22	3100007	2019-08-14	QC done	\N	shaon	2019-07-31 11:03:30.319	\N	\N	2	143	shaon	2019-07-31 10:55:34.265	shaon	2019-07-31 11:03:30.319
20190730-153825-YI3YIbbTKezh5aH	20190730-154033-g9V62EU1J1FTDn6	2019-07-29	shaon	66666	2019-07-29	666666	2019-07-29	QC done	\N	tanim	2019-07-31 12:06:43.799	\N	\N	1	143	shaon	2019-07-30 15:40:33.311	tanim	2019-07-31 12:06:43.799
20190731-155350-9exptRFa4JyfhtN	1564566830283	2019-06-02	tanim	1	2019-06-03	2	2019-06-09	Ready for QC	Shiropa	\N	\N	\N	\N	1	143	tanim	2019-07-31 15:53:50.283	\N	\N
20190729-163920-cnSFhQNr3bw8U3a	1564396760030	2019-06-02	tanim	1	2019-06-03	2	2019-06-09	QC done	Shiropa	tanim	2019-07-31 12:07:04.272	\N	\N	1	143	tanim	2019-07-29 16:39:20.03	tanim	2019-07-31 12:07:04.272
20190730-114732-ICyUrv7H3ihYIP7	20190730-114757-wEOzsnJrRKi3S8A	2019-08-01	shaon	30-08	2019-07-30	30008	2019-07-28	Asset adding	\N	shaon	2019-07-30 13:11:06.03	tanim	2019-07-31 12:08:34.124	1	143	shaon	2019-07-30 11:47:57.019	tanim	2019-07-31 12:08:34.124
20190730-131410-o7BiII0CXTImVP2	1564470850821	2019-07-01	shaon	30-08	2019-07-30	300008	2019-07-19	Asset adding	\N	shaon	2019-07-30 13:17:06.801	tanim	2019-07-31 12:09:04.297	1	143	shaon	2019-07-30 13:14:10.821	tanim	2019-07-31 12:09:04.297
\.


--
-- Data for Name: temporary_item_detail; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.temporary_item_detail (oid, received_quantity, qualified_quantity, disqualified_quantity, extra_quantity, remarks, item_oid, temporary_item_oid) FROM stdin;
1	5.00000000	\N	\N	0.00000000	\N	1	1
2	7.00000000	\N	\N	0.00000000	\N	2	1
3	5.00000000	\N	\N	0.00000000	\N	1	2
4	8.00000000	\N	\N	0.00000000	\N	2	2
5	3.00000000	\N	\N	0.00000000	\N	1	3
20190724-124840-BeiVi2T206pcd9z	1.00000000	1.00000000	0.00000000	0.00000000	t1	12	20190724-124840-6mDRg7PSmb8XAwD
20190728-111439-kjjoxmk6V0181jC	1.00000000	0.00000000	1.00000000	0.00000000	\N	14	20190728-111439-FvqFqEmpAoftiVd
20190728-164437-gCYX9Bp3rdbQNTd	12.00000000	2.00000000	10.00000000	0.00000000	\N	14	20190728-164437-BDSgr7HEiguTArf
20190724-164730-xqxKQOhN72lqkl5	1.00000000	1.00000000	0.00000000	1.00000000	Done	14	20190724-164730-rztLidOhLnvL0aG
20190728-175347-sbsEwSfB3KtaVdH	15.00000000	0.00000000	15.00000000	0.00000000	\N	5	20190728-175347-rcrxkktj4GJVkV0
20190728-184034-jZQr0IJZ6ZhJ2Ng	5.00000000	0.00000000	5.00000000	0.00000000	\N	2	20190728-184034-pSxEqrQpNzQzxFP
20190729-100523-CtavQXxTVrAKSKK	20.00000000	0.00000000	20.00000000	0.00000000	\N	2	20190729-100523-p52kj9RUQcG8Fj7
20190729-110124-kFaB0m2pQuzaQ7G	40.00000000	40.00000000	0.00000000	1.00000000	\N	14	20190729-110124-8Bt6Dflq4STsoYr
20190729-111511-AviwtxaFWoydjmM	68.00000000	40.00000000	28.00000000	56.00000000	hgjgjgjk	17	20190729-111511-TEEUc5bYCIzeaKp
20190724-165346-2OT7TEgU8gJBuz6	1.00000000	1.00000000	0.00000000	1.00000000	yjtufu	17	20190724-165346-4teRYvHk1LPwnoW
20190730-114400-oWhlRWm5vCQr0zh	4.00000000	0.00000000	0.00000000	1.00000000	No Comments	10	20190730-114400-W0GkZ9Zu3zlqSzQ
20190730-114757-wGE6V7K83WzFifv	5.00000000	0.00000000	5.00000000	2.00000000	\N	18	20190730-114732-ICyUrv7H3ihYIP7
20190724-164939-9m9G0FOWw0YrSZ2	1.00000000	1.00000000	0.00000000	1.00000000	Done	17	20190724-164939-mEVAYn3J6EouFbA
20190730-120809-gVg6Mdniwk9jTUP	3.00000000	2.00000000	1.00000000	2.00000000	\N	15	20190730-114951-tR10pznW8jvcJ3r
20190730-131410-IAjXVIqWNYpRUbr	20.00000000	25.00000000	-5.00000000	5.00000000	\N	8	20190730-131410-o7BiII0CXTImVP2
20190730-132459-KG7dlmLUHdncpQA	50.00000000	30.00000000	20.00000000	5.00000000	\N	16	20190730-132459-RhQWIpeOc7LcGae
20190730-154519-PbtwjqhYBEYlKAw	12345.00000000	0.00000000	0.00000000	0.00000000	\N	2	20190730-154519-gZ1BuM1UOPYfaXu
20190731-105534-8SlpL1BKujPGQOv	40.00000000	-34.00000000	74.00000000	20.00000000	\N	2	20190731-105534-vhxqAv5bh8J3bBm
20190730-154033-BJ5tabKnZvmYSEG	10.00000000	100.00000000	-90.00000000	0.00000000	\N	14	20190730-153825-YI3YIbbTKezh5aH
20190729-163920-siPmMIyYnaYfdJp	2.00000000	2000.00000000	-1998.00000000	0.00000000	good	1	20190729-163920-cnSFhQNr3bw8U3a
20190731-121401-FwVoXHEsEscGzlt	41.00000000	0.00000000	41.00000000	8.00000000	\N	12	20190731-121401-AbkRz82oVpPiiet
20190729-164117-N4fqCb2g7yzl3Je	2.00000000	0.00000000	2.00000000	0.00000000	good	1	20190729-164117-7c4XcXxcsMdrW3Q
20190731-121825-YToEKqtXX70Rk0Q	70.00000000	0.00000000	0.00000000	845.00000000	\N	3	20190731-121825-gHokLYC2dHgqQvg
20190731-135919-AWmRFV3HttClgMf	45.00000000	0.00000000	0.00000000	4.00000000	\N	12	20190731-135919-UzA3W0VimJo2jsH
20190731-155350-AwOjThIVrepA28Z	2.00000000	0.00000000	0.00000000	0.00000000	good	1	20190731-155350-9exptRFa4JyfhtN
\.


--
-- Data for Name: tenant; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.tenant (oid, tenant_code, tenant_name, rent_type, address, contact_no, emergency_contact, nid_number, tin_number, bin_number, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: tender; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.tender (oid, tender_code, asset_oid, circulation_date, description, document_oid) FROM stdin;
\.


--
-- Data for Name: ticket_generation; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.ticket_generation (oid, ticket_gen_code, fuel_requisition_oid, station_oid, quantity) FROM stdin;
\.


--
-- Data for Name: vehicle_allocation; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.vehicle_allocation (oid, vehicle_allocation_code, vehicle_requisition_oid, distributor_oid, allocation_date) FROM stdin;
\.


--
-- Data for Name: vehicle_info; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.vehicle_info (oid, vehicle_code, garage_oid, engine_no, insurance_no, fitness_registration_no, registration_no, registration_date, number_plate, insurance_expiry_date, fitness_expiry_date, registration_expiry_date, engine_cc, color, fuel_type, fuel_capacity, horse_power, tex_token, vehicle_type, transmission_type, motor_type, no_of_seats, country_of_origin, lifetime, status, description, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: vehicle_requisition; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.vehicle_requisition (oid, vehicle_requisition_code, vehicle_type, requester_oid, requsition_date, start_date, end_date, purpose, organization, decision_by, decision_date, status, vehicle_oid, driver_oid, description, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: vehicle_return; Type: TABLE DATA; Schema: ast; Owner: grp
--

COPY ast.vehicle_return (oid, vehicle_return_code, receiver_oid, garage_oid, return_date, vehicle_requisition_oid) FROM stdin;
\.


--
-- Data for Name: allowance_setup; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.allowance_setup (oid, grade, house_rent_allowance, medical_allowance, mobile_allowance, other_allowance, percentage, start_date, end_date) FROM stdin;
\.


--
-- Data for Name: city_corporation; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.city_corporation (oid, name_en, name_bn, bbs_code, district_oid) FROM stdin;
\.


--
-- Data for Name: city_corporation_ward; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.city_corporation_ward (oid, name_en, name_bn, bbs_code, district_oid, city_corporation_oid) FROM stdin;
\.


--
-- Data for Name: committee_setup; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.committee_setup (oid, committee_code, committee_type_oid, committee_name, max_member, min_member, forming_date, purpose, comments, status, organization_info_oid) FROM stdin;
\.


--
-- Data for Name: committee_setup_line; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.committee_setup_line (oid, committee_setup_line_code, committee_setup_oid, employee_info_oid, member_role, orgaization_oid) FROM stdin;
\.


--
-- Data for Name: committee_type_setup; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.committee_type_setup (oid, committee_type_code, committee_type, approved_by) FROM stdin;
\.


--
-- Data for Name: deduction_setup; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.deduction_setup (oid, grade, house_rent_deduction, tax_deduction, gpf_deduction, other_deduction, percentage, start_date, end_date) FROM stdin;
\.


--
-- Data for Name: document; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.document (oid, document_code, document_name, document_type_oid, category, path, description) FROM stdin;
\.


--
-- Data for Name: document_type; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.document_type (oid, document_type_code, document_type_name) FROM stdin;
\.


--
-- Data for Name: donee_info; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.donee_info (oid, donee_code, donee_name, party_type_oid, address, contact_no, organization, description, status) FROM stdin;
\.


--
-- Data for Name: donor_info; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.donor_info (oid, donor_code, donor_name, party_type_oid, address, contact_no, organization, description, status) FROM stdin;
\.


--
-- Data for Name: entitlement; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.entitlement (oid, entitlement_code, designation_oid, post_code, fiscal_year, item_oid, quantity) FROM stdin;
\.


--
-- Data for Name: fiscal_year; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.fiscal_year (oid, name_en, name_bn, status, start_date, end_date) FROM stdin;
1	2018-2019	২০১৮-২০১৯	Previous	2018-07-01	2019-06-30
2	2019-2020	২০১৯-২০২০	Current	2019-07-01	2020-06-30
3	2020-2021	২০২০-২০২১	Next	2020-07-01	2021-06-30
4	2021-2022	২০২১-২০২২	Upcoming	2021-07-01	2022-06-30
5	2022-2023	২০২২-২০২৩	Upcoming	2022-07-01	2023-06-30
6	2023-2024	২০২৩-২০২৪	Upcoming	2023-07-01	2024-06-30
\.


--
-- Data for Name: geo_country; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.geo_country (oid, name_en, name_bn) FROM stdin;
1	Bangladesh	বাংলাদেশ
\.


--
-- Data for Name: geo_district; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.geo_district (oid, name_en, name_bn, bbs_code, division_oid) FROM stdin;
\.


--
-- Data for Name: geo_division; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.geo_division (oid, name_en, name_bn, bbs_code, country_oid) FROM stdin;
1	Barisal	বরিশাল	10	1
2	Chittagong	চট্টগ্রাম	20	1
3	Dhaka	ঢাকা	30	1
4	Khulna	খুলনা	40	1
5	Rajshahi	রাজশাহী	50	1
6	Rangpur	রংপুর	60	1
7	Sylhet	সিলেট	70	1
9	Mymensingh	ময়মনসিংহ	45	1
\.


--
-- Data for Name: geo_post_office; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.geo_post_office (oid, name_en, name_bn, bbs_code, union_oid) FROM stdin;
\.


--
-- Data for Name: geo_thana; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.geo_thana (oid, name_en, name_bn, bbs_code, district_oid) FROM stdin;
\.


--
-- Data for Name: geo_union; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.geo_union (oid, name_en, name_bn, bbs_code, upazila_oid) FROM stdin;
\.


--
-- Data for Name: geo_union_ward; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.geo_union_ward (oid, name_en, name_bn, bbs_code, union_oid) FROM stdin;
\.


--
-- Data for Name: geo_upazila; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.geo_upazila (oid, name_en, name_bn, bbs_code, district_oid) FROM stdin;
\.


--
-- Data for Name: item; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.item (oid, code, name_en, name_bn, attribute_schema, uom_oid, item_category_oid) FROM stdin;
1	I-1	Printer paper	ছাপার কাগজ	\N	9	1
2	I-2	Carbon paper	কার্বন কাগজ	\N	7	1
3	I-3	Letterhead	লেটারহেড	\N	7	1
4	I-4	Pencil	পেন্সিল	\N	5	2
5	I-5	Eraser	রবার	\N	6	2
6	I-6	Pen	কলম	\N	5	2
7	I-7	Monitor	মনিটর	\N	6	3
8	I-8	Keyboard	কীবোর্ড	\N	6	3
9	I-9	Mouse	মাউস	\N	6	3
10	I-10	Photocopier Machine	ফটোকপি মেশিন	\N	6	3
11	I-11	Scanner	স্ক্যানার	\N	6	3
12	I-12	Chair	চেয়ার	\N	6	4
13	I-13	Table	টেবিল	\N	6	4
14	I-14	Bookcase	বই রাখিবার আলমারি	\N	6	4
15	I-15	First-aid kit	প্রাথমিক চিকিৎসার সরঞ্জাম	\N	8	5
16	I-16	Facial tissue	মুখে ব্যবহারের টিস্যু	\N	8	5
17	I-17	Broom	ঝাড়ু	\N	6	5
18	I-18	Petrol	পেট্রল	\N	3	5
19	I-19	Sugar	\nচিনি	\N	1	5
20	I-20	Safran	জাফরান	\N	2	5
\.


--
-- Data for Name: item_category; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.item_category (oid, code, name_en, name_bn, description_en, description_bn, status, attribute_schema_en, attribute_schema_bn) FROM stdin;
1	IC-1	Paper	কাগজ	Paper	কাগজ	Available	{}	{}
2	IC-2	Writing Implements	লেখার সরঞ্জাম	Writing Implements	লেখার সরঞ্জাম	Available	{}	{}
3	IC-3	Electrical Items	বৈদ্যুতিক আইটেম	Electrical Items	বৈদ্যুতিক আইটেম	Available	{}	{}
4	IC-4	Furniture	ফার্নিচার	Furniture	ফার্নিচার	Available	{}	{}
5	IC-5	Miscellaneous	বিবিধ	Miscellaneous	বিবিধ	Available	{}	{}
\.


--
-- Data for Name: item_uom; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.item_uom (oid, code, name_en, name_bn, mnemonic_en, mnemonic_bn, status) FROM stdin;
1	uom_kg	Kilogram	কিলোগ্রাম	Kg	কেজি	Active
2	uom_gm	Gram	গ্রাম	g	গ্রাম	Active
3	uom_lt	Litre	লিটার	Lt	লিটার	Active
4	uom_pc	Piece	টুকরা	Pc	টুকরা	Active
5	uom_dz	Dozen	ডজন	Dz	ডজন	Active
6	uom_pcs	Pcs	টি	Pcs	টি	Active
7	uom_nos	Nos	সেটসমূহ	Nos	সেটসমূহ	Active
8	uom_box	Box	বাক্স	Box	বাক্স	Active
9	uom_ream	Ream	রীম	Ream	রীম	Active
\.


--
-- Data for Name: loan_sanction; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.loan_sanction (oid, employee_oid, last_date_of_loan, date_of_retirement, loan_type, loan_amount, no_of_installment, loan_letter_upload, notes) FROM stdin;
\.


--
-- Data for Name: ministry; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.ministry (oid, name_en, name_bn, code_en, code_bn, status, sort_order, ministry_type_oid) FROM stdin;
20	Ministry of Planning	পরিকল্পনা মন্ত্রণালয়	MoP	\N	Active	1	1
57	Division	তথ্য ও যোগাযোগ প্রযুক্তি বিভাগ	ICTD	\N	Active	2	2
\.


--
-- Data for Name: ministry_type; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.ministry_type (oid, name_en, name_bn, sort_order) FROM stdin;
1	Ministry	মন্ত্রণালয়	1
2	Division	বিভাগ	2
\.


--
-- Data for Name: municipality; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.municipality (oid, name_en, name_bn, bbs_code, upazila_oid) FROM stdin;
\.


--
-- Data for Name: municipality_ward; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.municipality_ward (oid, name_en, name_bn, bbs_code, upazila_oid, municipality_oid) FROM stdin;
\.


--
-- Data for Name: office; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.office (oid, name_en, name_bn, code_en, code_bn, address_en, address_bn, email, web, status, sort_order, parent_oid, office_origin_oid, office_layer_oid, ministry_oid) FROM stdin;
143	Planning Division	পরিকল্পনা বিভাগ	\N	\N	Planning Division, Dhaka	পরিকল্পনা বিভাগ, ঢাকা	\N	www.plandiv.gov.bd	Active	0	0	92	70	20
2102	Planning Commission	পরিকল্পনা কমিশন	\N	\N	planning commission	পরিকল্পনা কমিশন	\N	\N	Active	0	143	184	281	20
2935	National Academy for Planning and Development (NAPD)	জাতীয় পরিকল্পনা ও উন্নয়ন একাডেমি (এনএপিডি)	\N	\N	3/A, Nilkhet, Dhaka-1205	৩/এ, নীলক্ষেত, ঢাকা-১২০৫	info@napd.gov.bd	www.napd.gov.bd	Active	0	143	247	277	20
\.


--
-- Data for Name: office_layer; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.office_layer (oid, name_en, name_bn, status, level_order, sort_order, parent_oid, ministry_oid) FROM stdin;
70	Division	বিভাগ	Active	1	0	70	20
147	Organization	সংস্থা	Active	3	3	70	20
251	District Office	জেলা কার্যালয়	Active	5	0	147	20
252	Divisional Office	বিভাগীয় কার্যালয়	Active	4	3	147	20
276	Ministry	মন্ত্রনালয়ের কার্যালয়	Active	1	1	\N	20
277	Academy/Training Institute	একাডেমি/ প্রশিক্ষণ কেন্দ্র	Active	3	4	276	20
281	Commission	কমিশন	Active	3	2	276	20
292	Beuro	ব্যুরো	Active	3	3	70	20
244	Division	বিভাগ	Active	0	0	\N	57
\.


--
-- Data for Name: office_origin; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.office_origin (oid, name_en, name_bn, status, level_order, sort_order, parent_oid, office_layer_oid, ministry_oid) FROM stdin;
77	IMED	বাস্তবায়ন পরিবীক্ষণ ও মূল্যায়ন বিভাগ	Active	1	2	0	70	20
92	Planning Division	পরিকল্পনা বিভাগ	Active	2	2	0	70	20
162	Statistics and Informatics Division	পরিসংখ্যান ও তথ্য ব্যবস্থাপনা বিভাগ	Active	2	3	0	70	20
164	Bangladesh Bureau of Statistics	বাংলাদেশ পরিসংখ্যান ব্যুরো	Active	4	0	162	292	20
184	planning commission	পরিকল্পনা কমিশন	Active	6	2	92	281	20
247	NAPD	জাতীয় পরিকল্পনা ও উন্নয়ন একাডেমি	Active	6	4	92	277	20
470	District Statistics Office	জেলা পরিসংখ্যান কার্যালয়, (জেলার নাম)	Active	6	0	477	251	20
477	Divisional Statistics Office, (Division Name)	বিভাগীয় পরিসংখ্যান কার্যালয়, (বিভাগের নাম)	Active	4	2	164	252	20
\.


--
-- Data for Name: office_origin_unit; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.office_origin_unit (oid, name_en, name_bn, status, level_order, sort_order, parent_oid, office_unit_category_oid, office_origin_oid, office_layer_oid, ministry_oid) FROM stdin;
1260	Office of the Minister	মন্ত্রীর দপ্তর	Active	\N	0	0	\N	77	70	20
1261	Office of the Deputy Minister	প্রতিমন্ত্রীর দপ্তর	Active	\N	0	1260	\N	77	70	20
1262	Office of the Secretary	সচিবের দপ্তর	Active	\N	0	1261	\N	77	70	20
1263	Director General (Agri & Rural)	মহাপরিচালক ( কৃষি, পল্লী উন্নয়ন ও গবেষনা)	Active	\N	0	1262	\N	77	70	20
1264	Director General (Water & Environment)	মহাপরিচালক শিক্ষা ও সামাজিক)	Active	\N	0	1262	\N	77	70	20
1265	Director General (Education)	মহাপরিচালক (যোগাযোগ ও স্থানীয় সরকার)	Active	\N	0	1262	\N	77	70	20
1266	Director General (Social)	মহাপরিচালক (সামাজিক)	Inactive	\N	0	1262	\N	77	70	20
1267	Director General & PD (CPTU)	মহাপরিচালক ও প্রকল্প পরিচালক (সিপিটিইউ)	Inactive	\N	0	1262	\N	77	70	20
1268	Director General (Local Govt.)	মহাপরিচালক (স্থানীয় সরকার)	Inactive	\N	0	1262	\N	77	70	20
1269	Director General (Evaluation)	মহাপরিচালক (মূল্যায়ন)	Inactive	\N	0	1262	\N	77	70	20
1270	Director General (Industry)	মহাপরিচালক (শিল্প)	Inactive	\N	0	1262	\N	77	70	20
1271	Director General (Power)	মহাপরিচালক (বিদ্যুৎ)	Inactive	\N	0	1262	\N	77	70	20
1272	Joint Secretary	প্রধান (শিল্প ও শক্তি)	Active	\N	0	1262	\N	77	70	20
1273	Joint Secretary (Co-ordination)	মহাপরিচালক (সিপিটিইউ)	Active	\N	0	1262	\N	77	70	20
1274	Chief Communication	মহাপরিচালক (মূল্যায়ন)	Active	\N	0	1262	\N	77	70	20
1275	Director	পরিচালক (কৃষি)	Active	\N	0	1263	\N	77	70	20
1276	Director	পরিচালক (পানি)	Active	\N	0	1263	\N	77	70	20
1277	Director	পরিচালক (পল্লী উন্নয়ন)	Active	\N	0	1263	\N	77	70	20
1278	Director	পরিচালক	Inactive	\N	0	1263	\N	77	70	20
1279	Deputy Director	(কৃষি-২)	Active	\N	0	1275	\N	77	70	20
1280	Deputy Director	(কৃষি-৩)	Active	\N	0	1275	\N	77	70	20
1281	Deputy Director	উপ-পরিচালক	Inactive	\N	0	1275	\N	77	70	20
1282	Agriculture -1	(কৃষি-১)	Active	\N	0	1275	\N	77	70	20
1283	Assistant Director	সহকারী পরিচালক (কৃষি-২)	Active	\N	0	1275	\N	77	70	20
1284	Deputy Director	উপ পরিচালক (পানি-১)	Active	\N	0	1276	\N	77	70	20
1285	Deputy Director	উপ-পরিচালক	Inactive	\N	0	1276	\N	77	70	20
1286	Assistant Director	সহকারী পরিচালক (পানি-১)	Active	\N	0	1276	\N	77	70	20
1287	Assistant Director	সহকারী পরিচালক (পানি-২)	Active	\N	0	1276	\N	77	70	20
1289	\N	পরিচালক (সমন্বয়)	Active	\N	0	13847	\N	77	70	20
1290	\N	প্রশাসন অনুবিভাগ	Active	\N	0	1262	\N	77	70	20
1291	\N	উপ পরিচালক (পল্লী উন্নয়ন)	Active	\N	0	1277	\N	77	70	20
1292	\N	সহকারী পরিচালক (পল্লী উন্নয়ন-১)	Active	\N	0	1277	\N	77	70	20
1293	\N	সহকারী পরিচালক (পল্লী উন্নয়ন-২)	Active	\N	0	1277	\N	77	70	20
1294	\N	পরিচালক (স্বাস্থ্য)	Active	\N	0	1264	\N	77	70	20
1295	\N	পরিচালক (শিক্ষা)	Active	\N	0	1264	\N	77	70	20
1296	\N	পরিচালক (যুব)	Active	\N	0	1264	\N	77	70	20
1297	\N	উপ পরিচালক (শিক্ষা)	Active	\N	0	1295	\N	77	70	20
1298	\N	উপ পরিচালক (স্বাস্থ্য)	Active	\N	0	1294	\N	77	70	20
1299	\N	সহকারী পরিচালক (স্বাস্থ্য-১)	Active	\N	0	1294	\N	77	70	20
1300	\N	সহকারী পরিচালক (স্বাস্থ্য-২)	Active	\N	0	1294	\N	77	70	20
1301	\N	সহকারী পরিচালক (শিক্ষা)	Active	\N	0	1295	\N	77	70	20
1302	\N	সহকারী পরিচালক (যুব-১)	Inactive	\N	0	1264	\N	77	70	20
1303	\N	সহকারী পরিচালক (যুব-১)	Active	\N	0	1296	\N	77	70	20
1304	\N	সহকারী পরিচালক (যুব-২)	Active	\N	0	1296	\N	77	70	20
1305	\N	পরিচালক (স্থানীয় সরকার)	Active	\N	0	1265	\N	77	70	20
1306	\N	পরিচালক (পরিবহন)	Active	\N	0	1265	\N	77	70	20
1307	\N	উপ পরিচালক (স্থানীয় সরকার)	Active	\N	0	1305	\N	77	70	20
1308	\N	উপ পরিচালক (স্থানীয় সরকার-১)	Active	\N	0	1305	\N	77	70	20
1309	\N	সহকারী পরিচালক (স্থানীয় সরকার-২)	Active	\N	0	1305	\N	77	70	20
1310	\N	উপ পরিচালক (পরিবহন)	Inactive	\N	0	1265	\N	77	70	20
1311	Assistant Director (Transport)	সহ পরিচালক (পরিবহন)	Active	\N	0	1306	\N	77	70	20
1312	\N	সহকারী পরিচালক (পরিবহন-১)	Active	\N	0	1306	\N	77	70	20
1313	\N	সহকারী পরিচালক (পরিবহন-২)	Active	\N	0	1306	\N	77	70	20
1314	\N	সমন্বয়-১	Active	\N	0	1289	\N	77	70	20
1315	\N	সমন্বয়-২	Active	\N	0	1289	\N	77	70	20
1316	\N	সমন্বয়-৩	Active	\N	0	1289	\N	77	70	20
1317	\N	সমন্বয়-৪	Active	\N	0	1289	\N	77	70	20
1318	\N	এমআইএস	Active	\N	0	1289	\N	77	70	20
1319	\N	পরিচালক (শিল্প)	Active	\N	0	1272	\N	77	70	20
1320	\N	পরিচালক (শক্তি)	Active	\N	0	1272	\N	77	70	20
1321	\N	পরিচালক-৩	Active	\N	0	1272	\N	77	70	20
1322	\N	উপ পরিচালক (শিল্প-১)	Active	\N	0	1319	\N	77	70	20
1323	\N	উপ পরিচালক (শিল্প-২)	Active	\N	0	1319	\N	77	70	20
1324	\N	সহকারী পরিচালক (শিল্প-১)	Active	\N	0	1319	\N	77	70	20
1325	\N	সহকারী পরিচালক (শিল্প-২)	Active	\N	0	1319	\N	77	70	20
1326	\N	উপ পরিচালক (জ্বালানী)	Active	\N	0	1320	\N	77	70	20
1327	\N	উপ পরিচালক-২	Active	\N	0	1320	\N	77	70	20
1328	\N	উপ পরিচালক-৩	Active	\N	0	1320	\N	77	70	20
1329	\N	সহকারী পরিচালক	Active	\N	0	1320	\N	77	70	20
1330	\N	উপ পরিচালক (পূর্ত-১)	Active	\N	0	1321	\N	77	70	20
1331	\N	উপ পরিচালক (পূর্ত-২)	Active	\N	0	1321	\N	77	70	20
1332	\N	সহকারী পরিচালক (পূর্ত-১)	Active	\N	0	1321	\N	77	70	20
1333	\N	সহকারী পরিচালক (পূর্ত-২)	Active	\N	0	1321	\N	77	70	20
1334	\N	বিধি ও প্রশিক্ষণ	Active	\N	0	1273	\N	77	70	20
1335	\N	পরিচালক (সমন্বয়, সিপিটিইউ)	Active	\N	0	1273	\N	77	70	20
1336	\N	ই-জিপি	Active	\N	0	1273	\N	77	70	20
1337	\N	সিস্টেম এনালিস্ট	Active	\N	0	1273	\N	77	70	20
1338	\N	মতামত পূর্ত ও সেবা	Active	\N	0	1334	\N	77	70	20
1339	\N	মতামত (পণ্য)	Active	\N	0	1334	\N	77	70	20
1340	\N	যোগাযোগ ও সামাজিক সচেতনতা	Active	\N	0	1335	\N	77	70	20
1341	\N	সমন্বয় শাখা	Active	\N	0	1335	\N	77	70	20
1342	\N	পরিবীক্ষণ ও মূল্যায়ন	Active	\N	0	1336	\N	77	70	20
1343	\N	মূল্যায়ন-১	Active	\N	0	1274	\N	77	70	20
1344	\N	মূল্যায়ন-২	Active	\N	0	1274	\N	77	70	20
1345	\N	উপ পরিচালক (মূল্যায়ন-১)	Active	\N	0	1343	\N	77	70	20
1346	\N	সহকারী পরিচালক (মূল্যায়ন-১)	Active	\N	0	1343	\N	77	70	20
1347	\N	সহকারী পরিচালক (মূল্যায়ন-২)	Active	\N	0	1343	\N	77	70	20
1348	\N	উপ পরিচালক (মূল্যায়ন-২)	Active	\N	0	1344	\N	77	70	20
1349	\N	সহকারী পরিচালক (মূল্যায়ন-৩)	Active	\N	0	1344	\N	77	70	20
1350	\N	সহকারী পরিচালক (মূল্যায়ন-৪)	Active	\N	0	1344	\N	77	70	20
1351	\N	প্রশাসন-১	Active	\N	0	1290	\N	77	70	20
1352	\N	প্রশাসন-২	Active	\N	0	1290	\N	77	70	20
1978	\N	মন্ত্রীর দপ্তর	Active	\N	0	0	\N	92	70	20
1979	\N	প্রতিমন্ত্রীর দপ্তর	Active	\N	0	1978	\N	92	70	20
1980	\N	সচিবের দপ্তর	Active	\N	0	1979	\N	92	70	20
1981	\N	প্রশাসন অনুবিভাগ	Active	\N	0	1980	\N	92	70	20
1982	\N	বাজেট অনুবিভাগ	Active	\N	0	1980	\N	92	70	20
1983	\N	এনইসি-একনেক ও সমন্বয় অনুবিভাগ	Active	\N	0	1980	\N	92	70	20
1984	\N	একনেক ও সমন্বয় অনুবিভাগ	Active	\N	0	1980	\N	92	70	20
1985	\N	প্রশাসন উইং	Active	\N	0	1981	\N	92	70	20
1986	\N	প্রশাসন অধিশাখা	Active	\N	0	1985	\N	92	70	20
1987	\N	পিটিসি অধিশাখা	Active	\N	0	1985	\N	92	70	20
1988	\N	আইসিটি সেল	Active	\N	0	1985	\N	92	70	20
1989	\N	পরিকল্পনা অধিশাখা	Active	\N	0	1985	\N	92	70	20
1990	\N	প্রশাসন-১ শাখা	Active	\N	0	1986	\N	92	70	20
1991	\N	প্রশাসন-২ শাখা	Active	\N	0	1986	\N	92	70	20
1992	\N	প্রশাসন-৩ শাখা	Active	\N	0	1986	\N	92	70	20
1993	\N	প্রশাসন-৪ শাখা	Active	\N	0	1986	\N	92	70	20
1994	\N	এসএসআরসি শাখা	Active	\N	0	1986	\N	92	70	20
1995	\N	আইন শাখা	Active	\N	0	1986	\N	92	70	20
1996	\N	প্রটোকল শাখা	Active	\N	0	1986	\N	92	70	20
1997	\N	সম্মেলন শাখা	Active	\N	0	1996	\N	92	70	20
1998	\N	গ্রহণ ও প্রেরণ শাখা	Active	\N	0	1996	\N	92	70	20
1999	\N	বাজেট অধিশাখা	Active	\N	0	1982	\N	92	70	20
2000	\N	বাজেট শাখা	Active	\N	0	1999	\N	92	70	20
2001	\N	সাধারণ-১ শাখা	Active	\N	0	1999	\N	92	70	20
2002	\N	সাধারণ-২ শাখা	Active	\N	0	1999	\N	92	70	20
2003	\N	হিসাব শাখা	Active	\N	0	1999	\N	92	70	20
2004	\N	এনইসি ও সমন্বয় অধিশাখা	Active	\N	0	1983	\N	92	70	20
2005	\N	একনেক অধিশাখা	Active	\N	0	1983	\N	92	70	20
2006	\N	এনইসি শাখা	Active	\N	0	2004	\N	92	70	20
2007	\N	সমন্বয়-১ শাখা	Active	\N	0	2004	\N	92	70	20
2008	\N	সমন্বয়-২ শাখা	Active	\N	0	2004	\N	92	70	20
2009	\N	একনেক-১ শাখা	Active	\N	0	2005	\N	92	70	20
2010	\N	একনেক-২ শাখা	Active	\N	0	2005	\N	92	70	20
2011	\N	পরিকল্পনা শাখা	Active	\N	0	1989	\N	92	70	20
2012	\N	মেইনটেন্যান্স শাখা	Active	\N	0	1988	\N	92	70	20
2013	\N	ই-সার্ভিস শাখা	Active	\N	0	2012	\N	92	70	20
2014	\N	পিটি শাখা	Active	\N	0	1987	\N	92	70	20
2015	\N	সমন্বয় শাখা	Active	\N	0	1987	\N	92	70	20
2016	\N	লাইব্রেরী শাখা	Active	\N	0	1987	\N	92	70	20
2017	\N	সামাজিক বিজ্ঞান গবেষণা পরিষদ	Active	\N	0	1986	\N	92	70	20
2874	\N	মন্ত্রীর দপ্তর	Active	\N	0	0	\N	162	70	20
2875	\N	প্রতিমন্ত্রীর দপ্তর	Active	\N	0	2874	\N	162	70	20
2876	\N	সচিবের দপ্তর	Active	\N	0	2875	\N	162	70	20
2877	\N	প্রশাসন অনুবিভাগ	Active	\N	0	2876	\N	162	70	20
2881	\N	তথ্য ব্যবস্থাপনা অনুবিভাগ	Active	\N	0	2876	\N	162	70	20
2882	\N	উন্নয়ন অনুবিভাগ	Active	\N	0	2876	\N	162	70	20
2883	\N	যুগ্ম সচিব (প্রশাসন)	Active	\N	0	2877	\N	162	70	20
2884	\N	বাজেট, আর্থিক ব্যবস্থাপনা ও সমন্বয় অধিশাখা	Active	\N	0	2877	\N	162	70	20
2885	\N	প্রশাসন-১ শাখা	Active	\N	0	2883	\N	162	70	20
2886	\N	প্রশাসন-২ শাখা	Active	\N	0	2883	\N	162	70	20
2887	\N	প্রশাসন-৩ শাখা	Active	\N	0	2883	\N	162	70	20
2888	\N	বাজেট, আর্থিক ব্যবস্থাপনা শাখা	Active	\N	0	2884	\N	162	70	20
2889	\N	আর্থিক ও সমন্বয় শাখা	Active	\N	0	2884	\N	162	70	20
2890	\N	তথ্য ব্যবস্থাপনা অধিশাখা	Active	\N	0	2881	\N	162	70	20
2891	\N	উন্নয়ন-২ শাখা	Active	\N	0	2895	\N	162	70	20
2892	\N	তথ্য ব্যবস্থাপনা-১ শাখা	Active	\N	0	2890	\N	162	70	20
2893	\N	তথ্য ব্যবস্থাপনা-২ শাখা	Active	\N	0	2890	\N	162	70	20
2894	\N	আইসিটি শাখা-১	Active	\N	0	2890	\N	162	70	20
2895	\N	উন্নয়ন অধিশাখা	Active	\N	0	2882	\N	162	70	20
2896	\N	উন্নয়ন-১ শাখা	Active	\N	0	2895	\N	162	70	20
2897	\N	উন্নয়ন-৩ শাখা	Active	\N	0	2895	\N	162	70	20
2898	\N	উন্নয়ন-৪ শাখা	Active	\N	0	2895	\N	162	70	20
2933	\N	হিসাব শাখা	Active	\N	0	2884	\N	162	70	20
2935	DG Office	মহাপরিচালক-এর দপ্তর	Active	\N	0	0	\N	164	147	20
2936	\N	উপ-মহাপরিচালক-এর দপ্তর	Active	\N	0	2935	\N	164	147	20
2937	\N	সেন্সাস উইং	Active	\N	0	2936	\N	164	147	20
2938	\N	এগ্রিকালচার উইং	Active	\N	0	2936	\N	164	147	20
2939	\N	ইন্ডাস্ট্রি এন্ড লেবার উইং	Active	\N	0	2936	\N	164	147	20
2940	\N	এফএ এন্ড এমআইএস উইং	Active	\N	0	2936	\N	164	147	20
2941	\N	ন্যাশনাল একাউন্টিং উইং	Active	\N	0	2936	\N	164	147	20
2942	\N	কম্পিউটার উইং	Active	\N	0	2936	\N	164	147	20
2943	\N	ডেমোগ্রাফী এন্ড হেলথ উইং	Active	\N	0	2936	\N	164	147	20
2944	\N	স্ট্যাটিসটিক্যাল স্টাফ ট্রেনিং ইনস্টিটিউট	Active	\N	0	2936	\N	164	147	20
2945	\N	আদমশুমারি অধিশাখা	Active	\N	0	2937	\N	164	147	20
2946	\N	প্রশাসন ও সমন্বয়	Active	\N	0	2945	\N	164	147	20
2947	\N	আদমশুমারি	Active	\N	0	2945	\N	164	147	20
2948	\N	ডাটা ম্যানেজমেন্ট শাখা	Active	\N	0	2945	\N	164	147	20
2949	\N	মাঠ জরিপ	Active	\N	0	2945	\N	164	147	20
2950	\N	প্রশাসন ও টেকনিক্যাল	Active	\N	0	2938	\N	164	147	20
2951	\N	প্রধান শস্য ও ফরম শাখা	Active	\N	0	2950	\N	164	147	20
2952	\N	প্রকাশনা ও ডিরাভড শাখা	Active	\N	0	2950	\N	164	147	20
2953	\N	অপ্রধান শস্য শাখা	Active	\N	0	2950	\N	164	147	20
2954	\N	ইন্ডাস্ট্রি এন্ড লেবার	Active	\N	0	2939	\N	164	147	20
2955	\N	শ্রমশক্তি জরিপ শাখা	Active	\N	0	2954	\N	164	147	20
2956	\N	সমন্বিত ব্যবসায় জরিপ	Active	\N	0	2954	\N	164	147	20
2957	\N	উৎপাদন শিল্প জরিপ	Active	\N	0	2954	\N	164	147	20
2958	\N	প্রশাসন শাখা	Active	\N	0	2954	\N	164	147	20
2959	\N	এফএ ও এমআইএস অধিশাখা	Inactive	\N	0	2940	\N	164	147	20
2960	\N	ন্যাশনাল একাউন্টিং	Active	\N	0	2941	\N	164	147	20
2961	\N	জাতীয় আয়	Active	\N	0	2960	\N	164	147	20
2962	\N	বৈদেশিক বাণিজ্য	Active	\N	0	2960	\N	164	147	20
2963	\N	চলতি উৎপাদন	Active	\N	0	2960	\N	164	147	20
2964	\N	মূল্য ও মজুরি	Active	\N	0	2960	\N	164	147	20
2965	\N	জাতীয় ব্যয়	Active	\N	0	2960	\N	164	147	20
2966	\N	যুগ্ম পরিচালকের দপ্তর	Active	\N	0	2942	\N	164	147	20
2967	\N	আইসিটি ল্যাব শাখা	Active	\N	0	3741	\N	164	147	20
2968	\N	প্রশাসন ও হিসাব শাখা	Active	\N	0	2966	\N	164	147	20
2969	\N	প্রোগ্রামিং শাখা	Active	\N	0	3739	\N	164	147	20
2970	\N	ডেমোগ্রাফী এন্ড হেলথ	Active	\N	0	2943	\N	164	147	20
2971	\N	জনতত্ত্ব ও গবেষণা	Active	\N	0	2970	\N	164	147	20
2972	\N	স্বাস্থ্য ও পরিবেশ	Active	\N	0	2970	\N	164	147	20
2973	\N	প্রশাসন ও সামাজিক পরিসংখ্যান	Active	\N	0	2970	\N	164	147	20
2974	\N	প্রশাসন ও প্রশিক্ষণ	Active	\N	0	2944	\N	164	147	20
3111	website section	ওয়েব সাইট ও ওয়েব পোর্টাল শাখা	Active	\N	0	3738	\N	164	147	20
3228	\N	বৈদেশিক বাণিজ্য শাখা	Active	\N	0	3737	\N	164	147	20
3229	\N	জিও কোড শাখা	Active	\N	0	3737	\N	164	147	20
3466	\N	প্রশাসন শাখা	Active	\N	0	2950	\N	164	147	20
3558	\N	চেয়ারম্যানের দপ্তর	Active	\N	0	0	\N	184	147	20
3559	\N	ভাইস চেয়ারম্যানের দপ্তর	Active	\N	0	3558	\N	184	147	20
3560	\N	আর্থ সামাজিক অবকাঠামো বিভাগ (সদস্য)-এর দপ্তর	Active	\N	0	3559	\N	184	147	20
3561	\N	আর্থ সামাজিক অবকাঠামো বিভাগ (প্রধান)-এর দপ্তর	Active	\N	0	3560	\N	184	147	20
3562	\N	শিক্ষা অনুবিভাগ	Active	\N	0	3561	\N	184	147	20
3563	\N	প্যামস্টেক অনুবিভাগ	Active	\N	0	3561	\N	184	147	20
3564	\N	জনসংখ্যা অনুবিভাগ	Active	\N	0	3561	\N	184	147	20
3565	\N	স্কাইসোয়াম অনুবিভাগ	Active	\N	0	3561	\N	184	147	20
3566	\N	স্বাস্থ্য অনুবিভাগ	Active	\N	0	3561	\N	184	147	20
3689	\N	প্রশাসন-৩	Active	\N	0	1290	\N	77	70	20
3737	\N	জিও কোড ও বৈদেশিক বাণিজ্য	Active	\N	0	2942	\N	164	147	20
3738	\N	ওয়েবসাইট, পিডিএস, রেডেটাম ও ওয়েব পোর্টাল	Active	\N	0	2942	\N	164	147	20
3739	\N	ডাটাসেল, এন্ট্রি, প্রোগ্রামিং, সংরক্ষণ ও রিকভারি	Active	\N	0	2942	\N	164	147	20
3741	\N	এনএসডিএস ও আইসিটি শাখা	Active	\N	0	2942	\N	164	147	20
3742	\N	সিস্টেম ডেভেলপমেন্ট	Active	\N	0	2942	\N	164	147	20
3743	\N	সমন্বয় শাখা	Active	\N	0	2966	\N	164	147	20
3744	\N	বৈদেশিক বাণিজ্য	Inactive	\N	0	3737	\N	164	147	20
3746	\N	পিডিএস ও ই-ফাইলিং শাখা	Active	\N	0	3738	2	164	147	20
3747	\N	ডাটা সেল ও এন্ট্রি শাখা	Active	\N	0	3739	\N	164	147	20
3748	\N	ডাটা সংরক্ষণ ও রিকভারি শাখা	Active	\N	0	3739	2	164	147	20
3749	\N	প্রোগ্রামিং শাখা	Inactive	\N	0	3739	2	164	147	20
3750	\N	এনএসডিএস শাখা	Active	\N	0	3741	2	164	147	20
3751	\N	আইসিটি ল্যাব শাখা	Inactive	\N	0	3741	\N	164	147	20
3752	\N	অটোমেশন ও প্রকিরমেন্ট সেল	Active	\N	0	2942	2	164	147	20
3961	\N	কার্যক্রম বিভাগ (সদস্য)-এর দপ্তর	Active	\N	0	3559	\N	184	147	20
3962	\N	কার্যক্রম বিভাগ (প্রধান)-এর দপ্তর	Active	\N	0	3961	\N	184	147	20
3963	\N	কৃষি, শিল্প ও শক্তি এবং সমন্বয় অনুবিভাগ	Active	\N	0	3962	\N	184	147	20
3964	\N	ভৌত অনুবিভাগ	Active	\N	0	3962	\N	184	147	20
3965	\N	আর্থ-সামাজিক অনুবিভাগ	Active	\N	0	3962	\N	184	147	20
3988	\N	সাধারণ অর্থনীতি বিভাগ (সদস্য)-এর দপ্তর	Active	\N	0	3559	\N	184	147	20
3989	\N	সাধারণ অর্থনীতি বিভাগ (প্রধান)-এর দপ্তর	Active	\N	0	3988	\N	184	147	20
3990	\N	মাল্টি সেক্টরাল ইস্যুজ ও সমন্বয় অনুবিভাগ	Active	\N	0	3989	\N	184	147	20
3991	\N	রাজস্ব ও মুদ্রানীতি অনুবিভাগ	Active	\N	0	3989	\N	184	147	20
3992	\N	দারিদ্র্য বিশ্লেষণ ও পরিবীক্ষণ অনুবিভাগ	Active	\N	0	3989	\N	184	147	20
3993	\N	সামষ্টিক ও প্রেক্ষিত পরিকল্পনা অনুবিভাগ	Active	\N	0	3989	\N	184	147	20
3994	\N	আন্তর্জাতিক অর্থনীতি অনুবিভাগ	Active	\N	0	3989	\N	184	147	20
4029	\N	ভৌত অবকাঠামো বিভাগ (সদস্য)-এর দপ্তর	Active	\N	0	3559	\N	184	147	20
4030	\N	ভৌত অবকাঠামো বিভাগ (প্রধান)-এর দপ্তর	Active	\N	0	4029	\N	184	147	20
4031	\N	যোগাযোগ অনুবিভাগ	Active	\N	0	4030	\N	184	147	20
4032	\N	পরিবহণ সমন্বয় অনুবিভাগ	Active	\N	0	4030	\N	184	147	20
4033	\N	রেল পরিবহন অনুবিভাগ	Active	\N	0	4030	\N	184	147	20
4034	\N	ভৌত পরিকল্পনা, পানি সরবরাহ ও গৃহায়ণ অনুবিভাগ-১	Active	\N	0	4030	\N	184	147	20
4035	\N	ভৌত পরিকল্পনা, পানি সরবরাহ ও গৃহায়ণ অনুবিভাগ-২	Active	\N	0	4030	\N	184	147	20
4036	\N	সড়ক পরিবহণ অনুবিভাগ	Active	\N	0	4030	\N	184	147	20
4043	\N	প্রকাশনা	Active	\N	0	2960	\N	164	147	20
4044	\N	ইসিডিএস সেল	Active	\N	0	2960	\N	164	147	20
4045	\N	প্রশাসন	Active	\N	0	2960	\N	164	147	20
4046	\N	সহকারী পরিসংখ্যান কর্মকর্তা	Inactive	\N	0	4043	\N	164	147	20
4047	\N	পরিসংখ্যান অনুসন্ধানকারী	Inactive	\N	0	4043	\N	164	147	20
4051	\N	অপারেশন, মেইনটেন্যান্স ও নেটওয়ার্কিং শাখা	Active	\N	0	2942	\N	164	147	20
4052	\N	শিল্প ও শক্তি বিভাগ (সদস্য)-এর দপ্তর	Active	\N	0	3559	\N	184	147	20
4053	\N	শিল্প ও শক্তি বিভাগ (প্রধান)-এর দপ্তর	Active	\N	0	4052	\N	184	147	20
4054	\N	শিল্প ও সমন্বয় অনুবিভাগ	Active	\N	0	4053	\N	184	147	20
4055	\N	বিদ্যুৎ অনুবিভাগ	Active	\N	0	4053	\N	184	147	20
4060	\N	এফএ এন্ড এমআইএস	Active	\N	0	2940	\N	164	147	20
4061	\N	প্রশাসন শাখা	Active	\N	0	4060	\N	164	147	20
4062	\N	বিনিক শাখা	Active	\N	0	4060	\N	164	147	20
4063	\N	সমন্বয় শাখা	Active	\N	0	4060	\N	164	147	20
4064	\N	প্রকাশনা শাখা	Active	\N	0	4060	\N	164	147	20
4069	\N	তৈল, গ্যাস ও প্রাকৃতিক সম্পদ অনুবিভাগ	Active	\N	0	4053	\N	184	147	20
4070	\N	পাট, বস্ত্র ও বেপজা অনুবিভাগ	Active	\N	0	4053	\N	184	147	20
4071	\N	ইঞ্জিনিয়ারিং এন্ড ইলেক্ট্রনিক্স অনুবিভাগ	Active	\N	0	4053	\N	184	147	20
4077	\N	কৃষি পানি সম্পদ ও পল্লী প্রতিষ্ঠান বিভাগ (সদস্য)-এর দপ্তর	Active	\N	0	3559	\N	184	147	20
4078	\N	কৃষি পানি সম্পদ ও পল্লী প্রতিষ্ঠান বিভাগ (প্রধান)-এর দপ্তর	Active	\N	0	4077	\N	184	147	20
4079	\N	কৃষি পানি সম্পদ ও পল্লী প্রতিষ্ঠান বিভাগ (প্রধান)-এর দপ্তর	Inactive	\N	0	4077	\N	184	147	20
4080	\N	কৃষি পানি সম্পদ ও পল্লী প্রতিষ্ঠান বিভাগ (প্রধান)-এর দপ্তর	Inactive	\N	0	4077	\N	184	147	20
4081	Rural Institution Wing	পল্লী প্রতিষ্ঠান অনুবিভাগ	Active	\N	0	4078	\N	184	147	20
4082	\N	বন, মৎস্য ও প্রাণিসম্পদ অনুবিভাগ	Active	\N	0	4078	\N	184	147	20
4083	\N	ফসল অনুবিভাগ	Active	\N	0	4078	\N	184	147	20
4084	\N	খাদ্য ও সার মনিটরিং অনুবিভাগ	Active	\N	0	4078	\N	184	147	20
4085	\N	সেচ অনুবিভাগ	Active	\N	0	4078	\N	184	147	20
4934	\N	জিআইএস শাখা	Active	\N	0	2942	\N	164	147	20
5070	\N	পরিচালক (রেলপথ)	Active	\N	0	1265	\N	77	70	20
5071	\N	পরিচালক (সমন্বয় যোগাযোগ ও স্হানীয় সরকার )	Active	\N	0	1265	\N	77	70	20
5854	\N	ফিল্ড শাখা	Active	\N	0	4060	\N	164	147	20
5855	\N	হিসাব শাখা	Active	\N	0	4060	\N	164	147	20
5856	\N	অডিট শাখা	Active	\N	0	4060	\N	164	147	20
5857	\N	আইন সেল	Active	\N	0	4060	\N	164	147	20
5957	\N	মহাপরিচালক-এর দপ্তর	Active	\N	0	0	\N	247	147	20
5958	\N	অতিরিক্ত মহাপরিচালক-এর দপ্তর	Active	\N	0	5957	\N	247	147	20
5960	\N	প্রশাসন ও অর্থ অনুবিভাগ	Active	\N	0	5958	\N	247	147	20
5962	\N	প্রশিক্ষণ অনুবিভাগ	Active	\N	0	5958	\N	247	147	20
5963	\N	গবেষণা ও প্রকাশনা অনুবিভাগ	Active	\N	0	5958	\N	247	147	20
5965	\N	প্রশাসন ও অর্থ অধিশাখা	Active	\N	0	5960	\N	247	147	20
5967	\N	আইসিটি অধিশাখা	Active	\N	0	5960	\N	247	147	20
5968	\N	প্রশাসন শাখা	Active	\N	0	5965	\N	247	147	20
5969	\N	মেডিকেল শাখা	Active	\N	0	5965	\N	247	147	20
5970	\N	অর্থ ও হিসাব শাখা	Active	\N	0	5965	\N	247	147	20
5971	\N	ক্যাফেটেরিয়া শাখা	Active	\N	0	5965	\N	247	147	20
5972	\N	ডরমিটরী শাখা	Active	\N	0	5965	\N	247	147	20
5973	\N	সাধারণ সেবা শাখা	Active	\N	0	5965	\N	247	147	20
5974	\N	আইসিটি শাখা	Active	\N	0	5967	\N	247	147	20
5975	\N	অডিও-ভিজুয়্যাল শাখা	Active	\N	0	5967	\N	247	147	20
5976	\N	প্রশিক্ষণ-১ অধিশাখা	Active	\N	0	5962	\N	247	147	20
5977	\N	প্রশিক্ষণ-২ অধিশাখা	Active	\N	0	5962	\N	247	147	20
5978	\N	প্রশিক্ষণ-৩ অধিশাখা	Active	\N	0	5962	\N	247	147	20
5979	\N	প্রশিক্ষণ-১ শাখা	Active	\N	0	5976	\N	247	147	20
5980	\N	প্রশিক্ষণ সেল	Active	\N	0	5976	\N	247	147	20
5981	\N	প্রশিক্ষণ-২ শাখা	Active	\N	0	5977	\N	247	147	20
5982	\N	প্রশিক্ষণ-৩ শাখা	Active	\N	0	5978	\N	247	147	20
5983	\N	গবেষণা ও প্রকাশনা অধিশাখা	Active	\N	0	5963	\N	247	147	20
5984	\N	গবেষণা ও প্রকাশনা শাখা	Active	\N	0	5983	\N	247	147	20
5985	\N	মূল্যায়ন শাখা	Active	\N	0	5983	\N	247	147	20
5986	\N	গ্রন্থাগার	Active	\N	0	5983	\N	247	147	20
6118	\N	শুমারি ও জরিপ শাখা	Active	\N	0	2945	\N	164	147	20
6463	\N	উপ পরিচালক (জ্বালানী-১)	Active	\N	0	1326	\N	77	70	20
6593	\N	ফ্রন্ট ডেস্ক শাখা	Active	\N	0	2883	\N	162	70	20
6626	\N	প্লানিং এন্ড ডেভেলপমেন্ট সেল	Active	\N	0	2936	\N	164	147	20
9694	\N	সহকারী পরিচালক (শক্তি -২)	Active	\N	0	1320	\N	77	70	20
9695	\N	সহকারী পরিচালক (শক্তি -৩)	Active	\N	0	1320	\N	77	70	20
9696	\N	পরিচালক (স্থানীয় সরকার-২)	Active	\N	0	1265	\N	77	70	20
9697	\N	উপপরিচাল (রেল)	Active	\N	0	5070	\N	77	70	20
9698	\N	সহকারী পরিচালক (রেল)	Active	\N	0	5070	\N	77	70	20
9699	\N	সহকারী পরিচালক (পানি-৩)	Active	\N	0	1276	\N	77	70	20
9700	\N	কাউন্সিল শাখা	Active	\N	0	1289	\N	77	70	20
10886	\N	মহাপরিচালক (স্বাস্হ্য ও গৃহায়ন)	Active	\N	0	1262	\N	77	70	20
10887	\N	মহাপরিচালক (স্হানীয় সরকার ও পল্লী উন্নয়ন)	Active	\N	0	1262	\N	77	70	20
10888	\N	মহাপরিচালক (নিবিড় পরিবীক্ষণ ও গবেষণা)	Active	\N	0	1262	\N	77	70	20
10889	\N	উপ পরিচালক (স্বাস্থ্য-২)	Active	\N	0	1294	\N	77	70	20
11479	\N	পরিচালক (স্বাস্হ্য ও গৃহায়ন)-০১	Inactive	\N	0	10886	\N	77	70	20
11480	\N	পরিচালক (স্বাস্হ্য ও গৃহায়ন)-০২	Inactive	\N	0	10886	\N	77	70	20
11601	\N	এসডিজি সেল	Active	\N	0	2883	\N	162	70	20
11611	\N	পরিচালক (নিবিড় পরিবীক্ষণ ও গবেষণা)	Active	\N	0	10888	\N	77	70	20
11612	\N	উপপরিচালক (নিবিড় পরিবীক্ষণ ও গবেষণা)-০১	Active	\N	0	11611	\N	77	70	20
11613	\N	উপপরিচালক (নিবিড় পরিবীক্ষণ ও গবেষণা)-০২	Active	\N	0	11611	\N	77	70	20
11614	\N	সহকারী পরিচালক (নিবিড় পরিবীক্ষণ ও গবেষণা)-০১	Active	\N	0	11611	\N	77	70	20
11615	\N	সহকারী পরিচালক (নিবিড় পরিবীক্ষণ ও গবেষণা)-০২	Active	\N	0	11611	\N	77	70	20
11616	\N	সহকারী পরিচালক (নিবিড় পরিবীক্ষণ ও গবেষণা)-০৩	Active	\N	0	11611	\N	77	70	20
11617	\N	পরিচালক (স্বাস্হ্য ও গৃহায়ন)-০১	Active	\N	0	10886	\N	77	70	20
11618	\N	পরিচালক (স্বাস্হ্য ও গৃহায়ন)-০২	Active	\N	0	10886	\N	77	70	20
11619	\N	পরিচালক (স্বাস্হ্য ও গৃহায়ন)-০৩	Active	\N	0	10886	\N	77	70	20
11620	\N	উপপরিচালক (স্বাস্হ্য ও গৃহায়ন)-০১	Active	\N	0	10886	\N	77	70	20
11621	\N	উপপরিচালক (স্বাস্হ্য ও গৃহায়ন)-০২	Active	\N	0	10886	\N	77	70	20
11622	\N	সহকারী পরিচালক (স্বাস্হ্য ও গৃহায়ন)-০৪	Active	\N	0	10886	\N	77	70	20
11623	\N	সহকারী পরিচালক (স্বাস্হ্য ও গৃহায়ন)-০১	Active	\N	0	10886	\N	77	70	20
11624	\N	সহকারী পরিচালক (স্বাস্হ্য ও গৃহায়ন)-০২	Active	\N	0	11611	\N	77	70	20
11625	\N	সহকারী পরিচালক (স্বাস্হ্য ও গৃহায়ন-৩)	Active	\N	0	11611	\N	77	70	20
11626	\N	সহকারী পরিচালক (স্বাস্হ্য ও গৃহায়ন)-০৪	Active	\N	0	10886	\N	77	70	20
11627	\N	পরিচালক (স্হানীয় সরকার)-০৪	Active	\N	0	10887	\N	77	70	20
11628	\N	পরিচালক (স্হানীয় সরকার ও পল্লী উন্নয়ন)-০২	Active	\N	0	10887	\N	77	70	20
11629	\N	উপপরিচালক (স্হানীয় সরকার ও পল্লী উন্নয়ন)-০১	Active	\N	0	10887	\N	77	70	20
11630	\N	উপপরিচালক (স্হানীয় সরকার ও পল্লী উন্নয়ন)-০২	Active	\N	0	10887	\N	77	70	20
11631	\N	সহকারী পরিচালক (স্হানীয় সরকার ও পল্লী উন্নয়ন)-০১	Active	\N	0	10887	\N	77	70	20
11632	Deputy Director Local Government-03	উপ পরিচালক স্হানীয় সরকার -০৩	Active	\N	0	10887	\N	77	70	20
11633	\N	সহকারী পরিচালক (স্হানীয় সরকার ও পল্লী উন্নয়ন)-০৩	Active	\N	0	10887	\N	77	70	20
11634	\N	সহকারী পরিচালক (স্হানীয় সরকার ও পল্লী উন্নয়ন)-০৩	Active	\N	0	10887	\N	77	70	20
11730	\N	সহকারী পরিচালক (স্বাস্হ্য ও গৃহায়ন)-০২	Active	\N	0	10886	\N	77	70	20
11731	\N	সহকারী পরিচালক (স্বাস্হ্য ও গৃহায়ন)-০৩	Active	\N	0	10886	\N	77	70	20
13543	\N	মূল্যায়ন কর্মকর্তা (স্বাস্হ্য ও গৃহায়ণ)	Active	\N	0	10886	\N	77	70	20
13544	\N	মূল্যায়ন কর্মকর্তা (নিবীড় পরিবীক্ষণ ও গবেষণা)	Active	\N	0	10886	\N	77	70	20
13545	\N	মূল্যায়ন কর্মকর্তা (নিবীড় পরিবীক্ষণ ও গবেষণা)	Active	\N	0	11611	\N	77	70	20
13546	\N	মূল্যায়ন কর্মকর্তা -১ (পরিবহন)	Active	\N	0	1306	\N	77	70	20
13547	\N	মূল্যায়ন কর্মকর্তা -২ (পরিবহন)	Active	\N	0	1306	\N	77	70	20
13548	\N	পরিচালক (সামাজিক)	Active	\N	0	1264	\N	77	70	20
13549	\N	উপ পরিচালক (সামাজিক-১)	Active	\N	0	13548	\N	77	70	20
13550	\N	উপ পরিচালক (সামাজিক-২)	Active	\N	0	13548	\N	77	70	20
13551	\N	উপ পরিচালক (শিক্ষা-২)	Active	\N	0	1295	\N	77	70	20
13552	\N	উপ পরিচালক (শিক্ষা-১)	Active	\N	0	1295	\N	77	70	20
13616	\N	এসডিজি সেল	Active	\N	0	2936	\N	164	147	20
13627	\N	বিবিধ শাখা	Active	\N	0	2954	\N	164	147	20
13847	\N	যুগ্মসচিব (সমন্বয়)	Active	\N	0	1262	\N	77	70	20
13848	\N	উপসচিব (সমন্বয়)	Active	\N	0	13847	\N	77	70	20
13849	\N	সিনিয়র সহকারী সচিব (সমন্বয়)	Active	\N	0	13848	\N	77	70	20
14362	\N	উপ পরিচালক (পরিবহন-০১)	Active	\N	0	5070	\N	77	70	20
14428	\N	সংসদ বিষয়ক কাউন্সিল অফিস	Active	\N	0	2877	\N	162	70	20
14431	\N	হিসাব শাখা	Active	\N	0	2960	\N	164	147	20
14971	\N	পরিচালক (পরিকল্পনা)	Active	\N	0	1273	\N	77	70	20
15038	\N	জেলা পরিসংখ্যান কর্মকর্তার দপ্তর	Active	\N	0	0	\N	470	251	20
15039	\N	মূল্যায়ন কর্মকর্তা (কৃষি ও পানি সম্পদ)	Active	\N	0	1263	\N	77	70	20
15040	\N	মূল্যায়ন কর্মকর্তা (শিক্ষা ও সামাজিক)	Active	\N	0	1264	\N	77	70	20
15041	\N	মূল্যায়ন কর্মকর্তা (শিল্প ও শক্তি)	Active	\N	0	1272	\N	77	70	20
15042	\N	মূল্যায়ন কর্মকর্তা (পরিবহন)	Active	\N	0	1265	\N	77	70	20
15458	\N	কৃষি (শস্য, মৎস্য ও প্রাণি সম্পদ) শুমারি-২০১৮ প্রকল্প	Active	\N	0	2936	\N	164	147	20
15625	\N	সেক্টর-৪	Active	\N	0	1263	\N	77	70	20
17644	\N	যুগ্ম-পরিচালক এর দপ্তর	Active	\N	0	0	\N	477	252	20
17951	\N	প্রোগামের দপ্তর (আইসিটি সেল)	Active	\N	0	2881	\N	162	70	20
18207	\N	পরিচালক-৩ (কৃষি ও পানি সম্পদ)	Active	\N	0	1263	\N	77	70	20
18355	\N	উপপরিচালক (নিবিড় পরিবীক্ষণ ও গবেষণা)-০৩	Active	\N	0	11611	\N	77	70	20
18356	\N	উপপরিচালক (নিবিড় পরিবীক্ষণ ও গবেষণা)-০৪	Active	\N	0	11611	\N	77	70	20
18640	\N	সিডিএমআরআই-ইসিবিএসএস(পিএইচ -২) প্রজেক্ট	Active	\N	0	1262	\N	77	70	20
19012	\N	এনার্জি মডেলিং এন্ড ইকনমিক্স উইং	Active	\N	0	4052	\N	184	147	20
20023	\N	অফিস সহকারী কাম কম্পিঊটার অপারেটর	Inactive	\N	0	15038	\N	470	251	20
20563	\N	আইসিটি সেল	Active	\N	0	2881	\N	162	70	20
20597	\N	পরিচালক -১ (নিবিড় পরিবীক্ষণ ও গবেষণা)	Inactive	\N	0	11611	\N	77	70	20
20601	\N	পরিচালক -১ (নিবিড় পরিবীক্ষণ ও গবেষণা)	Active	\N	0	10888	\N	77	70	20
21344	\N	আইন শাখা	Active	\N	0	2883	\N	162	70	20
21642	\N	কর্মসম্পাদন ব্যবস্থাপনা শাখা	Active	\N	0	1982	\N	92	70	20
\.


--
-- Data for Name: office_origin_unit_post; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.office_origin_unit_post (oid, name_en, name_bn, status, level_order, sort_order, parent_oid, office_origin_unit_oid) FROM stdin;
1	Secretary	সচিব	Active	1	1	0	1260
2	PS to Secretary	সচিবের ব্যাক্তিগত সহকারি	Active	2	1	1	1261
3	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	3	1	2	1262
4	Office Asst-cum-Computer Operator	অফিস-সহকারী-কাম-কম্পিউটার অপারেটর 	Active	3	2	2	1263
5	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	3	2	1264
6	Additional Secretary	অতিরিক্ত সচিব	Active	1	1	1	1265
7	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	6	1266
8	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	7	1267
9	Joint Secretary	যুুগ্ম সচিব	Active	1	1	1	1268
10	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	9	1269
11	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	10	1270
12	Joint Secretary	যুুগ্ম সচিব	Active	1	1	1	1271
13	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	12	1272
14	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	13	1273
15	Joint Secretary	যুুগ্ম সচিব	Active	1	1	1	1274
16	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	15	1275
17	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	16	1276
18	Senior System Analyst	সিনিয়র সিস্টেম এনালিস্ট	Active	1	1	6	1277
19	Computer Operator 	কর্মকর্তা অপারেটর	Active	2	1	18	1278
20	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	18	1279
21	Deputy Secretary 	ভারপ্রাপ্ত সচিব	Active	1	1	6	1280
22	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	21	1281
23	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	21	1282
24	Deputy Secretary 	ভারপ্রাপ্ত সচিব	Active	1	1	6	1283
25	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	24	1284
26	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	24	1285
27	Deputy Secretary 	ভারপ্রাপ্ত সচিব	Active	1	1	9	1286
28	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	27	1287
29	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	27	1289
30	Deputy Secretary 	ভারপ্রাপ্ত সচিব	Active	1	1	9	1290
31	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	30	1291
32	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	30	1292
33	Deputy Secretary 	ভারপ্রাপ্ত সচিব	Active	1	1	12	1293
34	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	33	1294
35	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	33	1295
36	Deputy Chief 	ভারপ্রাপ্ত প্রধান	Active	1	1	15	1296
37	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	36	1297
38	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	36	1298
39	Deputy Secretary 	ভারপ্রাপ্ত সচিব	Active	1	1	15	1299
40	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	39	1300
41	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	39	1301
42	Senior Assistant Secretary /Assistant Secretary	সিনিয়র সহকারি সচিব / সহকারি সচিব	Active	1	1	27	1302
43	Administrative Officer (A.O)	প্রশাসনিক কর্মকর্তা	Active	2	1	42	1303
44	Office Asst-cum-Computer Operator	অফিস-সহকারী-কাম-কম্পিউটার অপারেটর 	Active	3	1	42	1304
45	Senior Assistant Chief	সিনিয়র সহকারি প্রধান / সহকারি প্রধান	Active	1	1	27	1305
46	Administrative Officer (A.O)	প্রশাসনিক কর্মকর্তা	Active	2	1	45	1306
47	Office Asst-cum-Computer Operator	অফিস-সহকারী-কাম-কম্পিউটার অপারেটর 	Active	3	1	45	1307
48	Accounts Officer	হিসাবরক্ষণ কর্মকর্তা	Active	1	1	27	1308
49	Assistant Accounts Officer (Former Accountant)	সহকারী হিসাবরক্ষণ কর্মকর্তা	Active	2	1	48	1309
\.


--
-- Data for Name: office_unit; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.office_unit (oid, name_en, name_bn, status, sort_order, parent_oid, office_unit_category_oid, parent_office_origin_unit_oid, office_origin_unit_oid, office_oid, office_layer_oid, ministry_oid) FROM stdin;
10104	\N	মহাপরিচালক-এর দপ্তর	Active	0	\N	1	\N	5957	2935	147	20
10105	\N	অতিরিক্ত মহাপরিচালক-এর দপ্তর	Active	0	10104	1	5957	5958	2935	147	20
10106	\N	প্রশাসন ও অর্থ অনুবিভাগ	Active	0	10105	3	5958	5960	2935	147	20
10107	\N	প্রশাসন ও অর্থ অধিশাখা	Active	0	10106	1	5960	5965	2935	147	20
10108	\N	অর্থ ও হিসাব শাখা	Active	0	10107	2	5965	5970	2935	147	20
10109	\N	ক্যাফেটেরিয়া শাখা	Active	0	10107	2	5965	5971	2935	147	20
10110	\N	ডরমিটরী শাখা	Active	0	10107	2	5965	5972	2935	147	20
10111	\N	সাধারণ সেবা শাখা	Active	0	10107	1	5965	5973	2935	147	20
10112	\N	প্রশাসন শাখা	Active	0	10107	1	5965	5968	2935	147	20
10113	\N	মেডিকেল শাখা	Active	0	10107	1	5965	5969	2935	147	20
10114	\N	আইসিটি অধিশাখা	Active	0	10106	4	5960	5967	2935	147	20
10115	\N	অডিও-ভিজুয়্যাল শাখা	Active	0	10114	1	5967	5975	2935	147	20
10116	\N	আইসিটি শাখা	Active	0	10114	1	5967	5974	2935	147	20
10117	\N	প্রশিক্ষণ অনুবিভাগ	Active	0	10105	3	5958	5962	2935	147	20
10118	\N	প্রশিক্ষণ-১ অধিশাখা	Active	0	10117	1	5962	5976	2935	147	20
10119	\N	প্রশিক্ষণ-১ শাখা	Active	0	10118	1	5976	5979	2935	147	20
10120	\N	প্রশিক্ষণ সেল	Active	0	10118	2	5976	5980	2935	147	20
10121	\N	প্রশিক্ষণ-৩ অধিশাখা	Active	0	10117	1	5962	5978	2935	147	20
10122	\N	প্রশিক্ষণ-৩ শাখা	Active	0	10121	1	5978	5982	2935	147	20
10123	\N	প্রশিক্ষণ-২ অধিশাখা	Active	0	10117	1	5962	5977	2935	147	20
10124	\N	প্রশিক্ষণ-২ শাখা	Active	0	10123	1	5977	5981	2935	147	20
10125	\N	গবেষণা ও প্রকাশনা অনুবিভাগ	Active	0	10105	3	5958	5963	2935	147	20
10126	\N	গবেষণা ও প্রকাশনা অধিশাখা	Active	0	10125	1	5963	5983	2935	147	20
10127	\N	গবেষণা ও প্রকাশনা শাখা	Active	0	10126	1	5983	5984	2935	147	20
10128	\N	মূল্যায়ন শাখা	Active	0	10126	1	5983	5985	2935	147	20
10129	\N	গ্রন্থাগার	Active	0	10126	2	5983	5986	2935	147	20
18333	\N	সড়ক পরিবহণ অনুবিভাগ	Active	0	18332	1	4030	4036	2102	147	20
18358	\N	সাধারণ অর্থনীতি বিভাগ (সদস্য)-এর দপ্তর	Active	0	18330	1	3559	3988	2102	147	20
18359	\N	সাধারণ অর্থনীতি বিভাগ (প্রধান)-এর দপ্তর	Active	0	18358	1	3988	3989	2102	147	20
18360	\N	মাল্টি সেক্টরাল ইস্যুজ ও সমন্বয় অনুবিভাগ	Active	0	18359	1	3989	3990	2102	147	20
18361	\N	আন্তর্জাতিক অর্থনীতি অনুবিভাগ	Active	0	18359	1	3989	3994	2102	147	20
18362	\N	সামষ্টিক ও প্রেক্ষিত পরিকল্পনা অনুবিভাগ	Active	0	18359	1	3989	3993	2102	147	20
18363	\N	রাজস্ব ও মুদ্রানীতি অনুবিভাগ	Active	0	18359	1	3989	3991	2102	147	20
18364	\N	দারিদ্র্য বিশ্লেষণ ও পরিবীক্ষণ অনুবিভাগ	Active	0	18359	1	3989	3992	2102	147	20
\.


--
-- Data for Name: office_unit_category; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.office_unit_category (oid, name_en, name_bn, sort_order) FROM stdin;
1	Office	দপ্তর	1
2	Section	শাখা	2
3	Wing	অনুবিভাগ	3
4	Branch	অধিশাখা	4
\.


--
-- Data for Name: office_unit_post; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.office_unit_post (oid, name_en, name_bn, status, level_order, sort_order, parent_oid, parent_office_unit_oid, office_origin_unit_post_oid, office_oid, office_unit_oid) FROM stdin;
1	Secretary	সচিব	Active	1	1	\N	10104	1	2935	10104
2	PS to Secretary	সচিবের ব্যাক্তিগত সহকারি	Active	2	1	1	10105	2	2935	10105
3	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	3	1	2	10106	3	2935	10106
4	Office Asst-cum-Computer Operator	অফিস-সহকারী-কাম-কম্পিউটার অপারেটর 	Active	3	2	2	10107	4	2935	10107
5	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	3	2	10107	5	2935	10108
6	Additional Secretary	অতিরিক্ত সচিব	Active	1	1	1	10107	6	2935	10109
7	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	6	10107	7	2935	10110
8	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	7	10107	8	2935	10111
9	Joint Secretary	যুুগ্ম সচিব	Active	1	1	1	10107	9	2935	10112
10	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	9	10106	10	2935	10113
11	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	10	10114	11	2935	10114
12	Joint Secretary	যুুগ্ম সচিব	Active	1	1	1	10114	12	2935	10115
13	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	12	10105	13	2935	10116
14	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	13	10117	14	2935	10117
15	Joint Secretary	যুুগ্ম সচিব	Active	1	1	1	10118	15	2935	10118
16	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	15	10118	16	2935	10119
17	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	16	10104	17	2935	10120
18	Senior System Analyst	সিনিয়র সিস্টেম এনালিস্ট	Active	1	1	6	10105	18	2935	10121
19	Computer Operator 	কর্মকর্তা অপারেটর	Active	2	1	18	10106	19	2935	10122
20	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	18	10107	20	2935	10123
21	Deputy Secretary 	ভারপ্রাপ্ত সচিব	Active	1	1	6	10107	21	2935	10124
22	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	21	10107	22	2935	10125
23	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	21	10107	23	2935	10126
24	Deputy Secretary 	ভারপ্রাপ্ত সচিব	Active	1	1	6	10107	24	2935	10127
25	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	24	10107	25	2935	10128
26	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	24	10106	26	2935	10129
27	Deputy Secretary 	ভারপ্রাপ্ত সচিব	Active	1	1	9	10114	27	2102	18333
28	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	27	10114	28	2102	18358
29	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	27	10105	29	2102	18359
30	Deputy Secretary 	ভারপ্রাপ্ত সচিব	Active	1	1	9	10117	30	2102	18360
31	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	30	10118	31	2102	18361
32	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	30	10118	32	2102	18362
33	Deputy Secretary 	ভারপ্রাপ্ত সচিব	Active	1	1	12	10104	33	2102	18363
34	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	33	10105	34	2102	18364
35	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	33	10106	35	143	10124
36	Deputy Chief 	ভারপ্রাপ্ত প্রধান	Active	1	1	15	10107	36	143	10125
37	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	36	10107	37	143	10126
38	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	36	10107	38	143	10127
39	Deputy Secretary 	ভারপ্রাপ্ত সচিব	Active	1	1	15	10107	39	143	10128
40	Personal Officer (P.O)	ব্যক্তিগত কর্মকর্তা	Active	2	1	39	10107	40	143	10129
41	Office Support Staff	অফিস সহকারী স্টাফ	Active	3	1	39	10107	41	143	18333
42	Senior Assistant Secretary /Assistant Secretary	সিনিয়র সহকারি সচিব / সহকারি সচিব	Active	1	1	27	10106	42	143	18358
43	Administrative Officer (A.O)	প্রশাসনিক কর্মকর্তা	Active	2	1	42	10114	43	143	18359
44	Office Asst-cum-Computer Operator	অফিস-সহকারী-কাম-কম্পিউটার অপারেটর 	Active	3	1	42	10114	44	143	10128
45	Senior Assistant Chief	সিনিয়র সহকারি প্রধান / সহকারি প্রধান	Active	1	1	27	10105	45	143	10129
46	Administrative Officer (A.O)	প্রশাসনিক কর্মকর্তা	Active	2	1	45	10117	46	143	18333
47	Office Asst-cum-Computer Operator	অফিস-সহকারী-কাম-কম্পিউটার অপারেটর 	Active	3	1	45	10118	47	143	18358
48	Accounts Officer	হিসাবরক্ষণ কর্মকর্তা	Active	1	1	27	10118	48	143	18359
49	Assistant Accounts Officer (Former Accountant)	সহকারী হিসাবরক্ষণ কর্মকর্তা	Active	2	1	48	10118	49	143	18359
\.


--
-- Data for Name: party_type; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.party_type (oid, party_type_code, party_type_name) FROM stdin;
\.


--
-- Data for Name: payroll_process; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.payroll_process (oid, employee_oid, house_rent, total_allowance, total_deduction, final_amount) FROM stdin;
\.


--
-- Data for Name: prepare_salary_bill; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.prepare_salary_bill (oid, grade, employee_type, employee_oid, attachment) FROM stdin;
\.


--
-- Data for Name: store; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.store (oid, store_code, store_name, store_location, organization_oid, store_keeper_oid, store_contact_number, description, status, startingdate) FROM stdin;
\.


--
-- Data for Name: vendor; Type: TABLE DATA; Schema: cmn; Owner: grp
--

COPY cmn.vendor (oid, code, name_en, name_bn, contact_address, contact_no, email, billing_address, description, status, created_by, created_on, updated_by, updated_on) FROM stdin;
1	V-1	Abul Kasam	আবুল কাসেম	Shaymoli	16354823818	Kasam@gmail.com	Shaymoli	\N	Available	System	2019-07-23 20:57:27.736206	\N	\N
2	V-2	Kamal Uddin	কামাল উদ্দিন	Karwanbazar	1635482381	kamal@gmail.com	Karwanbazar	\N	Not available	System	2019-07-23 20:57:27.736206	\N	\N
3	V-3	Monjur Mahmud	মঞ্জুর মাহমুদ	Mohammadpur	16578962355	monjur@gmail.com	Mohammadpur	\N	Available	System	2019-07-23 20:57:27.736206	\N	\N
\.


--
-- Data for Name: acr_main_report; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.acr_main_report (oid, employee_oid, moral, intellectual, materialistic, recommend_for_training, qualification_for_promotion, other_recommendation, esign, designation) FROM stdin;
\.


--
-- Data for Name: acr_medical_report; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.acr_medical_report (oid, employee_oid, height, weight, eye_power, blood_group, blood_pressure, medical_class_division, attachment, date, esign) FROM stdin;
\.


--
-- Data for Name: acr_summary; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.acr_summary (oid, employee_oid, date_of_submission, subject_of_evaluation, obtained_marks, total_obtained_marks, esign) FROM stdin;
\.


--
-- Data for Name: acr_under_controlling_auth; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.acr_under_controlling_auth (oid, employee_oid, submission_date_ca, subject_of_evaluation, marks_from_ca, total_marks_ca, acr_form_upload, e_sign_of_authority) FROM stdin;
\.


--
-- Data for Name: acr_under_controlling_ministry; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.acr_under_controlling_ministry (oid, employee_oid, submission_date, submission_date_ca, total_marks_ca, submission_date_csa, total_marks_csa, date_of_filled_up_form, e_sign_of_auth) FROM stdin;
\.


--
-- Data for Name: acr_under_counter_signing_auth; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.acr_under_counter_signing_auth (oid, employee_oid, submission_date_ca, total_marks_ca, submission_date_csa, total_marks_csa, acr_form_upload, comments, e_sign_of_auth) FROM stdin;
\.


--
-- Data for Name: admit_card; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.admit_card (oid, exam_oid, applicant_oid, roll_no, exam_date, exam_venue, exam_type) FROM stdin;
\.


--
-- Data for Name: application_form_for_recruitment; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.application_form_for_recruitment (oid, circular_oid, applicant_code, application_code, application_date, name, nid_no, date_of_birth, age, father_name, mother_name, address, contact_number, gender, occupation, religion, nationality, board, educational_qualification, experience, quota) FROM stdin;
\.


--
-- Data for Name: apply_for_leave; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.apply_for_leave (oid, application_code, leave_oid, applicant_oid, total_no_of_days_leave, total_no_of_using_leave, leave_remaining, leave_from, leave_to, duration_of_leave, attachment_url, substitute_oid) FROM stdin;
\.


--
-- Data for Name: apply_for_prl; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.apply_for_prl (oid, employee_oid, duration_from, duration_to, prl_duration, file_title, file_code, file_type, attachment, decision_status, application_code, decision_by) FROM stdin;
\.


--
-- Data for Name: at_leave; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.at_leave (oid, employee_oid, leave_status_oid) FROM stdin;
\.


--
-- Data for Name: at_meeting_or_events; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.at_meeting_or_events (oid, employee_oid, meeting_or_event_venue, meeting_time, attendance_id) FROM stdin;
\.


--
-- Data for Name: attendance_penalty_committee; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.attendance_penalty_committee (oid, committee_oid, committee_name, short_description, member_oid, member_name, designation, role) FROM stdin;
\.


--
-- Data for Name: award_application; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.award_application (oid, employee_oid, award_title, attachments, date_of_receiving_award, notes, decision_status, application_code, decision_by) FROM stdin;
\.


--
-- Data for Name: award_setup; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.award_setup (oid, award_title, award_code, description, department, notes) FROM stdin;
\.


--
-- Data for Name: batch; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.batch (oid, batch_no_en, batch_no_bn, batch_year_en, batch_year_bn) FROM stdin;
\.


--
-- Data for Name: bundle; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.bundle (oid, bundle_type, role_oid, bundle_name) FROM stdin;
\.


--
-- Data for Name: cadre; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.cadre (oid, name_en, name_bn, cadre_date, sort_order) FROM stdin;
\.


--
-- Data for Name: call_for_exam; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.call_for_exam (oid, circular_oid, exam_configuration_oid, exam_code, exam_date, exam_type, exam_start, exam_end) FROM stdin;
\.


--
-- Data for Name: create_short_list; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.create_short_list (oid, short_list_code, application_code, application_name, educational_qualification, experience, short_list_created_by_oid, district) FROM stdin;
\.


--
-- Data for Name: daily_attendance; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.daily_attendance (oid, employee_oid, date, is_present) FROM stdin;
\.


--
-- Data for Name: deputation_info; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.deputation_info (oid, type_ingoing, type_outgoing, employee_oid, previous_ministry, deputed_ministry, previous_organization, deputed_organization, no_of_go, order_date, start_date, time_period) FROM stdin;
\.


--
-- Data for Name: education_qualification; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.education_qualification (oid, employee_info_oid, degree_title, institution_name, principal_subject, result, year_of_passing, permission_acceptance_from_godata) FROM stdin;
\.


--
-- Data for Name: employee; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.employee (oid, grp_username, name_en, name_bn, nick_name_en, nick_name_bn, father_name_en, father_name_bn, mother_name_en, mother_name_bn, gender, date_of_birth, mobile_no, email, address, blood_group, nid, religion, marital_status, etin, bank_account_number, emergency_contact_number, joining_at_present_post_on, joining_at_present_workstation_on, status, present_office_unit_post_oid, created_by, created_on, updated_by, updated_on) FROM stdin;
tanim	tanim	Md. Kamruzzaman Tanim	মোঃ কামরুজ্জামান তানিম	Tanim	তানিম	Md. Shamsuzzaman	মোঃ শামসুজ্জামান	Khodeza Khatun	খোদেজা খাতুন	Male	1985-07-10	01721060879	tanim1109135@gmail.com	Dhanmondi, Dhaka	A+	5098829603	Islam	Married	543-72-6789	123456789	01445665128	2019-06-09	2019-06-09	Active	39	System	2019-07-23 20:57:27.833138	\N	\N
mashud	mashud	Mashud Karim	মাসুদ করিম	Mashud	মাসুদ	Alim Reza	আলিম রেজা	Shamima Nila	শামীমা নীলা	Male	1987-07-10	01658933254	masud09@gmail.com	Shaymoli, Dhaka	B+	4659523312	Islam	Married	549-24-2563	125634644	01445665129	2019-06-09	2019-06-09	Active	36	System	2019-07-23 20:57:27.833138	\N	\N
jakir	jakir	Md. Jakir Hossain	মোঃ জাকির হোসেন	Jakir	জাকির	Md. Zohurul Islam	মোঃ জহুরুল ইসলাম	Rabeya Khatun	রাবেয়া খাতুন	Male	1994-05-12	01515236127	jakir@cokreates.com	Purana Paltan, Dhaka	A+	19932711719000188	Islam	Unmarried	544-65-5554	245843458	01738151934	2019-01-20	2019-01-20	Active	40	System	2019-07-23 20:57:27.833138	\N	\N
kamal	kamal	Md. Kamal Parvez	মোঃ কামাল পারভেজ	Kamal	কামাল	Md. Hasan Mahmud	মোঃ হাসান মাহমুদ	Rashida Begum	রাশিদা বেগম	Male	1988-09-03	01703777773	kamal@cokreates.com	Ambagan, Moghbazar, Dhaka	O+	4176467613	Islam	Married	545-89-8965	562656456	01911933313	2019-01-20	2019-01-20	Active	41	System	2019-07-23 20:57:27.833138	\N	\N
tasdiq	tasdiq	Tasdiqul Islam	তাসদিকুল ইসলাম	Tasdiq	তাসদিক	Shafiqul Islam	শফিকুল ইসলাম	Selina Rahman	সেলিনা রহমান	Male	1994-10-15	01624542420	tasdiq@cokreates.com	Suhrawardy Hall,BUET, Dhaka	B+	5952708589	Islam	Unmarried	895-56-8965	456456456	01759548855	2019-01-15	2019-01-15	Active	42	System	2019-07-23 20:57:27.833138	\N	\N
shaon	shaon	Ahmad Shadi Shaon	আহমদ সাদী শাওন 	Shaon	শাওন	Md. Shahid Ullah	মোঃ  শহীদ উল্লাহ	Hasina Parvin	হাসিনা পারভীন	Male	1994-06-08	01754935454	shaon@cokreates.com	Gopibagh, 1st lane, Dhaka	O+	5952708898	Islam	Unmarried	155-89-8624	564312312	01628862166	2019-01-15	2019-01-15	Active	43	System	2019-07-23 20:57:27.833138	\N	\N
shiropa	shiropa	Shiropa Sharmin	শিরোপা শারমিন	Shiropa	শিরোপা	Nurul Hoque	নুরুল হক	Hosna Ara Begum	হোসনে আরা বেগম	Female	1994-07-18	01635482381	shiropa@cokreates.com	Shekhertek, Mohammadpur, Dhaka	A+	552 958 4384	Islam	Married	158-58-5560	123456451	01676847917	2019-01-20	2019-01-20	Active	44	System	2019-07-23 20:57:27.833138	\N	\N
kabir	kabir	Md. Humayun Kabir	মোঃ হুমায়ুন কবির	Kabir	কবির	Md. Alamin Jamader	মোঃ আলমীন জামায়দার	Sonia Ahmed	সোনিয়া আহমেদ	Male	1992-09-13	01927040075	humayun@cokreates.com	Tejgaon, Dhaka	AB+	195 145 4584	Islam	Unmarried	555-89-4522	123564564	01643862401	2019-01-20	2019-01-20	Active	45	System	2019-07-23 20:57:27.833138	\N	\N
\.


--
-- Data for Name: employee_evaluation_category_setup; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.employee_evaluation_category_setup (oid, acr_form_part_number, acr_form_part_title, acr_form_evaluation_category) FROM stdin;
\.


--
-- Data for Name: employee_in_queue_for_promotion_or_posting; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.employee_in_queue_for_promotion_or_posting (oid, employee_oid, no_of_go) FROM stdin;
\.


--
-- Data for Name: employee_perf_summary; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.employee_perf_summary (oid, unit_weight_of_perf_metrics, performance_metrics) FROM stdin;
\.


--
-- Data for Name: exam_configuration; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.exam_configuration (oid, exam_type, total_marks, pass_marks, assigned_by) FROM stdin;
\.


--
-- Data for Name: exam_venue; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.exam_venue (oid, applicant_oid, shortlist_oid, district, venue, exam_oid) FROM stdin;
\.


--
-- Data for Name: final_selection; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.final_selection (oid, candidate_oid, police_verification_attachment_url, medical_verification_url, remarks) FROM stdin;
\.


--
-- Data for Name: foreign_training; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.foreign_training (oid, training_type_oid, training_title, institution, country, starting_from, end_to, sponsoring_agency) FROM stdin;
\.


--
-- Data for Name: grade; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.grade (oid, name_en, name_bn, sort_order, cadre_oid) FROM stdin;
\.


--
-- Data for Name: in_service_training; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.in_service_training (oid, training_type_oid, training_title, institution, location, starting_from, end_to, sponsoring_agency) FROM stdin;
\.


--
-- Data for Name: investigation_team_feedback; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.investigation_team_feedback (oid, employee_oid, date_of_submission, evaluation_by_investigation_team, notes, esign) FROM stdin;
\.


--
-- Data for Name: investigation_team_setup; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.investigation_team_setup (oid, committee_oid, employee_oid, controlling_authority, appointing_authority, assign_investigator, esign) FROM stdin;
\.


--
-- Data for Name: investigation_under_appointing_auth; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.investigation_under_appointing_auth (oid, employee_oid, date_of_submission, evaluation_by_aa, decision_status, application_code, decision_by, notes, esign) FROM stdin;
\.


--
-- Data for Name: investigation_under_controlling_auth; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.investigation_under_controlling_auth (oid, employee_oid, date_of_submission, evaluation_by_ca, decision_status, application_code, decision_by, notes, esign) FROM stdin;
\.


--
-- Data for Name: job_circular; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.job_circular (oid, post_oid, position_name, project, number_of_position, job_type, educational_qualification, age_duration, application_fee, other_qualification, least_experience, preferable_district_to_attend_exam, date_of_exam) FROM stdin;
\.


--
-- Data for Name: job_circular_approval; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.job_circular_approval (oid, circular_oid, approved_by_oid, approved_by_post_oid, approval_code, approval_status, comments) FROM stdin;
\.


--
-- Data for Name: justify_against_penalty; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.justify_against_penalty (oid, penalty_oid, employee_oid, justification, attachment_url) FROM stdin;
\.


--
-- Data for Name: leave_approval_by_admin; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.leave_approval_by_admin (oid, leave_oid, application_oid, approval_status, approved_by_post, approved_by_oid, approval_code, comments) FROM stdin;
\.


--
-- Data for Name: leave_approval_by_substitute; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.leave_approval_by_substitute (oid, leave_oid, substitute_oid, approved_from, approved_to, approval_status, reason, substitute_approval_code) FROM stdin;
\.


--
-- Data for Name: leave_setup; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.leave_setup (oid, leave_name, gender, no_of_time, duration, eligibility, description) FROM stdin;
\.


--
-- Data for Name: offense_setup; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.offense_setup (oid, offense_title, offense_code, description, start_date_of_offense, end_date_of_offense, go_number, esign) FROM stdin;
\.


--
-- Data for Name: organisational_training; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.organisational_training (oid, is_mandatory, duration, training_type_oid, objective, descriptions, training_name, start_date, end_date, start_time, end_time, organization, designation_required, education_required) FROM stdin;
\.


--
-- Data for Name: penalty_committee_list; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.penalty_committee_list (oid, attendance_committee_oid, committee_name) FROM stdin;
\.


--
-- Data for Name: penalty_for_attendance; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.penalty_for_attendance (oid, date, employee_oid, cause, action, remarks, penalty_code) FROM stdin;
\.


--
-- Data for Name: pending_participation_confirmation; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.pending_participation_confirmation (oid, training_oid, trainee_oid, is_attendant, reason) FROM stdin;
\.


--
-- Data for Name: pension_procedure_for_employee; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.pension_procedure_for_employee (oid, employee_oid, employee_post_oid, noc_upload, ipc_upload, date_of_retirement, employed_from, employed_upto, bank_account_no, bank_name, bank_branch, application_code, decision_by, current_post_joining_date, current_post_retirement_date, prl_status, rate_of_retirement_allowance, total_amount_of_retirement_allowance, monthly_amount_of_retirement_allowance, total_gratuity, loan_history, notes, pension_code, decision_status) FROM stdin;
\.


--
-- Data for Name: posting_and_promotion_committee_setup; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.posting_and_promotion_committee_setup (oid, committee_name, short_description, member_oid, role) FROM stdin;
\.


--
-- Data for Name: posting_info; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.posting_info (oid, employee_oid, division, district, starting_from, end_to, last_posting, pay_scale, no_of_go) FROM stdin;
\.


--
-- Data for Name: promotion_detail; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.promotion_detail (oid, employee_oid, date_of_promotion, rank, nature_of_promotion, pay_scale, no_of_go) FROM stdin;
\.


--
-- Data for Name: publication_application; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.publication_application (oid, employee_oid, publication_title, attachments, date_of_publication, notes, decision_status, application_code, decision_by) FROM stdin;
\.


--
-- Data for Name: publication_setup; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.publication_setup (oid, publication_title, publication_code, description, department, notes) FROM stdin;
\.


--
-- Data for Name: recruitment_committee; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.recruitment_committee (oid, committee_oid, committee_name, short_description, member_oid, member_name, designation, role) FROM stdin;
\.


--
-- Data for Name: recruitment_result; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.recruitment_result (oid, attachment_url, circular_oid, written_marks, viva_marks, applicant_oid) FROM stdin;
\.


--
-- Data for Name: role; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.role (oid, role_type, role_name_bn, role_name_en) FROM stdin;
\.


--
-- Data for Name: rules_of_award_publication; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.rules_of_award_publication (oid, rules_and_regulations, assigned_by, assigned_on) FROM stdin;
\.


--
-- Data for Name: rules_of_vesting_magisterial_power; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.rules_of_vesting_magisterial_power (oid, rules_and_regulations, assigned_by, assigned_on) FROM stdin;
\.


--
-- Data for Name: rules_regarding_acr_apa; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.rules_regarding_acr_apa (oid, rules_and_regulations, assigned_by, assigned_on) FROM stdin;
\.


--
-- Data for Name: rules_regarding_attendance; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.rules_regarding_attendance (oid, rules_and_regulations, assigned_by, assigned_on) FROM stdin;
\.


--
-- Data for Name: rules_regarding_discipline; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.rules_regarding_discipline (oid, rules_and_regulations, assigned_by, assigned_on) FROM stdin;
\.


--
-- Data for Name: rules_regarding_leave; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.rules_regarding_leave (oid, leave_type, rules_and_regulations, assigned_by, assigned_on) FROM stdin;
\.


--
-- Data for Name: rules_regarding_posting; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.rules_regarding_posting (oid, rules_and_regulations, assigned_by, assigned_on) FROM stdin;
\.


--
-- Data for Name: rules_regarding_promotion; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.rules_regarding_promotion (oid, rules_and_regulations, assigned_by, assigned_on) FROM stdin;
\.


--
-- Data for Name: rules_regarding_recruitment; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.rules_regarding_recruitment (oid, rules_and_regulations, assigned_by, assigned_on) FROM stdin;
\.


--
-- Data for Name: rules_regarding_training; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.rules_regarding_training (oid, training_type, rules_and_regulations, assigned_by, assigned_on) FROM stdin;
\.


--
-- Data for Name: select_candidate; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.select_candidate (oid, applicant_oid, circular_oid, is_selected) FROM stdin;
\.


--
-- Data for Name: service_history; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.service_history (oid, employee_info_oid, entry_to_govt_service_on, gazetted_on, encadrement_on, type_of_cadre) FROM stdin;
\.


--
-- Data for Name: survey_for_trainee; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.survey_for_trainee (oid, training_type_oid, trainer_detail_oid, trainee_id, survey_question_code, answer) FROM stdin;
\.


--
-- Data for Name: survey_for_trainer; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.survey_for_trainer (oid, training_type_oid, trainer_detail_oid, trainer_code, survey_question_code, answer) FROM stdin;
\.


--
-- Data for Name: survey_questionnaire_for_trainee; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.survey_questionnaire_for_trainee (oid, training_oid, question) FROM stdin;
\.


--
-- Data for Name: survey_questionnaire_for_trainer; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.survey_questionnaire_for_trainer (oid, training_type_oid, trainer_detail_oid, question) FROM stdin;
\.


--
-- Data for Name: termination; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.termination (oid, employee_oid, employee_post_oid, date_of_termination, reason, remarks, go_code, goverment_order, decision_status, application_code, decision_by) FROM stdin;
\.


--
-- Data for Name: trainee_result; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.trainee_result (oid, trainer, trainee, training, result) FROM stdin;
\.


--
-- Data for Name: trainer_detail; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.trainer_detail (oid, trainer_oid, training_history) FROM stdin;
\.


--
-- Data for Name: training_type; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.training_type (oid, type_name, type_code, type_description, principal_department) FROM stdin;
\.


--
-- Data for Name: vesting_of_magisterial_power; Type: TABLE DATA; Schema: hrm; Owner: grp
--

COPY hrm.vesting_of_magisterial_power (oid, employee_oid, area_of_work, work_description, reason_of_vesting, substitute_department, substitute_code, duration, start_date, end_date, go_code, government_order, notes) FROM stdin;
\.


--
-- Data for Name: annual_procurement_plan; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.annual_procurement_plan (oid, app_code, status, fiscal_year_oid, project_oid, budget_type_oid, office_oid, created_by, created_on, updated_by, updated_on) FROM stdin;
20190725-165455-HJpqngttagMsfxF	app001	Final	2	3	2	143	jakir	2019-07-25 16:54:55.199	\N	\N
\.


--
-- Data for Name: annual_procurement_plan_detail; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.annual_procurement_plan_detail (oid, app_detail_code, approving_authority, app_oid, fiscal_year_oid, budget_type_oid, project_name, app_code, package_no, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: annual_procurement_plan_reviewer; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.annual_procurement_plan_reviewer (oid, employee_info_oid, procurement_role, description, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: app_package; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.app_package (oid, package_type_oid, package_no, package_description, package_unit, package_quantity, procurement_method_oid, procurement_nature_oid, procurement_type_oid, contract_approving_authority_oid, source_of_fund, estimate_cost, tender_advertise_date, tender_opening_date, tender_evaluation_date, approval_to_award_date, notification_of_award_date, signing_of_contract_date, contract_signature_day, contract_completion_day, created_by, created_on, updated_by, updated_on, annual_procurement_plan_oid, item_category_oid) FROM stdin;
20190725-165937-LgyRzmc3Is7MG7I	2	01	eiofbjkgthngbf	nos	100	2	1	1	3	GOB	100000.000000	2019-07-25	2019-07-26	2019-07-27	2019-07-28	2019-07-30	2019-07-30	10	5	System	2019-07-25 16:59:37.064887	\N	\N	20190725-165455-HJpqngttagMsfxF	3
\.


--
-- Data for Name: approving_authority; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.approving_authority (oid, code, name_en, name_bn) FROM stdin;
1	Account Officer	Account Officer	অ্যাকাউন্ট অফিসার
2	AO	Authorized Officer	অনুমোদিত কর্মকর্তা
3	BOD	Board of Directors	পরিচালক বোর্ড
4	CCGP	Cabinet Committee on Government Purchase	সরকারি ক্রয়ের উপর মন্ত্রিপরিষদ কমিটি
5	HOPE	Head of Procuring Entity	প্রাধান্য সংস্থা প্রধান
6	Minister	Minister	মন্ত্রী
7	PE	Procuring Entity	প্রকিউরিং এনটিটি
8	PMC	PMC	পিএমসি
9	Secretary	Secretary	সম্পাদক
\.


--
-- Data for Name: attachment; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.attachment (oid, file_code, file_name_en, file_name_bn, file_path, is_delete, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: budget_type; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.budget_type (oid, code, name_en, name_bn, description, created_by, created_on, updated_by, updated_on) FROM stdin;
1	DEV	Development	উন্নয়ন	Development Budget means the budget for total estimated Project Costs and sources of payment attached to the Construction Disbursement Agreement, as the same may be amended, modified or supplemented from time to time in accordance with the terms hereof and the Construction Disbursement Agreement	tanim	2019-07-23 20:57:27.844879	\N	\N
2	REV	Revenue	রাজস্ব	The revenue budget consists of revenue receipts of the government (revenues from tax and other sources), and its expenditure	tasdiq	2019-07-23 20:57:27.844879	\N	\N
3	OF	Own Fund	নিজস্ব তহবিল	-	kamal	2019-07-23 20:57:27.844879	\N	\N
\.


--
-- Data for Name: chalan; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.chalan (oid, remarks, created_by, created_on, updated_by, updated_on, pakage_lot_oid) FROM stdin;
\.


--
-- Data for Name: committee_member; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.committee_member (oid, code, name_en, name_bn, organization, responsibility, employee_oid, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: consolidate_requisition; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.consolidate_requisition (oid, status, created_by, created_on, updated_by, updated_on, office_oid, fiscal_year_oid, requisition_declaration_oid) FROM stdin;
Consolidated-001	Consolidated	mashud	2019-07-23 20:57:27.862997	\N	\N	143	1	Declaration-Jan-01
Consolidated-002	Consolidated	kamal	2019-07-23 20:57:27.862997	tanim	2019-07-25 10:43:38.469	143	1	Declaration-Apr-04
20190725-165203-W3VJWIRfQtbruc9	Consolidated	jakir	2019-07-25 16:52:03.060693	\N	\N	143	2	20190725-102053-4ezKbnTuU0WCaX5
20190728-134622-EiEAASmmzPVkCTN	Draft	jakir	2019-07-28 13:46:22.920098	\N	\N	143	2	Declaration-May-05
\.


--
-- Data for Name: consolidate_requisition_detail; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.consolidate_requisition_detail (oid, required_quantity, approved_quantity, unit, remarks, item_category_oid, item_oid, consolidate_requisition_oid) FROM stdin;
Consolidate-Detail-0001	20.000000	18.000000	রীম	প্রযোজ্য নয়	1	1	Consolidated-001
Consolidate-Detail-0002	5.000000	5.000000	সেটসমূহ	প্রযোজ্য নয়	2	2	Consolidated-001
Consolidate-Detail-0003	8.000000	7.000000	সেটসমূহ	প্রযোজ্য নয়	1	3	Consolidated-001
Consolidate-Detail-0004	10.000000	9.000000	বাক্স	প্রযোজ্য নয়	5	15	Consolidated-001
Consolidate-Detail-0005	30.000000	25.000000	ডজন	প্রযোজ্য নয়	2	4	Consolidated-001
Consolidate-Detail-0006	7.000000	7.000000	ডজন	প্রযোজ্য নয়	2	6	Consolidated-001
Consolidate-Detail-0007	20.000000	17.000000	টি	প্রযোজ্য নয়	3	8	Consolidated-001
Consolidate-Detail-0008	25.000000	25.000000	টি	প্রযোজ্য নয়	3	9	Consolidated-001
20190725-104338-f51S5pHkqVZEBqt	6.000000	5.000000	বাক্স	প্রযোজ্য নয়	5	16	Consolidated-002
20190725-104338-yjunhwMbkVh89Nm	9.000000	0.000000	টি	প্রযোজ্য নয়	3	7	Consolidated-002
20190725-104338-AGyE0YFw295U9x1	10.000000	8.000000	রীম	প্রযোজ্য নয়	1	1	Consolidated-002
20190725-104338-V2etD43SQa9dkEh	5.000000	0.000000	টি	প্রযোজ্য নয়	2	2	Consolidated-002
20190725-104338-RYj0w5D8o0hoFjA	8.000000	8.000000	সেটসমূহ	প্রযোজ্য নয়	1	3	Consolidated-002
20190725-165203-cR7ofV7bKXatROS	10.000000	10.000000	টি	\N	4	12	20190725-165203-W3VJWIRfQtbruc9
20190725-165203-4LSabeGzY9Kz0Sw	20.000000	15.000000	ডজন	\N	2	6	20190725-165203-W3VJWIRfQtbruc9
20190728-134622-LiJTGJbxiwzVzJR	9.000000	0.000000	টি	\N	3	7	20190728-134622-EiEAASmmzPVkCTN
20190728-134622-nG2kqdC3Un5MqRR	20.000000	0.000000	টি	\N	3	8	20190728-134622-EiEAASmmzPVkCTN
20190728-134622-8NPwK3hKjoFhLsV	25.000000	0.000000	টি	\N	3	9	20190728-134622-EiEAASmmzPVkCTN
20190728-134622-Mo0MKJURKGKcs57	100.000000	0.000000	লিটার	\N	5	18	20190728-134622-EiEAASmmzPVkCTN
20190728-134622-hZUiTRFBglh4edl	10.000000	0.000000	রীম	\N	1	1	20190728-134622-EiEAASmmzPVkCTN
20190728-134622-tpVP8g5boi4Ieq9	5.000000	0.000000	সেটসমূহ	\N	1	2	20190728-134622-EiEAASmmzPVkCTN
\.


--
-- Data for Name: debarment; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.debarment (oid, vendor_oid, reported_by, approved_by, debarment_from, debarment_to) FROM stdin;
\.


--
-- Data for Name: item_purchase; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.item_purchase (oid, contract_amount, completion_date, contract_period, created_by, created_on, updated_by, updated_on, schedule_sale_oid, employee_info_oid, vendor_oid) FROM stdin;
\.


--
-- Data for Name: material_receive_info; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.material_receive_info (oid, material_receive_info_code, vendor_oid, temporary_store_oid, chalan_info, item_purchase_oid) FROM stdin;
\.


--
-- Data for Name: notification_of_award; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.notification_of_award (oid, ministry_name, agency_name, tender_id, package_no, reference_description, consultant_name, consultant_address, date_of_publication, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: package_lot; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.package_lot (oid, package_lot_code, app_package_oid, item_oid, lot_no, lot_description, unit, quantity, estimate_cost, remarks, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: package_type; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.package_type (oid, code, name_en, name_bn, description) FROM stdin;
1	eGP	electronic Government Procurement	ইলেকট্রনিক গভর্নমেন্ট প্রকিউরমেন্ট	National e-Government Procurement (e-GP) portal ( i.e. https://www.eprocure.gov.bd ) of the Government of the People’s Republic of Bangladesh is developed, owned and being operated by the Central Procurement Technical Unit (CPTU), IME Division of Ministry of Planning. The e-GP system provides an on-line platform to carry out the procurement activities by the Public Agencies - Procuring Agencies (PAs) and Procuring Entities (PEs).
2	Mannual	Mannual Procurement	ম্যানুয়াল প্রকিউরমেন্ট	The Procurement Manual is intended to provide guidance on procurement policies and procedures to all staff members involved in the various stages of the procurement actions conducted by the government in all offices and all locations, and is not intended to be exhaustive.
\.


--
-- Data for Name: procurement_contract; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.procurement_contract (completion_date, contract_period, comments, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: procurement_item; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.procurement_item (oid, code, name_en, name_bn, hs_code, is_avilable, required_quantity, item_category_oid, item_oid, uom_oid, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: procurement_method; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.procurement_method (oid, name_en, name_bn, description) FROM stdin;
1	DPM	ডি পি এম	Direct Procurement Method
2	LTM	এল টি এম	Limited Tendering Method
3	OSTETM	ও এস টি ই টি এম	One Stage Two Envelope Tendering Method
4	OTM	ও টি এম	Open Tendering Method
5	QCBS	কিউ সি বি এস	Quality and Cost Based selection
6	RFP	আর এফ পি	Request for Proposal
7	RFQ	আর এফ কিউ	Request for Quotation
8	SBCQ	এস বি সি কিউ	Selection based on Consultant’s Qualification
9	SSS	এস এস এস	Single Source Selection
10	TSTM	টি এস টি এম	Two-stage Tendering Method
\.


--
-- Data for Name: procurement_nature; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.procurement_nature (oid, name_en, name_bn, description) FROM stdin;
1	Goods	পণ্য	Goods are materials that satisfy human wants and provide utility, for example, to a consumer making a purchase of a satisfying product
2	Works	কাজ	Work is a word generally embodying concepts associated with the labor, force, energy, and/or effort required to produce a specific result
3	Services	সেবা	A service is a transaction in which no physical goods are transferred from the seller to the buyer
\.


--
-- Data for Name: procurement_project; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.procurement_project (oid, project_code, name_en, name_bn, description, start_date, end_date, commitment_amount, status, created_by, created_on, updated_by, updated_on, approval_date) FROM stdin;
1	P165477	Dhaka City Neighborhood Upgrading Project,Bangladesh	ঢাকা শহর প্রতিবেশী উন্নয়ন প্রকল্প, বাংলাদেশ	-	2019-03-29	2024-06-30	100.500000	Active	tasdiq	2019-07-23 20:57:27.846353	\N	\N	2019-03-29
2	P167762	Emergency Multi-Sector Rohingya Crisis Response Project,Bangladesh	জরুরী মাল্টি সেক্টর রোহিঙ্গা সংকট প্রতিক্রিয়া প্রকল্প,বাংলাদেশ	-	2019-03-07	2022-03-05	165.000000	Active	tasdiq	2019-07-23 20:57:27.846353	\N	\N	2019-03-07
3	P161869	Bangladesh Scaling-up Renewable Energy Project,Bangladesh 	বাংলাদেশ স্কেলিং আপ পুনর্নবীকরণযোগ্য শক্তি প্রকল্প,বাংলাদেশ	-	2019-03-01	2024-01-31	156.000000	Active	tasdiq	2019-07-23 20:57:27.846353	\N	\N	2019-03-01
4	P167491	Bangladesh Strengthening PFM Program to Enable Service Delivery,Bangladesh	সেবা বিতরণকে সক্ষম করার জন্য বাংলাদেশ শক্তিশালীকরণ পিএফএম প্রোগ্রাম,বাংলাদেশ	-	2019-03-01	2024-06-30	100.000000	Active	tasdiq	2019-07-23 20:57:27.846353	\N	\N	2019-03-01
5	P167190	Bangladesh Jobs Programmatic DPC,Bangladesh	বাংলাদেশ চাকরি প্রোগ্রাম্যাটিক ডিপিসি,বাংলাদেশ	-	2018-12-12	2019-06-27	250.000000	Closed	tasdiq	2019-07-23 20:57:27.846353	\N	\N	2018-12-12
6	P161246	Livestock and Dairy Development Project,Bangladesh 	পশুসম্পদ ও ডেইরি উন্নয়ন প্রকল্প,বাংলাদেশ	-	2018-12-12	2023-09-30	500.000000	Active	tasdiq	2019-07-23 20:57:27.846353	\N	\N	2018-12-12
\.


--
-- Data for Name: procurement_type; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.procurement_type (oid, name_en, name_bn, description) FROM stdin;
1	NCT	এনসিটি	National Competitive Tender is a Procurement proceeding wherein the procurement entity or the government decides that only the domestic suppliers or contractors may Participate in the tenders
2	ICT	আইসিটি	International Competitive Tender: A competitive bidding procedure is commonly used in the procurement of goods and services. It provides a wider choice in selecting the best bid among the competing suppliers and contractors.
\.


--
-- Data for Name: qc_committee; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.qc_committee (oid, code, authorized_by, name_en, name_bn, min_no_member, max_no_member, forming_date, purpose, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: qc_info; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.qc_info (oid, qc_detail_code, qc_info_oid, quantity, "position", comment, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: qc_list; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.qc_list (oid, qc_code, item_category_oid, item_oid, item_uom_oid, quantity, "position", created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: qc_report; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.qc_report (oid, qc_code, temporary_store_oid, qc_committee_oid, inspection_time, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: requisition_declaration; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.requisition_declaration (oid, start_date, end_date, status, description, declared_by, office_oid, fiscal_year_oid, created_by, created_on, updated_by, updated_on) FROM stdin;
Declaration-Jan-01	2018-12-01	2019-02-28	Consolidated	Requisition is closed now for this declaration	mashud	\N	1	tasdiq	2019-07-23 20:57:27.850044	\N	\N
Declaration-Jul-07	2016-07-01	2016-07-30	Draft	Declaration save as draft	tasdiq	\N	2	kamal	2019-07-23 20:57:27.850044	\N	\N
Declaration-Jun-06	2017-06-01	2017-06-30	Closed	Declaration Published for submitting requisition	tanim	\N	2	kamal	2019-07-23 20:57:27.850044	\N	\N
Declaration-Apr-04	2019-04-01	2019-04-30	Consolidated	Requisition is closed now for this declaration	kamal	\N	1	tasdiq	2019-07-23 20:57:27.850044	\N	\N
20190725-102053-4ezKbnTuU0WCaX5	2019-07-31	2019-08-02	Consolidated	yhztyz	tanim	143	2	tanim	2019-07-25 10:20:53.331724	\N	\N
Declaration-May-05	2018-05-01	2018-05-31	Consolidating	Requisition is closed now for this declaration	tasdiq	\N	2	tasdiq	2019-07-23 20:57:27.850044	\N	\N
20190728-134653-LaYh41KiwJtYPR4	2019-08-15	2019-08-29	Published	test RD	System	143	3	jakir	2019-07-28 13:46:54.005131	jakir	2019-07-28 13:47:12.158
\.


--
-- Data for Name: requisition_detail; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.requisition_detail (oid, quantity, unit, remarks, specification, item_oid, item_category_oid, requisition_oid) FROM stdin;
RD-0001	5.000000	সেটসমূহ	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	2	1	Req-1
RD-0002	8.000000	সেটসমূহ	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	3	1	Req-1
RD-0003	10.000000	রীম	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	1	1	Req-1
RD-0004	30.000000	টি	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	4	2	Req-2
RD-0005	7.000000	ডজন	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	6	2	Req-2
RD-0006	10.000000	বাক্স	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	15	5	Req-2
RD-0007	20.000000	টি	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	8	3	Req-3
RD-0008	25.000000	টি	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	9	3	Req-3
RD-0009	9.000000	টি	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	7	3	Req-4
RD-0010	6.000000	বাক্স	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	16	5	Req-4
RD-0011	10.000000	রীম	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	1	1	Req-5
RD-0012	8.000000	সেটসমূহ	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	3	1	Req-5
RD-0013	5.000000	টি	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	2	1	Req-5
RD-0014	10.000000	টি	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	17	5	Req-6
RD-0015	10.000000	টি	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	17	5	Req-6
RD-0016	7.000000	রীম	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	6	2	Req-6
RD-0017	25.000000	টি	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	9	3	Req-7
RD-0018	9.000000	টি	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	7	3	Req-7
RD-0019	20.000000	টি	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	8	3	Req-7
RD-0020	100.000000	লিটার	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	18	5	Req-8
RD-0021	10.000000	রীম	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	1	1	Req-8
RD-0022	5.000000	সেটসমূহ	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	2	1	Req-8
RD-0023	8.000000	টি	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	3	1	Req-9
RD-0024	5.000000	বাক্স	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	16	5	Req-9
RD-0025	30.000000	টি	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	4	2	Req-9
RD-0026	9.000000	টি	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	7	3	Req-10
RD-0027	7.000000	সেটসমূহ	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	6	2	Req-10
RD-0028	20.000000	টি	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	8	3	Req-10
RD-0029	5.000000	সেটসমূহ	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	5	2	Req-11
RD-0030	7.000000	ডজন	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	6	2	Req-11
RD-0031	50.000000	কিলোগ্রাম	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	19	5	Req-11
RD-0032	25.000000	টি	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	9	3	Req-11
RD-0033	20.000000	টি	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	8	3	Req-12
RD-0034	6.000000	টি	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	10	3	Req-12
RD-0035	9.000000	টি	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	7	3	Req-12
RD-0036	25.000000	গ্রাম	প্রযোজ্য নয়	কোন বিশেষ প্রয়োজনীয়তা নেই	20	5	Req-12
20190725-164735-D2F2Qt2SLNBpjt9	5.000000	ডজন	urgent	\N	6	2	20190725-164604-ubT2RH1xmEh2IGH
20190725-164735-mSxUSBcyEgTY9lB	10.000000	টি	\N	\N	12	4	20190725-164604-ubT2RH1xmEh2IGH
20190725-164825-i5ztZIpZyGKKyae	15.000000	ডজন	\N	\N	6	2	20190725-164825-iTmvIyEgy5I7cLV
\.


--
-- Data for Name: requisition_information; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.requisition_information (oid, requisition_date, remarks, status, created_by, created_on, updated_by, updated_on, requisition_declaration_oid, office_oid) FROM stdin;
Req-1	2018-12-01	Declaration Published for submitting requisition Declaration-Jan-01	Final	jakir	2019-07-23 20:57:27.851926	\N	\N	Declaration-Jan-01	143
Req-2	2018-12-05	Declaration Published for submitting requisition Declaration-Jan-01	Final	jakir	2019-07-23 20:57:27.851926	\N	\N	Declaration-Jan-01	143
Req-3	2018-12-10	Declaration Published for submitting requisition Declaration-Jan-01	Final	jakir	2019-07-23 20:57:27.851926	\N	\N	Declaration-Jan-01	143
Req-4	2019-04-05	Declaration Published for submitting requisition Declaration-Apr-04	Final	jakir	2019-07-23 20:57:27.851926	\N	\N	Declaration-Apr-04	143
Req-5	2019-04-15	Declaration Published for submitting requisition Declaration-Apr-04	Final	jakir	2019-07-23 20:57:27.851926	\N	\N	Declaration-Apr-04	143
Req-6	2019-04-25	Declaration Published for submitting requisition Declaration-Apr-04	Draft	jakir	2019-07-23 20:57:27.851926	\N	\N	Declaration-Apr-04	143
Req-7	2019-05-10	Declaration Published for submitting requisition Declaration-May-05	Final	jakir	2019-07-23 20:57:27.851926	\N	\N	Declaration-May-05	143
Req-8	2019-07-20	Declaration Published for submitting requisition Declaration-May-05	Final	jakir	2019-07-23 20:57:27.851926	\N	\N	Declaration-May-05	143
Req-9	2019-07-25	Declaration Published for submitting requisition Declaration-May-05	Draft	jakir	2019-07-23 20:57:27.851926	\N	\N	Declaration-May-05	143
Req-10	2019-07-12	Declaration Published for submitting requisition Declaration-Jun-06	Final	jakir	2019-07-23 20:57:27.851926	\N	\N	Declaration-Jun-06	143
Req-11	2019-07-24	Declaration Published for submitting requisition Declaration-Jun-06	Final	jakir	2019-07-23 20:57:27.851926	\N	\N	Declaration-Jun-06	143
Req-12	2019-07-29	Declaration Published for submitting requisition Declaration-Jun-06	Draft	jakir	2019-07-23 20:57:27.851926	\N	\N	Declaration-Jun-06	143
20190725-164604-ubT2RH1xmEh2IGH	2019-07-25	\N	Final	tasdiq	2019-07-25 16:46:04.2	tasdiq	2019-07-25 16:47:35.092	20190725-102053-4ezKbnTuU0WCaX5	143
20190725-164825-iTmvIyEgy5I7cLV	2019-07-25	\N	Final	jakir	2019-07-25 16:48:25.311	\N	\N	20190725-102053-4ezKbnTuU0WCaX5	143
\.


--
-- Data for Name: reviewer; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.reviewer (oid, name_en, name_bn, description, employee_oid, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: schedule_info; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.schedule_info (oid, app_package_oid, expected_date_of_advert, date_of_submission, submission_date_of_evolution_report, approval_date_of_award_of_contract, issue_date_of_notification_of_award, date_of_completion_contract, total_days_of_completion, description) FROM stdin;
\.


--
-- Data for Name: schedule_sale; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.schedule_sale (oid, schedule_sale_code, schedule_info_oid, employee_info_oid, vendor_oid, payment_received_by, paid_amount, purchase_date, security_deposit, payment_method, pay_order_no, pay_order_bank_name, description) FROM stdin;
\.


--
-- Data for Name: temporary_store; Type: TABLE DATA; Schema: prc; Owner: grp
--

COPY prc.temporary_store (oid, store_code, office_name_en, office_name_bn, store_name_en, store_name_bn, store_location, is_active, store_type, created_by, created_on, updated_by, updated_on) FROM stdin;
\.


--
-- Data for Name: flyway_schema_history; Type: TABLE DATA; Schema: public; Owner: grp
--

COPY public.flyway_schema_history (installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) FROM stdin;
1	\N	00 00 00 00  sec	SQL	R__00_00_00_00__sec.sql	-1300121208	grp	2019-07-23 20:57:26.370566	13	t
2	\N	01 01 01 01  ministry	SQL	01-cmn/01-base/R__01_01_01_01__ministry.sql	970317488	grp	2019-07-23 20:57:26.392652	8	t
3	\N	01 01 01 02  office	SQL	01-cmn/01-base/R__01_01_01_02__office.sql	246020814	grp	2019-07-23 20:57:26.413772	47	t
4	\N	01 01 01 03  geo	SQL	01-cmn/01-base/R__01_01_01_03__geo.sql	-1160630519	grp	2019-07-23 20:57:26.461915	25	t
5	\N	01 01 02 04  fiscal-year	SQL	01-cmn/02-configuration/R__01_01_02_04__fiscal-year.sql	656523151	grp	2019-07-23 20:57:26.49193	4	t
6	\N	01 01 02 05  unit-of-measure	SQL	01-cmn/02-configuration/R__01_01_02_05__unit-of-measure.sql	1767666449	grp	2019-07-23 20:57:26.502575	3	t
7	\N	01 01 02 06  item	SQL	01-cmn/02-configuration/R__01_01_02_06__item.sql	-651270512	grp	2019-07-23 20:57:26.510367	7	t
8	\N	01 01 02 07  entitlement	SQL	01-cmn/02-configuration/R__01_01_02_07__entitlement.sql	-488777699	grp	2019-07-23 20:57:26.52152	4	t
9	\N	01 01 02 08  store	SQL	01-cmn/02-configuration/R__01_01_02_08__store.sql	1429127692	grp	2019-07-23 20:57:26.529676	3	t
10	\N	01 01 02 09  party	SQL	01-cmn/02-configuration/R__01_01_02_09__party.sql	1673004447	grp	2019-07-23 20:57:26.537191	3	t
11	\N	01 01 02 10  vendor	SQL	01-cmn/02-configuration/R__01_01_02_10__vendor.sql	-792645011	grp	2019-07-23 20:57:26.54395	3	t
12	\N	01 01 02 11  donor	SQL	01-cmn/02-configuration/R__01_01_02_11__donor.sql	-141269241	grp	2019-07-23 20:57:26.551351	7	t
13	\N	01 01 02 12  committee	SQL	01-cmn/02-configuration/R__01_01_02_12__committee.sql	-1738736169	grp	2019-07-23 20:57:26.562507	9	t
14	\N	01 01 02 13  document	SQL	01-cmn/02-configuration/R__01_01_02_13__document.sql	760714740	grp	2019-07-23 20:57:26.575203	6	t
15	\N	01 01 03 14  payroll-and-fund-information-manageement	SQL	01-cmn/03-payroll-and-fund-information/R__01_01_03_14__payroll-and-fund-information-manageement.sql	-519628399	grp	2019-07-23 20:57:26.584663	9	t
16	\N	01 01 03 15  update-loan-rule	SQL	01-cmn/03-payroll-and-fund-information/R__01_01_03_15__update-loan-rule.sql	501884618	grp	2019-07-23 20:57:26.596777	3	t
17	\N	01 02 01 01  application	SQL	02-sec/01-base/R__01_02_01_01__application.sql	-272508962	grp	2019-07-23 20:57:26.608838	27	t
18	\N	01 02 01 02  authentication	SQL	02-sec/01-base/R__01_02_01_02__authentication.sql	-1441321130	grp	2019-07-23 20:57:26.639707	6	t
19	\N	01 02 01 03  authorization	SQL	02-sec/01-base/R__01_02_01_03__authorization.sql	1173427422	grp	2019-07-23 20:57:26.650508	10	t
20	\N	01 03 01 01  configuration	SQL	03-hrm/01-base/R__01_03_01_01__configuration.sql	-269204625	grp	2019-07-23 20:57:26.662678	9	t
21	\N	01 03 01 02  employee	SQL	03-hrm/01-base/R__01_03_01_02__employee.sql	948459810	grp	2019-07-23 20:57:26.678743	5	t
22	\N	01 03 02 03  employee-qualification	SQL	03-hrm/02-personnel-information-management/R__01_03_02_03__employee-qualification.sql	1663513833	grp	2019-07-23 20:57:26.68848	5	t
23	\N	01 03 03 04  training-type-configure	SQL	03-hrm/03-training/R__01_03_03_04__training-type-configure.sql	83889792	grp	2019-07-23 20:57:26.696276	2	t
24	\N	01 03 03 05  training-rules-configure	SQL	03-hrm/03-training/R__01_03_03_05__training-rules-configure.sql	881462156	grp	2019-07-23 20:57:26.703388	4	t
25	\N	01 03 03 06  in-service-training-info	SQL	03-hrm/03-training/R__01_03_03_06__in-service-training-info.sql	1356485633	grp	2019-07-23 20:57:26.709164	3	t
26	\N	01 03 03 07  foreign-training-info	SQL	03-hrm/03-training/R__01_03_03_07__foreign-training-info.sql	-1712823574	grp	2019-07-23 20:57:26.715422	3	t
27	\N	01 03 03 08  organizational-training-info	SQL	03-hrm/03-training/R__01_03_03_08__organizational-training-info.sql	144914336	grp	2019-07-23 20:57:26.722171	4	t
28	\N	01 03 03 09  training-info	SQL	03-hrm/03-training/R__01_03_03_09__training-info.sql	-1634940585	grp	2019-07-23 20:57:26.730708	11	t
29	\N	01 03 03 10  post-training-survey	SQL	03-hrm/03-training/R__01_03_03_10__post-training-survey.sql	-1538325354	grp	2019-07-23 20:57:26.745574	15	t
30	\N	01 03 04 11  leave-setup-configure	SQL	03-hrm/04-leave/R__01_03_04_11__leave-setup-configure.sql	381279265	grp	2019-07-23 20:57:26.764487	34	t
31	\N	01 03 04 12  leave-rules-configure	SQL	03-hrm/04-leave/R__01_03_04_12__leave-rules-configure.sql	758739545	grp	2019-07-23 20:57:26.801195	5	t
32	\N	01 03 04 13  leave-application	SQL	03-hrm/04-leave/R__01_03_04_13__leave-application.sql	117473299	grp	2019-07-23 20:57:26.80841	3	t
33	\N	01 03 04 14  leave-management	SQL	03-hrm/04-leave/R__01_03_04_14__leave-management.sql	-2112207226	grp	2019-07-23 20:57:26.814092	9	t
34	\N	01 03 05 15  recruitment-rules-configure	SQL	03-hrm/05-recruitment/R__01_03_05_15__recruitment-rules-configure.sql	1679518708	grp	2019-07-23 20:57:26.82632	6	t
35	\N	01 03 06 16  recruitment-exam-configure	SQL	03-hrm/06-recruitment /R__01_03_06_16__recruitment-exam-configure.sql	-848641856	grp	2019-07-23 20:57:26.836924	3	t
36	\N	01 03 07 17  recruitment-committee-configure	SQL	03-hrm/07-recruitment/R__01_03_07_17__recruitment-committee-configure.sql	-1946999787	grp	2019-07-23 20:57:26.844178	4	t
37	\N	01 03 07 18  recruitment-process	SQL	03-hrm/07-recruitment/R__01_03_07_18__recruitment-process.sql	-830075460	grp	2019-07-23 20:57:26.853352	7	t
38	\N	01 03 07 19  recruitment-application	SQL	03-hrm/07-recruitment/R__01_03_07_19__recruitment-application.sql	298994851	grp	2019-07-23 20:57:26.862706	3	t
39	\N	01 03 07 20  recruitment-management	SQL	03-hrm/07-recruitment/R__01_03_07_20__recruitment-management.sql	-1807586365	grp	2019-07-23 20:57:26.87039	3	t
40	\N	01 03 07 21  recruitment-exam-management	SQL	03-hrm/07-recruitment/R__01_03_07_21__recruitment-exam-management.sql	-449649762	grp	2019-07-23 20:57:26.876544	10	t
41	\N	01 03 07 22  recruitment-management	SQL	03-hrm/07-recruitment/R__01_03_07_22__recruitment-management.sql	-1644324784	grp	2019-07-23 20:57:26.890492	5	t
42	\N	01 03 08 23  posting-rules-configure	SQL	03-hrm/08-posting-and-promotion/R__01_03_08_23__posting-rules-configure.sql	-1307329362	grp	2019-07-23 20:57:26.898417	4	t
43	\N	01 03 08 24  promotion-rules-configure	SQL	03-hrm/08-posting-and-promotion/R__01_03_08_24__promotion-rules-configure.sql	103148864	grp	2019-07-23 20:57:26.90487	2	t
44	\N	01 03 08 25  posting-and-promotion-info-management	SQL	03-hrm/08-posting-and-promotion/R__01_03_08_25__posting-and-promotion-info-management.sql	-1716881174	grp	2019-07-23 20:57:26.9094	8	t
45	\N	01 03 09 26  attendance-rules-configure	SQL	03-hrm/09-attendance/R__01_03_09_26__attendance-rules-configure.sql	274346424	grp	2019-07-23 20:57:26.921458	2	t
46	\N	01 03 09 27  daily-attendance-management	SQL	03-hrm/09-attendance/R__01_03_09_27__daily-attendance-management.sql	1278998178	grp	2019-07-23 20:57:26.926881	2	t
47	\N	01 03 09 28  integration-with-leave-management 	SQL	03-hrm/09-attendance/R__01_03_09_28__integration-with-leave-management .sql	997072425	grp	2019-07-23 20:57:26.931734	2	t
48	\N	01 03 09 29  daily-attendance-management	SQL	03-hrm/09-attendance/R__01_03_09_29__daily-attendance-management.sql	2072888660	grp	2019-07-23 20:57:26.936639	2	t
49	\N	01 03 09 30  attendance-report	SQL	03-hrm/09-attendance/R__01_03_09_30__attendance-report.sql	15244665	grp	2019-07-23 20:57:26.941624	10	t
50	\N	01 03 10 31  acr-apa-rules-configure	SQL	03-hrm/10-acr/R__01_03_10_31__acr-apa-rules-configure.sql	-1734015105	grp	2019-07-23 20:57:26.954974	2	t
51	\N	01 03 10 32  employee-evaluation	SQL	03-hrm/10-acr/R__01_03_10_32__employee-evaluation.sql	2139516689	grp	2019-07-23 20:57:26.960618	2	t
52	\N	01 03 10 33  reviewing-acr-forms	SQL	03-hrm/10-acr/R__01_03_10_33__reviewing-acr-forms.sql	1626876196	grp	2019-07-23 20:57:26.964797	7	t
53	\N	01 03 10 34  submission-of-acr-forms	SQL	03-hrm/10-acr/R__01_03_10_34__submission-of-acr-forms.sql	1598367782	grp	2019-07-23 20:57:26.97866	16	t
54	\N	01 03 11 35  discipline-rules-configure	SQL	03-hrm/11-discipline-investigation/R__01_03_11_35__discipline-rules-configure.sql	-978752116	grp	2019-07-23 20:57:26.997811	3	t
55	\N	01 03 11 36  discipline-configuration	SQL	03-hrm/11-discipline-investigation/R__01_03_11_36__discipline-configuration.sql	-2023868620	grp	2019-07-23 20:57:27.003704	5	t
56	\N	01 03 11 37  management-of-investigation	SQL	03-hrm/11-discipline-investigation/R__01_03_11_37__management-of-investigation.sql	1785733615	grp	2019-07-23 20:57:27.012126	9	t
57	\N	01 03 12 38  award-publication-rules-configure	SQL	03-hrm/12-award-and-publication/R__01_03_12_38__award-publication-rules-configure.sql	443598558	grp	2019-07-23 20:57:27.025572	3	t
58	\N	01 03 12 39  award-publication-configure	SQL	03-hrm/12-award-and-publication/R__01_03_12_39__award-publication-configure.sql	1949860925	grp	2019-07-23 20:57:27.032472	6	t
59	\N	01 03 12 40  award-application	SQL	03-hrm/12-award-and-publication/R__01_03_12_40__award-application.sql	2116887991	grp	2019-07-23 20:57:27.04661	3	t
60	\N	01 03 12 41  publication-application	SQL	03-hrm/12-award-and-publication/R__01_03_12_41__publication-application.sql	937425736	grp	2019-07-23 20:57:27.052765	3	t
61	\N	01 03 13 42  rules-of-vesting-configure	SQL	03-hrm/13-vesting-magisterial-power/R__01_03_13_42__rules-of-vesting-configure.sql	-656240654	grp	2019-07-23 20:57:27.058701	3	t
62	\N	01 03 13 43  vesting-of-magisterial-power	SQL	03-hrm/13-vesting-magisterial-power/R__01_03_13_43__vesting-of-magisterial-power.sql	1341181948	grp	2019-07-23 20:57:27.063939	3	t
63	\N	01 03 14 44  prl-management	SQL	03-hrm/14-retirement-termination/R__01_03_14_44__prl-management.sql	-1072137026	grp	2019-07-23 20:57:27.069167	3	t
64	\N	01 03 14 45  pension-management	SQL	03-hrm/14-retirement-termination/R__01_03_14_45__pension-management.sql	811489139	grp	2019-07-23 20:57:27.074806	4	t
65	\N	01 03 14 46  termination-management	SQL	03-hrm/14-retirement-termination/R__01_03_14_46__termination-management.sql	-940172052	grp	2019-07-23 20:57:27.080946	3	t
66	\N	01 04 01 01  committee	SQL	04-prc/01-setting/R__01_04_01_01__committee.sql	1156987992	grp	2019-07-23 20:57:27.086447	6	t
67	\N	01 04 01 02  procurement	SQL	04-prc/01-setting/R__01_04_01_02__procurement.sql	-1145349920	grp	2019-07-23 20:57:27.095289	6	t
68	\N	01 04 01 03  procurement-item	SQL	04-prc/01-setting/R__01_04_01_03__procurement-item.sql	-455464313	grp	2019-07-23 20:57:27.104168	4	t
69	\N	01 04 01 04  associate-setting	SQL	04-prc/01-setting/R__01_04_01_04__associate-setting.sql	75655108	grp	2019-07-23 20:57:27.110835	15	t
70	\N	01 04 02 05  annual-procurement-plan-generation	SQL	04-prc/02-annual-procurement-plan-generation/R__01_04_02_05__annual-procurement-plan-generation.sql	-2001119031	grp	2019-07-23 20:57:27.128789	34	t
71	\N	01 04 03 06  tendering-in-egp	SQL	04-prc/03-notification-of-award/R__01_04_03_06__tendering-in-egp.sql	-1420379867	grp	2019-07-23 20:57:27.166868	2	t
72	\N	01 04 04 07  contract-signing	SQL	04-prc/04-contract/R__01_04_04_07__contract-signing.sql	101713271	grp	2019-07-23 20:57:27.172158	2	t
73	\N	01 04 05 08  material-receive-info	SQL	04-prc/05-goods-receive/R__01_04_05_08__material-receive-info.sql	1508140151	grp	2019-07-23 20:57:27.176795	4	t
74	\N	01 04 05 09  mrv-generation	SQL	04-prc/05-goods-receive/R__01_04_05_09__mrv-generation.sql	812876749	grp	2019-07-23 20:57:27.184149	3	t
75	\N	01 04 05 10  material-receive-info	SQL	04-prc/05-goods-receive/R__01_04_05_10__material-receive-info.sql	-1453857445	grp	2019-07-23 20:57:27.189944	4	t
76	\N	01 04 06 11  report	SQL	04-prc/06-report-and-dashbord/R__01_04_06_11__report.sql	-328781798	grp	2019-07-23 20:57:27.197153	10	t
77	\N	01 04 07 12  annual-procurement-plan-generation	SQL	04-prc/07-annual-procurement-plan-generation/R__01_04_07_12__annual-procurement-plan-generation.sql	552673821	grp	2019-07-23 20:57:27.210041	4	t
78	\N	01 05 01 01  vehicle	SQL	05-ast/01-configuration/R__01_05_01_01__vehicle.sql	728552444	grp	2019-07-23 20:57:27.21795	7	t
79	\N	01 05 01 02  additional-setups	SQL	05-ast/01-configuration/R__01_05_01_02__additional-setups.sql	2131917890	grp	2019-07-23 20:57:27.22845	5	t
80	\N	01 05 01 03  methods	SQL	05-ast/01-configuration/R__01_05_01_03__methods.sql	-1545323160	grp	2019-07-23 20:57:27.237642	4	t
81	\N	01 05 01 04  rules-and-regulation	SQL	05-ast/01-configuration/R__01_05_01_04__rules-and-regulation.sql	-714173360	grp	2019-07-23 20:57:27.244532	2	t
82	\N	01 05 02 05  temporary-item	SQL	05-ast/02-commissioning-and-acquisition/R__01_05_02_05__temporary-item.sql	-1524509412	grp	2019-07-23 20:57:27.249299	11	t
83	\N	01 05 02 06  acquisition	SQL	05-ast/02-commissioning-and-acquisition/R__01_05_02_06__acquisition.sql	-1138934082	grp	2019-07-23 20:57:27.262819	12	t
84	\N	01 05 03 07  asset-requisition	SQL	05-ast/03-deployment-and-operation/R__01_05_03_07__asset-requisition.sql	703363463	grp	2019-07-23 20:57:27.277431	5	t
85	\N	01 05 03 08  return-process	SQL	05-ast/03-deployment-and-operation/R__01_05_03_08__return-process.sql	-726569127	grp	2019-07-23 20:57:27.285071	3	t
86	\N	01 05 03 09  maintenance	SQL	05-ast/03-deployment-and-operation/R__01_05_03_09__maintenance.sql	-1089683972	grp	2019-07-23 20:57:27.291017	2	t
87	\N	01 05 03 10  asset-allocation	SQL	05-ast/03-deployment-and-operation/R__01_05_03_10__asset-allocation.sql	-1594334971	grp	2019-07-23 20:57:27.295724	12	t
88	\N	01 05 03 11  contract-renewal	SQL	05-ast/03-deployment-and-operation/R__01_05_03_11__contract-renewal.sql	989009282	grp	2019-07-23 20:57:27.310668	3	t
89	\N	01 05 03 12  return-process	SQL	05-ast/03-deployment-and-operation/R__01_05_03_12__return-process.sql	1294222843	grp	2019-07-23 20:57:27.315984	3	t
90	\N	01 05 04 13  maintenance-request	SQL	05-ast/04-maintenance/R__01_05_04_13__maintenance-request.sql	1557924361	grp	2019-07-23 20:57:27.321086	6	t
91	\N	01 05 04 14  post-maintenance	SQL	05-ast/04-maintenance/R__01_05_04_14__post-maintenance.sql	2049264825	grp	2019-07-23 20:57:27.330158	3	t
92	\N	01 05 04 15  auto-maintenance	SQL	05-ast/04-maintenance/R__01_05_04_15__auto-maintenance.sql	2112835259	grp	2019-07-23 20:57:27.335918	3	t
93	\N	01 05 04 16  payment-for-maintenance	SQL	05-ast/04-maintenance/R__01_05_04_16__payment-for-maintenance.sql	203496410	grp	2019-07-23 20:57:27.341311	3	t
94	\N	01 05 05 17  vehicle-acquisition	SQL	05-ast/05-vehicle-management/R__01_05_05_17__vehicle-acquisition.sql	911139483	grp	2019-07-23 20:57:27.34649	3	t
95	\N	01 05 05 18  route	SQL	05-ast/05-vehicle-management/R__01_05_05_18__route.sql	-438830958	grp	2019-07-23 20:57:27.352287	6	t
96	\N	01 05 05 19  driver	SQL	05-ast/05-vehicle-management/R__01_05_05_19__driver.sql	-1814446055	grp	2019-07-23 20:57:27.361068	3	t
97	\N	01 05 05 20  vehicle-requisition	SQL	05-ast/05-vehicle-management/R__01_05_05_20__vehicle-requisition.sql	-482473311	grp	2019-07-23 20:57:27.367462	5	t
98	\N	01 05 05 21  vehicle-allocation	SQL	05-ast/05-vehicle-management/R__01_05_05_21__vehicle-allocation.sql	-1050663742	grp	2019-07-23 20:57:27.37587	3	t
99	\N	01 05 05 22  vehicle-return	SQL	05-ast/05-vehicle-management/R__01_05_05_22__vehicle-return.sql	-1265202274	grp	2019-07-23 20:57:27.382236	4	t
100	\N	01 05 05 23  fuel	SQL	05-ast/05-vehicle-management/R__01_05_05_23__fuel.sql	-1272777610	grp	2019-07-23 20:57:27.390081	12	t
101	\N	01 05 06 24  asset-disposal-request	SQL	05-ast/06-dicommisioning-and-disposal/R__01_05_06_24__asset-disposal-request.sql	-516104815	grp	2019-07-23 20:57:27.405234	12	t
102	\N	01 05 06 25  asset-lost-or-damage	SQL	05-ast/06-dicommisioning-and-disposal/R__01_05_06_25__asset-lost-or-damage.sql	1821425120	grp	2019-07-23 20:57:27.420574	3	t
103	\N	01 05 07 26  activity-log	SQL	05-ast/07-asset-tracking/R__01_05_07_26__activity-log.sql	1294984280	grp	2019-07-23 20:57:27.428194	6	t
104	\N	03 00 00 00  createIndex	SQL	R__03_00_00_00__createIndex.sql	1518585257	grp	2019-07-23 20:57:27.436745	67	t
105	\N	04 00 00 00  createFunction	SQL	R__04_00_00_00__createFunction.sql	0	grp	2019-07-23 20:57:27.507381	0	t
106	\N	05 00 00 00  createTrigger	SQL	R__05_00_00_00__createTrigger.sql	0	grp	2019-07-23 20:57:27.510407	0	t
107	\N	06 00 00 00  createView	SQL	R__06_00_00_00__createView.sql	0	grp	2019-07-23 20:57:27.51338	0	t
108	\N	07 01 01 01  ministry	SQL	01-cmn/01-base/R__07_01_01_01__ministry.sql	-703510332	grp	2019-07-23 20:57:27.522784	3	t
109	\N	07 01 01 02  office	SQL	01-cmn/01-base/R__07_01_01_02__office.sql	-611813847	grp	2019-07-23 20:57:27.704415	179	t
110	\N	07 01 01 03  geo	SQL	01-cmn/01-base/R__07_01_01_03__geo.sql	1233939020	grp	2019-07-23 20:57:27.711779	4	t
111	\N	07 01 02 04  fiscal-year	SQL	01-cmn/02-configuration/R__07_01_02_04__fiscal-year.sql	-311088126	grp	2019-07-23 20:57:27.717335	2	t
112	\N	07 01 02 05  unit-of-measure	SQL	01-cmn/02-configuration/R__07_01_02_05__unit-of-measure.sql	265796916	grp	2019-07-23 20:57:27.723293	3	t
113	\N	07 01 02 06  item	SQL	01-cmn/02-configuration/R__07_01_02_06__item.sql	-124036615	grp	2019-07-23 20:57:27.733858	8	t
114	\N	07 01 02 10  vendor	SQL	01-cmn/02-configuration/R__07_01_02_10__vendor.sql	-478799964	grp	2019-07-23 20:57:27.738212	2	t
115	\N	07 02 01 01  application	SQL	02-sec/01-base/R__07_02_01_01__application.sql	-147101550	grp	2019-07-23 20:57:27.790906	50	t
116	\N	07 02 01 02  authentication	SQL	02-sec/01-base/R__07_02_01_02__authentication.sql	1124468987	grp	2019-07-23 20:57:27.822494	29	t
117	\N	07 02 01 03  authorization	SQL	02-sec/01-base/R__07_02_01_03__authorization.sql	299841935	grp	2019-07-23 20:57:27.831344	6	t
118	\N	07 03 01 02  employee	SQL	03-hrm/01-base/R__07_03_01_02__employee.sql	-1817366377	grp	2019-07-23 20:57:27.836961	3	t
119	\N	07 04 01 02  procurement	SQL	04-prc/01-setting/R__07_04_01_02__procurement.sql	660817155	grp	2019-07-23 20:57:27.842891	4	t
120	\N	07 04 01 04  associate-setting	SQL	04-prc/01-setting/R__07_04_01_04__associate-setting.sql	-740600698	grp	2019-07-23 20:57:27.84786	3	t
121	\N	07 04 02 05  annual-procurement-plan-generation	SQL	04-prc/02-annual-procurement-plan-generation/R__07_04_02_05__annual-procurement-plan-generation.sql	110882779	grp	2019-07-23 20:57:27.870207	20	t
122	\N	07 05 02 05  temporary-item	SQL	05-ast/02-commissioning-and-acquisition/R__07_05_02_05__temporary-item.sql	1346930990	grp	2019-07-23 20:57:27.87681	3	t
\.


--
-- Data for Name: oauth_access_token; Type: TABLE DATA; Schema: public; Owner: grp
--

COPY public.oauth_access_token (token_id, token, authentication_id, user_name, client_id, authentication, refresh_token) FROM stdin;
2cf1319f6d00a899687ee9d66325fa91	\\xaced0005737200436f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f4175746832416363657373546f6b656e0cb29e361b24face0200064c00156164646974696f6e616c496e666f726d6174696f6e74000f4c6a6176612f7574696c2f4d61703b4c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b4c000c72656672657368546f6b656e74003f4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f636f6d6d6f6e2f4f417574683252656672657368546f6b656e3b4c000573636f706574000f4c6a6176612f7574696c2f5365743b4c0009746f6b656e547970657400124c6a6176612f6c616e672f537472696e673b4c000576616c756571007e00057870737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f72646572787200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f40000000000001770800000002000000017400036a746974002432663965336463652d343936392d346663622d386364332d36393461656236376366383278007372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c6612ca4b787372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e71007e0002787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c756571007e0005787074019565794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a7a61476c79623342684969776963324e76634755694f6c7369636d56685a434973496e64796158526c496c3073496d463061534936496a4a6d4f57557a5a474e6c4c5451354e6a6b744e475a6a596930345932517a4c5459354e47466c596a5933593259344d694973496d5634634349364d5455324e5441344d4459774d7977695958563061473979615852705a584d694f6c736951564e554c6b4654553056554c556c4f4c554e4951564a4852534973496b46545643355551556448535535484c55564f56456c5557534a644c434a7164476b694f6949314e7a55334f5445334d4330774e544d774c54517a5a6a63744f5467335979316c4f5749774e544a6d59544d32595455694c434a6a62476c6c626e5266615751694f694a6e636e4174643256694c584276636e526862434a392e72714136696e734150627a31496f6f444e706e73705273724a526c53677a736d6577334168324c6a6649557371007e000c77080000016c6612ca4b78737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c0001637400164c6a6176612f7574696c2f436f6c6c656374696f6e3b7870737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f400000000000027400047265616474000577726974657874000662656172657274015965794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a6c654841694f6a45314e6a55774f4441324d444d73496e567a5a584a66626d46745a534936496e4e6f61584a76634745694c434a686458526f62334a7064476c6c6379493657794a425531517551564e54525651745355347451306842556b64464969776951564e554c6c52425230644a546b6374525535555356525a496c3073496d703061534936496a4a6d4f57557a5a474e6c4c5451354e6a6b744e475a6a596930345932517a4c5459354e47466c596a5933593259344d694973496d4e7361575675644639705a434936496d6479634331335a57497463473979644746734969776963324e76634755694f6c7369636d56685a434973496e64796158526c496c31392e6131596e6764376d485069646c386c49366966326c44736d763750504f56364966666737646f6f577a5477	3f1bd6a709bc1c29fc313580e6927ea9	shiropa	grp-web-portal	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b78707400134153542e41535345542d494e2d4348415247457371007e000d7400124153542e54414747494e472d454e544954597871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0016787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0016787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000067708000000080000000374000a6772616e745f7479706574000870617373776f726474000b63726564656e7469616c7374000474727565740008757365726e616d65740007736869726f706178737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e0027770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e001c3f40000000000000770800000010000000007870707371007e0027770c000000103f40000000000000787371007e0027770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0036737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e001c3f400000000000067708000000080000000371007e001e71007e001f71007e002071007e002171007e002271007e00237800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00164c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0024737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000271007e000f71007e0011787071007e0023740003313433	a1a68f312bad821165fc3740ede45178
f2bcbfd0b0ee0f61d222dc40c632ec67	\\xaced0005737200436f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f4175746832416363657373546f6b656e0cb29e361b24face0200064c00156164646974696f6e616c496e666f726d6174696f6e74000f4c6a6176612f7574696c2f4d61703b4c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b4c000c72656672657368546f6b656e74003f4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f636f6d6d6f6e2f4f417574683252656672657368546f6b656e3b4c000573636f706574000f4c6a6176612f7574696c2f5365743b4c0009746f6b656e547970657400124c6a6176612f6c616e672f537472696e673b4c000576616c756571007e00057870737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f72646572787200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c770800000010000000017400036a746974002439656366383561392d643332612d346463322d383566642d33613430626234343835303978007372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c66231cbf787372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e71007e0002787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c756571007e0005787074019565794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a7a6147467662694973496e4e6a6233426c496a7062496e4a6c595751694c434a33636d6c305a534a644c434a6864476b694f6949355a574e6d4f4456684f53316b4d7a4a684c54526b597a49744f44566d5a43307a59545177596d49304e4467314d446b694c434a6c654841694f6a45314e6a55774f4445324e7a4973496d463164476876636d6c306157567a496a7062496b465456433552517931445430314e53565255525555694c434a42553151755245465551533146546c525357533150554556535156525055694a644c434a7164476b694f694935593245775a4459795a69316c597a63314c5452684e6a49744f54597a4e7930325a6a67344e324e6a597a6b784f4441694c434a6a62476c6c626e5266615751694f694a6e636e4174643256694c584276636e526862434a392e634a345372526c644f767a65585241495f616f6b677061686b324371424d704969494166763963513650597371007e000c77080000016c66231cbf78737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c0001637400164c6a6176612f7574696c2f436f6c6c656374696f6e3b7870737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000043f400000000000027400047265616474000577726974657874000662656172657274015965794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a6c654841694f6a45314e6a55774f4445324e7a4973496e567a5a584a66626d46745a534936496e4e6f59573975496977695958563061473979615852705a584d694f6c736951564e554c6c46444c554e505455314a5646524652534973496b465456433545515652424c55564f56464a5a4c55395152564a4256453953496c3073496d703061534936496a6c6c593259344e5745354c57517a4d6d45744e47526a4d6930344e575a6b4c544e684e444269596a51304f4455774f534973496d4e7361575675644639705a434936496d6479634331335a57497463473979644746734969776963324e76634755694f6c7369636d56685a434973496e64796158526c496c31392e3831534a5a5144565f76484a377944796548326d6b4e78774a61562d3647594a564a7a4f387371584a3777	aa0f4e72ecf86d04df0ba658a9015a4c	shaon	grp-web-portal	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b78707400174153542e444154412d454e5452592d4f50455241544f527371007e000d7400104153542e51432d434f4d4d49545445457871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0016787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0016787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000067708000000080000000374000a6772616e745f7479706574000870617373776f726474000b63726564656e7469616c7374000474727565740008757365726e616d657400057368616f6e78737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e0027770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e001c3f40000000000000770800000010000000007870707371007e0027770c000000103f40000000000000787371007e0027770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0036737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e001c3f400000000000067708000000080000000371007e001e71007e001f71007e002071007e002171007e002271007e00237800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00164c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0024737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000271007e000f71007e0011787071007e0023740003313433	cd491123bbf261e5e76fd6946895f5c7
2ba064a65a96cdfc042f20f63f5df811	\\xaced0005737200436f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f4175746832416363657373546f6b656e0cb29e361b24face0200064c00156164646974696f6e616c496e666f726d6174696f6e74000f4c6a6176612f7574696c2f4d61703b4c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b4c000c72656672657368546f6b656e74003f4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f636f6d6d6f6e2f4f417574683252656672657368546f6b656e3b4c000573636f706574000f4c6a6176612f7574696c2f5365743b4c0009746f6b656e547970657400124c6a6176612f6c616e672f537472696e673b4c000576616c756571007e00057870737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f72646572787200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c770800000010000000017400036a746974002462643164656433342d363532642d343034372d623962612d34393338373333653463633778007372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c61117891787372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e71007e0002787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c756571007e0005787074017865794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a685a47317062694973496e4e6a6233426c496a7062496e4a6c595751694c434a33636d6c305a534a644c434a6864476b694f694a695a44466b5a57517a4e4330324e544a6b4c5451774e446374596a6c69595330304f544d344e7a4d7a5a54526a597a63694c434a6c654841694f6a45314e6a51354f5459324d7a4173496d463164476876636d6c306157567a496a7062496c42535179355152534973496c4253517935505a6d5a7059325679496c3073496d703061534936496a5268596d4e6a4e6a686a4c544d77593259744e47457759693035596a59354c574a684d6a4a694e4751305a4463314d534973496d4e7361575675644639705a434936496d6479634331335a5749746347397964474673496e302e69555375444e707442502d63414a782d487278317546614f4257664f446b6e42456b64496c474769484c347371007e000c77080000016c6111789178737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c0001637400164c6a6176612f7574696c2f436f6c6c656374696f6e3b7870737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000043f400000000000027400047265616474000577726974657874000662656172657274013c65794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a6c654841694f6a45314e6a51354f5459324d7a4173496e567a5a584a66626d46745a534936496d466b62576c75496977695958563061473979615852705a584d694f6c736955464a444c6c42464969776955464a444c6b396d5a6d6c6a5a58496958537769616e5270496a6f69596d51785a47566b4d7a51744e6a55795a4330304d4451334c574935596d45744e446b7a4f44637a4d32553059324d3349697769593278705a57353058326c6b496a6f695a334a774c58646c5969317762334a30595777694c434a7a593239775a53493657794a795a57466b4969776964334a70644755695858302e3247344368566e453835415450514e7978553441534a746c7459344c465a5341765069795479536f766e6f	2b52b486425d3eec6d0710c476e763ff	admin	grp-web-portal	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000b5052432e4f6666696365727371007e000d7400065052432e50457871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0016787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0016787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000067708000000080000000374000a6772616e745f7479706574000870617373776f726474000b63726564656e7469616c7374000474727565740008757365726e616d6574000561646d696e78737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e0027770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e001c3f40000000000000770800000010000000007870707371007e0027770c000000103f40000000000000787371007e0027770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0036737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e001c3f400000000000067708000000080000000371007e001e71007e001f71007e002071007e002171007e002271007e00237800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00164c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0024737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000271007e000f71007e0011787071007e002370	87c48efa75c746b912e6484871e30c9d
c24db2473c7a6bdea7e58660972facda	\\xaced0005737200436f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f4175746832416363657373546f6b656e0cb29e361b24face0200064c00156164646974696f6e616c496e666f726d6174696f6e74000f4c6a6176612f7574696c2f4d61703b4c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b4c000c72656672657368546f6b656e74003f4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f636f6d6d6f6e2f4f417574683252656672657368546f6b656e3b4c000573636f706574000f4c6a6176612f7574696c2f5365743b4c0009746f6b656e547970657400124c6a6176612f6c616e672f537472696e673b4c000576616c756571007e00057870737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f72646572787200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c770800000010000000017400036a746974002463643031666537332d346632622d343933322d386530632d63306434306463623338326178007372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c669f8939787372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e71007e0002787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c756571007e0005787074017965794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a3059584e6b615845694c434a7a593239775a53493657794a795a57466b4969776964334a70644755695853776959585270496a6f69593251774d575a6c4e7a4d744e475979596930304f544d794c54686c4d474d74597a426b4e44426b5932497a4f444a68496977695a586877496a6f784e5459314d4467354f4449334c434a686458526f62334a7064476c6c6379493657794a51556b4d75554555694c434a51556b4d7554325a6d61574e6c63694a644c434a7164476b694f6949314d446c684d474e695a4330355a5449774c5451774d544d744f446b355969307a597a63314d324e6b595759314f4455694c434a6a62476c6c626e5266615751694f694a6e636e4174643256694c584276636e526862434a392e5162704468666d4742343648596a2d435663417a466d7468504b4c6d76635a3072616f647a557141744c457371007e000c77080000016c669f893978737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c0001637400164c6a6176612f7574696c2f436f6c6c656374696f6e3b7870737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000043f400000000000027400047265616474000577726974657874000662656172657274013d65794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a6c654841694f6a45314e6a55774f446b344d6a6373496e567a5a584a66626d46745a534936496e52686332527063534973496d463164476876636d6c306157567a496a7062496c42535179355152534973496c4253517935505a6d5a7059325679496c3073496d703061534936496d4e6b4d44466d5a54637a4c54526d4d6d49744e446b7a4d6930345a54426a4c574d775a4451775a474e694d7a677959534973496d4e7361575675644639705a434936496d6479634331335a57497463473979644746734969776963324e76634755694f6c7369636d56685a434973496e64796158526c496c31392e6341414b5674586e4676455f703239486734545754426551634362566672735454496e5775675071457530	c34d03d564357dc51726f2c46134dcd7	tasdiq	grp-web-portal	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000b5052432e4f6666696365727371007e000d7400065052432e50457871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0016787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0016787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000067708000000080000000374000a6772616e745f7479706574000870617373776f726474000b63726564656e7469616c7374000474727565740008757365726e616d6574000674617364697178737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e0027770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e001c3f40000000000000770800000010000000007870707371007e0027770c000000103f40000000000000787371007e0027770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0036737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e001c3f400000000000067708000000080000000371007e001e71007e001f71007e002071007e002171007e002271007e00237800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00164c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0024737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000271007e000f71007e0011787071007e0023740003313433	26d3729f0ea1af22c33e49fea3dda499
88ac060830f09ca6e27e56eeea7835b2	\\xaced0005737200436f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f4175746832416363657373546f6b656e0cb29e361b24face0200064c00156164646974696f6e616c496e666f726d6174696f6e74000f4c6a6176612f7574696c2f4d61703b4c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b4c000c72656672657368546f6b656e74003f4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f636f6d6d6f6e2f4f417574683252656672657368546f6b656e3b4c000573636f706574000f4c6a6176612f7574696c2f5365743b4c0009746f6b656e547970657400124c6a6176612f6c616e672f537472696e673b4c000576616c756571007e00057870737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f72646572787200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c770800000010000000017400036a746974002465393763363961612d613830322d343465322d383661302d63306266613936356137373678007372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c667bdf75787372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e71007e0002787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c756571007e000578707401b765794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a305957357062534973496e4e6a6233426c496a7062496e4a6c595751694c434a33636d6c305a534a644c434a6864476b694f694a6c4f54646a4e6a6c68595331684f4441794c5451305a5449744f445a684d43316a4d474a6d59546b324e5745334e7a59694c434a6c654841694f6a45314e6a55774f4463304f446b73496d463164476876636d6c306157567a496a7062496b465456433552517931445430314e53565255525555694c434a425531517551564e54525651745355347451306842556b64464969776955464a444c6c42464969776951564e554c6b4654553056554c553142546b46485256496958537769616e5270496a6f6959324d32597a55304e5451744f4745344e4330304f4451774c5749345a6a4d74596d45344f5451784e474a685957493349697769593278705a57353058326c6b496a6f695a334a774c58646c5969317762334a305957776966512e6641486a764548514f7364755879445358436449486a4c306b744f4674446e4e346f3153765333797253637371007e000c77080000016c667bdf7578737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c0001637400164c6a6176612f7574696c2f436f6c6c656374696f6e3b7870737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000043f400000000000027400047265616474000577726974657874000662656172657274017b65794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a6c654841694f6a45314e6a55774f4463304f446b73496e567a5a584a66626d46745a534936496e5268626d6c74496977695958563061473979615852705a584d694f6c736951564e554c6c46444c554e505455314a5646524652534973496b46545643354255314e465643314a5469314453454653523055694c434a51556b4d75554555694c434a425531517551564e54525651745455464f5155644655694a644c434a7164476b694f694a6c4f54646a4e6a6c68595331684f4441794c5451305a5449744f445a684d43316a4d474a6d59546b324e5745334e7a59694c434a6a62476c6c626e5266615751694f694a6e636e4174643256694c584276636e526862434973496e4e6a6233426c496a7062496e4a6c595751694c434a33636d6c305a534a6466512e33644c55694c786739556f4d4a6f6e2d314e38794d68564735334d4b6849444e654550344b555342366545	bc070660f73d1f94b5acb7fbe4a00ae0	tanim	grp-web-portal	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000004770400000004737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b78707400134153542e41535345542d494e2d4348415247457371007e000d7400114153542e41535345542d4d414e414745527371007e000d7400104153542e51432d434f4d4d49545445457371007e000d7400065052432e50457871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e001a787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00184c000573636f706571007e001a787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00187870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000037708000000040000000274000a6772616e745f7479706574000870617373776f7264740008757365726e616d6574000574616e696d78737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e0029770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e00203f40000000000000770800000010000000007870707371007e0029770c000000103f40000000000000787371007e0029770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000477040000000471007e000f71007e001171007e001371007e00157871007e0038737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e00203f400000000000067708000000080000000271007e002271007e002371007e002471007e00257800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e001a4c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0026737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000471007e000f71007e001171007e001371007e0015787071007e0025740003313433	ce997bd975ec574068477de064619fab
fe4dff5e2e8ef70019a1729f4c35980f	\\xaced0005737200436f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f4175746832416363657373546f6b656e0cb29e361b24face0200064c00156164646974696f6e616c496e666f726d6174696f6e74000f4c6a6176612f7574696c2f4d61703b4c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b4c000c72656672657368546f6b656e74003f4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f636f6d6d6f6e2f4f417574683252656672657368546f6b656e3b4c000573636f706574000f4c6a6176612f7574696c2f5365743b4c0009746f6b656e547970657400124c6a6176612f6c616e672f537472696e673b4c000576616c756571007e00057870737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f72646572787200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c770800000010000000017400036a746974002432613964396130352d386661352d343164362d613564632d30636136383939623336666178007372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c611204dc787372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e71007e0002787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c756571007e0005787074017865794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a715957747063694973496e4e6a6233426c496a7062496e4a6c595751694c434a33636d6c305a534a644c434a6864476b694f69497959546c6b4f5745774e5330345a6d45314c5451785a4459745954566b59793077593245324f446b35596a4d325a6d45694c434a6c654841694f6a45314e6a51354f5459324e6a5973496d463164476876636d6c306157567a496a7062496c42535179355152534973496c4253517935505a6d5a7059325679496c3073496d703061534936496a41304e474a685a574d784c5451784e6a51744e4467314e5330344f54646b4c545532597a63785a5759355a5467344e534973496d4e7361575675644639705a434936496d6479634331335a5749746347397964474673496e302e7446595a72456932484777445a354243527436424668425868575761633339555577414d4a757a5a4a36497371007e000c77080000016c611204dc78737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c0001637400164c6a6176612f7574696c2f436f6c6c656374696f6e3b7870737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000043f400000000000027400047265616474000577726974657874000662656172657274013c65794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a6c654841694f6a45314e6a51354f5459324e6a5973496e567a5a584a66626d46745a534936496d706861326c79496977695958563061473979615852705a584d694f6c736955464a444c6c42464969776955464a444c6b396d5a6d6c6a5a58496958537769616e5270496a6f694d6d45355a446c684d4455744f475a684e5330304d5751324c5745315a474d744d474e684e6a67354f57497a4e6d5a6849697769593278705a57353058326c6b496a6f695a334a774c58646c5969317762334a30595777694c434a7a593239775a53493657794a795a57466b4969776964334a70644755695858302e45686643514b59554f725a47324158385f4e3670774d586e3449795137684454706b793534426e466a3534	776cf647434b2b5f568d85c774f1e1d7	jakir	grp-web-portal	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000b5052432e4f6666696365727371007e000d7400065052432e50457871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0016787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0016787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000067708000000080000000374000a6772616e745f7479706574000870617373776f726474000b63726564656e7469616c7374000474727565740008757365726e616d657400056a616b697278737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e0027770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e001c3f40000000000000770800000010000000007870707371007e0027770c000000103f40000000000000787371007e0027770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0036737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e001c3f400000000000067708000000080000000371007e001e71007e001f71007e002071007e002171007e002271007e00237800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00164c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0024737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000271007e000f71007e0011787071007e0023740003313433	c60b588f5a1f1b0a64cbc57f50004b52
\.


--
-- Data for Name: oauth_client_details; Type: TABLE DATA; Schema: public; Owner: grp
--

COPY public.oauth_client_details (client_id, resource_ids, client_secret, scope, authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, refresh_token_validity, additional_information, autoapprove) FROM stdin;
grp-web-portal	\N	$2a$10$Nj9AB/ffFQX/FEj3KgdmLe9vu6NlVMfBrG6/bsq9zD0eUAlMMJ6o.	read,write	password,refresh_token,client_credentials,authorization_code	\N	ROLE_CLIENT,ROLE_TRUSTED_CLIENT	86400	86400	\N	\N
\.


--
-- Data for Name: oauth_client_token; Type: TABLE DATA; Schema: public; Owner: grp
--

COPY public.oauth_client_token (token_id, token, authentication_id, user_name, client_id) FROM stdin;
\.


--
-- Data for Name: oauth_code; Type: TABLE DATA; Schema: public; Owner: grp
--

COPY public.oauth_code (code, authentication) FROM stdin;
\.


--
-- Data for Name: oauth_refresh_token; Type: TABLE DATA; Schema: public; Owner: grp
--

COPY public.oauth_refresh_token (token_id, token, authentication) FROM stdin;
3ce3f083cc301d639b99c0d5ce66a06f	\\xaced00057372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c75657400124c6a6176612f6c616e672f537472696e673b787074017965794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a3059584e6b615845694c434a7a593239775a53493657794a795a57466b4969776964334a70644755695853776959585270496a6f694f4449314e4755325a6d51744e4441324d53303059324d304c5467784e5455744d5451785a57566b4e6a45774d574e68496977695a586877496a6f784e5459304e6a51784e6a63334c434a686458526f62334a7064476c6c6379493657794a51556b4d75554555694c434a51556b4d7554325a6d61574e6c63694a644c434a7164476b694f6949354e6d4d304d575a684e793034596d45334c5451334e5749744f4463324d4331685954426d4e6d49304e4459315a574d694c434a6a62476c6c626e5266615751694f694a6e636e4174643256694c584276636e526862434a392e3667356e5f3773517a77414675596b78624d3357334f4f6243636171756355366e4f396c4f507931366f307372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c4be950b078	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000b5052432e4f6666696365727371007e000d7400065052432e50457871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0016787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0016787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000067708000000080000000374000a6772616e745f7479706574000870617373776f726474000b63726564656e7469616c7374000474727565740008757365726e616d6574000674617364697178737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e0027770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e001c3f40000000000000770800000010000000007870707371007e0027770c000000103f40000000000000787371007e0027770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0036737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e001c3f400000000000067708000000080000000371007e001e71007e001f71007e002071007e002171007e002271007e00237800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00164c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0024737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000271007e000f71007e0011787071007e0023740003313433
0540134adfa2dedaca534833012cb5e3	\\xaced00057372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c75657400124c6a6176612f6c616e672f537472696e673b787074017865794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a725957316862434973496e4e6a6233426c496a7062496e4a6c595751694c434a33636d6c305a534a644c434a6864476b694f69497a4f4449774d6a4a68596930794d6a55354c5451324e6a4d744f5463784d5330324e5463795a4751334d7a55785a6d55694c434a6c654841694f6a45314e6a517a4e7a67354d444973496d463164476876636d6c306157567a496a7062496c42535179355152534973496c4253517935505a6d5a7059325679496c3073496d703061534936496d55324d3251784f4745314c54466a4e5441744e4456684d433034596a49344c5755344e4755314d446c6b4d44597a4f434973496d4e7361575675644639705a434936496d6479634331335a5749746347397964474673496e302e6f777972546e346e4143304d53686550684f695a446b6c346f372d524a61495f3862794635684a796e48417372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c3c3fb04178	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000b5052432e4f6666696365727371007e000d7400065052432e50457871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0016787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0016787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000067708000000080000000374000a6772616e745f7479706574000870617373776f726474000b63726564656e7469616c7374000474727565740008757365726e616d657400056b616d616c78737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e0027770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e001c3f40000000000000770800000010000000007870707371007e0027770c000000103f40000000000000787371007e0027770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0036737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e001c3f400000000000067708000000080000000371007e001e71007e001f71007e002071007e002171007e002271007e00237800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00164c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0024737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000271007e000f71007e0011787071007e0023740003313433
87c48efa75c746b912e6484871e30c9d	\\xaced00057372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c75657400124c6a6176612f6c616e672f537472696e673b787074017865794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a685a47317062694973496e4e6a6233426c496a7062496e4a6c595751694c434a33636d6c305a534a644c434a6864476b694f694a695a44466b5a57517a4e4330324e544a6b4c5451774e446374596a6c69595330304f544d344e7a4d7a5a54526a597a63694c434a6c654841694f6a45314e6a51354f5459324d7a4173496d463164476876636d6c306157567a496a7062496c42535179355152534973496c4253517935505a6d5a7059325679496c3073496d703061534936496a5268596d4e6a4e6a686a4c544d77593259744e47457759693035596a59354c574a684d6a4a694e4751305a4463314d534973496d4e7361575675644639705a434936496d6479634331335a5749746347397964474673496e302e69555375444e707442502d63414a782d487278317546614f4257664f446b6e42456b64496c474769484c347372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c6111789178	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000b5052432e4f6666696365727371007e000d7400065052432e50457871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0016787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0016787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000067708000000080000000374000a6772616e745f7479706574000870617373776f726474000b63726564656e7469616c7374000474727565740008757365726e616d6574000561646d696e78737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e0027770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e001c3f40000000000000770800000010000000007870707371007e0027770c000000103f40000000000000787371007e0027770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0036737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e001c3f400000000000067708000000080000000371007e001e71007e001f71007e002071007e002171007e002271007e00237800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00164c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0024737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000271007e000f71007e0011787071007e002370
b21cd4832b298b107d345f2247527978	\\xaced00057372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c75657400124c6a6176612f6c616e672f537472696e673b787074017865794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a685a47317062694973496e4e6a6233426c496a7062496e4a6c595751694c434a33636d6c305a534a644c434a6864476b694f6949304d6a45794d5441304d79316b4d546b344c5451785a5463744f575a6d4d6930774f5745354d324a6a4d6d51324d5755694c434a6c654841694f6a45314e6a517a4f5449314f445173496d463164476876636d6c306157567a496a7062496c42535179355152534973496c4253517935505a6d5a7059325679496c3073496d703061534936496a597859545932596a41304c5751304d6d4d744e446b345a5331695954526b4c57566d59574d314d7a5668595749774e534973496d4e7361575675644639705a434936496d6479634331335a5749746347397964474673496e302e4f3049656d6c3569324d4c5767794831564d43686266507965723271705832785641454e2d2d6a434e30637372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c3d10769878	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000b5052432e4f6666696365727371007e000d7400065052432e50457871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0016787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0016787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000067708000000080000000374000a6772616e745f7479706574000870617373776f726474000b63726564656e7469616c7374000474727565740008757365726e616d6574000561646d696e78737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e0027770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e001c3f40000000000000770800000010000000007870707371007e0027770c000000103f40000000000000787371007e0027770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0036737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e001c3f400000000000067708000000080000000371007e001e71007e001f71007e002071007e002171007e002271007e00237800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00164c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0024737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000271007e000f71007e0011787071007e002370
f0c385a26a447163e4bb04fe72785aeb	\\xaced00057372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c75657400124c6a6176612f6c616e672f537472696e673b787074017865794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a715957747063694973496e4e6a6233426c496a7062496e4a6c595751694c434a33636d6c305a534a644c434a6864476b694f694a694d7a55344e44566b4e4330784e7a497a4c54517a596a4d744f4459785a4330335a54426d4e6a6b31595467795a6a49694c434a6c654841694f6a45314e6a51304e6a63314d544173496d463164476876636d6c306157567a496a7062496c42535179355152534973496c4253517935505a6d5a7059325679496c3073496d703061534936496a52684f446c6d4e544a6c4c5441774d324d744e4449795a5331694f5752694c575a6b5a4449305a6a566c4d324d334d434973496d4e7361575675644639705a434936496d6479634331335a5749746347397964474673496e302e666a31354a52397877395a4866527a53775267504230344c7845737737326a5047535835473673427971387372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c4187bc4c78	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000b5052432e4f6666696365727371007e000d7400065052432e50457871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0016787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0016787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000067708000000080000000374000a6772616e745f7479706574000870617373776f726474000b63726564656e7469616c7374000474727565740008757365726e616d657400056a616b697278737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e0027770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e001c3f40000000000000770800000010000000007870707371007e0027770c000000103f40000000000000787371007e0027770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0036737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e001c3f400000000000067708000000080000000371007e001e71007e001f71007e002071007e002171007e002271007e00237800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00164c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0024737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000271007e000f71007e0011787071007e0023740003313433
303d790c631f4bc1c57aa75250598554	\\xaced00057372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c75657400124c6a6176612f6c616e672f537472696e673b787074019565794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a7a6147467662694973496e4e6a6233426c496a7062496e4a6c595751694c434a33636d6c305a534a644c434a6864476b694f694a6d4f5463334e5756695a5330304f4441314c54526b5a444d745954517859693077593245325a4751344e7a51304e7a41694c434a6c654841694f6a45314e6a51324e4459784f444173496d463164476876636d6c306157567a496a7062496b465456433552517931445430314e53565255525555694c434a42553151755245465551533146546c525357533150554556535156525055694a644c434a7164476b694f6949335a6d51314f44686d597930784d324e6d4c5451334f57517459544a6c4e793034596d51344d324a694f5456695a444d694c434a6a62476c6c626e5266615751694f694a6e636e4174643256694c584276636e526862434a392e4f4e66354c42752d426a4b34535a34684d6c7239493864367975346f725f52453250793946454b325844517372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c4c2e06d478	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b78707400174153542e444154412d454e5452592d4f50455241544f527371007e000d7400104153542e51432d434f4d4d49545445457871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0016787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0016787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000067708000000080000000374000a6772616e745f7479706574000870617373776f726474000b63726564656e7469616c7374000474727565740008757365726e616d657400057368616f6e78737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e0027770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e001c3f40000000000000770800000010000000007870707371007e0027770c000000103f40000000000000787371007e0027770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0036737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e001c3f400000000000067708000000080000000371007e001e71007e001f71007e002071007e002171007e002271007e00237800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00164c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0024737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000271007e000f71007e0011787071007e0023740003313433
710c94ab5ce4f3a4e8d8e9fb1c6055b5	\\xaced00057372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c75657400124c6a6176612f6c616e672f537472696e673b78707401b765794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a305957357062534973496e4e6a6233426c496a7062496e4a6c595751694c434a33636d6c305a534a644c434a6864476b694f694a6d4f5446684e7a426b5a6930774d6a68694c5452694f444574595745774e533168596d4532595749354e3255325a6d59694c434a6c654841694f6a45314e6a51324e544d784e7a6b73496d463164476876636d6c306157567a496a7062496b465456433552517931445430314e53565255525555694c434a425531517551564e54525651745355347451306842556b64464969776955464a444c6c42464969776951564e554c6b4654553056554c553142546b46485256496958537769616e5270496a6f69596d4a684d7a68694e6d4974597a4d7a4e693030597a4e6b4c546c6c4e4445745a4451314f5755794f5449334d6d497949697769593278705a57353058326c6b496a6f695a334a774c58646c5969317762334a305957776966512e337532426a6b4474594f7776532d4d6f366233554d4f70357138597a77764d63393065427671342d6762417372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c4c98d28d78	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000004770400000004737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b78707400134153542e41535345542d494e2d4348415247457371007e000d7400114153542e41535345542d4d414e414745527371007e000d7400104153542e51432d434f4d4d49545445457371007e000d7400065052432e50457871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e001a787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00184c000573636f706571007e001a787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00187870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000037708000000040000000274000a6772616e745f7479706574000870617373776f7264740008757365726e616d6574000574616e696d78737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e0029770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e00203f40000000000000770800000010000000007870707371007e0029770c000000103f40000000000000787371007e0029770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000477040000000471007e000f71007e001171007e001371007e00157871007e0038737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e00203f400000000000067708000000080000000271007e002271007e002371007e002471007e00257800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e001a4c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0026737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000471007e000f71007e001171007e001371007e0015787071007e0025740003313433
cdd2859e02cb25047b1dc026c8a4c425	\\xaced00057372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c75657400124c6a6176612f6c616e672f537472696e673b787074017865794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a725957316862434973496e4e6a6233426c496a7062496e4a6c595751694c434a33636d6c305a534a644c434a6864476b694f6949324e6d4535595459784f53316a4d44526d4c54526d4e7a51745954597a4d43316b4f47557a4e7a67305a6d566b4d4467694c434a6c654841694f6a45314e6a517a4f5451334e546773496d463164476876636d6c306157567a496a7062496c42535179355152534973496c4253517935505a6d5a7059325679496c3073496d703061534936496a686d4e575531597a49304c54646c4d474d744e4455324e6930344d6a59304c574a6b4d6a64695a4746694f574a684e694973496d4e7361575675644639705a434936496d6479634331335a5749746347397964474673496e302e6e6d744868534d666c5870714b69703153412d34463079774b6a4c30644352447156424b46654a786c45387372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c3d31a00078	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000b5052432e4f6666696365727371007e000d7400065052432e50457871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0016787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0016787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000067708000000080000000374000a6772616e745f7479706574000870617373776f726474000b63726564656e7469616c7374000474727565740008757365726e616d657400056b616d616c78737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e0027770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e001c3f40000000000000770800000010000000007870707371007e0027770c000000103f40000000000000787371007e0027770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0036737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e001c3f400000000000067708000000080000000371007e001e71007e001f71007e002071007e002171007e002271007e00237800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00164c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0024737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000271007e000f71007e0011787071007e0023740003313433
5b5f4a90306efff228389d8e7e4dfaf2	\\xaced00057372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c75657400124c6a6176612f6c616e672f537472696e673b787074019865794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a7259574a7063694973496e4e6a6233426c496a7062496e4a6c595751694c434a33636d6c305a534a644c434a6864476b694f694a684f4459304e475a6a59533030596d49784c5451774e5759744f575a6a4d5330794d446735596a41304f574934596d4d694c434a6c654841694f6a45314e6a51304d4449304d544173496d463164476876636d6c306157567a496a7062496b465456433545515652424c55564f56464a5a4c55395152564a42564539534969776951564e554c6c52425230644a546b6374525535555356525a496c3073496d703061534936496d45324f546c684e4451784c574579595755744e4751354d533169595755794c574d77597a5a6b5a4759344d6a526c5a534973496d4e7361575675644639705a434936496d6479634331335a5749746347397964474673496e302e4b30526c57387136694234675962754a545a6d373349366e76784f6e4a6652434e5853333963525578506b7372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c3da6628178	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b78707400174153542e444154412d454e5452592d4f50455241544f527371007e000d7400124153542e54414747494e472d454e544954597871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0016787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0016787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000067708000000080000000374000a6772616e745f7479706574000870617373776f726474000b63726564656e7469616c7374000474727565740008757365726e616d657400056b6162697278737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e0027770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e001c3f40000000000000770800000010000000007870707371007e0027770c000000103f40000000000000787371007e0027770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0036737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e001c3f400000000000067708000000080000000371007e001e71007e001f71007e002071007e002171007e002271007e00237800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00164c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0024737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000271007e000f71007e0011787071007e0023740003313433
c60b588f5a1f1b0a64cbc57f50004b52	\\xaced00057372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c75657400124c6a6176612f6c616e672f537472696e673b787074017865794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a715957747063694973496e4e6a6233426c496a7062496e4a6c595751694c434a33636d6c305a534a644c434a6864476b694f69497959546c6b4f5745774e5330345a6d45314c5451785a4459745954566b59793077593245324f446b35596a4d325a6d45694c434a6c654841694f6a45314e6a51354f5459324e6a5973496d463164476876636d6c306157567a496a7062496c42535179355152534973496c4253517935505a6d5a7059325679496c3073496d703061534936496a41304e474a685a574d784c5451784e6a51744e4467314e5330344f54646b4c545532597a63785a5759355a5467344e534973496d4e7361575675644639705a434936496d6479634331335a5749746347397964474673496e302e7446595a72456932484777445a354243527436424668425868575761633339555577414d4a757a5a4a36497372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c611204dc78	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000b5052432e4f6666696365727371007e000d7400065052432e50457871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0016787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0016787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000067708000000080000000374000a6772616e745f7479706574000870617373776f726474000b63726564656e7469616c7374000474727565740008757365726e616d657400056a616b697278737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e0027770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e001c3f40000000000000770800000010000000007870707371007e0027770c000000103f40000000000000787371007e0027770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0036737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e001c3f400000000000067708000000080000000371007e001e71007e001f71007e002071007e002171007e002271007e00237800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00164c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0024737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000271007e000f71007e0011787071007e0023740003313433
ce997bd975ec574068477de064619fab	\\xaced00057372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c75657400124c6a6176612f6c616e672f537472696e673b78707401b765794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a305957357062534973496e4e6a6233426c496a7062496e4a6c595751694c434a33636d6c305a534a644c434a6864476b694f694a6c4f54646a4e6a6c68595331684f4441794c5451305a5449744f445a684d43316a4d474a6d59546b324e5745334e7a59694c434a6c654841694f6a45314e6a55774f4463304f446b73496d463164476876636d6c306157567a496a7062496b465456433552517931445430314e53565255525555694c434a425531517551564e54525651745355347451306842556b64464969776955464a444c6c42464969776951564e554c6b4654553056554c553142546b46485256496958537769616e5270496a6f6959324d32597a55304e5451744f4745344e4330304f4451774c5749345a6a4d74596d45344f5451784e474a685957493349697769593278705a57353058326c6b496a6f695a334a774c58646c5969317762334a305957776966512e6641486a764548514f7364755879445358436449486a4c306b744f4674446e4e346f3153765333797253637372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c667bdf7578	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000004770400000004737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b78707400134153542e41535345542d494e2d4348415247457371007e000d7400114153542e41535345542d4d414e414745527371007e000d7400104153542e51432d434f4d4d49545445457371007e000d7400065052432e50457871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e001a787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00184c000573636f706571007e001a787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00187870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000067708000000080000000374000a6772616e745f7479706574000870617373776f726474000b63726564656e7469616c7374000474727565740008757365726e616d6574000574616e696d78737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e002b770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e00203f40000000000000770800000010000000007870707371007e002b770c000000103f40000000000000787371007e002b770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000477040000000471007e000f71007e001171007e001371007e00157871007e003a737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e00203f400000000000067708000000080000000371007e002271007e002371007e002471007e002571007e002671007e00277800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e001a4c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0028737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000471007e000f71007e001171007e001371007e0015787071007e0027740003313433
26d3729f0ea1af22c33e49fea3dda499	\\xaced00057372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c75657400124c6a6176612f6c616e672f537472696e673b787074017965794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a3059584e6b615845694c434a7a593239775a53493657794a795a57466b4969776964334a70644755695853776959585270496a6f69593251774d575a6c4e7a4d744e475979596930304f544d794c54686c4d474d74597a426b4e44426b5932497a4f444a68496977695a586877496a6f784e5459314d4467354f4449334c434a686458526f62334a7064476c6c6379493657794a51556b4d75554555694c434a51556b4d7554325a6d61574e6c63694a644c434a7164476b694f6949314d446c684d474e695a4330355a5449774c5451774d544d744f446b355969307a597a63314d324e6b595759314f4455694c434a6a62476c6c626e5266615751694f694a6e636e4174643256694c584276636e526862434a392e5162704468666d4742343648596a2d435663417a466d7468504b4c6d76635a3072616f647a557141744c457372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c669f893978	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000b5052432e4f6666696365727371007e000d7400065052432e50457871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0016787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0016787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000067708000000080000000374000a6772616e745f7479706574000870617373776f726474000b63726564656e7469616c7374000474727565740008757365726e616d6574000674617364697178737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e0027770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e001c3f40000000000000770800000010000000007870707371007e0027770c000000103f40000000000000787371007e0027770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0036737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e001c3f400000000000067708000000080000000371007e001e71007e001f71007e002071007e002171007e002271007e00237800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00164c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0024737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000271007e000f71007e0011787071007e0023740003313433
c1114dcb2af9ef60bb22a0e9ea6dbc7f	\\xaced00057372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c75657400124c6a6176612f6c616e672f537472696e673b78707401b765794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a305957357062534973496e4e6a6233426c496a7062496e4a6c595751694c434a33636d6c305a534a644c434a6864476b694f6949784d324978595445335a69316c4f44646a4c5452694e544574596d49355a5330355a6d566a5a575a6c59544e6b4d6d49694c434a6c654841694f6a45314e6a51334e4445774d6a5173496d463164476876636d6c306157567a496a7062496b465456433552517931445430314e53565255525555694c434a425531517551564e54525651745355347451306842556b64464969776955464a444c6c42464969776951564e554c6b4654553056554c553142546b46485256496958537769616e5270496a6f69596d55354d44566b4d544d744e6d55784e6930304d6a59344c5749304f5449744d4459784e4467784d7a597a4f444e6849697769593278705a57353058326c6b496a6f695a334a774c58646c5969317762334a305957776966512e5f567a434f36754139676833694650494e6d4d58737259415f4c585067764e32545a354c73414d475241417372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c51d53ad078	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000004770400000004737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b78707400134153542e41535345542d494e2d4348415247457371007e000d7400114153542e41535345542d4d414e414745527371007e000d7400104153542e51432d434f4d4d49545445457371007e000d7400065052432e50457871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e001a787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00184c000573636f706571007e001a787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00187870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000067708000000080000000374000a6772616e745f7479706574000870617373776f726474000b63726564656e7469616c7374000474727565740008757365726e616d6574000574616e696d78737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e002b770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e00203f40000000000000770800000010000000007870707371007e002b770c000000103f40000000000000787371007e002b770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000477040000000471007e000f71007e001171007e001371007e00157871007e003a737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e00203f400000000000067708000000080000000371007e002271007e002371007e002471007e002571007e002671007e00277800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e001a4c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0028737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000471007e000f71007e001171007e001371007e0015787071007e0027740003313433
65c4645ad4d8043d11c06275aa127264	\\xaced00057372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c75657400124c6a6176612f6c616e672f537472696e673b787074019565794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a7a6147467662694973496e4e6a6233426c496a7062496e4a6c595751694c434a33636d6c305a534a644c434a6864476b694f694977596a6b32596a566a4d5330344e324d7a4c54526b4e6d5174595445784e5330354d54646a4f4467794e5459354f5755694c434a6c654841694f6a45314e6a51324d7a4d344d444173496d463164476876636d6c306157567a496a7062496b465456433552517931445430314e53565255525555694c434a42553151755245465551533146546c525357533150554556535156525055694a644c434a7164476b694f694a6b4d7a4d345a5449325979316a4e325a6d4c5451774d4751744f44646a596931684e6a63344d3255774f474d774d7a63694c434a6a62476c6c626e5266615751694f694a6e636e4174643256694c584276636e526862434a392e374679617a53786c30463067456c7a535243656759696734616558524965454e4643735955626a73356d677372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c4b711f1478	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b78707400174153542e444154412d454e5452592d4f50455241544f527371007e000d7400104153542e51432d434f4d4d49545445457871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0016787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0016787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000067708000000080000000374000a6772616e745f7479706574000870617373776f726474000b63726564656e7469616c7374000474727565740008757365726e616d657400057368616f6e78737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e0027770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e001c3f40000000000000770800000010000000007870707371007e0027770c000000103f40000000000000787371007e0027770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0036737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e001c3f400000000000067708000000080000000371007e001e71007e001f71007e002071007e002171007e002271007e00237800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00164c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0024737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000271007e000f71007e0011787071007e0023740003313433
81a89506a357a1f058665d38d9b4977a	\\xaced00057372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c75657400124c6a6176612f6c616e672f537472696e673b78707401b765794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a305957357062534973496e4e6a6233426c496a7062496e4a6c595751694c434a33636d6c305a534a644c434a6864476b694f6949784d7a557a4d474a6d5a53307a4e4745784c5451304d6d4d74596d51324d53316a4f54566d4e3245314d574a6b4d6d51694c434a6c654841694f6a45314e6a51344f5445344d546b73496d463164476876636d6c306157567a496a7062496b465456433552517931445430314e53565255525555694c434a425531517551564e54525651745355347451306842556b64464969776955464a444c6c42464969776951564e554c6b4654553056554c553142546b46485256496958537769616e5270496a6f69595755344d6a56694f5463745954597a4f5330305a574d314c5746684e444d745954686d5a474d775a6d597a5a54633049697769593278705a57353058326c6b496a6f695a334a774c58646c5969317762334a305957776966512e46586264437445386462346c5a4a65443141514a4950796b446e383237797877524f415a4270414a6d73497372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c5ad22db578	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000004770400000004737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b78707400134153542e41535345542d494e2d4348415247457371007e000d7400114153542e41535345542d4d414e414745527371007e000d7400104153542e51432d434f4d4d49545445457371007e000d7400065052432e50457871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e001a787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00184c000573636f706571007e001a787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00187870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000067708000000080000000374000a6772616e745f7479706574000870617373776f726474000b63726564656e7469616c7374000474727565740008757365726e616d6574000574616e696d78737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e002b770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e00203f40000000000000770800000010000000007870707371007e002b770c000000103f40000000000000787371007e002b770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000477040000000471007e000f71007e001171007e001371007e00157871007e003a737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e00203f400000000000067708000000080000000371007e002271007e002371007e002471007e002571007e002671007e00277800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e001a4c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0028737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000471007e000f71007e001171007e001371007e0015787071007e0027740003313433
a1a68f312bad821165fc3740ede45178	\\xaced00057372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c75657400124c6a6176612f6c616e672f537472696e673b787074019565794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a7a61476c79623342684969776963324e76634755694f6c7369636d56685a434973496e64796158526c496c3073496d463061534936496a4a6d4f57557a5a474e6c4c5451354e6a6b744e475a6a596930345932517a4c5459354e47466c596a5933593259344d694973496d5634634349364d5455324e5441344d4459774d7977695958563061473979615852705a584d694f6c736951564e554c6b4654553056554c556c4f4c554e4951564a4852534973496b46545643355551556448535535484c55564f56456c5557534a644c434a7164476b694f6949314e7a55334f5445334d4330774e544d774c54517a5a6a63744f5467335979316c4f5749774e544a6d59544d32595455694c434a6a62476c6c626e5266615751694f694a6e636e4174643256694c584276636e526862434a392e72714136696e734150627a31496f6f444e706e73705273724a526c53677a736d6577334168324c6a6649557372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c6612ca4b78	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b78707400134153542e41535345542d494e2d4348415247457371007e000d7400124153542e54414747494e472d454e544954597871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0016787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0016787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000067708000000080000000374000a6772616e745f7479706574000870617373776f726474000b63726564656e7469616c7374000474727565740008757365726e616d65740007736869726f706178737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e0027770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e001c3f40000000000000770800000010000000007870707371007e0027770c000000103f40000000000000787371007e0027770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0036737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e001c3f400000000000067708000000080000000371007e001e71007e001f71007e002071007e002171007e002271007e00237800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00164c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0024737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000271007e000f71007e0011787071007e0023740003313433
cd491123bbf261e5e76fd6946895f5c7	\\xaced00057372004c6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744578706972696e674f417574683252656672657368546f6b656e2fdf47639dd0c9b70200014c000a65787069726174696f6e7400104c6a6176612f7574696c2f446174653b787200446f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e636f6d6d6f6e2e44656661756c744f417574683252656672657368546f6b656e73e10e0a6354d45e0200014c000576616c75657400124c6a6176612f6c616e672f537472696e673b787074019565794a68624763694f694a49557a49314e694973496e523563434936496b705856434a392e65794a316332567958323568625755694f694a7a6147467662694973496e4e6a6233426c496a7062496e4a6c595751694c434a33636d6c305a534a644c434a6864476b694f6949355a574e6d4f4456684f53316b4d7a4a684c54526b597a49744f44566d5a43307a59545177596d49304e4467314d446b694c434a6c654841694f6a45314e6a55774f4445324e7a4973496d463164476876636d6c306157567a496a7062496b465456433552517931445430314e53565255525555694c434a42553151755245465551533146546c525357533150554556535156525055694a644c434a7164476b694f694935593245775a4459795a69316c597a63314c5452684e6a49744f54597a4e7930325a6a67344e324e6a597a6b784f4441694c434a6a62476c6c626e5266615751694f694a6e636e4174643256694c584276636e526862434a392e634a345372526c644f767a65585241495f616f6b677061686b324371424d704969494166763963513650597372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000016c66231cbf78	\\xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001f40200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b78707400174153542e444154412d454e5452592d4f50455241544f527371007e000d7400104153542e51432d434f4d4d49545445457871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200075a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c00077265667265736874003b4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f546f6b656e526571756573743b4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0016787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0016787074000e6772702d7765622d706f7274616c737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000067708000000080000000374000a6772616e745f7479706574000870617373776f726474000b63726564656e7469616c7374000474727565740008757365726e616d657400057368616f6e78737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f4000000000000274000472656164740005777269746578017371007e0027770c000000103f400000000000027371007e000d74000b524f4c455f434c49454e547371007e000d740013524f4c455f545255535445445f434c49454e54787371007e001c3f40000000000000770800000010000000007870707371007e0027770c000000103f40000000000000787371007e0027770c000000103f40000000000000787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001f40200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0036737200176a6176612e7574696c2e4c696e6b6564486173684d617034c04e5c106cc0fb0200015a000b6163636573734f726465727871007e001c3f400000000000067708000000080000000371007e001e71007e001f71007e002071007e002171007e002271007e00237800707372001562642e6772702e6d6f64656c2e417574685573657244227214f53c3a880200014c00096f66666963654f696471007e000e787200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001f40200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00164c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e0024737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f7200000000000001f4020000787077040000000271007e000f71007e0011787071007e0023740003313433
\.


--
-- Data for Name: component; Type: TABLE DATA; Schema: sec; Owner: grp
--

COPY sec.component (oid, name_en, name_bn, status, url, subfeature_oid) FROM stdin;
1	Declaration Entry Form	ঘোষণা এন্ট্রি ফর্ম	Active	\N	\N
2	List of Declaration 	ঘোষণা তালিকা	Active	\N	\N
3	Edit Declaration	ঘোষণা সম্পাদনা করুন	Active	\N	\N
4	Requisition Entry Form	অনুরোধ এন্ট্রি ফর্ম	Active	\N	\N
5	List of Submission	জমা তালিকা	Active	\N	\N
6	Asset Info Add	এন্ট্রি ফর্ম	Active	\N	\N
7	Asset Info List	তালিকা	Active	commissioning-acquisition/asset-info	47
8	Temp Item Add	এন্ট্রি ফর্ম	Active	commissioning-acquisition/temp-item/add-temp-item	45
9	Temp Item List	তালিকা	Active	commissioning-acquisition/temp-item	45
\.


--
-- Data for Name: component_resource; Type: TABLE DATA; Schema: sec; Owner: grp
--

COPY sec.component_resource (oid, status, resource_oid, component_oid) FROM stdin;
1	Active	8	1
2	Active	6	2
3	Active	11	3
4	Active	14	4
5	Active	12	5
\.


--
-- Data for Name: grp_feature; Type: TABLE DATA; Schema: sec; Owner: grp
--

COPY sec.grp_feature (oid, name_en, name_bn, status, sort_order, sub_module_oid) FROM stdin;
1	Procurement	ক্রয়	Active	1	39
2	Asset	সম্পদ	Active	2	39
3	Committee	কমিটি	Active	3	39
4	Organization	প্রতিষ্ঠান	Active	4	39
5	Vendor	বিক্রেতা	Active	5	39
6	User Role 	ব্যবহারকারী ভূমিকা	Active	6	39
7	Procurement Item	আইটেম ক্রয়	Active	7	39
8	Associated setting	সংযুক্ত সেটিং	Active	8	39
9	Annual Procurement Plan (APP) Generation	বার্ষিক প্রকিউরমেন্ট প্ল্যান (এপিপি) জেনারেশন	Active	1	40
10	Tendering in eGP	eGP মধ্যে দরপত্র	Active	2	41
11	Contract Signing	চুক্তি স্বাক্ষর	Active	3	42
12	Material receive info	গৃহীত পণ্যের তথ্য	Active	4	43
13	Product Testing / Quality Check	পণ্য যাচাইকরণ / মান পরীক্ষা	Active	5	43
14	MRV Generation	এমআরভি জেনারেশন	Active	6	43
15	Report	প্রতিবেদন	Active	7	44
16	Dashboard	ড্যাশবোর্ড	Active	8	44
17	Assets	সম্পদ	Active	1	51
18	Method Rule Category	পদ্ধতি নিয়ম বিভাগ	Active	2	51
19	Rules and Regulations	নিয়ম এবং প্রবিধান	Active	3	51
20	Methods	পদ্ধতি	Active	4	51
21	Committee	কমিটি	Active	5	51
22	Route	রুট	Active	6	51
23	Vehicle	বাহন	Active	7	51
24	Driver	চালক	Active	8	51
25	Additional setups	অতিরিক্ত সেটআপ	Active	9	51
26	Temporary item	অস্থায়ী আইটেম	Active	1	52
27	Acquisition	অর্জন	Active	2	52
28	Distribution and transport	বিতরণ ও পরিবহন	Active	3	52
29	Report to other modules	অন্যান্য মডিউল রিপোর্ট করুন	Active	4	52
30	Asset Demand	সম্পদ চাহিদা	Active	5	53
31	Asset requisition	সম্পদ অনুরোধ	Active	6	53
32	Assets allocation (require normal process)	সম্পদ বরাদ্দ (স্বাভাবিক প্রক্রিয়া প্রয়োজন)	Active	7	53
33	Assets allocation (require long process)	সম্পদ বরাদ্দ (দীর্ঘ প্রক্রিয়া প্রয়োজন)	Active	8	53
34	Return Process (Normal asset)	রিটার্ন প্রক্রিয়া (সাধারন সম্পদ)	Active	9	53
35	Return Process (Required long process)	রিটার্ন প্রক্রিয়া (প্রয়োজন দীর্ঘ প্রক্রিয়া)	Active	10	53
36	Operations	অপারেশনস	Active	11	53
37	Contract renewal 	চুক্তি নবায়ন	Active	12	53
38	Maintenance request	রক্ষণাবেক্ষণ অনুরোধ	Active	1	54
39	Process	প্রক্রিয়া	Active	2	54
40	Post maintenance 	পোস্ট রক্ষণাবেক্ষণ	Active	3	54
41	Auto maintenance	অটো রক্ষণাবেক্ষণ	Active	4	54
42	For city corporation tax, mutation, utility bills	সিটি করপোরেশন ট্যাক্স, মিউটেশন, ইউটিলিটি বিল জন্য	Active	5	54
43	Payment for maintenance	রক্ষণাবেক্ষণের জন্য পেমেন্ট	Active	6	54
44	Asset transfer	সম্পদ স্থানান্তর	Active	7	54
45	Third party maintenance vendors	তৃতীয় পক্ষের রক্ষণাবেক্ষণ বিক্রেতাদের	Active	8	54
46	Asset disposal request	সম্পদ নিষ্পত্তি অনুরোধ	Active	1	55
47	Finalize Request	অনুরোধ চূড়ান্ত করা	Active	2	55
48	After Disposal	নিষ্পত্তি করার পরে	Active	3	55
49	Donation	দান	Active	4	55
50	Asset lost or damage	সম্পদ হারিয়ে বা ক্ষতি	Active	5	55
51	Other sub-module's change and communication	অন্যান্য উপ-মডিউল এর পরিবর্তন এবং যোগাযোগ	Active	6	56
52	Activity log	কার্য বিবরণ	Active	7	56
53	Vehicle acquisition	যানবাহন অধিগ্রহণ	Active	1	57
54	Vehicle requisition	যানবাহন প্রয়োজন	Active	2	57
55	Vehicle allocation or rejection	যানবাহন বরাদ্দ বা প্রত্যাখ্যান	Active	3	57
56	Vehicle return	যানবাহন ফেরত	Active	4	57
57	Fuel	জ্বালানি	Active	5	57
58	Different unexpected events	বিভিন্ন অপ্রত্যাশিত ঘটনা	Active	6	57
59	Billing	বিলিং	Active	7	57
60	Maintenance and disposal	রক্ষণাবেক্ষণ ও নিষ্পত্তি	Active	8	57
\.


--
-- Data for Name: grp_module; Type: TABLE DATA; Schema: sec; Owner: grp
--

COPY sec.grp_module (oid, name_en, name_bn, status, module_type, sort_order) FROM stdin;
1	Common	সাধারণ	Active	General	1
2	Security	সিকিউরিটি	Active	General	2
3	Human Resource Management	মানব সম্পদ ব্যবস্থাপনা	Active	Business	3
4	Budgeting	বাজেটিং	Active	Business	4
5	Accounts	অ্যাকাউন্টস	Active	Business	5
6	Audit	নিরীক্ষা	Active	Business	6
7	Procurement Management	ক্রয় ব্যবস্থাপনা	Active	Business	7
8	Inventory Management	তালিকাভুক্ত জিনিসপত্র ব্যবস্থাপনা	Active	Business	8
9	Asset Management	সম্পদ ব্যবস্থাপনা	Active	Business	9
10	Project Monitoring and Management	প্রকল্প পর্যবেক্ষণ এবং ব্যবস্থাপনা	Active	Business	10
11	Event and Meeting Management	ইভেন্ট এবং সভা ব্যবস্থাপনা	Active	Business	11
\.


--
-- Data for Name: grp_role; Type: TABLE DATA; Schema: sec; Owner: grp
--

COPY sec.grp_role (oid, name_en, name_bn, role_type, description_en, description_bn, menu_json) FROM stdin;
System	System	সিস্টেম	System	\N	\N	[]
Service	Service	সার্ভিস	Service	\N	\N	[]
PRC.Officer	Officer	অফিসার	Business	\N	\N	[{"name":"Dashboard","url":"dashboard","icon":"fa fa-tachometer"},{"name":"Procurement","url":"procurement","icon":"fa fa-building","children":[{"name":"App","url":"procurement/app","icon":"fa fa-angle-down","children":[{"name":"Requisition","url":"procurement/app/requisition","icon":"fa fa-angle-down","children":[{"name":"Submission","url":"procurement/app/requisition/published-declaration","icon":"fa fa-list-ul"}]}]}]}]
PRC.HOPE	Head of Procurement Entity	হেড অফ প্রকিউরমেন্ট এন্টিটি	Business	\N	\N	[]
AST.ASSET-MANAGER	Asset Manager	অ্যাসেট ম্যানেজার	Business	\N	\N	[{"name":"Dashboard","url":"dashboard","icon":"fa fa-tachometer"},{"name":"Asset","url":"asset","icon":"fa fa-building","children":[{"name":"commissioning-acquisition","url":"asset/commissioning-acquisition","icon":"fa fa-angle-down","children":[{"name":"temp-item","url":"asset/commissioning-acquisition/temp-item","icon":"fa fa-angle-down","children":[{"name":"temp-item-list","url":"asset/commissioning-acquisition/temp-item/temp-item-asset-manager","icon":"fa fa-list-ul"}]},{"name":"asset-info","url":"asset/commissioning-acquisition/asset-info","icon":"fa fa-angle-down","children":[{"name":"asset-info-list","url":"asset/commissioning-acquisition/asset-info/asset-info-list","icon":"fa fa-list-ul"}]}]}]}]
AST.DATA-ENTRY-OPERATOR	Data Entry Operator	ডাটা এন্ট্রি অপারেটর	Business	\N	\N	[{"name":"Dashboard","url":"dashboard","icon":"fa fa-tachometer"},{"name":"Asset","url":"asset","icon":"fa fa-building","children":[{"name":"commissioning-acquisition","url":"asset/commissioning-acquisition","icon":"fa fa-angle-down","children":[{"name":"temp-item","url":"asset/commissioning-acquisition/temp-item","icon":"fa fa-angle-down","children":[{"name":"temp-item-list","url":"asset/commissioning-acquisition/temp-item/temp-item-data-entry-operator","icon":"fa fa-list-ul"}]}]}]}]
AST.QC-COMMITTEE	QC Committee	কিউসি কমিটি	Business	\N	\N	[{"name":"Dashboard","url":"dashboard","icon":"fa fa-tachometer"},{"name":"Asset","url":"asset","icon":"fa fa-building","children":[{"name":"commissioning-acquisition","url":"asset/commissioning-acquisition","icon":"fa fa-angle-down","children":[{"name":"temp-item","url":"asset/commissioning-acquisition/temp-item","icon":"fa fa-angle-down","children":[{"name":"temp-item-list","url":"asset/commissioning-acquisition/temp-item/temp-item-committee","icon":"fa fa-list-ul"}]}]}]}]
AST.VENDORS	Vendor	ভেন্ডর	Business	\N	\N	[]
AST.ASSET-IN-CHARGE	Asset In-Charge	অ্যাসেট ইন-চার্জ	Business	\N	\N	[{"name":"Dashboard","url":"dashboard","icon":"fa fa-tachometer"},{"name":"Asset","url":"asset","icon":"fa fa-building","children":[{"name":"commissioning-acquisition","url":"asset/commissioning-acquisition","icon":"fa fa-angle-down","children":[{"name":"temp-item","url":"asset/commissioning-acquisition/temp-item","icon":"fa fa-angle-down","children":[{"name":"temp-item-list","url":"asset/commissioning-acquisition/temp-item/temp-item-list-after-qc","icon":"fa fa-list-ul"}]}]}]}]
AST.TOP-OFFICIAL	Top Official	টপ অফিসিয়াল	Business	\N	\N	[]
AST.EMPLOYEE	Employee	এমপ্লয়ী	Business	\N	\N	[]
AST.PROCUREMENT	Procurement	প্রকিউরমেন্ট	Business	\N	\N	[]
AST.ACCOUNTS	Accounts	একাউন্টস	Business	\N	\N	[]
AST.TAGGING-ENTITY	Tagging Entity	ট্যাগিং কর্তৃপক্ষ 	Business	\N	\N	[{"name":"Dashboard","url":"dashboard","icon":"fa fa-tachometer"},{"name":"Asset","url":"asset","icon":"fa fa-building","children":[{"name":"commissioning-acquisition","url":"asset/commissioning-acquisition","icon":"fa fa-angle-down","children":[{"name":"temp-item","url":"asset/commissioning-acquisition/temp-item","icon":"fa fa-angle-down","children":[{"name":"temp-item-list","url":"asset/commissioning-acquisition/temp-item/temp-item-list-for-tagging","icon":"fa fa-list-ul"}]}]}]}]
PRC.PE	Procurement Entity	প্রকিউরমেন্ট এন্টিটি	Business	\N	\N	[{"name":"Dashboard","url":"dashboard","icon":"fa fa-tachometer"},{"name":"Procurement","url":"procurement","icon":"fa fa-building","children":[{"name":"App","url":"procurement/app","icon":"fa fa-angle-down","children":[{"name":"Requisition","url":"procurement/app/requisition","icon":"fa fa-angle-down","children":[{"name":"Declaration","url":"procurement/app/requisition/declaration","icon":"fa fa-list-ul"}]},{"name":"My-APP","url":"/procurement/app/preparation/my-app-list","icon":"fa fa-angle-down"}]}]}]
\.


--
-- Data for Name: grp_role_permission; Type: TABLE DATA; Schema: sec; Owner: grp
--

COPY sec.grp_role_permission (oid, component_oid, grp_role_oid) FROM stdin;
1	8	AST.DATA-ENTRY-OPERATOR
2	9	AST.DATA-ENTRY-OPERATOR
\.


--
-- Data for Name: grp_subfeature; Type: TABLE DATA; Schema: sec; Owner: grp
--

COPY sec.grp_subfeature (oid, name_en, name_bn, status, sort_order, feature_oid) FROM stdin;
1	Procurement type	ক্রয়ের ধরণ	Active	\N	1
2	Procurement Method	ক্রয়ের পদ্ধতি	Active	\N	1
3	Procurement Nature	ক্রয়ের প্রকৃতি	Active	\N	1
4	Asset	সম্পদ	Active	\N	2
5	Asset category/ type	সম্পদ বিভাগ / ধরণ	Active	\N	2
6	Committee formation for procurement	ক্রয় কমিটির গঠন	Active	\N	3
7	Committee type	কমিটির ধরন	Active	\N	3
8	Organization Name	প্রতিষ্ঠানের নাম	Active	\N	4
9	Department	বিভাগ	Active	\N	4
10	Project	প্রকল্প	Active	\N	4
11	Employees	কর্মচারী	Active	\N	4
12	Designation	উপাধি	Active	\N	4
13	Vendor	বিক্রেতা	Active	\N	5
14	Service type	সেবার ধরণ	Active	\N	5
15	User Role Create	ব্যবহারকারী ভূমিকা তৈরি করুন	Active	\N	6
16	User Entitlement	ব্যবহারকারী এনটাইটেলমেন্ট	Active	\N	6
17	User Role Assignment	ব্যবহারকারী ভূমিকা নিয়োগ	Active	\N	6
18	Item Type Setup	আইটেম প্রকার সেটআপ	Active	\N	7
19	Item Category setup	আইটেম বিভাগ সেটআপ	Active	\N	7
20	Item Setup	আইটেম সেটআপ	Active	\N	7
21	Unit of Measurement	পরিমাপ একক	Active	\N	7
22	Attribute Setup / Item list	বৈশিষ্ট্য সেটআপ / আইটেম তালিকা	Active	\N	7
23	Store	দোকান	Active	\N	8
24	External members	বাহ্যিক সদস্য	Active	\N	8
25	Reviewers	সমালোচক	Active	\N	8
26	Fiscal year	অর্থবছর	Active	\N	8
27	Budget	বাজেট	Active	\N	8
28	Purchase Requisition	ফরমাশ - পত্র	Active	\N	9
29	Annual Procurement Plan Preparation	বার্ষিক সংগ্রহ পরিকল্পনা প্রস্তুতি	Active	\N	9
30	Annual Procurement Plan Modification	বার্ষিক সংগ্রহ পরিকল্পনা পরিবর্তন	Active	\N	9
31	Tendering in eGP	ই জিপি এর মাধ্যমে ক্রয়  	Active	\N	10
32	Contract preparation	চুক্তি প্রস্তুতি	Active	\N	11
33	Record Contract	রেকর্ড চুক্তি	Active	\N	11
34	Material receiving information	উপাদান প্রাপ্তির তথ্য	Active	\N	12
35	Pre-shipment QC for International Procurement	আন্তর্জাতিক অর্জনের জন্য প্রাক-চালান QC	Active	\N	13
36	QC after receiving managed by committee	কমিটি পরিচালিত হওয়ার পর প্রশ্নপত্র!	Active	\N	13
37	Complain letter sending for bad quality products	খারাপ মানের পণ্য জন্য পাঠানো চিঠি অভিযোগ	Active	\N	13
38	Notify responsible tagging entity for accepted quantity	গ্রহণযোগ্য পরিমাণ জন্য দায়ী ট্যাগিং সত্তা অবহিত	Active	\N	13
39	Chalan/invoice/bill update	চালান / চালান / বিল আপডেট	Active	\N	14
40	List of items	উপাদানের তালিকা	Active	\N	15
41	List of vendors 	বিক্রেতাদের তালিকা	Active	\N	15
42	Summary report 	সারসংক্ষেপ প্রতিবেদন	Active	\N	15
43	Work order	কাজের আদেশ	Active	\N	16
44	Contract	চুক্তি	Active	\N	16
45	Temporary item receive with supplier info	অস্থায়ী পণ্য সংরক্ষণ	Active	1	26
46	QC managed by committee	QC পরিচালনা	Active	2	26
47	Metadata save of asset	অ্যাসেট সংরক্ষন 	Active	1	27
\.


--
-- Data for Name: grp_submodule; Type: TABLE DATA; Schema: sec; Owner: grp
--

COPY sec.grp_submodule (oid, name_en, name_bn, status, sort_order, module_oid) FROM stdin;
1	Configuration	কনফিগারেশন	Active	1	3
2	Personnel Information Management (PIM)	কর্মী তথ্য ব্যবস্থাপনা (পিআইএম)	Active	2	3
3	Training and Development Management	প্রশিক্ষণ ও উন্নয়ন ব্যবস্থাপনা	Active	3	3
4	Leave Management	অবকাশ ব্যবস্থাপনা	Active	4	3
5	Recruitment or Appointment Management	নিয়োগ ব্যবস্থাপনা	Active	5	3
6	Promotion Management	পদোন্নতি ব্যবস্থাপনা	Active	6	3
7	Posting Management	পোস্টিং ব্যবস্থাপনা	Active	7	3
8	Attendance Management	উপস্থিতি ব্যবস্থাপনা	Active	8	3
9	ACR Management	এসিআর ব্যবস্থাপনা	Active	9	3
10	Discipline and Investigation Management	শৃঙ্খলা ও তদন্ত ব্যবস্থাপনা	Active	10	3
11	Award Management	পুরস্কার ব্যবস্থাপনা	Active	11	3
12	Publication Management	প্রকাশনা ব্যবস্থাপনা	Active	12	3
13	Information Management for Payroll and Fund Management	বেতন এবং তহবিল ব্যবস্থাপনা জন্য তথ্য ব্যবস্থাপনা	Active	13	3
14	Vesting of Magisterial Power	ম্যাজিস্ট্রেটের ক্ষমতা অর্পণ করা	Active	14	3
15	Retirement Management	অবসর ব্যবস্থাপনা	Active	15	3
16	Configuration	কনফিগারেশন	Active	1	4
17	Budget Projection	বাজেট অভিক্ষেপ	Active	2	4
18	Budget Preparation and Allocation	বাজেট প্রস্তুতি এবং বরাদ্দ	Active	3	4
19	Budget Distribution	বাজেট বিতরণ	Active	4	4
20	Budget Revision and Quarterly Expenditure Reconciliation	বাজেট সংশোধন এবং ত্রৈমাসিক ব্যয় পুনর্মিলন	Active	5	4
21	Managing Chart of Accounts	অ্যাকাউন্ট চার্ট ব্যবস্থাপনা	Active	1	5
22	Managing Payroll	বেতন ব্যবস্থাপনা	Active	2	5
23	Managing Pension	অবসর ব্যবস্থাপনা	Active	3	5
24	Loan Management	ঋণ ব্যবস্থাপনা	Active	4	5
25	Managing Expenditure	খরচ ব্যবস্থাপনা	Active	5	5
26	Managing Earned Revenue	উপার্জিত আয় ব্যবস্থাপনা	Active	6	5
27	VAT, TAX and AIT Management	ভ্যাট, ট্যাক্স এবং এআইটি ব্যবস্থাপনা	Active	7	5
28	Managing Assets or Inventory Accounts	সম্পদ বা তালিকাভুক্ত জিনিসপত্রের  হিসাব পরিচালনা	Active	8	5
29	Fund Management	তহবিল ব্যবস্থাপনা	Active	9	5
30	Central Accounts	প্রধান হিসাব	Active	10	5
31	Reports	প্রতিবেদন	Active	11	5
32	External Vendor Management	বাহ্যিক বিক্রেতা ব্যবস্থাপনা	Active	12	5
33	Configuration	কনফিগারেশন	Active	1	6
34	Regular Reporting	নিয়মিত প্রতিবেদন	Active	2	6
35	Customized Reporting Utility	কাস্টমাইজড প্রতিবেদন উপযোগ	Active	3	6
36	Information System for Internal Audit Operations	অভ্যন্তরীণ নিরীক্ষা পরিচালনার জন্য তথ্য ব্যবস্থাপনা	Active	4	6
37	Information System for Audit Objections	নিরীক্ষা আপত্তির  জন্য তথ্য ব্যবস্থাপনা	Active	5	6
38	Document Management for Audit Objections	নিরীক্ষা অভিযোজনের জন্য নথি ব্যবস্থাপনা	Active	6	6
39	Settings	সেটিংস	Active	1	7
40	Annual Procurement Plan (APP) Generation	বার্ষিক ক্রয় পরিকল্পনা সংঘটন	Active	2	7
41	Notification of Award (NOA)	পুরস্কার বিজ্ঞপ্তি	Active	3	7
42	Contracts	চুক্তি	Active	4	7
43	Goods Receive	পণ্য গ্রহণ	Active	5	7
44	Reports & Dashboard	রিপোর্ট এবং ড্যাশবোর্ড	Active	6	7
45	Configuration	কনফিগারেশন	Active	1	8
46	Inventory Requisition	তালিকাভুক্ত জিনিসপত্র দাবি-করণ	Active	2	8
47	Purchase Requisition	ফরমাশ - পত্র	Active	3	8
48	Stock Management	মজুত ব্যবস্থাপনা	Active	4	8
49	Transfer	হস্তান্তর	Active	5	8
50	Physical Inventory & Stock Report	বাস্তব তালিকাভুক্ত জিনিসপত্র এবং মজুত প্রতিবেদন	Active	6	8
51	Configuration	কনফিগারেশন	Active	1	9
52	Commissioning and Acquisition	কমিশন এবং অধিগ্রহণ	Active	2	9
53	Deployment and Operation	স্থাপনা ও অপারেশন	Active	3	9
54	Maintenance	রক্ষণাবেক্ষণ	Active	4	9
55	Decommissioning and Disposal	অপসরণ করা এবং নিষ্পত্তি	Active	5	9
56	Asset Tracking	সম্পদের খোজরাখা	Active	6	9
57	Vehicle Management	যানবাহন ব্যবস্থাপনা	Active	7	9
58	Project Initiation	প্রকল্প দীক্ষা	Active	1	10
59	Project Approval using PPS	পিপিএস ব্যবহার করে প্রকল্প অনুমোদন	Active	2	10
60	Resource Allocation using ERD	ইআরডি ব্যবহার করে রিসোর্স বরাদ্দ	Active	3	10
61	Fund Release using iBAS++	আইবাস ++ ব্যবহার করে তহবিল মুক্তি	Active	4	10
62	Project Execution and Management	প্রকল্প বাস্তবায়ন এবং ব্যবস্থাপনা	Active	5	10
63	Project Monitoring and Evaluation using PMIS	পিএমআইএস  ব্যবহার করে প্রকল্প পর্যবেক্ষণ এবং মূল্যায়ন 	Active	6	10
64	Configuration	কনফিগারেশন	Active	1	11
65	Meeting Creation and relevant tasks	সভা সৃষ্টি এবং প্রাসঙ্গিক কাজ	Active	2	11
66	Meeting time tasks	সভার সময় কাজ	Active	3	11
67	Resolution preparation and relevant tasks	অনুবন্ধ প্রস্তুতি এবং প্রাসঙ্গিক কাজ	Active	4	11
68	Configuration(Event)	কনফিগারেশন (ইভেন্ট)	Active	5	11
69	Event Creation	ইভেন্ট সৃষ্টি	Active	6	11
70	Post-event Tasks	ইভেন্টের পরে কাজ	Active	7	11
\.


--
-- Data for Name: grp_user; Type: TABLE DATA; Schema: sec; Owner: grp
--

COPY sec.grp_user (oid, username, password, status, account_expired, credentials_expired, account_locked) FROM stdin;
tanim	tanim	$2a$10$Nj9AB/ffFQX/FEj3KgdmLe9vu6NlVMfBrG6/bsq9zD0eUAlMMJ6o.	Active	No	No	No
mashud	mashud	$2a$10$Nj9AB/ffFQX/FEj3KgdmLe9vu6NlVMfBrG6/bsq9zD0eUAlMMJ6o.	Active	No	No	No
jakir	jakir	$2a$10$Nj9AB/ffFQX/FEj3KgdmLe9vu6NlVMfBrG6/bsq9zD0eUAlMMJ6o.	Active	No	No	No
kamal	kamal	$2a$10$Nj9AB/ffFQX/FEj3KgdmLe9vu6NlVMfBrG6/bsq9zD0eUAlMMJ6o.	Active	No	No	No
tasdiq	tasdiq	$2a$10$Nj9AB/ffFQX/FEj3KgdmLe9vu6NlVMfBrG6/bsq9zD0eUAlMMJ6o.	Active	No	No	No
shaon	shaon	$2a$10$Nj9AB/ffFQX/FEj3KgdmLe9vu6NlVMfBrG6/bsq9zD0eUAlMMJ6o.	Active	No	No	No
shiropa	shiropa	$2a$10$Nj9AB/ffFQX/FEj3KgdmLe9vu6NlVMfBrG6/bsq9zD0eUAlMMJ6o.	Active	No	No	No
kabir	kabir	$2a$10$Nj9AB/ffFQX/FEj3KgdmLe9vu6NlVMfBrG6/bsq9zD0eUAlMMJ6o.	Active	No	No	No
admin	admin	$2a$10$Nj9AB/ffFQX/FEj3KgdmLe9vu6NlVMfBrG6/bsq9zD0eUAlMMJ6o.	Active	No	No	No
\.


--
-- Data for Name: grp_user_role; Type: TABLE DATA; Schema: sec; Owner: grp
--

COPY sec.grp_user_role (oid, menu_json, default_status, grp_role_oid, grp_user_oid) FROM stdin;
tanim_pe	{}	No	PRC.PE	tanim
mashud_pe	{}	Yes	PRC.PE	mashud
mashud_office	{}	Yes	PRC.Officer	mashud
jakir_office	{}	Yes	PRC.Officer	jakir
jakir_pe	{}	Yes	PRC.PE	jakir
tasdiq_pe	{}	Yes	PRC.PE	tasdiq
kamal_office	{}	Yes	PRC.Officer	kamal
kamal_pe	{}	Yes	PRC.PE	kamal
tasdiq_office	{}	Yes	PRC.Officer	tasdiq
tanim_ast_am	{}	Yes	AST.ASSET-MANAGER	tanim
tanim_ast_qc	{}	No	AST.QC-COMMITTEE	tanim
tanim_ast_aic	{}	No	AST.ASSET-IN-CHARGE	tanim
mashud_ast_aic	{}	Yes	AST.ASSET-IN-CHARGE	mashud
mashud_ast_te	{}	No	AST.TAGGING-ENTITY	mashud
shaon_ast_deo	{}	Yes	AST.DATA-ENTRY-OPERATOR	shaon
shaon_ast_qc	{}	No	AST.QC-COMMITTEE	shaon
shiropa_ast_aic	{}	Yes	AST.ASSET-IN-CHARGE	shiropa
shiropa_ast_te	{}	No	AST.TAGGING-ENTITY	shiropa
kabir_ast_te	{}	Yes	AST.TAGGING-ENTITY	kabir
kabir_ast_deo	{}	No	AST.DATA-ENTRY-OPERATOR	kabir
admin_office	{}	Yes	PRC.Officer	admin
admin_pe	{}	Yes	PRC.PE	admin
\.


--
-- Data for Name: office_module; Type: TABLE DATA; Schema: sec; Owner: grp
--

COPY sec.office_module (oid, status, module_oid, office_oid) FROM stdin;
\.


--
-- Data for Name: resource; Type: TABLE DATA; Schema: sec; Owner: grp
--

COPY sec.resource (oid, end_point, status, service_oid) FROM stdin;
1	cmn/master/ministry/v1/get-list	Active	1
2	cmn/master/ministry/v1/get-type-list	Active	1
3	cmn/master/ministry/v1/save	Active	1
4	cmn/master/item/v1/get-list	Active	1
5	cmn/master/vendor/v1/get-list	Active	1
6	prc/app-requisition/declaration/v1/get-list	Active	4
7	prc/app-requisition/declaration/v1/get-by-oid	Active	4
8	prc/app-requisition/declaration/v1/save	Active	4
9	prc/app-requisition/declaration/v1/status-update	Active	4
10	prc/app-requisition/declaration/v1/draft-insert	Active	4
11	prc/app-requisition/declaration/v1/update	Active	4
12	prc/app-requisition/submission/v1/get-list	Active	4
13	prc/app-requisition/submission/v1/get-by-oid	Active	4
14	prc/app-requisition/submission/v1/save	Active	4
15	prc/app-requisition/submission/v1/update	Active	4
16	prc/app-requisition/consolidation/v1/get-list	Active	4
17	prc/app-requisition/consolidation/v1/get-by-oid	Active	4
18	prc/app-requisition/consolidation/v1/save	Active	4
19	prc/app-requisition/consolidation/v1/update	Active	4
\.


--
-- Data for Name: service; Type: TABLE DATA; Schema: sec; Owner: grp
--

COPY sec.service (oid, name_en, name_bn, status) FROM stdin;
1	cmn-service	সাধারণ সেবা	Active
2	sec-service	সিকিউরিটি-সেবা	Active
3	hrm-service	মানব সম্পদ-সেবা	Active
4	prc-service	ক্রয়-সেবা	Active
5	ast-service	সম্পদ-সেবা	Active
\.


--
-- Name: acquisition_billing pk_acquisition_billing; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.acquisition_billing
    ADD CONSTRAINT pk_acquisition_billing PRIMARY KEY (oid);


--
-- Name: acquisition_billing_line pk_acquisition_billing_line; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.acquisition_billing_line
    ADD CONSTRAINT pk_acquisition_billing_line PRIMARY KEY (oid);


--
-- Name: asset_allocation pk_asset_allocation; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_allocation
    ADD CONSTRAINT pk_asset_allocation PRIMARY KEY (oid);


--
-- Name: asset_complaint_letter pk_asset_complaint_letter; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_complaint_letter
    ADD CONSTRAINT pk_asset_complaint_letter PRIMARY KEY (oid);


--
-- Name: asset_disposal pk_asset_disposal; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_disposal
    ADD CONSTRAINT pk_asset_disposal PRIMARY KEY (oid);


--
-- Name: asset_information pk_asset_information; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_information
    ADD CONSTRAINT pk_asset_information PRIMARY KEY (oid);


--
-- Name: asset_lost_info pk_asset_lost_info; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_lost_info
    ADD CONSTRAINT pk_asset_lost_info PRIMARY KEY (oid);


--
-- Name: asset_maintenance pk_asset_maintenance; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_maintenance
    ADD CONSTRAINT pk_asset_maintenance PRIMARY KEY (oid);


--
-- Name: asset_maintenance_line pk_asset_maintenance_line; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_maintenance_line
    ADD CONSTRAINT pk_asset_maintenance_line PRIMARY KEY (oid);


--
-- Name: asset_maintenance_type pk_asset_maintenance_type; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_maintenance_type
    ADD CONSTRAINT pk_asset_maintenance_type PRIMARY KEY (oid);


--
-- Name: asset_requisition pk_asset_requisition; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_requisition
    ADD CONSTRAINT pk_asset_requisition PRIMARY KEY (oid);


--
-- Name: asset_requisition_line pk_asset_requisition_line; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_requisition_line
    ADD CONSTRAINT pk_asset_requisition_line PRIMARY KEY (oid);


--
-- Name: asset_return pk_asset_return; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_return
    ADD CONSTRAINT pk_asset_return PRIMARY KEY (oid);


--
-- Name: asset_tracking pk_asset_tracking; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_tracking
    ADD CONSTRAINT pk_asset_tracking PRIMARY KEY (oid);


--
-- Name: asset_tracking_line pk_asset_tracking_line; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_tracking_line
    ADD CONSTRAINT pk_asset_tracking_line PRIMARY KEY (oid);


--
-- Name: asset_transfer pk_asset_transfer; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_transfer
    ADD CONSTRAINT pk_asset_transfer PRIMARY KEY (oid);


--
-- Name: auto_maintenance pk_auto_maintenance; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.auto_maintenance
    ADD CONSTRAINT pk_auto_maintenance PRIMARY KEY (oid);


--
-- Name: contract_info pk_contract_info; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.contract_info
    ADD CONSTRAINT pk_contract_info PRIMARY KEY (oid);


--
-- Name: contract_renewal pk_contract_renewal; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.contract_renewal
    ADD CONSTRAINT pk_contract_renewal PRIMARY KEY (oid);


--
-- Name: contractual_driver pk_contractual_driver; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.contractual_driver
    ADD CONSTRAINT pk_contractual_driver PRIMARY KEY (oid);


--
-- Name: flat_info pk_flat_info; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.flat_info
    ADD CONSTRAINT pk_flat_info PRIMARY KEY (oid);


--
-- Name: floor_info pk_floor_info; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.floor_info
    ADD CONSTRAINT pk_floor_info PRIMARY KEY (oid);


--
-- Name: fuel_reconciliation pk_fuel_reconciliation; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.fuel_reconciliation
    ADD CONSTRAINT pk_fuel_reconciliation PRIMARY KEY (oid);


--
-- Name: fuel_requisition pk_fuel_requisition; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.fuel_requisition
    ADD CONSTRAINT pk_fuel_requisition PRIMARY KEY (oid);


--
-- Name: fuel_service_station pk_fuel_service_station; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.fuel_service_station
    ADD CONSTRAINT pk_fuel_service_station PRIMARY KEY (oid);


--
-- Name: fuel_type pk_fuel_type; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.fuel_type
    ADD CONSTRAINT pk_fuel_type PRIMARY KEY (oid);


--
-- Name: garage pk_garage; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.garage
    ADD CONSTRAINT pk_garage PRIMARY KEY (oid);


--
-- Name: maintenance_asset_receive pk_maintenance_asset_receive; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.maintenance_asset_receive
    ADD CONSTRAINT pk_maintenance_asset_receive PRIMARY KEY (oid);


--
-- Name: maintenance_payment pk_maintenance_payment; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.maintenance_payment
    ADD CONSTRAINT pk_maintenance_payment PRIMARY KEY (oid);


--
-- Name: method_rule_category pk_method_rule_category; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.method_rule_category
    ADD CONSTRAINT pk_method_rule_category PRIMARY KEY (oid);


--
-- Name: method_setup pk_method_setup; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.method_setup
    ADD CONSTRAINT pk_method_setup PRIMARY KEY (oid);


--
-- Name: rent_lease_info pk_rent_lease_info; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.rent_lease_info
    ADD CONSTRAINT pk_rent_lease_info PRIMARY KEY (oid);


--
-- Name: rent_receive pk_rent_receive; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.rent_receive
    ADD CONSTRAINT pk_rent_receive PRIMARY KEY (oid);


--
-- Name: route_config pk_route_config; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.route_config
    ADD CONSTRAINT pk_route_config PRIMARY KEY (oid);


--
-- Name: route_setup pk_route_setup; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.route_setup
    ADD CONSTRAINT pk_route_setup PRIMARY KEY (oid);


--
-- Name: rules_regulation pk_rules_regulation; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.rules_regulation
    ADD CONSTRAINT pk_rules_regulation PRIMARY KEY (oid);


--
-- Name: temporary_item pk_temporary_item; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.temporary_item
    ADD CONSTRAINT pk_temporary_item PRIMARY KEY (oid);


--
-- Name: temporary_item_detail pk_temporary_item_detail; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.temporary_item_detail
    ADD CONSTRAINT pk_temporary_item_detail PRIMARY KEY (oid);


--
-- Name: tenant pk_tenant; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.tenant
    ADD CONSTRAINT pk_tenant PRIMARY KEY (oid);


--
-- Name: tender pk_tender; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.tender
    ADD CONSTRAINT pk_tender PRIMARY KEY (oid);


--
-- Name: ticket_generation pk_ticket_generation; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.ticket_generation
    ADD CONSTRAINT pk_ticket_generation PRIMARY KEY (oid);


--
-- Name: vehicle_allocation pk_vehicle_allocation; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.vehicle_allocation
    ADD CONSTRAINT pk_vehicle_allocation PRIMARY KEY (oid);


--
-- Name: vehicle_info pk_vehicle_info; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.vehicle_info
    ADD CONSTRAINT pk_vehicle_info PRIMARY KEY (oid);


--
-- Name: vehicle_requisition pk_vehicle_requisition; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.vehicle_requisition
    ADD CONSTRAINT pk_vehicle_requisition PRIMARY KEY (oid);


--
-- Name: vehicle_return pk_vehicle_return; Type: CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.vehicle_return
    ADD CONSTRAINT pk_vehicle_return PRIMARY KEY (oid);


--
-- Name: allowance_setup pk_allowance_setup; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.allowance_setup
    ADD CONSTRAINT pk_allowance_setup PRIMARY KEY (oid);


--
-- Name: city_corporation pk_city_corporation; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.city_corporation
    ADD CONSTRAINT pk_city_corporation PRIMARY KEY (oid);


--
-- Name: city_corporation_ward pk_city_corporation_ward; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.city_corporation_ward
    ADD CONSTRAINT pk_city_corporation_ward PRIMARY KEY (oid);


--
-- Name: committee_setup pk_committee_setup; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.committee_setup
    ADD CONSTRAINT pk_committee_setup PRIMARY KEY (oid);


--
-- Name: committee_setup_line pk_committee_setup_line; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.committee_setup_line
    ADD CONSTRAINT pk_committee_setup_line PRIMARY KEY (oid);


--
-- Name: committee_type_setup pk_committee_type_setup; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.committee_type_setup
    ADD CONSTRAINT pk_committee_type_setup PRIMARY KEY (oid);


--
-- Name: deduction_setup pk_deduction_setup; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.deduction_setup
    ADD CONSTRAINT pk_deduction_setup PRIMARY KEY (oid);


--
-- Name: document pk_document; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.document
    ADD CONSTRAINT pk_document PRIMARY KEY (oid);


--
-- Name: document_type pk_document_type; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.document_type
    ADD CONSTRAINT pk_document_type PRIMARY KEY (oid);


--
-- Name: donee_info pk_donee_info; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.donee_info
    ADD CONSTRAINT pk_donee_info PRIMARY KEY (oid);


--
-- Name: donor_info pk_donor_info; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.donor_info
    ADD CONSTRAINT pk_donor_info PRIMARY KEY (oid);


--
-- Name: entitlement pk_entitlement; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.entitlement
    ADD CONSTRAINT pk_entitlement PRIMARY KEY (oid);


--
-- Name: fiscal_year pk_fiscal_year; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.fiscal_year
    ADD CONSTRAINT pk_fiscal_year PRIMARY KEY (oid);


--
-- Name: geo_country pk_geo_country; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.geo_country
    ADD CONSTRAINT pk_geo_country PRIMARY KEY (oid);


--
-- Name: geo_district pk_geo_district; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.geo_district
    ADD CONSTRAINT pk_geo_district PRIMARY KEY (oid);


--
-- Name: geo_division pk_geo_division; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.geo_division
    ADD CONSTRAINT pk_geo_division PRIMARY KEY (oid);


--
-- Name: geo_post_office pk_geo_post_office; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.geo_post_office
    ADD CONSTRAINT pk_geo_post_office PRIMARY KEY (oid);


--
-- Name: geo_thana pk_geo_thana; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.geo_thana
    ADD CONSTRAINT pk_geo_thana PRIMARY KEY (oid);


--
-- Name: geo_union pk_geo_union; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.geo_union
    ADD CONSTRAINT pk_geo_union PRIMARY KEY (oid);


--
-- Name: geo_union_ward pk_geo_union_ward; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.geo_union_ward
    ADD CONSTRAINT pk_geo_union_ward PRIMARY KEY (oid);


--
-- Name: geo_upazila pk_geo_upazila; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.geo_upazila
    ADD CONSTRAINT pk_geo_upazila PRIMARY KEY (oid);


--
-- Name: item pk_item; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.item
    ADD CONSTRAINT pk_item PRIMARY KEY (oid);


--
-- Name: item_category pk_item_category; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.item_category
    ADD CONSTRAINT pk_item_category PRIMARY KEY (oid);


--
-- Name: item_uom pk_item_uom; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.item_uom
    ADD CONSTRAINT pk_item_uom PRIMARY KEY (oid);


--
-- Name: loan_sanction pk_loan_sanction; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.loan_sanction
    ADD CONSTRAINT pk_loan_sanction PRIMARY KEY (oid);


--
-- Name: ministry pk_ministry; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.ministry
    ADD CONSTRAINT pk_ministry PRIMARY KEY (oid);


--
-- Name: ministry_type pk_ministry_type; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.ministry_type
    ADD CONSTRAINT pk_ministry_type PRIMARY KEY (oid);


--
-- Name: municipality pk_municipality; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.municipality
    ADD CONSTRAINT pk_municipality PRIMARY KEY (oid);


--
-- Name: municipality_ward pk_municipality_ward; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.municipality_ward
    ADD CONSTRAINT pk_municipality_ward PRIMARY KEY (oid);


--
-- Name: office pk_office; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office
    ADD CONSTRAINT pk_office PRIMARY KEY (oid);


--
-- Name: office_layer pk_office_layer; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_layer
    ADD CONSTRAINT pk_office_layer PRIMARY KEY (oid);


--
-- Name: office_origin pk_office_origin; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_origin
    ADD CONSTRAINT pk_office_origin PRIMARY KEY (oid);


--
-- Name: office_origin_unit pk_office_origin_unit; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_origin_unit
    ADD CONSTRAINT pk_office_origin_unit PRIMARY KEY (oid);


--
-- Name: office_origin_unit_post pk_office_origin_unit_post; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_origin_unit_post
    ADD CONSTRAINT pk_office_origin_unit_post PRIMARY KEY (oid);


--
-- Name: office_unit pk_office_unit; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_unit
    ADD CONSTRAINT pk_office_unit PRIMARY KEY (oid);


--
-- Name: office_unit_category pk_office_unit_category; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_unit_category
    ADD CONSTRAINT pk_office_unit_category PRIMARY KEY (oid);


--
-- Name: office_unit_post pk_office_unit_post; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_unit_post
    ADD CONSTRAINT pk_office_unit_post PRIMARY KEY (oid);


--
-- Name: party_type pk_party_type; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.party_type
    ADD CONSTRAINT pk_party_type PRIMARY KEY (oid);


--
-- Name: payroll_process pk_payroll_process; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.payroll_process
    ADD CONSTRAINT pk_payroll_process PRIMARY KEY (oid);


--
-- Name: prepare_salary_bill pk_prepare_salary_bill; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.prepare_salary_bill
    ADD CONSTRAINT pk_prepare_salary_bill PRIMARY KEY (oid);


--
-- Name: store pk_store; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.store
    ADD CONSTRAINT pk_store PRIMARY KEY (oid);


--
-- Name: vendor pk_vendor; Type: CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.vendor
    ADD CONSTRAINT pk_vendor PRIMARY KEY (oid);


--
-- Name: acr_main_report pk_acr_main_report; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.acr_main_report
    ADD CONSTRAINT pk_acr_main_report PRIMARY KEY (oid);


--
-- Name: acr_medical_report pk_acr_medical_report; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.acr_medical_report
    ADD CONSTRAINT pk_acr_medical_report PRIMARY KEY (oid);


--
-- Name: acr_summary pk_acr_summary; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.acr_summary
    ADD CONSTRAINT pk_acr_summary PRIMARY KEY (oid);


--
-- Name: acr_under_controlling_auth pk_acr_under_controlling_auth; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.acr_under_controlling_auth
    ADD CONSTRAINT pk_acr_under_controlling_auth PRIMARY KEY (oid);


--
-- Name: acr_under_controlling_ministry pk_acr_under_controlling_ministry; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.acr_under_controlling_ministry
    ADD CONSTRAINT pk_acr_under_controlling_ministry PRIMARY KEY (oid);


--
-- Name: acr_under_counter_signing_auth pk_acr_under_counter_signing_auth; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.acr_under_counter_signing_auth
    ADD CONSTRAINT pk_acr_under_counter_signing_auth PRIMARY KEY (oid);


--
-- Name: admit_card pk_admit_card; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.admit_card
    ADD CONSTRAINT pk_admit_card PRIMARY KEY (oid);


--
-- Name: application_form_for_recruitment pk_application_form_for_recruitment; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.application_form_for_recruitment
    ADD CONSTRAINT pk_application_form_for_recruitment PRIMARY KEY (oid);


--
-- Name: apply_for_leave pk_apply_for_leave; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.apply_for_leave
    ADD CONSTRAINT pk_apply_for_leave PRIMARY KEY (oid);


--
-- Name: apply_for_prl pk_apply_for_prl; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.apply_for_prl
    ADD CONSTRAINT pk_apply_for_prl PRIMARY KEY (oid);


--
-- Name: at_leave pk_at_leave; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.at_leave
    ADD CONSTRAINT pk_at_leave PRIMARY KEY (oid);


--
-- Name: at_meeting_or_events pk_at_meeting_or_events; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.at_meeting_or_events
    ADD CONSTRAINT pk_at_meeting_or_events PRIMARY KEY (oid);


--
-- Name: attendance_penalty_committee pk_attendance_penalty_committee; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.attendance_penalty_committee
    ADD CONSTRAINT pk_attendance_penalty_committee PRIMARY KEY (oid);


--
-- Name: award_application pk_award_application; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.award_application
    ADD CONSTRAINT pk_award_application PRIMARY KEY (oid);


--
-- Name: award_setup pk_award_setup; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.award_setup
    ADD CONSTRAINT pk_award_setup PRIMARY KEY (oid);


--
-- Name: batch pk_batch; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.batch
    ADD CONSTRAINT pk_batch PRIMARY KEY (oid);


--
-- Name: bundle pk_bundle; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.bundle
    ADD CONSTRAINT pk_bundle PRIMARY KEY (oid);


--
-- Name: cadre pk_cadre; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.cadre
    ADD CONSTRAINT pk_cadre PRIMARY KEY (oid);


--
-- Name: call_for_exam pk_call_for_exam; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.call_for_exam
    ADD CONSTRAINT pk_call_for_exam PRIMARY KEY (oid);


--
-- Name: create_short_list pk_create_short_list; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.create_short_list
    ADD CONSTRAINT pk_create_short_list PRIMARY KEY (oid);


--
-- Name: daily_attendance pk_daily_attendance; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.daily_attendance
    ADD CONSTRAINT pk_daily_attendance PRIMARY KEY (oid);


--
-- Name: deputation_info pk_deputation_info; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.deputation_info
    ADD CONSTRAINT pk_deputation_info PRIMARY KEY (oid);


--
-- Name: education_qualification pk_education_qualification; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.education_qualification
    ADD CONSTRAINT pk_education_qualification PRIMARY KEY (oid);


--
-- Name: employee pk_employee; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.employee
    ADD CONSTRAINT pk_employee PRIMARY KEY (oid);


--
-- Name: employee_evaluation_category_setup pk_employee_evaluation_category_setup; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.employee_evaluation_category_setup
    ADD CONSTRAINT pk_employee_evaluation_category_setup PRIMARY KEY (oid);


--
-- Name: employee_in_queue_for_promotion_or_posting pk_employee_in_queue_for_promotion_or_posting; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.employee_in_queue_for_promotion_or_posting
    ADD CONSTRAINT pk_employee_in_queue_for_promotion_or_posting PRIMARY KEY (oid);


--
-- Name: employee_perf_summary pk_employee_perf_summary; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.employee_perf_summary
    ADD CONSTRAINT pk_employee_perf_summary PRIMARY KEY (oid);


--
-- Name: exam_configuration pk_exam_configuration; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.exam_configuration
    ADD CONSTRAINT pk_exam_configuration PRIMARY KEY (oid);


--
-- Name: exam_venue pk_exam_venue; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.exam_venue
    ADD CONSTRAINT pk_exam_venue PRIMARY KEY (oid);


--
-- Name: final_selection pk_final_selection; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.final_selection
    ADD CONSTRAINT pk_final_selection PRIMARY KEY (oid);


--
-- Name: foreign_training pk_foreign_training; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.foreign_training
    ADD CONSTRAINT pk_foreign_training PRIMARY KEY (oid);


--
-- Name: grade pk_grade; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.grade
    ADD CONSTRAINT pk_grade PRIMARY KEY (oid);


--
-- Name: in_service_training pk_in_service_training; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.in_service_training
    ADD CONSTRAINT pk_in_service_training PRIMARY KEY (oid);


--
-- Name: investigation_team_feedback pk_investigation_team_feedback; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.investigation_team_feedback
    ADD CONSTRAINT pk_investigation_team_feedback PRIMARY KEY (oid);


--
-- Name: investigation_team_setup pk_investigation_team_setup; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.investigation_team_setup
    ADD CONSTRAINT pk_investigation_team_setup PRIMARY KEY (oid);


--
-- Name: investigation_under_appointing_auth pk_investigation_under_appointing_auth; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.investigation_under_appointing_auth
    ADD CONSTRAINT pk_investigation_under_appointing_auth PRIMARY KEY (oid);


--
-- Name: investigation_under_controlling_auth pk_investigation_under_controlling_auth; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.investigation_under_controlling_auth
    ADD CONSTRAINT pk_investigation_under_controlling_auth PRIMARY KEY (oid);


--
-- Name: job_circular pk_job_circular; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.job_circular
    ADD CONSTRAINT pk_job_circular PRIMARY KEY (oid);


--
-- Name: job_circular_approval pk_job_circular_approval; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.job_circular_approval
    ADD CONSTRAINT pk_job_circular_approval PRIMARY KEY (oid);


--
-- Name: justify_against_penalty pk_justify_against_penalty; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.justify_against_penalty
    ADD CONSTRAINT pk_justify_against_penalty PRIMARY KEY (oid);


--
-- Name: leave_approval_by_admin pk_leave_approval_by_admin; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.leave_approval_by_admin
    ADD CONSTRAINT pk_leave_approval_by_admin PRIMARY KEY (oid);


--
-- Name: leave_approval_by_substitute pk_leave_approval_by_substitute; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.leave_approval_by_substitute
    ADD CONSTRAINT pk_leave_approval_by_substitute PRIMARY KEY (oid);


--
-- Name: leave_setup pk_leave_setup; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.leave_setup
    ADD CONSTRAINT pk_leave_setup PRIMARY KEY (oid);


--
-- Name: offense_setup pk_offense_setup; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.offense_setup
    ADD CONSTRAINT pk_offense_setup PRIMARY KEY (oid);


--
-- Name: organisational_training pk_organisational_training; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.organisational_training
    ADD CONSTRAINT pk_organisational_training PRIMARY KEY (oid);


--
-- Name: penalty_committee_list pk_penalty_committee_list; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.penalty_committee_list
    ADD CONSTRAINT pk_penalty_committee_list PRIMARY KEY (oid);


--
-- Name: penalty_for_attendance pk_penalty_for_attendance; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.penalty_for_attendance
    ADD CONSTRAINT pk_penalty_for_attendance PRIMARY KEY (oid);


--
-- Name: pending_participation_confirmation pk_pending_participation_confirmation; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.pending_participation_confirmation
    ADD CONSTRAINT pk_pending_participation_confirmation PRIMARY KEY (oid);


--
-- Name: pension_procedure_for_employee pk_pension_procedure_for_employee; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.pension_procedure_for_employee
    ADD CONSTRAINT pk_pension_procedure_for_employee PRIMARY KEY (oid);


--
-- Name: posting_and_promotion_committee_setup pk_posting_and_promotion_committee_setup; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.posting_and_promotion_committee_setup
    ADD CONSTRAINT pk_posting_and_promotion_committee_setup PRIMARY KEY (oid);


--
-- Name: posting_info pk_posting_info; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.posting_info
    ADD CONSTRAINT pk_posting_info PRIMARY KEY (oid);


--
-- Name: promotion_detail pk_promotion_detail; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.promotion_detail
    ADD CONSTRAINT pk_promotion_detail PRIMARY KEY (oid);


--
-- Name: publication_application pk_publication_application; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.publication_application
    ADD CONSTRAINT pk_publication_application PRIMARY KEY (oid);


--
-- Name: publication_setup pk_publication_setup; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.publication_setup
    ADD CONSTRAINT pk_publication_setup PRIMARY KEY (oid);


--
-- Name: recruitment_committee pk_recruitment_committee; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.recruitment_committee
    ADD CONSTRAINT pk_recruitment_committee PRIMARY KEY (oid);


--
-- Name: recruitment_result pk_recruitment_result; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.recruitment_result
    ADD CONSTRAINT pk_recruitment_result PRIMARY KEY (oid);


--
-- Name: role pk_role; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.role
    ADD CONSTRAINT pk_role PRIMARY KEY (oid);


--
-- Name: rules_of_award_publication pk_rules_of_award_publication; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.rules_of_award_publication
    ADD CONSTRAINT pk_rules_of_award_publication PRIMARY KEY (oid);


--
-- Name: rules_of_vesting_magisterial_power pk_rules_of_vesting_magisterial_power; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.rules_of_vesting_magisterial_power
    ADD CONSTRAINT pk_rules_of_vesting_magisterial_power PRIMARY KEY (oid);


--
-- Name: rules_regarding_acr_apa pk_rules_regarding_acr_apa; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.rules_regarding_acr_apa
    ADD CONSTRAINT pk_rules_regarding_acr_apa PRIMARY KEY (oid);


--
-- Name: rules_regarding_attendance pk_rules_regarding_attendance; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.rules_regarding_attendance
    ADD CONSTRAINT pk_rules_regarding_attendance PRIMARY KEY (oid);


--
-- Name: rules_regarding_discipline pk_rules_regarding_discipline; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.rules_regarding_discipline
    ADD CONSTRAINT pk_rules_regarding_discipline PRIMARY KEY (oid);


--
-- Name: rules_regarding_leave pk_rules_regarding_leave; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.rules_regarding_leave
    ADD CONSTRAINT pk_rules_regarding_leave PRIMARY KEY (oid);


--
-- Name: rules_regarding_posting pk_rules_regarding_posting; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.rules_regarding_posting
    ADD CONSTRAINT pk_rules_regarding_posting PRIMARY KEY (oid);


--
-- Name: rules_regarding_promotion pk_rules_regarding_promotion; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.rules_regarding_promotion
    ADD CONSTRAINT pk_rules_regarding_promotion PRIMARY KEY (oid);


--
-- Name: rules_regarding_recruitment pk_rules_regarding_recruitment; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.rules_regarding_recruitment
    ADD CONSTRAINT pk_rules_regarding_recruitment PRIMARY KEY (oid);


--
-- Name: rules_regarding_training pk_rules_regarding_training; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.rules_regarding_training
    ADD CONSTRAINT pk_rules_regarding_training PRIMARY KEY (oid);


--
-- Name: select_candidate pk_select_candidate; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.select_candidate
    ADD CONSTRAINT pk_select_candidate PRIMARY KEY (oid);


--
-- Name: service_history pk_service_history; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.service_history
    ADD CONSTRAINT pk_service_history PRIMARY KEY (oid);


--
-- Name: survey_for_trainee pk_survey_for_trainee; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.survey_for_trainee
    ADD CONSTRAINT pk_survey_for_trainee PRIMARY KEY (oid);


--
-- Name: survey_for_trainer pk_survey_for_trainer; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.survey_for_trainer
    ADD CONSTRAINT pk_survey_for_trainer PRIMARY KEY (oid);


--
-- Name: survey_questionnaire_for_trainee pk_survey_questionnaire_for_trainee; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.survey_questionnaire_for_trainee
    ADD CONSTRAINT pk_survey_questionnaire_for_trainee PRIMARY KEY (oid);


--
-- Name: survey_questionnaire_for_trainer pk_survey_questionnaire_for_trainer; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.survey_questionnaire_for_trainer
    ADD CONSTRAINT pk_survey_questionnaire_for_trainer PRIMARY KEY (oid);


--
-- Name: termination pk_termination; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.termination
    ADD CONSTRAINT pk_termination PRIMARY KEY (oid);


--
-- Name: trainee_result pk_trainee_result; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.trainee_result
    ADD CONSTRAINT pk_trainee_result PRIMARY KEY (oid);


--
-- Name: trainer_detail pk_trainer_detail; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.trainer_detail
    ADD CONSTRAINT pk_trainer_detail PRIMARY KEY (oid);


--
-- Name: training_type pk_training_type; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.training_type
    ADD CONSTRAINT pk_training_type PRIMARY KEY (oid);


--
-- Name: vesting_of_magisterial_power pk_vesting_of_magisterial_power; Type: CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.vesting_of_magisterial_power
    ADD CONSTRAINT pk_vesting_of_magisterial_power PRIMARY KEY (oid);


--
-- Name: annual_procurement_plan pk_annual_procurement_plan; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.annual_procurement_plan
    ADD CONSTRAINT pk_annual_procurement_plan PRIMARY KEY (oid);


--
-- Name: annual_procurement_plan_detail pk_annual_procurement_plan_detail; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.annual_procurement_plan_detail
    ADD CONSTRAINT pk_annual_procurement_plan_detail PRIMARY KEY (oid);


--
-- Name: annual_procurement_plan_reviewer pk_annual_procurement_plan_reviewer; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.annual_procurement_plan_reviewer
    ADD CONSTRAINT pk_annual_procurement_plan_reviewer PRIMARY KEY (oid);


--
-- Name: app_package pk_app_package; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.app_package
    ADD CONSTRAINT pk_app_package PRIMARY KEY (oid);


--
-- Name: approving_authority pk_approving_authority; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.approving_authority
    ADD CONSTRAINT pk_approving_authority PRIMARY KEY (oid);


--
-- Name: attachment pk_attachment; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.attachment
    ADD CONSTRAINT pk_attachment PRIMARY KEY (oid);


--
-- Name: budget_type pk_budget_type; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.budget_type
    ADD CONSTRAINT pk_budget_type PRIMARY KEY (oid);


--
-- Name: chalan pk_chalan; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.chalan
    ADD CONSTRAINT pk_chalan PRIMARY KEY (oid);


--
-- Name: committee_member pk_committee_member; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.committee_member
    ADD CONSTRAINT pk_committee_member PRIMARY KEY (oid);


--
-- Name: consolidate_requisition pk_consolidate_requisition; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.consolidate_requisition
    ADD CONSTRAINT pk_consolidate_requisition PRIMARY KEY (oid);


--
-- Name: consolidate_requisition_detail pk_consolidate_requisition_detail; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.consolidate_requisition_detail
    ADD CONSTRAINT pk_consolidate_requisition_detail PRIMARY KEY (oid);


--
-- Name: debarment pk_debarment; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.debarment
    ADD CONSTRAINT pk_debarment PRIMARY KEY (oid);


--
-- Name: item_purchase pk_item_purchase; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.item_purchase
    ADD CONSTRAINT pk_item_purchase PRIMARY KEY (oid);


--
-- Name: material_receive_info pk_material_receive_info; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.material_receive_info
    ADD CONSTRAINT pk_material_receive_info PRIMARY KEY (oid);


--
-- Name: package_lot pk_package_lot; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.package_lot
    ADD CONSTRAINT pk_package_lot PRIMARY KEY (oid);


--
-- Name: package_type pk_package_type; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.package_type
    ADD CONSTRAINT pk_package_type PRIMARY KEY (oid);


--
-- Name: procurement_item pk_procurement_item; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.procurement_item
    ADD CONSTRAINT pk_procurement_item PRIMARY KEY (oid);


--
-- Name: procurement_method pk_procurement_method; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.procurement_method
    ADD CONSTRAINT pk_procurement_method PRIMARY KEY (oid);


--
-- Name: procurement_nature pk_procurement_nature; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.procurement_nature
    ADD CONSTRAINT pk_procurement_nature PRIMARY KEY (oid);


--
-- Name: procurement_project pk_procurement_project; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.procurement_project
    ADD CONSTRAINT pk_procurement_project PRIMARY KEY (oid);


--
-- Name: procurement_type pk_procurement_type; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.procurement_type
    ADD CONSTRAINT pk_procurement_type PRIMARY KEY (oid);


--
-- Name: qc_committee pk_qc_committee; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.qc_committee
    ADD CONSTRAINT pk_qc_committee PRIMARY KEY (oid);


--
-- Name: qc_info pk_qc_info; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.qc_info
    ADD CONSTRAINT pk_qc_info PRIMARY KEY (oid);


--
-- Name: qc_list pk_qc_list; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.qc_list
    ADD CONSTRAINT pk_qc_list PRIMARY KEY (oid);


--
-- Name: qc_report pk_qc_report; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.qc_report
    ADD CONSTRAINT pk_qc_report PRIMARY KEY (oid);


--
-- Name: requisition_declaration pk_requisition_declaration; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.requisition_declaration
    ADD CONSTRAINT pk_requisition_declaration PRIMARY KEY (oid);


--
-- Name: requisition_detail pk_requisition_detail; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.requisition_detail
    ADD CONSTRAINT pk_requisition_detail PRIMARY KEY (oid);


--
-- Name: requisition_information pk_requisition_information; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.requisition_information
    ADD CONSTRAINT pk_requisition_information PRIMARY KEY (oid);


--
-- Name: reviewer pk_reviewer; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.reviewer
    ADD CONSTRAINT pk_reviewer PRIMARY KEY (oid);


--
-- Name: schedule_info pk_schedule_info; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.schedule_info
    ADD CONSTRAINT pk_schedule_info PRIMARY KEY (oid);


--
-- Name: schedule_sale pk_schedule_sale; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.schedule_sale
    ADD CONSTRAINT pk_schedule_sale PRIMARY KEY (oid);


--
-- Name: temporary_store pk_temporary_store; Type: CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.temporary_store
    ADD CONSTRAINT pk_temporary_store PRIMARY KEY (oid);


--
-- Name: flyway_schema_history flyway_schema_history_pk; Type: CONSTRAINT; Schema: public; Owner: grp
--

ALTER TABLE ONLY public.flyway_schema_history
    ADD CONSTRAINT flyway_schema_history_pk PRIMARY KEY (installed_rank);


--
-- Name: oauth_client_details pk_oauth_client_details; Type: CONSTRAINT; Schema: public; Owner: grp
--

ALTER TABLE ONLY public.oauth_client_details
    ADD CONSTRAINT pk_oauth_client_details PRIMARY KEY (client_id);


--
-- Name: component pk_component; Type: CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.component
    ADD CONSTRAINT pk_component PRIMARY KEY (oid);


--
-- Name: component_resource pk_component_resource; Type: CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.component_resource
    ADD CONSTRAINT pk_component_resource PRIMARY KEY (oid);


--
-- Name: grp_feature pk_grp_feature; Type: CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.grp_feature
    ADD CONSTRAINT pk_grp_feature PRIMARY KEY (oid);


--
-- Name: grp_module pk_grp_module; Type: CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.grp_module
    ADD CONSTRAINT pk_grp_module PRIMARY KEY (oid);


--
-- Name: grp_role pk_grp_role; Type: CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.grp_role
    ADD CONSTRAINT pk_grp_role PRIMARY KEY (oid);


--
-- Name: grp_role_permission pk_grp_role_permission; Type: CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.grp_role_permission
    ADD CONSTRAINT pk_grp_role_permission PRIMARY KEY (oid);


--
-- Name: grp_subfeature pk_grp_subfeature; Type: CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.grp_subfeature
    ADD CONSTRAINT pk_grp_subfeature PRIMARY KEY (oid);


--
-- Name: grp_submodule pk_grp_submodule; Type: CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.grp_submodule
    ADD CONSTRAINT pk_grp_submodule PRIMARY KEY (oid);


--
-- Name: grp_user pk_grp_user; Type: CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.grp_user
    ADD CONSTRAINT pk_grp_user PRIMARY KEY (oid);


--
-- Name: grp_user_role pk_grp_user_role; Type: CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.grp_user_role
    ADD CONSTRAINT pk_grp_user_role PRIMARY KEY (oid);


--
-- Name: office_module pk_office_module; Type: CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.office_module
    ADD CONSTRAINT pk_office_module PRIMARY KEY (oid);


--
-- Name: resource pk_resource; Type: CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.resource
    ADD CONSTRAINT pk_resource PRIMARY KEY (oid);


--
-- Name: service pk_service; Type: CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.service
    ADD CONSTRAINT pk_service PRIMARY KEY (oid);


--
-- Name: index_acquisition_billing_code_acquisition_billing; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_acquisition_billing_code_acquisition_billing ON ast.acquisition_billing USING btree (acquisition_billing_code);


--
-- Name: index_asset_allocation_code_asset_allocation; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_asset_allocation_code_asset_allocation ON ast.asset_allocation USING btree (asset_allocation_code);


--
-- Name: index_asset_lost_code_asset_lost_info; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_asset_lost_code_asset_lost_info ON ast.asset_lost_info USING btree (asset_lost_code);


--
-- Name: index_asset_requisition_code_asset_requisition; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_asset_requisition_code_asset_requisition ON ast.asset_requisition USING btree (asset_requisition_code);


--
-- Name: index_asset_return_code_asset_return; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_asset_return_code_asset_return ON ast.asset_return USING btree (asset_return_code);


--
-- Name: index_asset_tracking_code_asset_tracking; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_asset_tracking_code_asset_tracking ON ast.asset_tracking USING btree (asset_tracking_code);


--
-- Name: index_auto_maintenance_code_auto_maintenance; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_auto_maintenance_code_auto_maintenance ON ast.auto_maintenance USING btree (auto_maintenance_code);


--
-- Name: index_code_temporary_item; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_code_temporary_item ON ast.temporary_item USING btree (code);


--
-- Name: index_complaint_letter_code_asset_complaint_letter; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_complaint_letter_code_asset_complaint_letter ON ast.asset_complaint_letter USING btree (complaint_letter_code);


--
-- Name: index_contract_code_contract_info; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_contract_code_contract_info ON ast.contract_info USING btree (contract_code);


--
-- Name: index_contract_renewal_code_contract_renewal; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_contract_renewal_code_contract_renewal ON ast.contract_renewal USING btree (contract_renewal_code);


--
-- Name: index_driver_code_contractual_driver; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_driver_code_contractual_driver ON ast.contractual_driver USING btree (driver_code);


--
-- Name: index_flat_code_flat_info; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_flat_code_flat_info ON ast.flat_info USING btree (flat_code);


--
-- Name: index_fuel_reconciliation_code_fuel_reconciliation; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_fuel_reconciliation_code_fuel_reconciliation ON ast.fuel_reconciliation USING btree (fuel_reconciliation_code);


--
-- Name: index_fuel_requisition_code_fuel_requisition; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_fuel_requisition_code_fuel_requisition ON ast.fuel_requisition USING btree (fuel_requisition_code);


--
-- Name: index_fuel_type_code_fuel_type; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_fuel_type_code_fuel_type ON ast.fuel_type USING btree (fuel_type_code);


--
-- Name: index_fuel_type_name_fuel_type; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_fuel_type_name_fuel_type ON ast.fuel_type USING btree (fuel_type_name);


--
-- Name: index_garage_code_garage; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_garage_code_garage ON ast.garage USING btree (garage_code);


--
-- Name: index_maintenance_code_asset_maintenance; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_maintenance_code_asset_maintenance ON ast.asset_maintenance USING btree (maintenance_code);


--
-- Name: index_maintenance_for_each_asset_code_asset_maintenance_line; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_maintenance_for_each_asset_code_asset_maintenance_line ON ast.asset_maintenance_line USING btree (maintenance_for_each_asset_code);


--
-- Name: index_maintenance_received_code_maintenance_asset_receive; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_maintenance_received_code_maintenance_asset_receive ON ast.maintenance_asset_receive USING btree (maintenance_received_code);


--
-- Name: index_maintenance_type_code_asset_maintenance_type; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_maintenance_type_code_asset_maintenance_type ON ast.asset_maintenance_type USING btree (maintenance_type_code);


--
-- Name: index_method_code_method_setup; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_method_code_method_setup ON ast.method_setup USING btree (method_code);


--
-- Name: index_method_rule_category_code_method_rule_category; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_method_rule_category_code_method_rule_category ON ast.method_rule_category USING btree (method_rule_category_code);


--
-- Name: index_method_rule_category_name_method_rule_category; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_method_rule_category_name_method_rule_category ON ast.method_rule_category USING btree (method_rule_category_name);


--
-- Name: index_oid_acquisition_billing; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_acquisition_billing ON ast.acquisition_billing USING btree (oid);


--
-- Name: index_oid_acquisition_billing_line; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_acquisition_billing_line ON ast.acquisition_billing_line USING btree (oid);


--
-- Name: index_oid_asset_allocation; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_asset_allocation ON ast.asset_allocation USING btree (oid);


--
-- Name: index_oid_asset_lost_info; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_asset_lost_info ON ast.asset_lost_info USING btree (oid);


--
-- Name: index_oid_asset_maintenance; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_asset_maintenance ON ast.asset_maintenance USING btree (oid);


--
-- Name: index_oid_asset_maintenance_line; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_asset_maintenance_line ON ast.asset_maintenance_line USING btree (oid);


--
-- Name: index_oid_asset_maintenance_type; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_asset_maintenance_type ON ast.asset_maintenance_type USING btree (oid);


--
-- Name: index_oid_asset_requisition; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_asset_requisition ON ast.asset_requisition USING btree (oid);


--
-- Name: index_oid_asset_requisition_line; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_asset_requisition_line ON ast.asset_requisition_line USING btree (oid);


--
-- Name: index_oid_asset_return; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_asset_return ON ast.asset_return USING btree (oid);


--
-- Name: index_oid_asset_tracking; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_asset_tracking ON ast.asset_tracking USING btree (oid);


--
-- Name: index_oid_asset_tracking_line; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_asset_tracking_line ON ast.asset_tracking_line USING btree (oid);


--
-- Name: index_oid_asset_transfer; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_asset_transfer ON ast.asset_transfer USING btree (oid);


--
-- Name: index_oid_auto_maintenance; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_auto_maintenance ON ast.auto_maintenance USING btree (oid);


--
-- Name: index_oid_contract_info; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_contract_info ON ast.contract_info USING btree (oid);


--
-- Name: index_oid_contract_renewal; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_contract_renewal ON ast.contract_renewal USING btree (oid);


--
-- Name: index_oid_contractual_driver; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_contractual_driver ON ast.contractual_driver USING btree (oid);


--
-- Name: index_oid_floor_info; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_floor_info ON ast.floor_info USING btree (oid);


--
-- Name: index_oid_fuel_reconciliation; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_fuel_reconciliation ON ast.fuel_reconciliation USING btree (oid);


--
-- Name: index_oid_fuel_requisition; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_fuel_requisition ON ast.fuel_requisition USING btree (oid);


--
-- Name: index_oid_fuel_service_station; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_fuel_service_station ON ast.fuel_service_station USING btree (oid);


--
-- Name: index_oid_fuel_type; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_fuel_type ON ast.fuel_type USING btree (oid);


--
-- Name: index_oid_garage; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_garage ON ast.garage USING btree (oid);


--
-- Name: index_oid_maintenance_asset_receive; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_maintenance_asset_receive ON ast.maintenance_asset_receive USING btree (oid);


--
-- Name: index_oid_maintenance_payment; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_maintenance_payment ON ast.maintenance_payment USING btree (oid);


--
-- Name: index_oid_method_rule_category; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_method_rule_category ON ast.method_rule_category USING btree (oid);


--
-- Name: index_oid_method_setup; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_method_setup ON ast.method_setup USING btree (oid);


--
-- Name: index_oid_rent_lease_info; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_rent_lease_info ON ast.rent_lease_info USING btree (oid);


--
-- Name: index_oid_rent_receive; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_rent_receive ON ast.rent_receive USING btree (oid);


--
-- Name: index_oid_route_config; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_route_config ON ast.route_config USING btree (oid);


--
-- Name: index_oid_route_setup; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_route_setup ON ast.route_setup USING btree (oid);


--
-- Name: index_oid_rules_regulation; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_rules_regulation ON ast.rules_regulation USING btree (oid);


--
-- Name: index_oid_ticket_generation; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_ticket_generation ON ast.ticket_generation USING btree (oid);


--
-- Name: index_oid_vehicle_info; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_vehicle_info ON ast.vehicle_info USING btree (oid);


--
-- Name: index_oid_vehicle_requisition; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_oid_vehicle_requisition ON ast.vehicle_requisition USING btree (oid);


--
-- Name: index_payment_code_maintenance_payment; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_payment_code_maintenance_payment ON ast.maintenance_payment USING btree (payment_code);


--
-- Name: index_rent_lease_lnfo_code_rent_lease_info; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_rent_lease_lnfo_code_rent_lease_info ON ast.rent_lease_info USING btree (rent_lease_lnfo_code);


--
-- Name: index_rent_receive_code_rent_receive; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_rent_receive_code_rent_receive ON ast.rent_receive USING btree (rent_receive_code);


--
-- Name: index_route_config_code_route_config; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_route_config_code_route_config ON ast.route_config USING btree (route_config_code);


--
-- Name: index_route_oid_route_setup; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_route_oid_route_setup ON ast.route_setup USING btree (route_oid);


--
-- Name: index_rules_regulation_code_rules_regulation; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_rules_regulation_code_rules_regulation ON ast.rules_regulation USING btree (rules_regulation_code);


--
-- Name: index_station_code_fuel_service_station; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_station_code_fuel_service_station ON ast.fuel_service_station USING btree (station_code);


--
-- Name: index_tenant_code_tenant; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_tenant_code_tenant ON ast.tenant USING btree (tenant_code);


--
-- Name: index_tender_code_tender; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_tender_code_tender ON ast.tender USING btree (tender_code);


--
-- Name: index_ticket_gen_code_ticket_generation; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_ticket_gen_code_ticket_generation ON ast.ticket_generation USING btree (ticket_gen_code);


--
-- Name: index_transfer_code_asset_transfer; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_transfer_code_asset_transfer ON ast.asset_transfer USING btree (transfer_code);


--
-- Name: index_vehicle_allocation_code_vehicle_allocation; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_vehicle_allocation_code_vehicle_allocation ON ast.vehicle_allocation USING btree (vehicle_allocation_code);


--
-- Name: index_vehicle_code_vehicle_info; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_vehicle_code_vehicle_info ON ast.vehicle_info USING btree (vehicle_code);


--
-- Name: index_vehicle_requisition_code_vehicle_requisition; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_vehicle_requisition_code_vehicle_requisition ON ast.vehicle_requisition USING btree (vehicle_requisition_code);


--
-- Name: index_vehicle_return_code_vehicle_return; Type: INDEX; Schema: ast; Owner: grp
--

CREATE UNIQUE INDEX index_vehicle_return_code_vehicle_return ON ast.vehicle_return USING btree (vehicle_return_code);


--
-- Name: index_code_item; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_code_item ON cmn.item USING btree (code);


--
-- Name: index_code_item_category; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_code_item_category ON cmn.item_category USING btree (code);


--
-- Name: index_code_item_uom; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_code_item_uom ON cmn.item_uom USING btree (code);


--
-- Name: index_code_vendor; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_code_vendor ON cmn.vendor USING btree (code);


--
-- Name: index_committee_code_committee_setup; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_committee_code_committee_setup ON cmn.committee_setup USING btree (committee_code);


--
-- Name: index_committee_setup_line_code_committee_setup_line; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_committee_setup_line_code_committee_setup_line ON cmn.committee_setup_line USING btree (committee_setup_line_code);


--
-- Name: index_committee_type_code_committee_type_setup; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_committee_type_code_committee_type_setup ON cmn.committee_type_setup USING btree (committee_type_code);


--
-- Name: index_document_code_document; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_document_code_document ON cmn.document USING btree (document_code);


--
-- Name: index_document_type_code_document_type; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_document_type_code_document_type ON cmn.document_type USING btree (document_type_code);


--
-- Name: index_document_type_name_document_type; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_document_type_name_document_type ON cmn.document_type USING btree (document_type_name);


--
-- Name: index_donee_code_donee_info; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_donee_code_donee_info ON cmn.donee_info USING btree (donee_code);


--
-- Name: index_donor_code_donor_info; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_donor_code_donor_info ON cmn.donor_info USING btree (donor_code);


--
-- Name: index_entitlement_code_entitlement; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_entitlement_code_entitlement ON cmn.entitlement USING btree (entitlement_code);


--
-- Name: index_name_bn_ministry; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_name_bn_ministry ON cmn.ministry USING btree (name_bn);


--
-- Name: index_name_bn_ministry_type; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_name_bn_ministry_type ON cmn.ministry_type USING btree (name_bn);


--
-- Name: index_name_bn_office_unit_category; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_name_bn_office_unit_category ON cmn.office_unit_category USING btree (name_bn);


--
-- Name: index_name_bn_vendor; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_name_bn_vendor ON cmn.vendor USING btree (name_bn);


--
-- Name: index_name_en_ministry; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_name_en_ministry ON cmn.ministry USING btree (name_en);


--
-- Name: index_name_en_ministry_type; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_name_en_ministry_type ON cmn.ministry_type USING btree (name_en);


--
-- Name: index_name_en_office_unit_category; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_name_en_office_unit_category ON cmn.office_unit_category USING btree (name_en);


--
-- Name: index_name_en_vendor; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_name_en_vendor ON cmn.vendor USING btree (name_en);


--
-- Name: index_oid_document; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_oid_document ON cmn.document USING btree (oid);


--
-- Name: index_oid_document_type; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_oid_document_type ON cmn.document_type USING btree (oid);


--
-- Name: index_party_type_code_party_type; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_party_type_code_party_type ON cmn.party_type USING btree (party_type_code);


--
-- Name: index_post_code_entitlement; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_post_code_entitlement ON cmn.entitlement USING btree (post_code);


--
-- Name: index_store_code_store; Type: INDEX; Schema: cmn; Owner: grp
--

CREATE UNIQUE INDEX index_store_code_store ON cmn.store USING btree (store_code);


--
-- Name: index_grp_username_employee; Type: INDEX; Schema: hrm; Owner: grp
--

CREATE UNIQUE INDEX index_grp_username_employee ON hrm.employee USING btree (grp_username);


--
-- Name: index_app_code_annual_procurement_plan; Type: INDEX; Schema: prc; Owner: grp
--

CREATE UNIQUE INDEX index_app_code_annual_procurement_plan ON prc.annual_procurement_plan USING btree (app_code);


--
-- Name: index_app_code_annual_procurement_plan_detail; Type: INDEX; Schema: prc; Owner: grp
--

CREATE UNIQUE INDEX index_app_code_annual_procurement_plan_detail ON prc.annual_procurement_plan_detail USING btree (app_code);


--
-- Name: index_app_detail_code_annual_procurement_plan_detail; Type: INDEX; Schema: prc; Owner: grp
--

CREATE UNIQUE INDEX index_app_detail_code_annual_procurement_plan_detail ON prc.annual_procurement_plan_detail USING btree (app_detail_code);


--
-- Name: index_code_budget_type; Type: INDEX; Schema: prc; Owner: grp
--

CREATE UNIQUE INDEX index_code_budget_type ON prc.budget_type USING btree (code);


--
-- Name: index_code_procurement_item; Type: INDEX; Schema: prc; Owner: grp
--

CREATE UNIQUE INDEX index_code_procurement_item ON prc.procurement_item USING btree (code);


--
-- Name: index_lot_no_package_lot; Type: INDEX; Schema: prc; Owner: grp
--

CREATE UNIQUE INDEX index_lot_no_package_lot ON prc.package_lot USING btree (lot_no);


--
-- Name: index_material_receive_info_code_material_receive_info; Type: INDEX; Schema: prc; Owner: grp
--

CREATE UNIQUE INDEX index_material_receive_info_code_material_receive_info ON prc.material_receive_info USING btree (material_receive_info_code);


--
-- Name: index_ministry_name_notification_of_award; Type: INDEX; Schema: prc; Owner: grp
--

CREATE UNIQUE INDEX index_ministry_name_notification_of_award ON prc.notification_of_award USING btree (ministry_name);


--
-- Name: index_oid_procurement_type; Type: INDEX; Schema: prc; Owner: grp
--

CREATE UNIQUE INDEX index_oid_procurement_type ON prc.procurement_type USING btree (oid);


--
-- Name: index_package_lot_code_package_lot; Type: INDEX; Schema: prc; Owner: grp
--

CREATE UNIQUE INDEX index_package_lot_code_package_lot ON prc.package_lot USING btree (package_lot_code);


--
-- Name: index_qc_code_qc_list; Type: INDEX; Schema: prc; Owner: grp
--

CREATE UNIQUE INDEX index_qc_code_qc_list ON prc.qc_list USING btree (qc_code);


--
-- Name: index_qc_code_qc_report; Type: INDEX; Schema: prc; Owner: grp
--

CREATE UNIQUE INDEX index_qc_code_qc_report ON prc.qc_report USING btree (qc_code);


--
-- Name: index_qc_detail_code_qc_info; Type: INDEX; Schema: prc; Owner: grp
--

CREATE UNIQUE INDEX index_qc_detail_code_qc_info ON prc.qc_info USING btree (qc_detail_code);


--
-- Name: index_schedule_sale_code_schedule_sale; Type: INDEX; Schema: prc; Owner: grp
--

CREATE UNIQUE INDEX index_schedule_sale_code_schedule_sale ON prc.schedule_sale USING btree (schedule_sale_code);


--
-- Name: index_store_code_temporary_store; Type: INDEX; Schema: prc; Owner: grp
--

CREATE UNIQUE INDEX index_store_code_temporary_store ON prc.temporary_store USING btree (store_code);


--
-- Name: flyway_schema_history_s_idx; Type: INDEX; Schema: public; Owner: grp
--

CREATE INDEX flyway_schema_history_s_idx ON public.flyway_schema_history USING btree (success);


--
-- Name: acquisition_billing_line fk_acquisition_billing_oid_acquisition_billing_line; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.acquisition_billing_line
    ADD CONSTRAINT fk_acquisition_billing_oid_acquisition_billing_line FOREIGN KEY (acquisition_billing_oid) REFERENCES ast.acquisition_billing(oid);


--
-- Name: acquisition_billing fk_approved_by_acquisition_billing; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.acquisition_billing
    ADD CONSTRAINT fk_approved_by_acquisition_billing FOREIGN KEY (approved_by) REFERENCES hrm.employee(oid);


--
-- Name: contract_renewal fk_approved_by_contract_renewal; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.contract_renewal
    ADD CONSTRAINT fk_approved_by_contract_renewal FOREIGN KEY (approved_by) REFERENCES hrm.employee(oid);


--
-- Name: fuel_reconciliation fk_approved_by_fuel_reconciliation; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.fuel_reconciliation
    ADD CONSTRAINT fk_approved_by_fuel_reconciliation FOREIGN KEY (approved_by) REFERENCES hrm.employee(oid);


--
-- Name: fuel_requisition fk_approved_by_fuel_requisition; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.fuel_requisition
    ADD CONSTRAINT fk_approved_by_fuel_requisition FOREIGN KEY (approved_by) REFERENCES hrm.employee(oid);


--
-- Name: rent_lease_info fk_approved_by_rent_lease_info; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.rent_lease_info
    ADD CONSTRAINT fk_approved_by_rent_lease_info FOREIGN KEY (approved_by) REFERENCES hrm.employee(oid);


--
-- Name: asset_maintenance_line fk_asset_category_asset_maintenance_line; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_maintenance_line
    ADD CONSTRAINT fk_asset_category_asset_maintenance_line FOREIGN KEY (asset_category) REFERENCES cmn.item_category(oid);


--
-- Name: asset_maintenance_type fk_asset_category_oid_asset_maintenance_type; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_maintenance_type
    ADD CONSTRAINT fk_asset_category_oid_asset_maintenance_type FOREIGN KEY (asset_category_oid) REFERENCES cmn.item_category(oid);


--
-- Name: asset_requisition_line fk_asset_category_oid_asset_requisition_line; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_requisition_line
    ADD CONSTRAINT fk_asset_category_oid_asset_requisition_line FOREIGN KEY (asset_category_oid) REFERENCES cmn.item_category(oid);


--
-- Name: maintenance_asset_receive fk_asset_maintenance_oid_maintenance_asset_receive; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.maintenance_asset_receive
    ADD CONSTRAINT fk_asset_maintenance_oid_maintenance_asset_receive FOREIGN KEY (asset_maintenance_oid) REFERENCES ast.asset_maintenance(oid);


--
-- Name: maintenance_payment fk_asset_maintenance_oid_maintenance_payment; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.maintenance_payment
    ADD CONSTRAINT fk_asset_maintenance_oid_maintenance_payment FOREIGN KEY (asset_maintenance_oid) REFERENCES ast.asset_maintenance(oid);


--
-- Name: acquisition_billing_line fk_asset_oid_acquisition_billing_line; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.acquisition_billing_line
    ADD CONSTRAINT fk_asset_oid_acquisition_billing_line FOREIGN KEY (asset_oid) REFERENCES ast.asset_information(oid);


--
-- Name: asset_allocation fk_asset_oid_asset_allocation; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_allocation
    ADD CONSTRAINT fk_asset_oid_asset_allocation FOREIGN KEY (asset_oid) REFERENCES ast.asset_information(oid);


--
-- Name: asset_disposal fk_asset_oid_asset_disposal; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_disposal
    ADD CONSTRAINT fk_asset_oid_asset_disposal FOREIGN KEY (asset_oid) REFERENCES ast.asset_information(oid);


--
-- Name: asset_lost_info fk_asset_oid_asset_lost_info; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_lost_info
    ADD CONSTRAINT fk_asset_oid_asset_lost_info FOREIGN KEY (asset_oid) REFERENCES ast.asset_information(oid);


--
-- Name: asset_maintenance_line fk_asset_oid_asset_maintenance_line; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_maintenance_line
    ADD CONSTRAINT fk_asset_oid_asset_maintenance_line FOREIGN KEY (asset_oid) REFERENCES ast.asset_information(oid);


--
-- Name: asset_return fk_asset_oid_asset_return; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_return
    ADD CONSTRAINT fk_asset_oid_asset_return FOREIGN KEY (asset_oid) REFERENCES ast.asset_information(oid);


--
-- Name: asset_tracking fk_asset_oid_asset_tracking; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_tracking
    ADD CONSTRAINT fk_asset_oid_asset_tracking FOREIGN KEY (asset_oid) REFERENCES ast.asset_information(oid);


--
-- Name: asset_transfer fk_asset_oid_asset_transfer; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_transfer
    ADD CONSTRAINT fk_asset_oid_asset_transfer FOREIGN KEY (asset_oid) REFERENCES ast.asset_information(oid);


--
-- Name: contract_info fk_asset_oid_contract_info; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.contract_info
    ADD CONSTRAINT fk_asset_oid_contract_info FOREIGN KEY (asset_oid) REFERENCES ast.asset_information(oid);


--
-- Name: tender fk_asset_oid_tender; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.tender
    ADD CONSTRAINT fk_asset_oid_tender FOREIGN KEY (asset_oid) REFERENCES ast.asset_information(oid);


--
-- Name: asset_allocation fk_asset_requisition_oid_asset_allocation; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_allocation
    ADD CONSTRAINT fk_asset_requisition_oid_asset_allocation FOREIGN KEY (asset_requisition_oid) REFERENCES ast.asset_requisition(oid);


--
-- Name: asset_requisition_line fk_asset_requisition_oid_asset_requisition_line; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_requisition_line
    ADD CONSTRAINT fk_asset_requisition_oid_asset_requisition_line FOREIGN KEY (asset_requisition_oid) REFERENCES ast.asset_requisition(oid);


--
-- Name: asset_return fk_asset_requisition_oid_asset_return; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_return
    ADD CONSTRAINT fk_asset_requisition_oid_asset_return FOREIGN KEY (asset_requisition_oid) REFERENCES ast.asset_requisition(oid);


--
-- Name: asset_tracking_line fk_asset_tracking_oid_asset_tracking_line; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_tracking_line
    ADD CONSTRAINT fk_asset_tracking_oid_asset_tracking_line FOREIGN KEY (asset_tracking_oid) REFERENCES ast.asset_tracking(oid);


--
-- Name: asset_maintenance_line fk_assigned_vendor_asset_maintenance_line; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_maintenance_line
    ADD CONSTRAINT fk_assigned_vendor_asset_maintenance_line FOREIGN KEY (assigned_vendor) REFERENCES cmn.vendor(oid);


--
-- Name: floor_info fk_building_id_floor_info; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.floor_info
    ADD CONSTRAINT fk_building_id_floor_info FOREIGN KEY (building_id) REFERENCES ast.asset_information(oid);


--
-- Name: asset_tracking fk_changed_by_asset_tracking; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_tracking
    ADD CONSTRAINT fk_changed_by_asset_tracking FOREIGN KEY (changed_by) REFERENCES hrm.employee(oid);


--
-- Name: asset_complaint_letter fk_committee_oid_asset_complaint_letter; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_complaint_letter
    ADD CONSTRAINT fk_committee_oid_asset_complaint_letter FOREIGN KEY (committee_oid) REFERENCES cmn.committee_setup(oid);


--
-- Name: asset_disposal fk_committee_oid_asset_disposal; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_disposal
    ADD CONSTRAINT fk_committee_oid_asset_disposal FOREIGN KEY (committee_oid) REFERENCES cmn.committee_setup(oid);


--
-- Name: asset_lost_info fk_committee_oid_asset_lost_info; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_lost_info
    ADD CONSTRAINT fk_committee_oid_asset_lost_info FOREIGN KEY (committee_oid) REFERENCES cmn.committee_setup(oid);


--
-- Name: contract_renewal fk_contract_info_oid_contract_renewal; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.contract_renewal
    ADD CONSTRAINT fk_contract_info_oid_contract_renewal FOREIGN KEY (contract_info_oid) REFERENCES ast.contract_info(oid);


--
-- Name: rent_receive fk_contract_info_oid_rent_receive; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.rent_receive
    ADD CONSTRAINT fk_contract_info_oid_rent_receive FOREIGN KEY (contract_info_oid) REFERENCES ast.contract_info(oid);


--
-- Name: asset_requisition fk_decided_by_asset_requisition; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_requisition
    ADD CONSTRAINT fk_decided_by_asset_requisition FOREIGN KEY (decided_by) REFERENCES hrm.employee(oid);


--
-- Name: asset_disposal fk_decision_by_asset_disposal; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_disposal
    ADD CONSTRAINT fk_decision_by_asset_disposal FOREIGN KEY (decision_by) REFERENCES hrm.employee(oid);


--
-- Name: asset_maintenance_line fk_decision_by_asset_maintenance_line; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_maintenance_line
    ADD CONSTRAINT fk_decision_by_asset_maintenance_line FOREIGN KEY (decision_by) REFERENCES hrm.employee(oid);


--
-- Name: vehicle_requisition fk_decision_by_vehicle_requisition; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.vehicle_requisition
    ADD CONSTRAINT fk_decision_by_vehicle_requisition FOREIGN KEY (decision_by) REFERENCES hrm.employee(oid);


--
-- Name: asset_allocation fk_distributor_oid_asset_allocation; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_allocation
    ADD CONSTRAINT fk_distributor_oid_asset_allocation FOREIGN KEY (distributor_oid) REFERENCES hrm.employee(oid);


--
-- Name: vehicle_allocation fk_distributor_oid_vehicle_allocation; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.vehicle_allocation
    ADD CONSTRAINT fk_distributor_oid_vehicle_allocation FOREIGN KEY (distributor_oid) REFERENCES hrm.employee(oid);


--
-- Name: asset_allocation fk_document_oid_asset_allocation; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_allocation
    ADD CONSTRAINT fk_document_oid_asset_allocation FOREIGN KEY (document_oid) REFERENCES cmn.document(oid);


--
-- Name: fuel_reconciliation fk_document_oid_fuel_reconciliation; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.fuel_reconciliation
    ADD CONSTRAINT fk_document_oid_fuel_reconciliation FOREIGN KEY (document_oid) REFERENCES cmn.document(oid);


--
-- Name: tender fk_document_oid_tender; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.tender
    ADD CONSTRAINT fk_document_oid_tender FOREIGN KEY (document_oid) REFERENCES cmn.document(oid);


--
-- Name: fuel_reconciliation fk_driver_oid_fuel_reconciliation; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.fuel_reconciliation
    ADD CONSTRAINT fk_driver_oid_fuel_reconciliation FOREIGN KEY (driver_oid) REFERENCES ast.contractual_driver(oid);


--
-- Name: fuel_requisition fk_driver_oid_fuel_requisition; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.fuel_requisition
    ADD CONSTRAINT fk_driver_oid_fuel_requisition FOREIGN KEY (driver_oid) REFERENCES ast.contractual_driver(oid);


--
-- Name: vehicle_requisition fk_driver_oid_vehicle_requisition; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.vehicle_requisition
    ADD CONSTRAINT fk_driver_oid_vehicle_requisition FOREIGN KEY (driver_oid) REFERENCES ast.contractual_driver(oid);


--
-- Name: route_config fk_employee_oid_route_config; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.route_config
    ADD CONSTRAINT fk_employee_oid_route_config FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: asset_allocation fk_end_user_oid_asset_allocation; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_allocation
    ADD CONSTRAINT fk_end_user_oid_asset_allocation FOREIGN KEY (end_user_oid) REFERENCES hrm.employee(oid);


--
-- Name: flat_info fk_floor_info_oid_flat_info; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.flat_info
    ADD CONSTRAINT fk_floor_info_oid_flat_info FOREIGN KEY (floor_info_oid) REFERENCES ast.floor_info(oid);


--
-- Name: ticket_generation fk_fuel_requisition_oid_ticket_generation; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.ticket_generation
    ADD CONSTRAINT fk_fuel_requisition_oid_ticket_generation FOREIGN KEY (fuel_requisition_oid) REFERENCES ast.fuel_requisition(oid);


--
-- Name: fuel_service_station fk_fuel_type_oid_fuel_service_station; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.fuel_service_station
    ADD CONSTRAINT fk_fuel_type_oid_fuel_service_station FOREIGN KEY (fuel_type_oid) REFERENCES ast.fuel_type(oid);


--
-- Name: vehicle_info fk_fuel_type_vehicle_info; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.vehicle_info
    ADD CONSTRAINT fk_fuel_type_vehicle_info FOREIGN KEY (fuel_type) REFERENCES ast.fuel_type(oid);


--
-- Name: vehicle_info fk_garage_oid_vehicle_info; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.vehicle_info
    ADD CONSTRAINT fk_garage_oid_vehicle_info FOREIGN KEY (garage_oid) REFERENCES ast.garage(oid);


--
-- Name: vehicle_return fk_garage_oid_vehicle_return; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.vehicle_return
    ADD CONSTRAINT fk_garage_oid_vehicle_return FOREIGN KEY (garage_oid) REFERENCES ast.garage(oid);


--
-- Name: asset_information fk_item_oid_asset_information; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_information
    ADD CONSTRAINT fk_item_oid_asset_information FOREIGN KEY (item_oid) REFERENCES cmn.item(oid);


--
-- Name: temporary_item_detail fk_item_oid_temporary_item_detail; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.temporary_item_detail
    ADD CONSTRAINT fk_item_oid_temporary_item_detail FOREIGN KEY (item_oid) REFERENCES cmn.item(oid);


--
-- Name: asset_maintenance_line fk_maintenance_oid_asset_maintenance_line; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_maintenance_line
    ADD CONSTRAINT fk_maintenance_oid_asset_maintenance_line FOREIGN KEY (maintenance_oid) REFERENCES ast.asset_maintenance(oid);


--
-- Name: asset_maintenance_line fk_maintenance_type_oid_asset_maintenance_line; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_maintenance_line
    ADD CONSTRAINT fk_maintenance_type_oid_asset_maintenance_line FOREIGN KEY (maintenance_type_oid) REFERENCES ast.asset_maintenance_type(oid);


--
-- Name: rules_regulation fk_method_category_oid_rules_regulation; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.rules_regulation
    ADD CONSTRAINT fk_method_category_oid_rules_regulation FOREIGN KEY (method_category_oid) REFERENCES ast.method_rule_category(oid);


--
-- Name: method_setup fk_method_rule_category_oid_method_setup; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.method_setup
    ADD CONSTRAINT fk_method_rule_category_oid_method_setup FOREIGN KEY (method_rule_category_oid) REFERENCES ast.method_rule_category(oid);


--
-- Name: vehicle_requisition fk_organization_vehicle_requisition; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.vehicle_requisition
    ADD CONSTRAINT fk_organization_vehicle_requisition FOREIGN KEY (organization) REFERENCES cmn.office(oid);


--
-- Name: maintenance_asset_receive fk_qc_committee_maintenance_asset_receive; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.maintenance_asset_receive
    ADD CONSTRAINT fk_qc_committee_maintenance_asset_receive FOREIGN KEY (qc_committee) REFERENCES cmn.committee_setup(oid);


--
-- Name: maintenance_asset_receive fk_received_by_maintenance_asset_receive; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.maintenance_asset_receive
    ADD CONSTRAINT fk_received_by_maintenance_asset_receive FOREIGN KEY (received_by) REFERENCES hrm.employee(oid);


--
-- Name: maintenance_payment fk_received_by_maintenance_payment; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.maintenance_payment
    ADD CONSTRAINT fk_received_by_maintenance_payment FOREIGN KEY (received_by) REFERENCES hrm.employee(oid);


--
-- Name: rent_receive fk_received_by_oid_rent_receive; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.rent_receive
    ADD CONSTRAINT fk_received_by_oid_rent_receive FOREIGN KEY (received_by_oid) REFERENCES hrm.employee(oid);


--
-- Name: asset_return fk_receiver_oid_asset_return; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_return
    ADD CONSTRAINT fk_receiver_oid_asset_return FOREIGN KEY (receiver_oid) REFERENCES hrm.employee(oid);


--
-- Name: vehicle_return fk_receiver_oid_vehicle_return; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.vehicle_return
    ADD CONSTRAINT fk_receiver_oid_vehicle_return FOREIGN KEY (receiver_oid) REFERENCES hrm.employee(oid);


--
-- Name: contract_info fk_rent_or_lease_info_oid_contract_info; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.contract_info
    ADD CONSTRAINT fk_rent_or_lease_info_oid_contract_info FOREIGN KEY (rent_or_lease_info_oid) REFERENCES ast.rent_lease_info(oid);


--
-- Name: asset_disposal fk_requester_oid_asset_disposal; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_disposal
    ADD CONSTRAINT fk_requester_oid_asset_disposal FOREIGN KEY (requester_oid) REFERENCES hrm.employee(oid);


--
-- Name: asset_maintenance fk_requester_oid_asset_maintenance; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_maintenance
    ADD CONSTRAINT fk_requester_oid_asset_maintenance FOREIGN KEY (requester_oid) REFERENCES hrm.employee(oid);


--
-- Name: asset_requisition fk_requester_oid_asset_requisition; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_requisition
    ADD CONSTRAINT fk_requester_oid_asset_requisition FOREIGN KEY (requester_oid) REFERENCES hrm.employee(oid);


--
-- Name: rent_lease_info fk_requester_oid_rent_lease_info; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.rent_lease_info
    ADD CONSTRAINT fk_requester_oid_rent_lease_info FOREIGN KEY (requester_oid) REFERENCES hrm.employee(oid);


--
-- Name: vehicle_requisition fk_requester_oid_vehicle_requisition; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.vehicle_requisition
    ADD CONSTRAINT fk_requester_oid_vehicle_requisition FOREIGN KEY (requester_oid) REFERENCES hrm.employee(oid);


--
-- Name: route_config fk_route_setup_oid_route_config; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.route_config
    ADD CONSTRAINT fk_route_setup_oid_route_config FOREIGN KEY (route_setup_oid) REFERENCES ast.route_setup(oid);


--
-- Name: auto_maintenance fk_set_by_auto_maintenance; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.auto_maintenance
    ADD CONSTRAINT fk_set_by_auto_maintenance FOREIGN KEY (set_by) REFERENCES hrm.employee(oid);


--
-- Name: ticket_generation fk_station_oid_ticket_generation; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.ticket_generation
    ADD CONSTRAINT fk_station_oid_ticket_generation FOREIGN KEY (station_oid) REFERENCES ast.fuel_service_station(oid);


--
-- Name: asset_allocation fk_store_oid_asset_allocation; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_allocation
    ADD CONSTRAINT fk_store_oid_asset_allocation FOREIGN KEY (store_oid) REFERENCES cmn.store(oid);


--
-- Name: asset_disposal fk_store_oid_asset_disposal; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_disposal
    ADD CONSTRAINT fk_store_oid_asset_disposal FOREIGN KEY (store_oid) REFERENCES cmn.store(oid);


--
-- Name: asset_return fk_store_oid_asset_return; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_return
    ADD CONSTRAINT fk_store_oid_asset_return FOREIGN KEY (store_oid) REFERENCES cmn.store(oid);


--
-- Name: acquisition_billing fk_supplier_oid_acquisition_billing; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.acquisition_billing
    ADD CONSTRAINT fk_supplier_oid_acquisition_billing FOREIGN KEY (supplier_oid) REFERENCES cmn.vendor(oid);


--
-- Name: asset_information fk_temporary_item_oid_asset_information; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_information
    ADD CONSTRAINT fk_temporary_item_oid_asset_information FOREIGN KEY (temporary_item_oid) REFERENCES ast.temporary_item(oid);


--
-- Name: temporary_item_detail fk_temporary_item_oid_temporary_item_detail; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.temporary_item_detail
    ADD CONSTRAINT fk_temporary_item_oid_temporary_item_detail FOREIGN KEY (temporary_item_oid) REFERENCES ast.temporary_item(oid);


--
-- Name: contract_info fk_tenant_oid_contract_info; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.contract_info
    ADD CONSTRAINT fk_tenant_oid_contract_info FOREIGN KEY (tenant_oid) REFERENCES ast.tenant(oid);


--
-- Name: rent_lease_info fk_tender_oid_rent_lease_info; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.rent_lease_info
    ADD CONSTRAINT fk_tender_oid_rent_lease_info FOREIGN KEY (tender_oid) REFERENCES ast.tender(oid);


--
-- Name: auto_maintenance fk_to_notify_auto_maintenance; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.auto_maintenance
    ADD CONSTRAINT fk_to_notify_auto_maintenance FOREIGN KEY (to_notify) REFERENCES hrm.employee(oid);


--
-- Name: contractual_driver fk_vehicle_oid_contractual_driver; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.contractual_driver
    ADD CONSTRAINT fk_vehicle_oid_contractual_driver FOREIGN KEY (vehicle_oid) REFERENCES ast.vehicle_info(oid);


--
-- Name: fuel_reconciliation fk_vehicle_oid_fuel_reconciliation; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.fuel_reconciliation
    ADD CONSTRAINT fk_vehicle_oid_fuel_reconciliation FOREIGN KEY (vehicle_oid) REFERENCES ast.vehicle_info(oid);


--
-- Name: fuel_requisition fk_vehicle_oid_fuel_requisition; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.fuel_requisition
    ADD CONSTRAINT fk_vehicle_oid_fuel_requisition FOREIGN KEY (vehicle_oid) REFERENCES ast.vehicle_info(oid);


--
-- Name: route_setup fk_vehicle_oid_route_setup; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.route_setup
    ADD CONSTRAINT fk_vehicle_oid_route_setup FOREIGN KEY (vehicle_oid) REFERENCES ast.vehicle_info(oid);


--
-- Name: vehicle_requisition fk_vehicle_oid_vehicle_requisition; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.vehicle_requisition
    ADD CONSTRAINT fk_vehicle_oid_vehicle_requisition FOREIGN KEY (vehicle_oid) REFERENCES ast.vehicle_info(oid);


--
-- Name: vehicle_allocation fk_vehicle_requisition_oid_vehicle_allocation; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.vehicle_allocation
    ADD CONSTRAINT fk_vehicle_requisition_oid_vehicle_allocation FOREIGN KEY (vehicle_requisition_oid) REFERENCES ast.vehicle_requisition(oid);


--
-- Name: vehicle_return fk_vehicle_requisition_oid_vehicle_return; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.vehicle_return
    ADD CONSTRAINT fk_vehicle_requisition_oid_vehicle_return FOREIGN KEY (vehicle_requisition_oid) REFERENCES ast.vehicle_requisition(oid);


--
-- Name: asset_complaint_letter fk_vendor_oid_asset_complaint_letter; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.asset_complaint_letter
    ADD CONSTRAINT fk_vendor_oid_asset_complaint_letter FOREIGN KEY (vendor_oid) REFERENCES cmn.vendor(oid);


--
-- Name: temporary_item fk_vendor_oid_temporary_item; Type: FK CONSTRAINT; Schema: ast; Owner: grp
--

ALTER TABLE ONLY ast.temporary_item
    ADD CONSTRAINT fk_vendor_oid_temporary_item FOREIGN KEY (vendor_oid) REFERENCES cmn.vendor(oid);


--
-- Name: city_corporation_ward fk_city_corporation_oid_city_corporation_ward; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.city_corporation_ward
    ADD CONSTRAINT fk_city_corporation_oid_city_corporation_ward FOREIGN KEY (city_corporation_oid) REFERENCES cmn.city_corporation(oid);


--
-- Name: committee_setup_line fk_committee_setup_oid_committee_setup_line; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.committee_setup_line
    ADD CONSTRAINT fk_committee_setup_oid_committee_setup_line FOREIGN KEY (committee_setup_oid) REFERENCES cmn.committee_setup(oid);


--
-- Name: committee_setup fk_committee_type_oid_committee_setup; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.committee_setup
    ADD CONSTRAINT fk_committee_type_oid_committee_setup FOREIGN KEY (committee_type_oid) REFERENCES cmn.committee_type_setup(oid);


--
-- Name: geo_division fk_country_oid_geo_division; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.geo_division
    ADD CONSTRAINT fk_country_oid_geo_division FOREIGN KEY (country_oid) REFERENCES cmn.geo_country(oid);


--
-- Name: city_corporation fk_district_oid_city_corporation; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.city_corporation
    ADD CONSTRAINT fk_district_oid_city_corporation FOREIGN KEY (district_oid) REFERENCES cmn.geo_district(oid);


--
-- Name: city_corporation_ward fk_district_oid_city_corporation_ward; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.city_corporation_ward
    ADD CONSTRAINT fk_district_oid_city_corporation_ward FOREIGN KEY (district_oid) REFERENCES cmn.geo_district(oid);


--
-- Name: geo_thana fk_district_oid_geo_thana; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.geo_thana
    ADD CONSTRAINT fk_district_oid_geo_thana FOREIGN KEY (district_oid) REFERENCES cmn.geo_district(oid);


--
-- Name: geo_upazila fk_district_oid_geo_upazila; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.geo_upazila
    ADD CONSTRAINT fk_district_oid_geo_upazila FOREIGN KEY (district_oid) REFERENCES cmn.geo_district(oid);


--
-- Name: geo_district fk_division_oid_geo_district; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.geo_district
    ADD CONSTRAINT fk_division_oid_geo_district FOREIGN KEY (division_oid) REFERENCES cmn.geo_division(oid);


--
-- Name: document fk_document_type_oid_document; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.document
    ADD CONSTRAINT fk_document_type_oid_document FOREIGN KEY (document_type_oid) REFERENCES cmn.document_type(oid);


--
-- Name: entitlement fk_fiscal_year_entitlement; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.entitlement
    ADD CONSTRAINT fk_fiscal_year_entitlement FOREIGN KEY (fiscal_year) REFERENCES cmn.fiscal_year(oid);


--
-- Name: item fk_item_category_oid_item; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.item
    ADD CONSTRAINT fk_item_category_oid_item FOREIGN KEY (item_category_oid) REFERENCES cmn.item_category(oid);


--
-- Name: entitlement fk_item_oid_entitlement; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.entitlement
    ADD CONSTRAINT fk_item_oid_entitlement FOREIGN KEY (item_oid) REFERENCES cmn.item(oid);


--
-- Name: office fk_ministry_oid_office; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office
    ADD CONSTRAINT fk_ministry_oid_office FOREIGN KEY (ministry_oid) REFERENCES cmn.ministry(oid);


--
-- Name: office_layer fk_ministry_oid_office_layer; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_layer
    ADD CONSTRAINT fk_ministry_oid_office_layer FOREIGN KEY (ministry_oid) REFERENCES cmn.ministry(oid);


--
-- Name: office_origin fk_ministry_oid_office_origin; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_origin
    ADD CONSTRAINT fk_ministry_oid_office_origin FOREIGN KEY (ministry_oid) REFERENCES cmn.ministry(oid);


--
-- Name: office_origin_unit fk_ministry_oid_office_origin_unit; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_origin_unit
    ADD CONSTRAINT fk_ministry_oid_office_origin_unit FOREIGN KEY (ministry_oid) REFERENCES cmn.ministry(oid);


--
-- Name: office_unit fk_ministry_oid_office_unit; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_unit
    ADD CONSTRAINT fk_ministry_oid_office_unit FOREIGN KEY (ministry_oid) REFERENCES cmn.ministry(oid);


--
-- Name: ministry fk_ministry_type_oid_ministry; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.ministry
    ADD CONSTRAINT fk_ministry_type_oid_ministry FOREIGN KEY (ministry_type_oid) REFERENCES cmn.ministry_type(oid);


--
-- Name: municipality_ward fk_municipality_oid_municipality_ward; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.municipality_ward
    ADD CONSTRAINT fk_municipality_oid_municipality_ward FOREIGN KEY (municipality_oid) REFERENCES cmn.municipality(oid);


--
-- Name: office fk_office_layer_oid_office; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office
    ADD CONSTRAINT fk_office_layer_oid_office FOREIGN KEY (office_layer_oid) REFERENCES cmn.office_layer(oid);


--
-- Name: office_origin fk_office_layer_oid_office_origin; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_origin
    ADD CONSTRAINT fk_office_layer_oid_office_origin FOREIGN KEY (office_layer_oid) REFERENCES cmn.office_layer(oid);


--
-- Name: office_origin_unit fk_office_layer_oid_office_origin_unit; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_origin_unit
    ADD CONSTRAINT fk_office_layer_oid_office_origin_unit FOREIGN KEY (office_layer_oid) REFERENCES cmn.office_layer(oid);


--
-- Name: office_unit fk_office_layer_oid_office_unit; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_unit
    ADD CONSTRAINT fk_office_layer_oid_office_unit FOREIGN KEY (office_layer_oid) REFERENCES cmn.office_layer(oid);


--
-- Name: office_unit fk_office_oid_office_unit; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_unit
    ADD CONSTRAINT fk_office_oid_office_unit FOREIGN KEY (office_oid) REFERENCES cmn.office(oid);


--
-- Name: office_unit_post fk_office_oid_office_unit_post; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_unit_post
    ADD CONSTRAINT fk_office_oid_office_unit_post FOREIGN KEY (office_oid) REFERENCES cmn.office(oid);


--
-- Name: office fk_office_origin_oid_office; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office
    ADD CONSTRAINT fk_office_origin_oid_office FOREIGN KEY (office_origin_oid) REFERENCES cmn.office_origin(oid);


--
-- Name: office_origin_unit fk_office_origin_oid_office_origin_unit; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_origin_unit
    ADD CONSTRAINT fk_office_origin_oid_office_origin_unit FOREIGN KEY (office_origin_oid) REFERENCES cmn.office_origin(oid);


--
-- Name: office_origin_unit_post fk_office_origin_unit_oid_office_origin_unit_post; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_origin_unit_post
    ADD CONSTRAINT fk_office_origin_unit_oid_office_origin_unit_post FOREIGN KEY (office_origin_unit_oid) REFERENCES cmn.office_origin_unit(oid);


--
-- Name: office_unit fk_office_origin_unit_oid_office_unit; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_unit
    ADD CONSTRAINT fk_office_origin_unit_oid_office_unit FOREIGN KEY (office_origin_unit_oid) REFERENCES cmn.office_origin_unit(oid);


--
-- Name: office_unit_post fk_office_origin_unit_post_oid_office_unit_post; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_unit_post
    ADD CONSTRAINT fk_office_origin_unit_post_oid_office_unit_post FOREIGN KEY (office_origin_unit_post_oid) REFERENCES cmn.office_origin_unit_post(oid);


--
-- Name: office_origin_unit fk_office_unit_category_oid_office_origin_unit; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_origin_unit
    ADD CONSTRAINT fk_office_unit_category_oid_office_origin_unit FOREIGN KEY (office_unit_category_oid) REFERENCES cmn.office_unit_category(oid);


--
-- Name: office_unit fk_office_unit_category_oid_office_unit; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_unit
    ADD CONSTRAINT fk_office_unit_category_oid_office_unit FOREIGN KEY (office_unit_category_oid) REFERENCES cmn.office_unit_category(oid);


--
-- Name: office_unit_post fk_office_unit_oid_office_unit_post; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_unit_post
    ADD CONSTRAINT fk_office_unit_oid_office_unit_post FOREIGN KEY (office_unit_oid) REFERENCES cmn.office_unit(oid);


--
-- Name: office_unit fk_parent_office_origin_unit_oid_office_unit; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_unit
    ADD CONSTRAINT fk_parent_office_origin_unit_oid_office_unit FOREIGN KEY (parent_office_origin_unit_oid) REFERENCES cmn.office_origin_unit(oid);


--
-- Name: office_unit_post fk_parent_office_unit_oid_office_unit_post; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_unit_post
    ADD CONSTRAINT fk_parent_office_unit_oid_office_unit_post FOREIGN KEY (parent_office_unit_oid) REFERENCES cmn.office_unit(oid);


--
-- Name: office_layer fk_parent_oid_office_layer; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.office_layer
    ADD CONSTRAINT fk_parent_oid_office_layer FOREIGN KEY (parent_oid) REFERENCES cmn.office_layer(oid);


--
-- Name: donee_info fk_party_type_oid_donee_info; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.donee_info
    ADD CONSTRAINT fk_party_type_oid_donee_info FOREIGN KEY (party_type_oid) REFERENCES cmn.party_type(oid);


--
-- Name: donor_info fk_party_type_oid_donor_info; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.donor_info
    ADD CONSTRAINT fk_party_type_oid_donor_info FOREIGN KEY (party_type_oid) REFERENCES cmn.party_type(oid);


--
-- Name: geo_post_office fk_union_oid_geo_post_office; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.geo_post_office
    ADD CONSTRAINT fk_union_oid_geo_post_office FOREIGN KEY (union_oid) REFERENCES cmn.geo_union(oid);


--
-- Name: geo_union_ward fk_union_oid_geo_union_ward; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.geo_union_ward
    ADD CONSTRAINT fk_union_oid_geo_union_ward FOREIGN KEY (union_oid) REFERENCES cmn.geo_union(oid);


--
-- Name: item fk_uom_oid_item; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.item
    ADD CONSTRAINT fk_uom_oid_item FOREIGN KEY (uom_oid) REFERENCES cmn.item_uom(oid);


--
-- Name: geo_union fk_upazila_oid_geo_union; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.geo_union
    ADD CONSTRAINT fk_upazila_oid_geo_union FOREIGN KEY (upazila_oid) REFERENCES cmn.geo_upazila(oid);


--
-- Name: municipality fk_upazila_oid_municipality; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.municipality
    ADD CONSTRAINT fk_upazila_oid_municipality FOREIGN KEY (upazila_oid) REFERENCES cmn.geo_upazila(oid);


--
-- Name: municipality_ward fk_upazila_oid_municipality_ward; Type: FK CONSTRAINT; Schema: cmn; Owner: grp
--

ALTER TABLE ONLY cmn.municipality_ward
    ADD CONSTRAINT fk_upazila_oid_municipality_ward FOREIGN KEY (upazila_oid) REFERENCES cmn.geo_upazila(oid);


--
-- Name: admit_card fk_applicant_oid_admit_card; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.admit_card
    ADD CONSTRAINT fk_applicant_oid_admit_card FOREIGN KEY (applicant_oid) REFERENCES hrm.application_form_for_recruitment(oid);


--
-- Name: apply_for_leave fk_applicant_oid_apply_for_leave; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.apply_for_leave
    ADD CONSTRAINT fk_applicant_oid_apply_for_leave FOREIGN KEY (applicant_oid) REFERENCES hrm.employee(oid);


--
-- Name: exam_venue fk_applicant_oid_exam_venue; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.exam_venue
    ADD CONSTRAINT fk_applicant_oid_exam_venue FOREIGN KEY (applicant_oid) REFERENCES hrm.application_form_for_recruitment(oid);


--
-- Name: recruitment_result fk_applicant_oid_recruitment_result; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.recruitment_result
    ADD CONSTRAINT fk_applicant_oid_recruitment_result FOREIGN KEY (applicant_oid) REFERENCES hrm.application_form_for_recruitment(oid);


--
-- Name: select_candidate fk_applicant_oid_select_candidate; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.select_candidate
    ADD CONSTRAINT fk_applicant_oid_select_candidate FOREIGN KEY (applicant_oid) REFERENCES hrm.application_form_for_recruitment(oid);


--
-- Name: create_short_list fk_application_code_create_short_list; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.create_short_list
    ADD CONSTRAINT fk_application_code_create_short_list FOREIGN KEY (application_code) REFERENCES hrm.application_form_for_recruitment(oid);


--
-- Name: leave_approval_by_admin fk_application_oid_leave_approval_by_admin; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.leave_approval_by_admin
    ADD CONSTRAINT fk_application_oid_leave_approval_by_admin FOREIGN KEY (application_oid) REFERENCES hrm.apply_for_leave(oid);


--
-- Name: job_circular_approval fk_approved_by_oid_job_circular_approval; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.job_circular_approval
    ADD CONSTRAINT fk_approved_by_oid_job_circular_approval FOREIGN KEY (approved_by_oid) REFERENCES hrm.employee(oid);


--
-- Name: leave_approval_by_admin fk_approved_by_oid_leave_approval_by_admin; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.leave_approval_by_admin
    ADD CONSTRAINT fk_approved_by_oid_leave_approval_by_admin FOREIGN KEY (approved_by_oid) REFERENCES hrm.employee(oid);


--
-- Name: rules_of_award_publication fk_assigned_by_rules_of_award_publication; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.rules_of_award_publication
    ADD CONSTRAINT fk_assigned_by_rules_of_award_publication FOREIGN KEY (assigned_by) REFERENCES hrm.employee(oid);


--
-- Name: rules_of_vesting_magisterial_power fk_assigned_by_rules_of_vesting_magisterial_power; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.rules_of_vesting_magisterial_power
    ADD CONSTRAINT fk_assigned_by_rules_of_vesting_magisterial_power FOREIGN KEY (assigned_by) REFERENCES hrm.employee(oid);


--
-- Name: rules_regarding_acr_apa fk_assigned_by_rules_regarding_acr_apa; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.rules_regarding_acr_apa
    ADD CONSTRAINT fk_assigned_by_rules_regarding_acr_apa FOREIGN KEY (assigned_by) REFERENCES hrm.employee(oid);


--
-- Name: rules_regarding_attendance fk_assigned_by_rules_regarding_attendance; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.rules_regarding_attendance
    ADD CONSTRAINT fk_assigned_by_rules_regarding_attendance FOREIGN KEY (assigned_by) REFERENCES hrm.employee(oid);


--
-- Name: rules_regarding_discipline fk_assigned_by_rules_regarding_discipline; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.rules_regarding_discipline
    ADD CONSTRAINT fk_assigned_by_rules_regarding_discipline FOREIGN KEY (assigned_by) REFERENCES hrm.employee(oid);


--
-- Name: rules_regarding_leave fk_assigned_by_rules_regarding_leave; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.rules_regarding_leave
    ADD CONSTRAINT fk_assigned_by_rules_regarding_leave FOREIGN KEY (assigned_by) REFERENCES hrm.employee(oid);


--
-- Name: rules_regarding_posting fk_assigned_by_rules_regarding_posting; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.rules_regarding_posting
    ADD CONSTRAINT fk_assigned_by_rules_regarding_posting FOREIGN KEY (assigned_by) REFERENCES hrm.employee(oid);


--
-- Name: rules_regarding_promotion fk_assigned_by_rules_regarding_promotion; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.rules_regarding_promotion
    ADD CONSTRAINT fk_assigned_by_rules_regarding_promotion FOREIGN KEY (assigned_by) REFERENCES hrm.employee(oid);


--
-- Name: rules_regarding_recruitment fk_assigned_by_rules_regarding_recruitment; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.rules_regarding_recruitment
    ADD CONSTRAINT fk_assigned_by_rules_regarding_recruitment FOREIGN KEY (assigned_by) REFERENCES hrm.employee(oid);


--
-- Name: rules_regarding_training fk_assigned_by_rules_regarding_training; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.rules_regarding_training
    ADD CONSTRAINT fk_assigned_by_rules_regarding_training FOREIGN KEY (assigned_by) REFERENCES hrm.employee(oid);


--
-- Name: penalty_committee_list fk_attendance_committee_oid_penalty_committee_list; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.penalty_committee_list
    ADD CONSTRAINT fk_attendance_committee_oid_penalty_committee_list FOREIGN KEY (attendance_committee_oid) REFERENCES hrm.attendance_penalty_committee(oid);


--
-- Name: grade fk_cadre_oid_grade; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.grade
    ADD CONSTRAINT fk_cadre_oid_grade FOREIGN KEY (cadre_oid) REFERENCES hrm.cadre(oid);


--
-- Name: final_selection fk_candidate_oid_final_selection; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.final_selection
    ADD CONSTRAINT fk_candidate_oid_final_selection FOREIGN KEY (candidate_oid) REFERENCES hrm.select_candidate(oid);


--
-- Name: application_form_for_recruitment fk_circular_oid_application_form_for_recruitment; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.application_form_for_recruitment
    ADD CONSTRAINT fk_circular_oid_application_form_for_recruitment FOREIGN KEY (circular_oid) REFERENCES hrm.job_circular(oid);


--
-- Name: call_for_exam fk_circular_oid_call_for_exam; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.call_for_exam
    ADD CONSTRAINT fk_circular_oid_call_for_exam FOREIGN KEY (circular_oid) REFERENCES hrm.job_circular(oid);


--
-- Name: job_circular_approval fk_circular_oid_job_circular_approval; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.job_circular_approval
    ADD CONSTRAINT fk_circular_oid_job_circular_approval FOREIGN KEY (circular_oid) REFERENCES hrm.job_circular(oid);


--
-- Name: recruitment_result fk_circular_oid_recruitment_result; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.recruitment_result
    ADD CONSTRAINT fk_circular_oid_recruitment_result FOREIGN KEY (circular_oid) REFERENCES hrm.job_circular(oid);


--
-- Name: select_candidate fk_circular_oid_select_candidate; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.select_candidate
    ADD CONSTRAINT fk_circular_oid_select_candidate FOREIGN KEY (circular_oid) REFERENCES hrm.job_circular(oid);


--
-- Name: attendance_penalty_committee fk_committee_oid_attendance_penalty_committee; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.attendance_penalty_committee
    ADD CONSTRAINT fk_committee_oid_attendance_penalty_committee FOREIGN KEY (committee_oid) REFERENCES cmn.committee_setup_line(oid);


--
-- Name: investigation_team_setup fk_committee_oid_investigation_team_setup; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.investigation_team_setup
    ADD CONSTRAINT fk_committee_oid_investigation_team_setup FOREIGN KEY (committee_oid) REFERENCES cmn.committee_setup(oid);


--
-- Name: recruitment_committee fk_committee_oid_recruitment_committee; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.recruitment_committee
    ADD CONSTRAINT fk_committee_oid_recruitment_committee FOREIGN KEY (committee_oid) REFERENCES cmn.committee_setup_line(oid);


--
-- Name: apply_for_prl fk_decision_by_apply_for_prl; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.apply_for_prl
    ADD CONSTRAINT fk_decision_by_apply_for_prl FOREIGN KEY (decision_by) REFERENCES hrm.employee(oid);


--
-- Name: award_application fk_decision_by_award_application; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.award_application
    ADD CONSTRAINT fk_decision_by_award_application FOREIGN KEY (decision_by) REFERENCES hrm.employee(oid);


--
-- Name: pension_procedure_for_employee fk_decision_by_pension_procedure_for_employee; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.pension_procedure_for_employee
    ADD CONSTRAINT fk_decision_by_pension_procedure_for_employee FOREIGN KEY (decision_by) REFERENCES hrm.employee(oid);


--
-- Name: publication_application fk_decision_by_publication_application; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.publication_application
    ADD CONSTRAINT fk_decision_by_publication_application FOREIGN KEY (decision_by) REFERENCES hrm.employee(oid);


--
-- Name: termination fk_decision_by_termination; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.termination
    ADD CONSTRAINT fk_decision_by_termination FOREIGN KEY (decision_by) REFERENCES hrm.employee(oid);


--
-- Name: education_qualification fk_employee_info_oid_education_qualification; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.education_qualification
    ADD CONSTRAINT fk_employee_info_oid_education_qualification FOREIGN KEY (employee_info_oid) REFERENCES hrm.employee(oid);


--
-- Name: service_history fk_employee_info_oid_service_history; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.service_history
    ADD CONSTRAINT fk_employee_info_oid_service_history FOREIGN KEY (employee_info_oid) REFERENCES hrm.employee(oid);


--
-- Name: acr_main_report fk_employee_oid_acr_main_report; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.acr_main_report
    ADD CONSTRAINT fk_employee_oid_acr_main_report FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: acr_medical_report fk_employee_oid_acr_medical_report; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.acr_medical_report
    ADD CONSTRAINT fk_employee_oid_acr_medical_report FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: acr_summary fk_employee_oid_acr_summary; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.acr_summary
    ADD CONSTRAINT fk_employee_oid_acr_summary FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: acr_under_controlling_auth fk_employee_oid_acr_under_controlling_auth; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.acr_under_controlling_auth
    ADD CONSTRAINT fk_employee_oid_acr_under_controlling_auth FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: acr_under_controlling_ministry fk_employee_oid_acr_under_controlling_ministry; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.acr_under_controlling_ministry
    ADD CONSTRAINT fk_employee_oid_acr_under_controlling_ministry FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: acr_under_counter_signing_auth fk_employee_oid_acr_under_counter_signing_auth; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.acr_under_counter_signing_auth
    ADD CONSTRAINT fk_employee_oid_acr_under_counter_signing_auth FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: apply_for_prl fk_employee_oid_apply_for_prl; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.apply_for_prl
    ADD CONSTRAINT fk_employee_oid_apply_for_prl FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: at_leave fk_employee_oid_at_leave; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.at_leave
    ADD CONSTRAINT fk_employee_oid_at_leave FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: at_meeting_or_events fk_employee_oid_at_meeting_or_events; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.at_meeting_or_events
    ADD CONSTRAINT fk_employee_oid_at_meeting_or_events FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: award_application fk_employee_oid_award_application; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.award_application
    ADD CONSTRAINT fk_employee_oid_award_application FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: daily_attendance fk_employee_oid_daily_attendance; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.daily_attendance
    ADD CONSTRAINT fk_employee_oid_daily_attendance FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: deputation_info fk_employee_oid_deputation_info; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.deputation_info
    ADD CONSTRAINT fk_employee_oid_deputation_info FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: employee_in_queue_for_promotion_or_posting fk_employee_oid_employee_in_queue_for_promotion_or_posting; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.employee_in_queue_for_promotion_or_posting
    ADD CONSTRAINT fk_employee_oid_employee_in_queue_for_promotion_or_posting FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: investigation_team_feedback fk_employee_oid_investigation_team_feedback; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.investigation_team_feedback
    ADD CONSTRAINT fk_employee_oid_investigation_team_feedback FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: investigation_team_setup fk_employee_oid_investigation_team_setup; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.investigation_team_setup
    ADD CONSTRAINT fk_employee_oid_investigation_team_setup FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: investigation_under_appointing_auth fk_employee_oid_investigation_under_appointing_auth; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.investigation_under_appointing_auth
    ADD CONSTRAINT fk_employee_oid_investigation_under_appointing_auth FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: investigation_under_controlling_auth fk_employee_oid_investigation_under_controlling_auth; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.investigation_under_controlling_auth
    ADD CONSTRAINT fk_employee_oid_investigation_under_controlling_auth FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: justify_against_penalty fk_employee_oid_justify_against_penalty; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.justify_against_penalty
    ADD CONSTRAINT fk_employee_oid_justify_against_penalty FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: penalty_for_attendance fk_employee_oid_penalty_for_attendance; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.penalty_for_attendance
    ADD CONSTRAINT fk_employee_oid_penalty_for_attendance FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: pension_procedure_for_employee fk_employee_oid_pension_procedure_for_employee; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.pension_procedure_for_employee
    ADD CONSTRAINT fk_employee_oid_pension_procedure_for_employee FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: posting_info fk_employee_oid_posting_info; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.posting_info
    ADD CONSTRAINT fk_employee_oid_posting_info FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: promotion_detail fk_employee_oid_promotion_detail; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.promotion_detail
    ADD CONSTRAINT fk_employee_oid_promotion_detail FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: publication_application fk_employee_oid_publication_application; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.publication_application
    ADD CONSTRAINT fk_employee_oid_publication_application FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: termination fk_employee_oid_termination; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.termination
    ADD CONSTRAINT fk_employee_oid_termination FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: vesting_of_magisterial_power fk_employee_oid_vesting_of_magisterial_power; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.vesting_of_magisterial_power
    ADD CONSTRAINT fk_employee_oid_vesting_of_magisterial_power FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: call_for_exam fk_exam_configuration_oid_call_for_exam; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.call_for_exam
    ADD CONSTRAINT fk_exam_configuration_oid_call_for_exam FOREIGN KEY (exam_configuration_oid) REFERENCES hrm.exam_configuration(oid);


--
-- Name: admit_card fk_exam_oid_admit_card; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.admit_card
    ADD CONSTRAINT fk_exam_oid_admit_card FOREIGN KEY (exam_oid) REFERENCES hrm.call_for_exam(oid);


--
-- Name: exam_venue fk_exam_oid_exam_venue; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.exam_venue
    ADD CONSTRAINT fk_exam_oid_exam_venue FOREIGN KEY (exam_oid) REFERENCES hrm.call_for_exam(oid);


--
-- Name: apply_for_leave fk_leave_oid_apply_for_leave; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.apply_for_leave
    ADD CONSTRAINT fk_leave_oid_apply_for_leave FOREIGN KEY (leave_oid) REFERENCES hrm.leave_setup(oid);


--
-- Name: leave_approval_by_admin fk_leave_oid_leave_approval_by_admin; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.leave_approval_by_admin
    ADD CONSTRAINT fk_leave_oid_leave_approval_by_admin FOREIGN KEY (leave_oid) REFERENCES hrm.leave_setup(oid);


--
-- Name: leave_approval_by_substitute fk_leave_oid_leave_approval_by_substitute; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.leave_approval_by_substitute
    ADD CONSTRAINT fk_leave_oid_leave_approval_by_substitute FOREIGN KEY (leave_oid) REFERENCES hrm.leave_setup(oid);


--
-- Name: at_leave fk_leave_status_oid_at_leave; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.at_leave
    ADD CONSTRAINT fk_leave_status_oid_at_leave FOREIGN KEY (leave_status_oid) REFERENCES hrm.daily_attendance(oid);


--
-- Name: attendance_penalty_committee fk_member_oid_attendance_penalty_committee; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.attendance_penalty_committee
    ADD CONSTRAINT fk_member_oid_attendance_penalty_committee FOREIGN KEY (member_oid) REFERENCES hrm.employee(oid);


--
-- Name: posting_and_promotion_committee_setup fk_member_oid_posting_and_promotion_committee_setup; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.posting_and_promotion_committee_setup
    ADD CONSTRAINT fk_member_oid_posting_and_promotion_committee_setup FOREIGN KEY (member_oid) REFERENCES hrm.employee(oid);


--
-- Name: recruitment_committee fk_member_oid_recruitment_committee; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.recruitment_committee
    ADD CONSTRAINT fk_member_oid_recruitment_committee FOREIGN KEY (member_oid) REFERENCES hrm.employee(oid);


--
-- Name: justify_against_penalty fk_penalty_oid_justify_against_penalty; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.justify_against_penalty
    ADD CONSTRAINT fk_penalty_oid_justify_against_penalty FOREIGN KEY (penalty_oid) REFERENCES hrm.penalty_for_attendance(oid);


--
-- Name: employee fk_present_office_unit_post_oid_employee; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.employee
    ADD CONSTRAINT fk_present_office_unit_post_oid_employee FOREIGN KEY (present_office_unit_post_oid) REFERENCES cmn.office_unit_post(oid);


--
-- Name: create_short_list fk_short_list_created_by_oid_create_short_list; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.create_short_list
    ADD CONSTRAINT fk_short_list_created_by_oid_create_short_list FOREIGN KEY (short_list_created_by_oid) REFERENCES hrm.employee(oid);


--
-- Name: exam_venue fk_shortlist_oid_exam_venue; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.exam_venue
    ADD CONSTRAINT fk_shortlist_oid_exam_venue FOREIGN KEY (shortlist_oid) REFERENCES hrm.create_short_list(oid);


--
-- Name: apply_for_leave fk_substitute_oid_apply_for_leave; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.apply_for_leave
    ADD CONSTRAINT fk_substitute_oid_apply_for_leave FOREIGN KEY (substitute_oid) REFERENCES hrm.employee(oid);


--
-- Name: leave_approval_by_substitute fk_substitute_oid_leave_approval_by_substitute; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.leave_approval_by_substitute
    ADD CONSTRAINT fk_substitute_oid_leave_approval_by_substitute FOREIGN KEY (substitute_oid) REFERENCES hrm.employee(oid);


--
-- Name: survey_for_trainee fk_survey_question_code_survey_for_trainee; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.survey_for_trainee
    ADD CONSTRAINT fk_survey_question_code_survey_for_trainee FOREIGN KEY (survey_question_code) REFERENCES hrm.survey_questionnaire_for_trainee(oid);


--
-- Name: survey_for_trainer fk_survey_question_code_survey_for_trainer; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.survey_for_trainer
    ADD CONSTRAINT fk_survey_question_code_survey_for_trainer FOREIGN KEY (survey_question_code) REFERENCES hrm.survey_questionnaire_for_trainee(oid);


--
-- Name: acr_under_counter_signing_auth fk_total_marks_ca_acr_under_counter_signing_auth; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.acr_under_counter_signing_auth
    ADD CONSTRAINT fk_total_marks_ca_acr_under_counter_signing_auth FOREIGN KEY (total_marks_ca) REFERENCES hrm.acr_under_controlling_auth(oid);


--
-- Name: survey_for_trainee fk_trainee_id_survey_for_trainee; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.survey_for_trainee
    ADD CONSTRAINT fk_trainee_id_survey_for_trainee FOREIGN KEY (trainee_id) REFERENCES hrm.employee(oid);


--
-- Name: pending_participation_confirmation fk_trainee_oid_pending_participation_confirmation; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.pending_participation_confirmation
    ADD CONSTRAINT fk_trainee_oid_pending_participation_confirmation FOREIGN KEY (trainee_oid) REFERENCES hrm.employee(oid);


--
-- Name: trainee_result fk_trainee_trainee_result; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.trainee_result
    ADD CONSTRAINT fk_trainee_trainee_result FOREIGN KEY (trainee) REFERENCES hrm.employee(oid);


--
-- Name: survey_for_trainer fk_trainer_code_survey_for_trainer; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.survey_for_trainer
    ADD CONSTRAINT fk_trainer_code_survey_for_trainer FOREIGN KEY (trainer_code) REFERENCES hrm.employee(oid);


--
-- Name: survey_for_trainee fk_trainer_detail_oid_survey_for_trainee; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.survey_for_trainee
    ADD CONSTRAINT fk_trainer_detail_oid_survey_for_trainee FOREIGN KEY (trainer_detail_oid) REFERENCES hrm.trainer_detail(oid);


--
-- Name: survey_for_trainer fk_trainer_detail_oid_survey_for_trainer; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.survey_for_trainer
    ADD CONSTRAINT fk_trainer_detail_oid_survey_for_trainer FOREIGN KEY (trainer_detail_oid) REFERENCES hrm.trainer_detail(oid);


--
-- Name: survey_questionnaire_for_trainer fk_trainer_detail_oid_survey_questionnaire_for_trainer; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.survey_questionnaire_for_trainer
    ADD CONSTRAINT fk_trainer_detail_oid_survey_questionnaire_for_trainer FOREIGN KEY (trainer_detail_oid) REFERENCES hrm.trainer_detail(oid);


--
-- Name: trainer_detail fk_trainer_oid_trainer_detail; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.trainer_detail
    ADD CONSTRAINT fk_trainer_oid_trainer_detail FOREIGN KEY (trainer_oid) REFERENCES hrm.employee(oid);


--
-- Name: trainee_result fk_trainer_trainee_result; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.trainee_result
    ADD CONSTRAINT fk_trainer_trainee_result FOREIGN KEY (trainer) REFERENCES hrm.employee(oid);


--
-- Name: pending_participation_confirmation fk_training_oid_pending_participation_confirmation; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.pending_participation_confirmation
    ADD CONSTRAINT fk_training_oid_pending_participation_confirmation FOREIGN KEY (training_oid) REFERENCES hrm.training_type(oid);


--
-- Name: survey_questionnaire_for_trainee fk_training_oid_survey_questionnaire_for_trainee; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.survey_questionnaire_for_trainee
    ADD CONSTRAINT fk_training_oid_survey_questionnaire_for_trainee FOREIGN KEY (training_oid) REFERENCES hrm.training_type(oid);


--
-- Name: trainee_result fk_training_trainee_result; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.trainee_result
    ADD CONSTRAINT fk_training_trainee_result FOREIGN KEY (training) REFERENCES hrm.training_type(oid);


--
-- Name: foreign_training fk_training_type_oid_foreign_training; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.foreign_training
    ADD CONSTRAINT fk_training_type_oid_foreign_training FOREIGN KEY (training_type_oid) REFERENCES hrm.training_type(oid);


--
-- Name: in_service_training fk_training_type_oid_in_service_training; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.in_service_training
    ADD CONSTRAINT fk_training_type_oid_in_service_training FOREIGN KEY (training_type_oid) REFERENCES hrm.training_type(oid);


--
-- Name: organisational_training fk_training_type_oid_organisational_training; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.organisational_training
    ADD CONSTRAINT fk_training_type_oid_organisational_training FOREIGN KEY (training_type_oid) REFERENCES hrm.training_type(oid);


--
-- Name: survey_for_trainee fk_training_type_oid_survey_for_trainee; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.survey_for_trainee
    ADD CONSTRAINT fk_training_type_oid_survey_for_trainee FOREIGN KEY (training_type_oid) REFERENCES hrm.training_type(oid);


--
-- Name: survey_for_trainer fk_training_type_oid_survey_for_trainer; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.survey_for_trainer
    ADD CONSTRAINT fk_training_type_oid_survey_for_trainer FOREIGN KEY (training_type_oid) REFERENCES hrm.training_type(oid);


--
-- Name: survey_questionnaire_for_trainer fk_training_type_oid_survey_questionnaire_for_trainer; Type: FK CONSTRAINT; Schema: hrm; Owner: grp
--

ALTER TABLE ONLY hrm.survey_questionnaire_for_trainer
    ADD CONSTRAINT fk_training_type_oid_survey_questionnaire_for_trainer FOREIGN KEY (training_type_oid) REFERENCES hrm.training_type(oid);


--
-- Name: app_package fk_annual_procurement_plan_oid_app_package; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.app_package
    ADD CONSTRAINT fk_annual_procurement_plan_oid_app_package FOREIGN KEY (annual_procurement_plan_oid) REFERENCES prc.annual_procurement_plan(oid);


--
-- Name: annual_procurement_plan_detail fk_app_oid_annual_procurement_plan_detail; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.annual_procurement_plan_detail
    ADD CONSTRAINT fk_app_oid_annual_procurement_plan_detail FOREIGN KEY (app_oid) REFERENCES prc.annual_procurement_plan(oid);


--
-- Name: package_lot fk_app_package_oid_package_lot; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.package_lot
    ADD CONSTRAINT fk_app_package_oid_package_lot FOREIGN KEY (app_package_oid) REFERENCES prc.app_package(oid);


--
-- Name: schedule_info fk_app_package_oid_schedule_info; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.schedule_info
    ADD CONSTRAINT fk_app_package_oid_schedule_info FOREIGN KEY (app_package_oid) REFERENCES prc.app_package(oid);


--
-- Name: debarment fk_approved_by_debarment; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.debarment
    ADD CONSTRAINT fk_approved_by_debarment FOREIGN KEY (approved_by) REFERENCES hrm.employee(oid);


--
-- Name: qc_committee fk_authorized_by_qc_committee; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.qc_committee
    ADD CONSTRAINT fk_authorized_by_qc_committee FOREIGN KEY (authorized_by) REFERENCES hrm.employee(oid);


--
-- Name: annual_procurement_plan fk_budget_type_oid_annual_procurement_plan; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.annual_procurement_plan
    ADD CONSTRAINT fk_budget_type_oid_annual_procurement_plan FOREIGN KEY (budget_type_oid) REFERENCES prc.budget_type(oid);


--
-- Name: annual_procurement_plan_detail fk_budget_type_oid_annual_procurement_plan_detail; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.annual_procurement_plan_detail
    ADD CONSTRAINT fk_budget_type_oid_annual_procurement_plan_detail FOREIGN KEY (budget_type_oid) REFERENCES prc.budget_type(oid);


--
-- Name: material_receive_info fk_chalan_info_material_receive_info; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.material_receive_info
    ADD CONSTRAINT fk_chalan_info_material_receive_info FOREIGN KEY (chalan_info) REFERENCES prc.chalan(oid);


--
-- Name: committee_member fk_code_committee_member; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.committee_member
    ADD CONSTRAINT fk_code_committee_member FOREIGN KEY (code) REFERENCES cmn.committee_setup(oid);


--
-- Name: consolidate_requisition_detail fk_consolidate_requisition_oid_consolidate_requisition_detail; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.consolidate_requisition_detail
    ADD CONSTRAINT fk_consolidate_requisition_oid_consolidate_requisition_detail FOREIGN KEY (consolidate_requisition_oid) REFERENCES prc.consolidate_requisition(oid);


--
-- Name: app_package fk_contract_approving_authority_oid_app_package; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.app_package
    ADD CONSTRAINT fk_contract_approving_authority_oid_app_package FOREIGN KEY (contract_approving_authority_oid) REFERENCES prc.approving_authority(oid);


--
-- Name: annual_procurement_plan_reviewer fk_employee_info_oid_annual_procurement_plan_reviewer; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.annual_procurement_plan_reviewer
    ADD CONSTRAINT fk_employee_info_oid_annual_procurement_plan_reviewer FOREIGN KEY (employee_info_oid) REFERENCES hrm.employee(oid);


--
-- Name: item_purchase fk_employee_info_oid_item_purchase; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.item_purchase
    ADD CONSTRAINT fk_employee_info_oid_item_purchase FOREIGN KEY (employee_info_oid) REFERENCES hrm.employee(oid);


--
-- Name: schedule_sale fk_employee_info_oid_schedule_sale; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.schedule_sale
    ADD CONSTRAINT fk_employee_info_oid_schedule_sale FOREIGN KEY (employee_info_oid) REFERENCES hrm.employee(oid);


--
-- Name: committee_member fk_employee_oid_committee_member; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.committee_member
    ADD CONSTRAINT fk_employee_oid_committee_member FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: reviewer fk_employee_oid_reviewer; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.reviewer
    ADD CONSTRAINT fk_employee_oid_reviewer FOREIGN KEY (employee_oid) REFERENCES hrm.employee(oid);


--
-- Name: annual_procurement_plan fk_fiscal_year_oid_annual_procurement_plan; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.annual_procurement_plan
    ADD CONSTRAINT fk_fiscal_year_oid_annual_procurement_plan FOREIGN KEY (fiscal_year_oid) REFERENCES cmn.fiscal_year(oid);


--
-- Name: annual_procurement_plan_detail fk_fiscal_year_oid_annual_procurement_plan_detail; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.annual_procurement_plan_detail
    ADD CONSTRAINT fk_fiscal_year_oid_annual_procurement_plan_detail FOREIGN KEY (fiscal_year_oid) REFERENCES cmn.fiscal_year(oid);


--
-- Name: consolidate_requisition fk_fiscal_year_oid_consolidate_requisition; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.consolidate_requisition
    ADD CONSTRAINT fk_fiscal_year_oid_consolidate_requisition FOREIGN KEY (fiscal_year_oid) REFERENCES cmn.fiscal_year(oid);


--
-- Name: app_package fk_item_category_oid_app_package; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.app_package
    ADD CONSTRAINT fk_item_category_oid_app_package FOREIGN KEY (item_category_oid) REFERENCES cmn.item_category(oid);


--
-- Name: consolidate_requisition_detail fk_item_category_oid_consolidate_requisition_detail; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.consolidate_requisition_detail
    ADD CONSTRAINT fk_item_category_oid_consolidate_requisition_detail FOREIGN KEY (item_category_oid) REFERENCES cmn.item_category(oid);


--
-- Name: procurement_item fk_item_category_oid_procurement_item; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.procurement_item
    ADD CONSTRAINT fk_item_category_oid_procurement_item FOREIGN KEY (item_category_oid) REFERENCES cmn.item_category(oid);


--
-- Name: qc_list fk_item_category_oid_qc_list; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.qc_list
    ADD CONSTRAINT fk_item_category_oid_qc_list FOREIGN KEY (item_category_oid) REFERENCES cmn.item_category(oid);


--
-- Name: requisition_detail fk_item_category_oid_requisition_detail; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.requisition_detail
    ADD CONSTRAINT fk_item_category_oid_requisition_detail FOREIGN KEY (item_category_oid) REFERENCES cmn.item_category(oid);


--
-- Name: consolidate_requisition_detail fk_item_oid_consolidate_requisition_detail; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.consolidate_requisition_detail
    ADD CONSTRAINT fk_item_oid_consolidate_requisition_detail FOREIGN KEY (item_oid) REFERENCES cmn.item(oid);


--
-- Name: package_lot fk_item_oid_package_lot; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.package_lot
    ADD CONSTRAINT fk_item_oid_package_lot FOREIGN KEY (item_oid) REFERENCES cmn.item(oid);


--
-- Name: procurement_item fk_item_oid_procurement_item; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.procurement_item
    ADD CONSTRAINT fk_item_oid_procurement_item FOREIGN KEY (item_oid) REFERENCES cmn.item(oid);


--
-- Name: qc_list fk_item_oid_qc_list; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.qc_list
    ADD CONSTRAINT fk_item_oid_qc_list FOREIGN KEY (item_oid) REFERENCES cmn.item(oid);


--
-- Name: requisition_detail fk_item_oid_requisition_detail; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.requisition_detail
    ADD CONSTRAINT fk_item_oid_requisition_detail FOREIGN KEY (item_oid) REFERENCES cmn.item(oid);


--
-- Name: material_receive_info fk_item_purchase_oid_material_receive_info; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.material_receive_info
    ADD CONSTRAINT fk_item_purchase_oid_material_receive_info FOREIGN KEY (item_purchase_oid) REFERENCES prc.item_purchase(oid);


--
-- Name: qc_list fk_item_uom_oid_qc_list; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.qc_list
    ADD CONSTRAINT fk_item_uom_oid_qc_list FOREIGN KEY (item_uom_oid) REFERENCES cmn.item_uom(oid);


--
-- Name: annual_procurement_plan fk_office_oid_annual_procurement_plan; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.annual_procurement_plan
    ADD CONSTRAINT fk_office_oid_annual_procurement_plan FOREIGN KEY (office_oid) REFERENCES cmn.office(oid);


--
-- Name: app_package fk_package_type_oid_app_package; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.app_package
    ADD CONSTRAINT fk_package_type_oid_app_package FOREIGN KEY (package_type_oid) REFERENCES prc.package_type(oid);


--
-- Name: chalan fk_pakage_lot_oid_chalan; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.chalan
    ADD CONSTRAINT fk_pakage_lot_oid_chalan FOREIGN KEY (pakage_lot_oid) REFERENCES prc.package_lot(oid);


--
-- Name: schedule_sale fk_payment_received_by_schedule_sale; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.schedule_sale
    ADD CONSTRAINT fk_payment_received_by_schedule_sale FOREIGN KEY (payment_received_by) REFERENCES hrm.employee(oid);


--
-- Name: app_package fk_procurement_method_oid_app_package; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.app_package
    ADD CONSTRAINT fk_procurement_method_oid_app_package FOREIGN KEY (procurement_method_oid) REFERENCES prc.procurement_method(oid);


--
-- Name: app_package fk_procurement_nature_oid_app_package; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.app_package
    ADD CONSTRAINT fk_procurement_nature_oid_app_package FOREIGN KEY (procurement_nature_oid) REFERENCES prc.procurement_nature(oid);


--
-- Name: app_package fk_procurement_type_oid_app_package; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.app_package
    ADD CONSTRAINT fk_procurement_type_oid_app_package FOREIGN KEY (procurement_type_oid) REFERENCES prc.procurement_type(oid);


--
-- Name: annual_procurement_plan fk_project_oid_annual_procurement_plan; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.annual_procurement_plan
    ADD CONSTRAINT fk_project_oid_annual_procurement_plan FOREIGN KEY (project_oid) REFERENCES prc.procurement_project(oid);


--
-- Name: qc_report fk_qc_committee_oid_qc_report; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.qc_report
    ADD CONSTRAINT fk_qc_committee_oid_qc_report FOREIGN KEY (qc_committee_oid) REFERENCES prc.qc_committee(oid);


--
-- Name: qc_info fk_qc_info_oid_qc_info; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.qc_info
    ADD CONSTRAINT fk_qc_info_oid_qc_info FOREIGN KEY (qc_info_oid) REFERENCES prc.qc_info(oid);


--
-- Name: notification_of_award fk_reference_description_notification_of_award; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.notification_of_award
    ADD CONSTRAINT fk_reference_description_notification_of_award FOREIGN KEY (reference_description) REFERENCES cmn.vendor(oid);


--
-- Name: debarment fk_reported_by_debarment; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.debarment
    ADD CONSTRAINT fk_reported_by_debarment FOREIGN KEY (reported_by) REFERENCES hrm.employee(oid);


--
-- Name: consolidate_requisition fk_requisition_declaration_oid_consolidate_requisition; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.consolidate_requisition
    ADD CONSTRAINT fk_requisition_declaration_oid_consolidate_requisition FOREIGN KEY (requisition_declaration_oid) REFERENCES prc.requisition_declaration(oid);


--
-- Name: requisition_information fk_requisition_declaration_oid_requisition_information; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.requisition_information
    ADD CONSTRAINT fk_requisition_declaration_oid_requisition_information FOREIGN KEY (requisition_declaration_oid) REFERENCES prc.requisition_declaration(oid);


--
-- Name: requisition_detail fk_requisition_oid_requisition_detail; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.requisition_detail
    ADD CONSTRAINT fk_requisition_oid_requisition_detail FOREIGN KEY (requisition_oid) REFERENCES prc.requisition_information(oid);


--
-- Name: schedule_sale fk_schedule_info_oid_schedule_sale; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.schedule_sale
    ADD CONSTRAINT fk_schedule_info_oid_schedule_sale FOREIGN KEY (schedule_info_oid) REFERENCES prc.schedule_info(oid);


--
-- Name: item_purchase fk_schedule_sale_oid_item_purchase; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.item_purchase
    ADD CONSTRAINT fk_schedule_sale_oid_item_purchase FOREIGN KEY (schedule_sale_oid) REFERENCES prc.schedule_sale(oid);


--
-- Name: material_receive_info fk_temporary_store_oid_material_receive_info; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.material_receive_info
    ADD CONSTRAINT fk_temporary_store_oid_material_receive_info FOREIGN KEY (temporary_store_oid) REFERENCES prc.temporary_store(oid);


--
-- Name: qc_report fk_temporary_store_oid_qc_report; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.qc_report
    ADD CONSTRAINT fk_temporary_store_oid_qc_report FOREIGN KEY (temporary_store_oid) REFERENCES prc.temporary_store(oid);


--
-- Name: procurement_item fk_uom_oid_procurement_item; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.procurement_item
    ADD CONSTRAINT fk_uom_oid_procurement_item FOREIGN KEY (uom_oid) REFERENCES cmn.item_uom(oid);


--
-- Name: debarment fk_vendor_oid_debarment; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.debarment
    ADD CONSTRAINT fk_vendor_oid_debarment FOREIGN KEY (vendor_oid) REFERENCES cmn.vendor(oid);


--
-- Name: item_purchase fk_vendor_oid_item_purchase; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.item_purchase
    ADD CONSTRAINT fk_vendor_oid_item_purchase FOREIGN KEY (vendor_oid) REFERENCES cmn.vendor(oid);


--
-- Name: material_receive_info fk_vendor_oid_material_receive_info; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.material_receive_info
    ADD CONSTRAINT fk_vendor_oid_material_receive_info FOREIGN KEY (vendor_oid) REFERENCES cmn.vendor(oid);


--
-- Name: schedule_sale fk_vendor_oid_schedule_sale; Type: FK CONSTRAINT; Schema: prc; Owner: grp
--

ALTER TABLE ONLY prc.schedule_sale
    ADD CONSTRAINT fk_vendor_oid_schedule_sale FOREIGN KEY (vendor_oid) REFERENCES cmn.vendor(oid);


--
-- Name: component_resource fk_component_oid_component_resource; Type: FK CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.component_resource
    ADD CONSTRAINT fk_component_oid_component_resource FOREIGN KEY (component_oid) REFERENCES sec.component(oid);


--
-- Name: grp_role_permission fk_component_oid_grp_role_permission; Type: FK CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.grp_role_permission
    ADD CONSTRAINT fk_component_oid_grp_role_permission FOREIGN KEY (component_oid) REFERENCES sec.component(oid);


--
-- Name: grp_subfeature fk_feature_oid_grp_subfeature; Type: FK CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.grp_subfeature
    ADD CONSTRAINT fk_feature_oid_grp_subfeature FOREIGN KEY (feature_oid) REFERENCES sec.grp_feature(oid);


--
-- Name: grp_role_permission fk_grp_role_oid_grp_role_permission; Type: FK CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.grp_role_permission
    ADD CONSTRAINT fk_grp_role_oid_grp_role_permission FOREIGN KEY (grp_role_oid) REFERENCES sec.grp_role(oid);


--
-- Name: grp_user_role fk_grp_role_oid_grp_user_role; Type: FK CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.grp_user_role
    ADD CONSTRAINT fk_grp_role_oid_grp_user_role FOREIGN KEY (grp_role_oid) REFERENCES sec.grp_role(oid);


--
-- Name: grp_user_role fk_grp_user_oid_grp_user_role; Type: FK CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.grp_user_role
    ADD CONSTRAINT fk_grp_user_oid_grp_user_role FOREIGN KEY (grp_user_oid) REFERENCES sec.grp_user(oid);


--
-- Name: grp_submodule fk_module_oid_grp_submodule; Type: FK CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.grp_submodule
    ADD CONSTRAINT fk_module_oid_grp_submodule FOREIGN KEY (module_oid) REFERENCES sec.grp_module(oid);


--
-- Name: office_module fk_module_oid_office_module; Type: FK CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.office_module
    ADD CONSTRAINT fk_module_oid_office_module FOREIGN KEY (module_oid) REFERENCES sec.grp_module(oid);


--
-- Name: office_module fk_office_oid_office_module; Type: FK CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.office_module
    ADD CONSTRAINT fk_office_oid_office_module FOREIGN KEY (office_oid) REFERENCES cmn.office(oid);


--
-- Name: component_resource fk_resource_oid_component_resource; Type: FK CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.component_resource
    ADD CONSTRAINT fk_resource_oid_component_resource FOREIGN KEY (resource_oid) REFERENCES sec.resource(oid);


--
-- Name: resource fk_service_oid_resource; Type: FK CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.resource
    ADD CONSTRAINT fk_service_oid_resource FOREIGN KEY (service_oid) REFERENCES sec.service(oid);


--
-- Name: grp_feature fk_sub_module_oid_grp_feature; Type: FK CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.grp_feature
    ADD CONSTRAINT fk_sub_module_oid_grp_feature FOREIGN KEY (sub_module_oid) REFERENCES sec.grp_submodule(oid);


--
-- Name: component fk_subfeature_oid_component; Type: FK CONSTRAINT; Schema: sec; Owner: grp
--

ALTER TABLE ONLY sec.component
    ADD CONSTRAINT fk_subfeature_oid_component FOREIGN KEY (subfeature_oid) REFERENCES sec.grp_subfeature(oid);


--
-- PostgreSQL database dump complete
--

